// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: bt.local:user:top_logic:1.0
// IP Revision: 35

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_top_logic_0_0 (
  in_uart_data,
  clk_100MHz_in,
  in_adc_data,
  uart_adc_start_sending,
  pll_data_ready,
  uart_byte_received,
  pll_read_data,
  out_data_uart,
  t_sweep,
  clk_100MHz_out,
  user_led,
  pamp_en,
  sw_ls,
  if_amp1_stu_en,
  if_amp2_stu_en,
  sw_if1,
  sw_if2,
  sw_if3,
  clk_sync,
  pll_trig,
  sw_ctrl,
  atten,
  start_uart_send,
  is_adc_data_sending,
  clk_10MHz_out,
  start_init_clk,
  clk_20MHz_out,
  pll_read,
  pll_write,
  pll_write_data,
  pll_write_addres,
  pll_read_addres
);

input wire [7 : 0] in_uart_data;
input wire clk_100MHz_in;
input wire [7 : 0] in_adc_data;
input wire uart_adc_start_sending;
input wire pll_data_ready;
input wire uart_byte_received;
input wire [23 : 0] pll_read_data;
output wire [7 : 0] out_data_uart;
output wire t_sweep;
output wire clk_100MHz_out;
output wire user_led;
output wire pamp_en;
output wire sw_ls;
output wire if_amp1_stu_en;
output wire if_amp2_stu_en;
output wire sw_if1;
output wire sw_if2;
output wire sw_if3;
output wire clk_sync;
output wire pll_trig;
output wire sw_ctrl;
output wire [5 : 0] atten;
output wire start_uart_send;
output wire is_adc_data_sending;
output wire clk_10MHz_out;
output wire start_init_clk;
output wire clk_20MHz_out;
output wire pll_read;
output wire pll_write;
output wire [23 : 0] pll_write_data;
output wire [4 : 0] pll_write_addres;
output wire [7 : 0] pll_read_addres;

  top_logic inst (
    .in_uart_data(in_uart_data),
    .clk_100MHz_in(clk_100MHz_in),
    .in_adc_data(in_adc_data),
    .uart_adc_start_sending(uart_adc_start_sending),
    .pll_data_ready(pll_data_ready),
    .uart_byte_received(uart_byte_received),
    .pll_read_data(pll_read_data),
    .out_data_uart(out_data_uart),
    .t_sweep(t_sweep),
    .clk_100MHz_out(clk_100MHz_out),
    .user_led(user_led),
    .pamp_en(pamp_en),
    .sw_ls(sw_ls),
    .if_amp1_stu_en(if_amp1_stu_en),
    .if_amp2_stu_en(if_amp2_stu_en),
    .sw_if1(sw_if1),
    .sw_if2(sw_if2),
    .sw_if3(sw_if3),
    .clk_sync(clk_sync),
    .pll_trig(pll_trig),
    .sw_ctrl(sw_ctrl),
    .atten(atten),
    .start_uart_send(start_uart_send),
    .is_adc_data_sending(is_adc_data_sending),
    .clk_10MHz_out(clk_10MHz_out),
    .start_init_clk(start_init_clk),
    .clk_20MHz_out(clk_20MHz_out),
    .pll_read(pll_read),
    .pll_write(pll_write),
    .pll_write_data(pll_write_data),
    .pll_write_addres(pll_write_addres),
    .pll_read_addres(pll_read_addres)
  );
endmodule

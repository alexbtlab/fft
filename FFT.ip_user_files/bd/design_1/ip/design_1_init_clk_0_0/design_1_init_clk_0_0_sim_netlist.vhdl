-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 12:27:52 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_init_clk_0_0 -prefix
--               design_1_init_clk_0_0_ design_1_init_clk_0_0_sim_netlist.vhdl
-- Design      : design_1_init_clk_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0_init_clk is
  port (
    clk_cs : out STD_LOGIC;
    clk_sclk : out STD_LOGIC;
    clk_mosi : out STD_LOGIC;
    start : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end design_1_init_clk_0_0_init_clk;

architecture STRUCTURE of design_1_init_clk_0_0_init_clk is
  signal \bits_left[1]_i_2_n_0\ : STD_LOGIC;
  signal \bits_left[3]_i_2_n_0\ : STD_LOGIC;
  signal \bits_left[6]_i_1_n_0\ : STD_LOGIC;
  signal \bits_left[6]_i_2_n_0\ : STD_LOGIC;
  signal bits_left_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \^clk_cs\ : STD_LOGIC;
  signal clk_cs_reg_i_1_n_0 : STD_LOGIC;
  signal clk_cs_reg_i_2_n_0 : STD_LOGIC;
  signal \^clk_mosi\ : STD_LOGIC;
  signal clk_mosi_reg_i_1_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_2_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_3_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_4_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_5_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_6_n_0 : STD_LOGIC;
  signal clk_mosi_reg_i_7_n_0 : STD_LOGIC;
  signal clk_sclk_INST_0_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal start_prev : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_left[1]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \bits_left[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \bits_left[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \bits_left[3]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \bits_left[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \bits_left[5]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \bits_left[6]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of clk_cs_reg_i_2 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of clk_mosi_reg_i_5 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of clk_mosi_reg_i_6 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of clk_sclk_INST_0 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of clk_sclk_INST_0_i_1 : label is "soft_lutpair5";
begin
  clk_cs <= \^clk_cs\;
  clk_mosi <= \^clk_mosi\;
\bits_left[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3333333333333332"
    )
        port map (
      I0 => bits_left_reg(1),
      I1 => bits_left_reg(0),
      I2 => bits_left_reg(6),
      I3 => bits_left_reg(4),
      I4 => bits_left_reg(5),
      I5 => \bits_left[1]_i_2_n_0\,
      O => p_0_in(0)
    );
\bits_left[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999999999998"
    )
        port map (
      I0 => bits_left_reg(0),
      I1 => bits_left_reg(1),
      I2 => bits_left_reg(6),
      I3 => bits_left_reg(4),
      I4 => bits_left_reg(5),
      I5 => \bits_left[1]_i_2_n_0\,
      O => p_0_in(1)
    );
\bits_left[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => bits_left_reg(2),
      I1 => bits_left_reg(3),
      O => \bits_left[1]_i_2_n_0\
    );
\bits_left[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F0F00E"
    )
        port map (
      I0 => \bits_left[3]_i_2_n_0\,
      I1 => bits_left_reg(3),
      I2 => bits_left_reg(2),
      I3 => bits_left_reg(0),
      I4 => bits_left_reg(1),
      O => p_0_in(2)
    );
\bits_left[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1F0E0"
    )
        port map (
      I0 => bits_left_reg(1),
      I1 => bits_left_reg(0),
      I2 => bits_left_reg(3),
      I3 => bits_left_reg(2),
      I4 => \bits_left[3]_i_2_n_0\,
      O => p_0_in(3)
    );
\bits_left[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => bits_left_reg(5),
      I1 => bits_left_reg(4),
      I2 => bits_left_reg(6),
      O => \bits_left[3]_i_2_n_0\
    );
\bits_left[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F00E"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => bits_left_reg(5),
      I2 => bits_left_reg(4),
      I3 => clk_sclk_INST_0_i_1_n_0,
      O => p_0_in(4)
    );
\bits_left[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C9C8"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(5),
      I2 => bits_left_reg(4),
      I3 => bits_left_reg(6),
      O => p_0_in(5)
    );
\bits_left[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => start,
      I1 => start_prev,
      O => \bits_left[6]_i_1_n_0\
    );
\bits_left[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(5),
      I2 => bits_left_reg(4),
      I3 => bits_left_reg(6),
      I4 => start,
      O => \bits_left[6]_i_2_n_0\
    );
\bits_left[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => bits_left_reg(4),
      I1 => bits_left_reg(5),
      I2 => clk_sclk_INST_0_i_1_n_0,
      I3 => bits_left_reg(6),
      O => p_0_in(6)
    );
\bits_left_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(0),
      Q => bits_left_reg(0),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(1),
      Q => bits_left_reg(1),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(2),
      Q => bits_left_reg(2),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(3),
      Q => bits_left_reg(3),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(4),
      Q => bits_left_reg(4),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(5),
      Q => bits_left_reg(5),
      R => \bits_left[6]_i_1_n_0\
    );
\bits_left_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => \bits_left[6]_i_2_n_0\,
      D => p_0_in(6),
      Q => bits_left_reg(6),
      S => \bits_left[6]_i_1_n_0\
    );
clk_cs_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8CAC"
    )
        port map (
      I0 => start_prev,
      I1 => \^clk_cs\,
      I2 => start,
      I3 => clk_cs_reg_i_2_n_0,
      O => clk_cs_reg_i_1_n_0
    );
clk_cs_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => bits_left_reg(4),
      I2 => bits_left_reg(5),
      I3 => clk_sclk_INST_0_i_1_n_0,
      O => clk_cs_reg_i_2_n_0
    );
clk_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => clk_cs_reg_i_1_n_0,
      Q => \^clk_cs\,
      R => '0'
    );
clk_mosi_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEEEFE0E0EEE0"
    )
        port map (
      I0 => clk_mosi_reg_i_2_n_0,
      I1 => clk_mosi_reg_i_3_n_0,
      I2 => \bits_left[6]_i_2_n_0\,
      I3 => start,
      I4 => start_prev,
      I5 => \^clk_mosi\,
      O => clk_mosi_reg_i_1_n_0
    );
clk_mosi_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000F00F800080008"
    )
        port map (
      I0 => clk_mosi_reg_i_4_n_0,
      I1 => clk_mosi_reg_i_5_n_0,
      I2 => bits_left_reg(2),
      I3 => bits_left_reg(3),
      I4 => bits_left_reg(1),
      I5 => clk_mosi_reg_i_6_n_0,
      O => clk_mosi_reg_i_2_n_0
    );
clk_mosi_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAEAAAAAAAAAAAA"
    )
        port map (
      I0 => clk_mosi_reg_i_7_n_0,
      I1 => bits_left_reg(5),
      I2 => start,
      I3 => bits_left_reg(2),
      I4 => bits_left_reg(4),
      I5 => bits_left_reg(1),
      O => clk_mosi_reg_i_3_n_0
    );
clk_mosi_reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5F44"
    )
        port map (
      I0 => bits_left_reg(1),
      I1 => bits_left_reg(5),
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(4),
      O => clk_mosi_reg_i_4_n_0
    );
clk_mosi_reg_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => bits_left_reg(6),
      I1 => start_prev,
      I2 => start,
      O => clk_mosi_reg_i_5_n_0
    );
clk_mosi_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04044404"
    )
        port map (
      I0 => bits_left_reg(4),
      I1 => bits_left_reg(0),
      I2 => start,
      I3 => start_prev,
      I4 => bits_left_reg(6),
      O => clk_mosi_reg_i_6_n_0
    );
clk_mosi_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => start_prev,
      I1 => bits_left_reg(6),
      I2 => bits_left_reg(0),
      I3 => bits_left_reg(5),
      I4 => bits_left_reg(3),
      I5 => bits_left_reg(2),
      O => clk_mosi_reg_i_7_n_0
    );
clk_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => clk_mosi_reg_i_1_n_0,
      Q => \^clk_mosi\,
      R => '0'
    );
clk_sclk_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => clk_sclk_INST_0_i_1_n_0,
      I1 => bits_left_reg(5),
      I2 => bits_left_reg(4),
      I3 => bits_left_reg(6),
      I4 => clk,
      O => clk_sclk
    );
clk_sclk_INST_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => bits_left_reg(1),
      I1 => bits_left_reg(0),
      I2 => bits_left_reg(3),
      I3 => bits_left_reg(2),
      O => clk_sclk_INST_0_i_1_n_0
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => start,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_init_clk_0_0 is
  port (
    clk : in STD_LOGIC;
    start : in STD_LOGIC;
    clk_sclk : out STD_LOGIC;
    clk_mosi : out STD_LOGIC;
    clk_cs : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_init_clk_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_init_clk_0_0 : entity is "design_1_init_clk_0_0,init_clk,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_init_clk_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_init_clk_0_0 : entity is "init_clk,Vivado 2019.1";
end design_1_init_clk_0_0;

architecture STRUCTURE of design_1_init_clk_0_0 is
begin
inst: entity work.design_1_init_clk_0_0_init_clk
     port map (
      clk => clk,
      clk_cs => clk_cs,
      clk_mosi => clk_mosi,
      clk_sclk => clk_sclk,
      start => start
    );
end STRUCTURE;

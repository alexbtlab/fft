`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2017 17:00:50
// Design Name: 
// Module Name: adc_get_data
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//interface ms_if (input clk);
//  logic sready;      // Indicates if slave is ready to accept data
//  logic rstn;        // Active low reset
//  logic [1:0] addr;  // Address 
//  logic [7:0] data;  // Data
 
//  modport slave ( input addr, data, rstn, clk,
//                 output sready);
 
//  modport master ( output addr, data,
//                  input  clk, sready, rstn);
//endinterface

interface diff;
  logic p, n;      // Indicates if slave is ready to accept data
  modport slave  (input p, n);
  modport master (output p, n);
endinterface

module adc_control(   
    
    //input wire clk_100MHz_adc,
    input wire test1,
    input wire test2,
    input wire test3,
    input wire test4,
    input wire tsweep,
    //input wire fpga_clk,
    //input wire adc_dco,
    //input wire adc_da,
    //input wire adc_db,
    input wire uart_clk,    
    output wire [7:0] output_data,    
    input wire is_sending,    
    output wire send_uart_byte,
    //output wire adc_clk,
    
   diff.slave adc_dax,
   diff.slave adc_dbx,
   diff.slave adc_dcox,
   diff.slave fpga_clkx,
   diff.master adc_clkx
   );
//////////////////////////////////////////////////////////////////////////////////////////////////////
    wire adc_clk, adc_dco, adc_da, adc_db, fpga_clk;

    OBUFDS #( 
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard 
        .SLEW("SLOW") // Specify the output slew rate 
    ) OBUFDS_adc_clk ( 
        .O(adc_clkx.p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_clkx.n), // Diff_n output (connect directly to top-level port) 
        .I(adc_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_dco (
        .O(adc_dco), // Buffer output
        .I(adc_dcox.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dcox.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da (
        .O(adc_da), // Buffer output
        .I(adc_dax.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dax.n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_db (
        .O(adc_db), // Buffer output
        .I(adc_dbx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dbx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_fpga_clk (
        .O(fpga_clk), // Buffer output
        .I(fpga_clkx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(fpga_clkx.n) // Diff_n buffer input (connect directly to top-level port)
    );
//////////////////////////////////////////////////////////////////////////////////////////////////// 
    wire clk_adc, clk_adc_delay, clk_fft, clk_bram, clk_mult;
    wire clk_locked;
    
      clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_adc),
      .clk_out2(clk_adc_delay), 
      .clk_out3(clk_fft),
      .clk_out4(clk_bram),
      .clk_out5(clk_mult),                 
      .clk_in1(fpga_clk),
      .locked(clk_locked)                      
    );
////////////////////////////////////////////////////////////////////////////////////////////////////
    reg[7:0] output_data_reg = 0;
    reg send_uart_byte_reg = 0;
    reg is_counting = 0;
    reg [18:0] cnt_uart = 0;
    
    assign output_data = output_data_reg;
    assign send_uart_byte = send_uart_byte_reg;
    
    reg adc_da_prev_reg=0;
    reg adc_db_prev_reg=0;
    
    reg [41:0] fft_input_data=0;
    reg fft_input_tvalid = 1;
    wire fft_input_tready;
    wire [41:0] fft_output_data;
    wire fft_output_tvalid;
    reg s_axis_data_tlast = 0;
    
    wire s_axis_config_tready, m_axis_data_tlast, event_frame_started, event_tlast_unexpected, event_tlast_missing;
    wire event_data_in_channel_halt, event_status_channel_halt, event_data_out_channel_halt;
    
    xfft_0 xfft(
    .aclk(clk_fft),
    .s_axis_config_tdata (16'b0), //forward transform
    .s_axis_config_tvalid (1'b0),
    .s_axis_config_tready (s_axis_config_tready), //OUT STD_LOGIC;
    .s_axis_data_tdata (fft_input_data),
    .s_axis_data_tvalid (fft_input_tvalid),
    .s_axis_data_tready (fft_input_tready),
    .s_axis_data_tlast(s_axis_data_tlast), //(???????? ?????????? ????????)
    .m_axis_data_tdata (fft_output_data),
    .m_axis_data_tready (1'b1),
    .m_axis_data_tvalid (fft_output_tvalid),
    .m_axis_data_tlast (m_axis_data_tlast), //OUT STD_LOGIC;
    .event_frame_started (event_frame_started), //OUT STD_LOGIC;
    .event_tlast_unexpected (event_tlast_unexpected), //OUT STD_LOGIC;
    .event_tlast_missing (event_tlast_missing), //OUT STD_LOGIC;
    .event_data_in_channel_halt (event_data_in_channel_halt), //OUT STD_LOGIC
    .event_status_channel_halt (event_status_channel_halt),
    .event_data_out_channel_halt (event_data_out_channel_halt));
////////////////////////////////////////////////////////////////////////////////////////////////////
    reg [7:0] adc_tfirstclk_reg = 1;
    reg is_tfirstclk_counting = 0; 
    reg adc_reading_allowed_reg = 0;
    reg adc_reading_allowed_neg_reg = 1;
    reg adc_bytes_read_num_reg = 0;
    reg [7:0] adc_current_clk_num_reg = 1;
    
    reg [17:0] adc_data_pos;
    reg [17:0] adc_data_neg;
    //reg [17:0] adc_data [2047:0];
      
    assign adc_clk = clk_adc&(adc_reading_allowed_reg>0)&(adc_reading_allowed_neg_reg>0);
    
    localparam ADC_TFIRSTCLK_MIN = 4; 
    
    reg is_sending_prev = 0;
    
    reg fpgaclk_prev = 0;
    
    reg[7:0] adc_current_strobe_num_reg = 1;
    
    reg adc_data_saving_needed = 0;
    
    reg [18:0] adc_data_last_saved_number = 0;
    reg [18:0] adc_current_transmit_data_number = 0;
    reg [2:0] adc_current_transmit_part_number = 0; 
    
    reg is_it_first_strobe = 0; 
    
    reg is_strobe_clk_active=0;
    reg is_strobe_clk_active_neg=1;
    
    wire clk_adc_strobe;
    
    assign clk_adc_strobe=clk_adc_delay&(is_strobe_clk_active>0)&(is_strobe_clk_active_neg>0);
    
    assign test1 = adc_current_strobe_num_reg[2];
    assign test2 = adc_current_strobe_num_reg[1];
    assign test3 = clk_adc_strobe;
    assign test4 = adc_current_strobe_num_reg[0];
         
    localparam ADC_BUF_DEPTH=8192;
    
    reg adc_bram_we=0;     
    reg [18:0] adc_bram_addr_write = 0;
    reg [18:0] adc_bram_addr_read = 0;
    reg [17:0] adc_bram_din = 0;
    wire [17:0] adc_bram_dout;
        
    bram bram_adc(
        .clk(clk_bram),
        .we(adc_bram_we),
        .addr_write(adc_bram_addr_write),
        .addr_read(adc_bram_addr_read),
        .din(adc_bram_din),
        .dout(adc_bram_dout));
//////////////////////////////////////////////////////////////////////////////////////////////////// 
    wire fft_bram_we;
    reg fft_bram_we_en=0;
    assign fft_bram_we = clk_fft&fft_bram_we_en;
    reg [18:0] fft_bram_addr_write = 0;
    reg [18:0] fft_bram_addr_read = 0;
    reg [39:0] fft_bram_din = 0;
    wire [39:0] fft_bram_dout;
        
    bram_40bit fft_bram(
        .clk(clk_bram),
        .we(fft_bram_we),
        .addr_write(fft_bram_addr_write),
        .addr_read(fft_bram_addr_read),
        .din(fft_bram_din),
        .dout(fft_bram_dout));
////////////////////////////////////////////////////////////////////////////////////////////////////    
  reg[12:0] sin_counter=0;
  
  wire [35:0] fft_data_re_cubed, fft_data_im_cubed;
  wire [39:0] fft_data_abs_cubed;
  
  reg fft_new_input_data_avail=0;
  reg fft_new_input_data_avail_prev=0;
  reg fft_new_input_data_exist=0;
  
  mult_gen_0 mult_re(
    //.CLK(clk_bram),
    .A(fft_output_data[17:0]),
    .B(fft_output_data[17:0]),
    .P(fft_data_re_cubed)); 
  mult_gen_0 mult_im(
    //.CLK(clk_bram),
    .A(fft_output_data[41:24]),
    .B(fft_output_data[41:24]),
    .P(fft_data_im_cubed));
 ////////////////////////////////////////////////////////////////////////////////////////////////////
   
 assign fft_data_abs_cubed = (40'b0|fft_data_re_cubed) + (40'b0|fft_data_re_cubed);
  
  always @(posedge clk_fft)
  begin
      
      if (fft_new_input_data_avail==1)
      begin
        fft_new_input_data_avail_prev <= 1;
        if (fft_new_input_data_avail_prev==0)
        fft_new_input_data_exist<=1;
      end
      else fft_new_input_data_avail_prev <= 2;
      
      if ((fft_input_tready==1)&(fft_new_input_data_exist>0))
      begin
        if (fft_input_tvalid==0) 
        begin
            sin_counter <= 0;
            fft_bram_we_en <= 1;            
        end
        else sin_counter <= sin_counter+1;
        //adc_bram_addr_read<=sin_counter[12:0];
        fft_input_tvalid <= 1;
      end
      else fft_input_tvalid <= 0;
      
      if ((fft_output_tvalid==1)&(fft_bram_addr_write<ADC_BUF_DEPTH)&(fft_bram_addr_write>0)&(fft_new_input_data_exist>0))
      begin                
        
        //FFT_DATA[fft_bram_addr_write] <= fft_data_abs_cubed>>8;
        if (fft_bram_addr_write==ADC_BUF_DEPTH-1) 
        begin
            fft_bram_we_en<=0; 
            fft_new_input_data_exist<=0;
        end      
      end
      
      fft_input_data <= adc_bram_dout;
      
      if ((fft_output_tvalid==1)&(fft_bram_addr_write<ADC_BUF_DEPTH))
      begin        
        fft_bram_addr_write <= fft_bram_addr_write+1;
        
        fft_bram_din <= {{22{fft_output_data[41]}}, fft_output_data[41:24]}*{{22{fft_output_data[41]}}, fft_output_data[41:24]}+{{22{fft_output_data[17]}}, fft_output_data[17:0]}*{{22{fft_output_data[17]}}, fft_output_data[17:0]};//fft_data_abs_cubed;
      end
      else fft_bram_addr_write<=0;     
  end 
  
  /*always @(negedge clk_fft)
  begin
     
      
      
  end */
   
    always @(negedge uart_clk) adc_bram_addr_read <= adc_current_transmit_data_number;//fft_bram_addr_read <= adc_current_transmit_data_number;
    
    always @(posedge uart_clk)
    begin    
        if (is_sending == 1)
        begin
            if (is_sending_prev==0) 
            begin
                adc_current_transmit_part_number <= 1; 
                adc_current_transmit_data_number <= 0;               
            end
            is_sending_prev <= 1;
        end
        else is_sending_prev <= 0;
        
        if (adc_current_transmit_data_number < ADC_BUF_DEPTH)
        begin
            if ((adc_current_transmit_part_number>0)&(is_counting==0))
            begin
                if (adc_current_transmit_part_number == 1) output_data_reg <= adc_bram_dout[17:14];// fft_bram_dout[39:32];//0|FFT_DATA[adc_current_transmit_data_number][17:12];
                else if (adc_current_transmit_part_number == 2) output_data_reg <= adc_bram_dout[13:11];//fft_bram_dout[31:24];//0|FFT_DATA[adc_current_transmit_data_number][11:6];
                else if (adc_current_transmit_part_number == 3) output_data_reg <= adc_bram_dout[10:07];//fft_bram_dout[23:16];//0|FFT_DATA[adc_current_transmit_data_number][5:0];
                else if (adc_current_transmit_part_number == 4) output_data_reg <= adc_bram_dout[06:03];//fft_bram_dout[15:08];//0|FFT_DATA[adc_current_transmit_data_number][5:0];
                else if (adc_current_transmit_part_number == 5) output_data_reg <= adc_bram_dout[02:00];//fft_bram_dout[07:00];//0|FFT_DATA[adc_current_transmit_data_number][5:0];
                
                /*if (adc_current_transmit_part_number == 1) output_data_reg <= 0|adc_data[adc_current_transmit_data_number][17:12];
                else if (adc_current_transmit_part_number == 2) output_data_reg <=0|adc_data[adc_current_transmit_data_number][11:6];
                else if (adc_current_transmit_part_number == 3) output_data_reg <= 0|adc_data[adc_current_transmit_data_number][5:0]; */ 
                send_uart_byte_reg <= 1;
                is_counting <= 1;
                if (adc_current_transmit_part_number == 5) 
                begin
                    adc_current_transmit_data_number <= adc_current_transmit_data_number + 1;
                    if (adc_current_transmit_data_number < ADC_BUF_DEPTH-1)
                    begin                        
                        adc_current_transmit_part_number<=1;
                        fft_new_input_data_avail <= 0;
                    end
                    else
                    begin                        
                        adc_current_transmit_part_number <= 0; 
                        fft_new_input_data_avail <= 1;
                    end
                end
                else adc_current_transmit_part_number <= adc_current_transmit_part_number + 1;
            end
        end
        
        if (cnt_uart<1900)
        begin
            if (is_counting>0) cnt_uart <= cnt_uart+1;        
            if (cnt_uart == 10) send_uart_byte_reg <= 0;
        end    
        else             
        begin            
            cnt_uart <= 0;
            is_counting <= 0;            
        end
    end
    
    always @(posedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_reg==1) is_strobe_clk_active<=1;
        else is_strobe_clk_active<=0;
        
    end
    
     always @(negedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_neg_reg==1) is_strobe_clk_active_neg<=1;
        else is_strobe_clk_active_neg<=0;
        
    end
    
    reg[3:0] fpga_clk_counter=0;
    reg tfirstclk_passed = 0;
    reg tfirstclk_passed_prev = 0;
    
    always @(posedge fpga_clk)
    begin
        if (fpga_clk_counter<9) fpga_clk_counter<=fpga_clk_counter+1;
            else fpga_clk_counter <= 0;
            if (fpga_clk_counter==7) tfirstclk_passed <= 1;
            if (fpga_clk_counter==9) tfirstclk_passed <= 0;
    end
    
     
     
    always @(posedge (clk_adc&clk_locked))
    begin
       if (tfirstclk_passed==0) tfirstclk_passed_prev<=0;
       else
       if (tfirstclk_passed_prev == 0)
       begin
            tfirstclk_passed_prev<=1;
            adc_reading_allowed_reg <= 1;
            is_it_first_strobe <= 1;
       end
                     
       if (adc_reading_allowed_reg == 1)
       begin
         
         if (adc_current_clk_num_reg < 5) 
         begin            
            adc_current_clk_num_reg<=adc_current_clk_num_reg+1;
            is_it_first_strobe <= 0;            
         end
         else if (adc_current_clk_num_reg == 5)
         begin
            adc_reading_allowed_reg <= 0;
            adc_current_clk_num_reg <= 1;            
            end
       end
    end
    
    always @(negedge (clk_adc&clk_locked))
    begin
        if (adc_current_clk_num_reg == 5) adc_reading_allowed_neg_reg <= 0;
        if (adc_current_clk_num_reg == 1) adc_reading_allowed_neg_reg <= 1;  
    end 
    
    always @(posedge (clk_adc_strobe&clk_locked))
    begin 
         if (is_it_first_strobe==1)
         begin          
            adc_data_pos[17] <= adc_da;
            adc_data_pos[16] <= adc_db;
            adc_current_strobe_num_reg<=1;
         end
         else if (adc_current_strobe_num_reg<5)         
         begin 
            adc_data_pos[17-((adc_current_strobe_num_reg)<<2)] <= adc_da;
            adc_data_pos[16-((adc_current_strobe_num_reg)<<2)] <= adc_db;
            
            adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;            
         end 
         else adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;      
    end
   
    reg is_sending_prev1=0;
    reg writing_adc_data_needed=1;
    reg is_data_ready = 0;
    reg is_new_sweep_started = 0;
    reg tsweep_prev = 1;
    reg [17:0] adc_data;
    reg adc_bram_we_backup = 0;
   
    always @(negedge (clk_adc_strobe&clk_locked))    
    begin
        if (is_sending == 0)
        begin
            if (is_sending_prev1==1) writing_adc_data_needed<=1;            
            is_sending_prev1 <= 0;
        end
        else is_sending_prev1 <= 1;
    
        
    
       if ((writing_adc_data_needed==1)&(is_new_sweep_started==1)&(adc_current_transmit_data_number == ADC_BUF_DEPTH)) adc_data_last_saved_number <= 0; 
               
       if (adc_current_strobe_num_reg==5)  
       begin
            
            if (tsweep>0)
            begin
                if (tsweep_prev==0) is_new_sweep_started<=1;
                else is_new_sweep_started <= 0;
                tsweep_prev <= 1;
            end
            else tsweep_prev <= 0;
            
            if ((adc_data_last_saved_number < ADC_BUF_DEPTH)&(is_data_ready==1))
            begin
                writing_adc_data_needed<=0;
                //adc_data_saving_needed <= 1;                
                
                //adc_data[adc_data_last_saved_number] <= adc_data_pos|adc_data_neg;
                
                adc_bram_we_backup <= 1'b1;
                                
                
                adc_bram_din <= adc_data_pos|adc_data_neg;
                adc_bram_addr_write <= adc_data_last_saved_number;
                
                is_data_ready <= 0;
                
                adc_data_last_saved_number <= adc_data_last_saved_number + 1;
            end
            
       end
       else if (adc_current_strobe_num_reg<5)
       begin
       if ((adc_current_strobe_num_reg==1)&(adc_bram_we_backup == 1'b1)) adc_bram_we <= 1'b1;
       if (adc_current_strobe_num_reg==4) begin adc_bram_we <= 1'b0; adc_bram_we_backup<=1'b0; end
       //adc_data_saving_needed <= 0;
       //bram_addr_write <= 0;
       
       adc_data_neg[15-((adc_current_strobe_num_reg-1)<<2)] <= adc_da;
       adc_data_neg[14-((adc_current_strobe_num_reg-1)<<2)] <= adc_db;
       if (adc_current_strobe_num_reg==4) is_data_ready <= 1;
       end
    end
    
    
    
endmodule


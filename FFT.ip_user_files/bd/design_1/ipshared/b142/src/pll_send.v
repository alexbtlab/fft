`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 12:54:55
// Design Name: 
// Module Name: SPI_send
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pll_send(
    input wire clk,
    input wire [23:0] pll_data,
    input wire [4:0] reg_address,
    input wire start,
    output wire pll_clk,
    output wire pll_mosi,
    output wire pll_cs    
    );
    
    reg is_clk_running = 0;
    reg [5:0] bits_send = 0;
    reg start_prev = 0;
    reg pll_mosi_reg = 0, pll_cs_reg=0;
    
          
    
    assign pll_clk = clk & is_clk_running;    
    assign pll_cs = pll_cs_reg;
    assign pll_mosi = pll_mosi_reg;    
        
    always @(negedge clk)
    begin
        start_prev <= start;
        if ((start_prev == 1'b0)&(start==1'b1))
        begin 
            bits_send <= 0;            
            pll_mosi_reg <= pll_data[23]; 
            is_clk_running <= 1;           
        end        
        else if (is_clk_running==1)
        begin
            bits_send <= bits_send + 1;
            if (bits_send < 23)
            begin
                pll_mosi_reg <= pll_data[22-bits_send];
            end
            else if (bits_send <28)
            begin
                pll_mosi_reg <= reg_address>>(27-bits_send);
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send == 31)
            begin                
                bits_send<=0;
                pll_cs_reg <= 1;
                is_clk_running <= 0;
            end
        end 
        else 
        begin
            pll_cs_reg <= 0;
            pll_mosi_reg <= 0;            
        end
    end
endmodule
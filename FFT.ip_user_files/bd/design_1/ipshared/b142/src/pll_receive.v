`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 12:53:38
// Design Name: 
// Module Name: SPI_receive
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pll_receive(
    input wire clk,
    input wire [7:0] reg_address,
    input wire start,
    output wire pll_clk,
    output wire pll_mosi,
    input wire pll_miso,
    output wire pll_cs,
    output wire [23:0] read_data,
    output wire data_ready
    );
    
    reg is_first_cycle_going = 0;
    reg is_second_cycle_going = 0;
    reg [5:0] bits_send = 0;
    reg start_prev = 0;
    reg pll_mosi_reg = 0, pll_cs_reg=0;
    reg [23:0] read_data_reg = 0; 
    reg data_ready_reg = 0;
      
    assign data_ready = data_ready_reg;
    assign pll_clk = clk & (is_first_cycle_going|is_second_cycle_going);    
    assign pll_cs = pll_cs_reg;
    assign pll_mosi = pll_mosi_reg;
    assign read_data = read_data_reg;
        
    always @(negedge clk)
    begin
        start_prev <= start;
        if ((start_prev == 1'b0)&(start==1'b1))
        begin
            read_data_reg <= 0;
            data_ready_reg <= 0;
            bits_send <= 0;            
            pll_mosi_reg <= 0; //read operation 
            is_first_cycle_going <= 1;           
        end        
        else if (is_first_cycle_going==1)
        begin
            bits_send <= bits_send + 1;
            if (bits_send < 19)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send <23)
            begin
                pll_mosi_reg <= reg_address>>(22-bits_send);
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send == 31)
            begin                
                bits_send<=0;
                pll_cs_reg <= 1;
                is_first_cycle_going <=0;
                is_second_cycle_going <=1;
            end
        end                
        else if (is_second_cycle_going==1)
        begin
            if (pll_cs_reg == 1) pll_cs_reg <= 0;
            bits_send <= bits_send + 1;
            if (bits_send < 19)
            begin
                pll_mosi_reg <= 0;
                read_data_reg <= read_data_reg | (pll_miso<<(23-bits_send));
            end
            else if (bits_send < 24)
            begin
                pll_mosi_reg <= reg_address>>(22-bits_send); 
                read_data_reg <= read_data_reg | (pll_miso<<(23-bits_send));                
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
             else if (bits_send ==31)
            begin               
                is_second_cycle_going<=0;
                bits_send<=0;
                pll_cs_reg <= 1;                
            end
        end
        else 
        begin
            pll_cs_reg <= 0;
            pll_mosi_reg <= 0;
            data_ready_reg <= 1;
        end
    end
endmodule
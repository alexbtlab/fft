`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 12:50:08
// Design Name: 
// Module Name: uart_receive
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_receive(
    input wire clk,
    input wire uart_rx,
    output wire byte_received,
    output wire [7:0] uart_data
    );
    reg byte_received_reg = 1'b0;
    reg start_bit_recieved = 1'b0;
    reg is_receiving_going = 1'b0;
    reg [3:0] uart_num_of_rbit = 4'b0;
    reg[7:0] uart_data_reg = 8'b0;
    reg [6:0] uart_cnt;
    
    assign byte_received = byte_received_reg;
    assign uart_data = uart_data_reg;
        
    always @(posedge clk) 
         begin
           if ((uart_rx==1'b0)&(is_receiving_going == 1'b0)) start_bit_recieved <= 1'b1;
           if (start_bit_recieved == 1'b1)
           begin 
                uart_cnt <= uart_cnt + 1;
                if (uart_cnt == 49)
                begin
                    uart_cnt <= 0;
                    start_bit_recieved <= 0;
                    if (uart_rx==1'b0) is_receiving_going <= 1'b1;
                    byte_received_reg <= 0;            
                end;
           end;
           if (is_receiving_going == 1'b1)
           begin
                uart_cnt <= uart_cnt+1;
                if (uart_cnt == 99)
                begin                
                    uart_cnt<=0;
                    uart_num_of_rbit <= uart_num_of_rbit + 1;
                    if (uart_num_of_rbit <= 7) uart_data_reg[uart_num_of_rbit] <= uart_rx;                          
                    else 
                    if (uart_num_of_rbit == 8)
                    begin
                         is_receiving_going <= 1'b0;
                         uart_num_of_rbit <= 0; 
                         byte_received_reg <= 1;                                      
                    end            
                end;            
           end;
         end;
         
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 12:52:37
// Design Name: 
// Module Name: uart_send
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_send(
    input wire [7:0] uart_data,
    input wire clk,    
    output wire uart_tx,
    output wire is_transmission_going,
    input wire send_byte      
    );
    
    reg send_byte_prev = 1'b0;
    reg uart_tx_reg = 1'b1;
    reg is_transmission_going_reg = 1'b0;
    reg [3:0] bit_num = 4'b0;
    reg [6:0] cnt = 0;
        
    
    assign uart_tx = uart_tx_reg;
    assign is_transmission_going = is_transmission_going_reg;
    
    
    always @(posedge clk)
    begin
        if (send_byte == 1)
        begin
            send_byte_prev <= 1;
            if ((send_byte_prev == 0)&(is_transmission_going_reg==0))         
            begin
                is_transmission_going_reg = 1;
                cnt<=0;
                uart_tx_reg = 0;
                bit_num = 0;
            end
        end
        else send_byte_prev <= 0;
        
        if (is_transmission_going) cnt = cnt+1;
        if (cnt==100)
        begin
            cnt <= 0;
            bit_num <= bit_num+1;
            if (bit_num < 8) uart_tx_reg = uart_data[bit_num];
            if (bit_num == 8) uart_tx_reg = 1'b1;
            if (bit_num == 9) is_transmission_going_reg <= 0;
        end
        
    end
   
endmodule

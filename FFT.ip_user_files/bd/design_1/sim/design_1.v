//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Wed Jun  3 15:14:35 2020
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=5,numReposBlks=5,numNonXlnxBlks=4,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (adc_clk_n,
    adc_clk_p,
    adc_da_n,
    adc_da_p,
    adc_db_n,
    adc_db_p,
    adc_dco_n,
    adc_dco_p,
    atten,
    clk_100MHz,
    clk_cs,
    clk_mosi,
    clk_sclk,
    clk_sync,
    fpga_clk_n,
    fpga_clk_p,
    if_amp1_stu_en,
    if_amp2_stu_en,
    pamp_en,
    pll_ld_sdo,
    pll_mosi,
    pll_sck,
    pll_sen,
    pll_trig,
    sw_ctrl,
    sw_if1,
    sw_if2,
    sw_if3,
    sw_ls,
    user_io_10,
    user_io_9,
    user_led);
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clk n" *) output adc_clk_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clk p" *) output adc_clk_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_da n" *) input adc_da_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_da p" *) input adc_da_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_db n" *) input adc_db_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_db p" *) input adc_db_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dco n" *) input adc_dco_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dco p" *) input adc_dco_p;
  output [5:0]atten;
  input clk_100MHz;
  output clk_cs;
  output clk_mosi;
  output clk_sclk;
  output clk_sync;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clk n" *) input fpga_clk_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clk p" *) input fpga_clk_p;
  output if_amp1_stu_en;
  output if_amp2_stu_en;
  output pamp_en;
  input pll_ld_sdo;
  output pll_mosi;
  output pll_sck;
  output pll_sen;
  output pll_trig;
  output sw_ctrl;
  output sw_if1;
  output sw_if2;
  output sw_if3;
  output sw_ls;
  output user_io_10;
  input user_io_9;
  output user_led;

  wire adc_control_0_adc_clkx_n;
  wire adc_control_0_adc_clkx_p;
  wire [7:0]adc_control_0_output_data;
  wire adc_control_0_send_uart_byte;
  wire adc_dax_0_1_n;
  wire adc_dax_0_1_p;
  wire adc_dbx_0_1_n;
  wire adc_dbx_0_1_p;
  wire adc_dcox_0_1_n;
  wire adc_dcox_0_1_p;
  wire fpga_clkx_0_1_n;
  wire fpga_clkx_0_1_p;
  wire init_clk_0_clk_cs;
  wire init_clk_0_clk_mosi;
  wire init_clk_0_clk_sclk;
  wire pll_0_pll_mosi;
  wire [23:0]pll_0_pll_read_data;
  wire pll_0_pll_sck;
  wire pll_0_pll_sen;
  wire pll_ld_sdo_0_1;
  wire pll_pll_data_ready;
  wire top_logic_0_clk_100MHz_out;
  wire top_logic_0_clk_10MHz_out;
  wire top_logic_0_clk_20MHz_out;
  wire [7:0]top_logic_0_out_data_uart;
  wire top_logic_0_pll_read;
  wire [7:0]top_logic_0_pll_read_addres;
  wire top_logic_0_pll_write;
  wire [4:0]top_logic_0_pll_write_addres;
  wire [23:0]top_logic_0_pll_write_data;
  wire top_logic_0_start_init_clk;
  wire top_logic_0_start_uart_send;
  wire top_logic_0_t_sweep;
  wire [5:0]top_logic_atten;
  wire top_logic_clk_sync;
  wire top_logic_if_amp1_stu_en;
  wire top_logic_if_amp2_stu_en;
  wire top_logic_is_adc_data_sending;
  wire top_logic_pamp_en;
  wire top_logic_pll_trig;
  wire top_logic_sw_ctrl;
  wire top_logic_sw_if1;
  wire top_logic_sw_if2;
  wire top_logic_sw_if3;
  wire top_logic_sw_ls;
  wire top_logic_user_led;
  wire uart_0_byte_received;
  wire [7:0]uart_0_uart_data_rx;
  wire uart_0_uart_tx;
  wire uart_clk_0_1;
  wire uart_rx_0_1;

  assign adc_clk_n = adc_control_0_adc_clkx_n;
  assign adc_clk_p = adc_control_0_adc_clkx_p;
  assign adc_dax_0_1_n = adc_da_n;
  assign adc_dax_0_1_p = adc_da_p;
  assign adc_dbx_0_1_n = adc_db_n;
  assign adc_dbx_0_1_p = adc_db_p;
  assign adc_dcox_0_1_n = adc_dco_n;
  assign adc_dcox_0_1_p = adc_dco_p;
  assign atten[5:0] = top_logic_atten;
  assign clk_cs = init_clk_0_clk_cs;
  assign clk_mosi = init_clk_0_clk_mosi;
  assign clk_sclk = init_clk_0_clk_sclk;
  assign clk_sync = top_logic_clk_sync;
  assign fpga_clkx_0_1_n = fpga_clk_n;
  assign fpga_clkx_0_1_p = fpga_clk_p;
  assign if_amp1_stu_en = top_logic_if_amp1_stu_en;
  assign if_amp2_stu_en = top_logic_if_amp2_stu_en;
  assign pamp_en = top_logic_pamp_en;
  assign pll_ld_sdo_0_1 = pll_ld_sdo;
  assign pll_mosi = pll_0_pll_mosi;
  assign pll_sck = pll_0_pll_sck;
  assign pll_sen = pll_0_pll_sen;
  assign pll_trig = top_logic_pll_trig;
  assign sw_ctrl = top_logic_sw_ctrl;
  assign sw_if1 = top_logic_sw_if1;
  assign sw_if2 = top_logic_sw_if2;
  assign sw_if3 = top_logic_sw_if3;
  assign sw_ls = top_logic_sw_ls;
  assign uart_clk_0_1 = clk_100MHz;
  assign uart_rx_0_1 = user_io_9;
  assign user_io_10 = uart_0_uart_tx;
  assign user_led = top_logic_user_led;
  design_1_adc_control_0_0 adc_control
       (.adc_clkx_n(adc_control_0_adc_clkx_n),
        .adc_clkx_p(adc_control_0_adc_clkx_p),
        .adc_dax_n(adc_dax_0_1_n),
        .adc_dax_p(adc_dax_0_1_p),
        .adc_dbx_n(adc_dbx_0_1_n),
        .adc_dbx_p(adc_dbx_0_1_p),
        .adc_dcox_n(adc_dcox_0_1_n),
        .adc_dcox_p(adc_dcox_0_1_p),
        .fpga_clkx_n(fpga_clkx_0_1_n),
        .fpga_clkx_p(fpga_clkx_0_1_p),
        .is_sending(top_logic_is_adc_data_sending),
        .output_data(adc_control_0_output_data),
        .send_uart_byte(adc_control_0_send_uart_byte),
        .test1(1'b0),
        .test2(1'b0),
        .test3(1'b0),
        .test4(1'b0),
        .tsweep(top_logic_0_t_sweep),
        .uart_clk(uart_clk_0_1));
  design_1_init_clk_0_0 init_clk
       (.clk(top_logic_0_clk_10MHz_out),
        .clk_cs(init_clk_0_clk_cs),
        .clk_mosi(init_clk_0_clk_mosi),
        .clk_sclk(init_clk_0_clk_sclk),
        .start(top_logic_0_start_init_clk));
  design_1_pll_0_1 pll
       (.clk_pll_spi(top_logic_0_clk_20MHz_out),
        .pll_data_ready(pll_pll_data_ready),
        .pll_ld_sdo(pll_ld_sdo_0_1),
        .pll_mosi(pll_0_pll_mosi),
        .pll_read(top_logic_0_pll_read),
        .pll_read_address_reg(top_logic_0_pll_read_addres),
        .pll_read_data(pll_0_pll_read_data),
        .pll_sck(pll_0_pll_sck),
        .pll_sen(pll_0_pll_sen),
        .pll_write(top_logic_0_pll_write),
        .pll_write_address_reg(top_logic_0_pll_write_addres),
        .pll_write_data(top_logic_0_pll_write_data));
  design_1_top_logic_0_0 top_logic
       (.atten(top_logic_atten),
        .clk_100MHz_in(uart_clk_0_1),
        .clk_100MHz_out(top_logic_0_clk_100MHz_out),
        .clk_10MHz_out(top_logic_0_clk_10MHz_out),
        .clk_20MHz_out(top_logic_0_clk_20MHz_out),
        .clk_sync(top_logic_clk_sync),
        .if_amp1_stu_en(top_logic_if_amp1_stu_en),
        .if_amp2_stu_en(top_logic_if_amp2_stu_en),
        .in_adc_data(adc_control_0_output_data),
        .in_uart_data(uart_0_uart_data_rx),
        .is_adc_data_sending(top_logic_is_adc_data_sending),
        .out_data_uart(top_logic_0_out_data_uart),
        .pamp_en(top_logic_pamp_en),
        .pll_data_ready(pll_pll_data_ready),
        .pll_read(top_logic_0_pll_read),
        .pll_read_addres(top_logic_0_pll_read_addres),
        .pll_read_data(pll_0_pll_read_data),
        .pll_trig(top_logic_pll_trig),
        .pll_write(top_logic_0_pll_write),
        .pll_write_addres(top_logic_0_pll_write_addres),
        .pll_write_data(top_logic_0_pll_write_data),
        .start_init_clk(top_logic_0_start_init_clk),
        .start_uart_send(top_logic_0_start_uart_send),
        .sw_ctrl(top_logic_sw_ctrl),
        .sw_if1(top_logic_sw_if1),
        .sw_if2(top_logic_sw_if2),
        .sw_if3(top_logic_sw_if3),
        .sw_ls(top_logic_sw_ls),
        .t_sweep(top_logic_0_t_sweep),
        .uart_adc_start_sending(adc_control_0_send_uart_byte),
        .uart_byte_received(uart_0_byte_received),
        .user_led(top_logic_user_led));
  design_1_uart_0_0 uart
       (.byte_received(uart_0_byte_received),
        .clk(top_logic_0_clk_100MHz_out),
        .send_byte(top_logic_0_start_uart_send),
        .uart_data_rx(uart_0_uart_data_rx),
        .uart_data_tx(top_logic_0_out_data_uart),
        .uart_rx(uart_rx_0_1),
        .uart_tx(uart_0_uart_tx));
endmodule

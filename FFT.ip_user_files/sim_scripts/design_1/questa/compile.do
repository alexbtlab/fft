vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm
vlib questa_lib/msim/xbip_utils_v3_0_10
vlib questa_lib/msim/xbip_pipe_v3_0_6
vlib questa_lib/msim/xbip_bram18k_v3_0_6
vlib questa_lib/msim/mult_gen_v12_0_15
vlib questa_lib/msim/axi_utils_v2_0_6
vlib questa_lib/msim/c_reg_fd_v12_0_6
vlib questa_lib/msim/xbip_dsp48_wrapper_v3_0_4
vlib questa_lib/msim/xbip_dsp48_addsub_v3_0_6
vlib questa_lib/msim/xbip_addsub_v3_0_6
vlib questa_lib/msim/c_addsub_v12_0_13
vlib questa_lib/msim/c_mux_bit_v12_0_6
vlib questa_lib/msim/c_shift_ram_v12_0_13
vlib questa_lib/msim/cmpy_v6_0_17
vlib questa_lib/msim/floating_point_v7_0_16
vlib questa_lib/msim/xfft_v9_1_2

vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm
vmap xbip_utils_v3_0_10 questa_lib/msim/xbip_utils_v3_0_10
vmap xbip_pipe_v3_0_6 questa_lib/msim/xbip_pipe_v3_0_6
vmap xbip_bram18k_v3_0_6 questa_lib/msim/xbip_bram18k_v3_0_6
vmap mult_gen_v12_0_15 questa_lib/msim/mult_gen_v12_0_15
vmap axi_utils_v2_0_6 questa_lib/msim/axi_utils_v2_0_6
vmap c_reg_fd_v12_0_6 questa_lib/msim/c_reg_fd_v12_0_6
vmap xbip_dsp48_wrapper_v3_0_4 questa_lib/msim/xbip_dsp48_wrapper_v3_0_4
vmap xbip_dsp48_addsub_v3_0_6 questa_lib/msim/xbip_dsp48_addsub_v3_0_6
vmap xbip_addsub_v3_0_6 questa_lib/msim/xbip_addsub_v3_0_6
vmap c_addsub_v12_0_13 questa_lib/msim/c_addsub_v12_0_13
vmap c_mux_bit_v12_0_6 questa_lib/msim/c_mux_bit_v12_0_6
vmap c_shift_ram_v12_0_13 questa_lib/msim/c_shift_ram_v12_0_13
vmap cmpy_v6_0_17 questa_lib/msim/cmpy_v6_0_17
vmap floating_point_v7_0_16 questa_lib/msim/floating_point_v7_0_16
vmap xfft_v9_1_2 questa_lib/msim/xfft_v9_1_2

vlog -work xil_defaultlib -64 -sv "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1" "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xbip_utils_v3_0_10 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_utils_v3_0_vh_rfs.vhd" \

vcom -work xbip_pipe_v3_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \

vcom -work xbip_bram18k_v3_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \

vcom -work mult_gen_v12_0_15 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/mult_gen_v12_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/sim/mult_gen_0.vhd" \

vcom -work axi_utils_v2_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/axi_utils_v2_0_vh_rfs.vhd" \

vcom -work c_reg_fd_v12_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_reg_fd_v12_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_wrapper_v3_0_4 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_addsub_v3_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \

vcom -work xbip_addsub_v3_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_addsub_v3_0_vh_rfs.vhd" \

vcom -work c_addsub_v12_0_13 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_addsub_v12_0_vh_rfs.vhd" \

vcom -work c_mux_bit_v12_0_6 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_mux_bit_v12_0_vh_rfs.vhd" \

vcom -work c_shift_ram_v12_0_13 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_shift_ram_v12_0_vh_rfs.vhd" \

vcom -work cmpy_v6_0_17 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/cmpy_v6_0_vh_rfs.vhd" \

vcom -work floating_point_v7_0_16 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/floating_point_v7_0_vh_rfs.vhd" \

vcom -work xfft_v9_1_2 -64 -93 \
"../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xfft_v9_1_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/sim/xfft_0.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1" "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0" \
"../../../bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1/clk_wiz_1_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1/clk_wiz_1.v" \
"../../../bd/design_1/ipshared/995d/src/bram_40bit.v" \

vlog -work xil_defaultlib -64 -sv "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1" "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0" \
"../../../bd/design_1/ipshared/995d/src/bram.sv" \
"../../../bd/design_1/ipshared/995d/hdl/adc_control.sv" \
"../../../bd/design_1/ip/design_1_adc_control_0_0/sim/design_1_adc_control_0_0.sv" \

vlog -work xil_defaultlib -64 "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1" "+incdir+../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0" \
"../../../bd/design_1/ipshared/979f/hdl/init_clk_v1_0.v" \
"../../../bd/design_1/ip/design_1_init_clk_0_0/sim/design_1_init_clk_0_0.v" \
"../../../bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0/clk_wiz_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0/clk_wiz_0.v" \
"../../../bd/design_1/ipshared/dc94/hdl/top_logic_v1_0.v" \
"../../../bd/design_1/ip/design_1_top_logic_0_0/sim/design_1_top_logic_0_0.v" \
"../../../bd/design_1/ipshared/b142/src/pll_receive.v" \
"../../../bd/design_1/ipshared/b142/src/pll_send.v" \
"../../../bd/design_1/ipshared/b142/hdl/pll_v1_0.v" \
"../../../bd/design_1/ip/design_1_pll_0_1/sim/design_1_pll_0_1.v" \
"../../../bd/design_1/ipshared/ce55/hdl/uart_receive.v" \
"../../../bd/design_1/ipshared/ce55/hdl/uart_send.v" \
"../../../bd/design_1/ipshared/ce55/hdl/uart_v1_0.v" \
"../../../bd/design_1/ip/design_1_uart_0_0/sim/design_1_uart_0_0.v" \
"../../../bd/design_1/sim/design_1.v" \

vlog -work xil_defaultlib \
"glbl.v"


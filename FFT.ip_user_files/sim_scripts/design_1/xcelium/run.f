-makelib xcelium_lib/xil_defaultlib -sv \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/xbip_utils_v3_0_10 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_utils_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_pipe_v3_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_bram18k_v3_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/mult_gen_v12_0_15 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/hdl/mult_gen_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_adc_control_0_0/src/mult_gen_0_1/sim/mult_gen_0.vhd" \
-endlib
-makelib xcelium_lib/axi_utils_v2_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/axi_utils_v2_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_reg_fd_v12_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_reg_fd_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_dsp48_wrapper_v3_0_4 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_dsp48_addsub_v3_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_addsub_v3_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xbip_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_addsub_v12_0_13 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_addsub_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_mux_bit_v12_0_6 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_mux_bit_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_shift_ram_v12_0_13 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/c_shift_ram_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/cmpy_v6_0_17 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/cmpy_v6_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/floating_point_v7_0_16 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/floating_point_v7_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xfft_v9_1_2 \
  "../../../../FFT.srcs/sources_1/bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/hdl/xfft_v9_1_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_adc_control_0_0/src/xfft_0/sim/xfft_0.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1/clk_wiz_1_clk_wiz.v" \
  "../../../bd/design_1/ip/design_1_adc_control_0_0/src/clk_wiz_1/clk_wiz_1.v" \
  "../../../bd/design_1/ipshared/995d/src/bram_40bit.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib -sv \
  "../../../bd/design_1/ipshared/995d/src/bram.sv" \
  "../../../bd/design_1/ipshared/995d/hdl/adc_control.sv" \
  "../../../bd/design_1/ip/design_1_adc_control_0_0/sim/design_1_adc_control_0_0.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ipshared/979f/hdl/init_clk_v1_0.v" \
  "../../../bd/design_1/ip/design_1_init_clk_0_0/sim/design_1_init_clk_0_0.v" \
  "../../../bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0/clk_wiz_0_clk_wiz.v" \
  "../../../bd/design_1/ip/design_1_top_logic_0_0/src/clk_wiz_0/clk_wiz_0.v" \
  "../../../bd/design_1/ipshared/dc94/hdl/top_logic_v1_0.v" \
  "../../../bd/design_1/ip/design_1_top_logic_0_0/sim/design_1_top_logic_0_0.v" \
  "../../../bd/design_1/ipshared/b142/src/pll_receive.v" \
  "../../../bd/design_1/ipshared/b142/src/pll_send.v" \
  "../../../bd/design_1/ipshared/b142/hdl/pll_v1_0.v" \
  "../../../bd/design_1/ip/design_1_pll_0_1/sim/design_1_pll_0_1.v" \
  "../../../bd/design_1/ipshared/ce55/hdl/uart_receive.v" \
  "../../../bd/design_1/ipshared/ce55/hdl/uart_send.v" \
  "../../../bd/design_1/ipshared/ce55/hdl/uart_v1_0.v" \
  "../../../bd/design_1/ip/design_1_uart_0_0/sim/design_1_uart_0_0.v" \
  "../../../bd/design_1/sim/design_1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib


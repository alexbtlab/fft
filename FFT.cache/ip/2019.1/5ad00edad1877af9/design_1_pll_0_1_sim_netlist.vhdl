-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 13:15:56 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_pll_0_1_sim_netlist.vhdl
-- Design      : design_1_pll_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive is
  port (
    pll_mosi_read : out STD_LOGIC;
    pll_data_ready : out STD_LOGIC;
    pll_read_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_sck : out STD_LOGIC;
    pll_sen : out STD_LOGIC;
    pll_read : in STD_LOGIC;
    clk_pll_spi : in STD_LOGIC;
    pll_ld_sdo : in STD_LOGIC;
    pll_read_address_reg : in STD_LOGIC_VECTOR ( 7 downto 0 );
    is_clk_running : in STD_LOGIC;
    pll_sen_write : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive is
  signal \bits_send[0]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[1]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[2]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[3]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_2_n_0\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[0]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[1]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[2]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[3]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[4]\ : STD_LOGIC;
  signal data_ready_reg7_out : STD_LOGIC;
  signal data_ready_reg_i_1_n_0 : STD_LOGIC;
  signal is_first_cycle_going : STD_LOGIC;
  signal is_first_cycle_going_i_1_n_0 : STD_LOGIC;
  signal is_second_cycle_going_i_1_n_0 : STD_LOGIC;
  signal is_second_cycle_going_reg_n_0 : STD_LOGIC;
  signal pll_cs_reg_i_1_n_0 : STD_LOGIC;
  signal \^pll_data_ready\ : STD_LOGIC;
  signal \^pll_mosi_read\ : STD_LOGIC;
  signal pll_mosi_reg_i_1_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_2_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_3_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_4_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_6_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_7_n_0 : STD_LOGIC;
  signal \^pll_read_data\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pll_sen_read : STD_LOGIC;
  signal read_data_reg : STD_LOGIC;
  signal read_data_reg025_out : STD_LOGIC;
  signal \read_data_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[18]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \read_data_reg[23]_i_4_n_0\ : STD_LOGIC;
  signal \read_data_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_send[1]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \bits_send[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \bits_send[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \bits_send[4]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of data_ready_reg_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_3 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_5 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \read_data_reg[15]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_data_reg[16]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_data_reg[16]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \read_data_reg[17]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \read_data_reg[17]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \read_data_reg[18]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \read_data_reg[18]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \read_data_reg[19]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \read_data_reg[19]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \read_data_reg[1]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \read_data_reg[20]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \read_data_reg[23]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \read_data_reg[23]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \read_data_reg[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \read_data_reg[3]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \read_data_reg[7]_i_1\ : label is "soft_lutpair6";
begin
  pll_data_ready <= \^pll_data_ready\;
  pll_mosi_read <= \^pll_mosi_read\;
  pll_read_data(23 downto 0) <= \^pll_read_data\(23 downto 0);
\bits_send[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \bits_send_reg_n_0_[0]\,
      O => \bits_send[0]_i_1_n_0\
    );
\bits_send[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \bits_send_reg_n_0_[1]\,
      I1 => \bits_send_reg_n_0_[0]\,
      O => \bits_send[1]_i_1_n_0\
    );
\bits_send[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      O => \bits_send[2]_i_1_n_0\
    );
\bits_send[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => \bits_send_reg_n_0_[0]\,
      O => \bits_send[3]_i_1_n_0\
    );
\bits_send[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => is_first_cycle_going,
      O => \bits_send[4]_i_1_n_0\
    );
\bits_send[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \bits_send_reg_n_0_[4]\,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => \bits_send_reg_n_0_[3]\,
      O => \bits_send[4]_i_2_n_0\
    );
\bits_send_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => \bits_send[4]_i_1_n_0\,
      D => \bits_send[0]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[0]\,
      R => read_data_reg025_out
    );
\bits_send_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => \bits_send[4]_i_1_n_0\,
      D => \bits_send[1]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[1]\,
      R => read_data_reg025_out
    );
\bits_send_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => \bits_send[4]_i_1_n_0\,
      D => \bits_send[2]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[2]\,
      R => read_data_reg025_out
    );
\bits_send_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => \bits_send[4]_i_1_n_0\,
      D => \bits_send[3]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[3]\,
      R => read_data_reg025_out
    );
\bits_send_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => \bits_send[4]_i_1_n_0\,
      D => \bits_send[4]_i_2_n_0\,
      Q => \bits_send_reg_n_0_[4]\,
      R => read_data_reg025_out
    );
data_ready_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AB00ABAB"
    )
        port map (
      I0 => \^pll_data_ready\,
      I1 => is_first_cycle_going,
      I2 => is_second_cycle_going_reg_n_0,
      I3 => start_prev,
      I4 => pll_read,
      O => data_ready_reg_i_1_n_0
    );
data_ready_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => data_ready_reg_i_1_n_0,
      Q => \^pll_data_ready\,
      R => '0'
    );
is_first_cycle_going_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAFFFF2AAA2AAA"
    )
        port map (
      I0 => is_first_cycle_going,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[3]\,
      I3 => \read_data_reg[16]_i_2_n_0\,
      I4 => start_prev,
      I5 => pll_read,
      O => is_first_cycle_going_i_1_n_0
    );
is_first_cycle_going_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => is_first_cycle_going_i_1_n_0,
      Q => is_first_cycle_going,
      R => '0'
    );
is_second_cycle_going_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAEAAA2AAA"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[3]\,
      I3 => \read_data_reg[16]_i_2_n_0\,
      I4 => is_first_cycle_going,
      I5 => read_data_reg025_out,
      O => is_second_cycle_going_i_1_n_0
    );
is_second_cycle_going_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => is_second_cycle_going_i_1_n_0,
      Q => is_second_cycle_going_reg_n_0,
      R => '0'
    );
pll_cs_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DCDCFFDC54540054"
    )
        port map (
      I0 => pll_mosi_reg_i_3_n_0,
      I1 => is_first_cycle_going,
      I2 => is_second_cycle_going_reg_n_0,
      I3 => pll_read,
      I4 => start_prev,
      I5 => pll_sen_read,
      O => pll_cs_reg_i_1_n_0
    );
pll_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => pll_cs_reg_i_1_n_0,
      Q => pll_sen_read,
      R => '0'
    );
pll_mosi_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000022200020"
    )
        port map (
      I0 => pll_mosi_reg_i_2_n_0,
      I1 => read_data_reg025_out,
      I2 => \^pll_mosi_read\,
      I3 => pll_mosi_reg_i_3_n_0,
      I4 => pll_mosi_reg_i_4_n_0,
      I5 => data_ready_reg7_out,
      O => pll_mosi_reg_i_1_n_0
    );
pll_mosi_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEEE0000"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[4]\,
      O => pll_mosi_reg_i_2_n_0
    );
pll_mosi_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[3]\,
      I4 => \bits_send_reg_n_0_[4]\,
      O => pll_mosi_reg_i_3_n_0
    );
pll_mosi_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"008B000000008B00"
    )
        port map (
      I0 => pll_mosi_reg_i_6_n_0,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => pll_mosi_reg_i_7_n_0,
      I3 => \bits_send_reg_n_0_[4]\,
      I4 => \read_data_reg[16]_i_2_n_0\,
      I5 => \bits_send_reg_n_0_[3]\,
      O => pll_mosi_reg_i_4_n_0
    );
pll_mosi_reg_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => is_first_cycle_going,
      I1 => is_second_cycle_going_reg_n_0,
      O => data_ready_reg7_out
    );
pll_mosi_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => pll_read_address_reg(1),
      I1 => pll_read_address_reg(5),
      I2 => \bits_send[1]_i_1_n_0\,
      I3 => pll_read_address_reg(3),
      I4 => \bits_send[2]_i_1_n_0\,
      I5 => pll_read_address_reg(7),
      O => pll_mosi_reg_i_6_n_0
    );
pll_mosi_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => pll_read_address_reg(0),
      I1 => pll_read_address_reg(4),
      I2 => \bits_send[1]_i_1_n_0\,
      I3 => pll_read_address_reg(2),
      I4 => \bits_send[2]_i_1_n_0\,
      I5 => pll_read_address_reg(6),
      O => pll_mosi_reg_i_7_n_0
    );
pll_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => pll_mosi_reg_i_1_n_0,
      Q => \^pll_mosi_read\,
      R => '0'
    );
pll_sck_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA8"
    )
        port map (
      I0 => clk_pll_spi,
      I1 => is_clk_running,
      I2 => is_first_cycle_going,
      I3 => is_second_cycle_going_reg_n_0,
      O => pll_sck
    );
pll_sen_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_sen_read,
      I1 => pll_sen_write,
      O => pll_sen
    );
\read_data_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(0),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[16]_i_2_n_0\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[0]_i_1_n_0\
    );
\read_data_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(10),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[10]_i_1_n_0\
    );
\read_data_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(11),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[11]_i_1_n_0\
    );
\read_data_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(12),
      I1 => pll_ld_sdo,
      I2 => \bits_send_reg_n_0_[2]\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[12]_i_1_n_0\
    );
\read_data_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAEAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(13),
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[13]_i_1_n_0\
    );
\read_data_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAEAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(14),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[14]_i_1_n_0\
    );
\read_data_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAABAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(15),
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[15]_i_1_n_0\
    );
\read_data_reg[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[15]_i_2_n_0\
    );
\read_data_reg[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAABAAA"
    )
        port map (
      I0 => \^pll_read_data\(16),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[16]_i_2_n_0\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[16]_i_1_n_0\
    );
\read_data_reg[16]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \bits_send_reg_n_0_[0]\,
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[2]\,
      O => \read_data_reg[16]_i_2_n_0\
    );
\read_data_reg[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^pll_read_data\(17),
      I1 => \read_data_reg[17]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[17]_i_1_n_0\
    );
\read_data_reg[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      O => \read_data_reg[17]_i_2_n_0\
    );
\read_data_reg[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^pll_read_data\(18),
      I1 => \read_data_reg[18]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[18]_i_1_n_0\
    );
\read_data_reg[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[18]_i_2_n_0\
    );
\read_data_reg[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^pll_read_data\(19),
      I1 => \read_data_reg[19]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[19]_i_1_n_0\
    );
\read_data_reg[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[19]_i_2_n_0\
    );
\read_data_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^pll_read_data\(1),
      I1 => \read_data_reg[17]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[1]_i_1_n_0\
    );
\read_data_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAABAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(20),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[20]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => pll_ld_sdo,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[20]_i_1_n_0\
    );
\read_data_reg[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \bits_send_reg_n_0_[1]\,
      I1 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[20]_i_2_n_0\
    );
\read_data_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAABAAAA"
    )
        port map (
      I0 => \^pll_read_data\(21),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[21]_i_1_n_0\
    );
\read_data_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAABAAAA"
    )
        port map (
      I0 => \^pll_read_data\(22),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[22]_i_1_n_0\
    );
\read_data_reg[22]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => pll_ld_sdo,
      O => \read_data_reg[22]_i_2_n_0\
    );
\read_data_reg[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pll_read,
      I1 => start_prev,
      O => read_data_reg025_out
    );
\read_data_reg[23]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"002A"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[3]\,
      I3 => is_first_cycle_going,
      O => read_data_reg
    );
\read_data_reg[23]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^pll_read_data\(23),
      I1 => \read_data_reg[23]_i_4_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[23]_i_3_n_0\
    );
\read_data_reg[23]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[2]\,
      O => \read_data_reg[23]_i_4_n_0\
    );
\read_data_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^pll_read_data\(2),
      I1 => \read_data_reg[18]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[2]_i_1_n_0\
    );
\read_data_reg[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^pll_read_data\(3),
      I1 => \read_data_reg[19]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[3]_i_1_n_0\
    );
\read_data_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(4),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[20]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => pll_ld_sdo,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[4]_i_1_n_0\
    );
\read_data_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(5),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[5]_i_1_n_0\
    );
\read_data_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(6),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[6]_i_1_n_0\
    );
\read_data_reg[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^pll_read_data\(7),
      I1 => \read_data_reg[23]_i_4_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[7]_i_1_n_0\
    );
\read_data_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(8),
      I1 => pll_ld_sdo,
      I2 => \bits_send_reg_n_0_[2]\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[8]_i_1_n_0\
    );
\read_data_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^pll_read_data\(9),
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[9]_i_1_n_0\
    );
\read_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[0]_i_1_n_0\,
      Q => \^pll_read_data\(0),
      R => read_data_reg025_out
    );
\read_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[10]_i_1_n_0\,
      Q => \^pll_read_data\(10),
      R => read_data_reg025_out
    );
\read_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[11]_i_1_n_0\,
      Q => \^pll_read_data\(11),
      R => read_data_reg025_out
    );
\read_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[12]_i_1_n_0\,
      Q => \^pll_read_data\(12),
      R => read_data_reg025_out
    );
\read_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[13]_i_1_n_0\,
      Q => \^pll_read_data\(13),
      R => read_data_reg025_out
    );
\read_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[14]_i_1_n_0\,
      Q => \^pll_read_data\(14),
      R => read_data_reg025_out
    );
\read_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[15]_i_1_n_0\,
      Q => \^pll_read_data\(15),
      R => read_data_reg025_out
    );
\read_data_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[16]_i_1_n_0\,
      Q => \^pll_read_data\(16),
      R => read_data_reg025_out
    );
\read_data_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[17]_i_1_n_0\,
      Q => \^pll_read_data\(17),
      R => read_data_reg025_out
    );
\read_data_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[18]_i_1_n_0\,
      Q => \^pll_read_data\(18),
      R => read_data_reg025_out
    );
\read_data_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[19]_i_1_n_0\,
      Q => \^pll_read_data\(19),
      R => read_data_reg025_out
    );
\read_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[1]_i_1_n_0\,
      Q => \^pll_read_data\(1),
      R => read_data_reg025_out
    );
\read_data_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[20]_i_1_n_0\,
      Q => \^pll_read_data\(20),
      R => read_data_reg025_out
    );
\read_data_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[21]_i_1_n_0\,
      Q => \^pll_read_data\(21),
      R => read_data_reg025_out
    );
\read_data_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[22]_i_1_n_0\,
      Q => \^pll_read_data\(22),
      R => read_data_reg025_out
    );
\read_data_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[23]_i_3_n_0\,
      Q => \^pll_read_data\(23),
      R => read_data_reg025_out
    );
\read_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[2]_i_1_n_0\,
      Q => \^pll_read_data\(2),
      R => read_data_reg025_out
    );
\read_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[3]_i_1_n_0\,
      Q => \^pll_read_data\(3),
      R => read_data_reg025_out
    );
\read_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[4]_i_1_n_0\,
      Q => \^pll_read_data\(4),
      R => read_data_reg025_out
    );
\read_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[5]_i_1_n_0\,
      Q => \^pll_read_data\(5),
      R => read_data_reg025_out
    );
\read_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[6]_i_1_n_0\,
      Q => \^pll_read_data\(6),
      R => read_data_reg025_out
    );
\read_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[7]_i_1_n_0\,
      Q => \^pll_read_data\(7),
      R => read_data_reg025_out
    );
\read_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[8]_i_1_n_0\,
      Q => \^pll_read_data\(8),
      R => read_data_reg025_out
    );
\read_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => read_data_reg,
      D => \read_data_reg[9]_i_1_n_0\,
      Q => \^pll_read_data\(9),
      R => read_data_reg025_out
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => pll_read,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send is
  port (
    is_clk_running : out STD_LOGIC;
    pll_sen_write : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_write : in STD_LOGIC;
    clk_pll_spi : in STD_LOGIC;
    pll_write_address_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_write_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_mosi_read : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send is
  signal bits_send0 : STD_LOGIC;
  signal \bits_send[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_3_n_0\ : STD_LOGIC;
  signal bits_send_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^is_clk_running\ : STD_LOGIC;
  signal is_clk_running_i_1_n_0 : STD_LOGIC;
  signal \pll_cs_reg_i_1__0_n_0\ : STD_LOGIC;
  signal pll_mosi_reg_i_10_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_11_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_12_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_13_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_14_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_15_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_16_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_17_n_0 : STD_LOGIC;
  signal \pll_mosi_reg_i_1__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_2__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_3__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_4__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_5__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_6__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_7__0_n_0\ : STD_LOGIC;
  signal pll_mosi_reg_i_8_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_9_n_0 : STD_LOGIC;
  signal pll_mosi_write : STD_LOGIC;
  signal \^pll_sen_write\ : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_send[0]_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \bits_send[1]_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \bits_send[4]_i_2__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \bits_send[4]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \pll_cs_reg_i_1__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_12 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_14 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_17 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \pll_mosi_reg_i_2__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \pll_mosi_reg_i_4__0\ : label is "soft_lutpair13";
begin
  is_clk_running <= \^is_clk_running\;
  pll_sen_write <= \^pll_sen_write\;
\bits_send[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6066"
    )
        port map (
      I0 => bits_send_reg(0),
      I1 => \^is_clk_running\,
      I2 => start_prev,
      I3 => pll_write,
      O => \bits_send[0]_i_1__0_n_0\
    );
\bits_send[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0BBBB000"
    )
        port map (
      I0 => start_prev,
      I1 => pll_write,
      I2 => \^is_clk_running\,
      I3 => bits_send_reg(0),
      I4 => bits_send_reg(1),
      O => \bits_send[1]_i_1__0_n_0\
    );
\bits_send[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0BB0BB00BB00BB00"
    )
        port map (
      I0 => start_prev,
      I1 => pll_write,
      I2 => \^is_clk_running\,
      I3 => bits_send_reg(2),
      I4 => bits_send_reg(0),
      I5 => bits_send_reg(1),
      O => \bits_send[2]_i_1__0_n_0\
    );
\bits_send[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1450505050505050"
    )
        port map (
      I0 => bits_send0,
      I1 => \^is_clk_running\,
      I2 => bits_send_reg(3),
      I3 => bits_send_reg(2),
      I4 => bits_send_reg(1),
      I5 => bits_send_reg(0),
      O => \bits_send[3]_i_1__0_n_0\
    );
\bits_send[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1555555540000000"
    )
        port map (
      I0 => bits_send0,
      I1 => \^is_clk_running\,
      I2 => bits_send_reg(3),
      I3 => bits_send_reg(2),
      I4 => \bits_send[4]_i_3_n_0\,
      I5 => bits_send_reg(4),
      O => \bits_send[4]_i_1__0_n_0\
    );
\bits_send[4]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pll_write,
      I1 => start_prev,
      O => bits_send0
    );
\bits_send[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => bits_send_reg(1),
      I1 => bits_send_reg(0),
      O => \bits_send[4]_i_3_n_0\
    );
\bits_send_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \bits_send[0]_i_1__0_n_0\,
      Q => bits_send_reg(0),
      R => '0'
    );
\bits_send_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \bits_send[1]_i_1__0_n_0\,
      Q => bits_send_reg(1),
      R => '0'
    );
\bits_send_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \bits_send[2]_i_1__0_n_0\,
      Q => bits_send_reg(2),
      R => '0'
    );
\bits_send_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \bits_send[3]_i_1__0_n_0\,
      Q => bits_send_reg(3),
      R => '0'
    );
\bits_send_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \bits_send[4]_i_1__0_n_0\,
      Q => bits_send_reg(4),
      R => '0'
    );
is_clk_running_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF2AAAAAAA"
    )
        port map (
      I0 => \^is_clk_running\,
      I1 => \bits_send[4]_i_3_n_0\,
      I2 => bits_send_reg(2),
      I3 => bits_send_reg(3),
      I4 => bits_send_reg(4),
      I5 => bits_send0,
      O => is_clk_running_i_1_n_0
    );
is_clk_running_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => is_clk_running_i_1_n_0,
      Q => \^is_clk_running\,
      R => '0'
    );
\pll_cs_reg_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EF20AA20"
    )
        port map (
      I0 => \^pll_sen_write\,
      I1 => start_prev,
      I2 => pll_write,
      I3 => \^is_clk_running\,
      I4 => \pll_mosi_reg_i_2__0_n_0\,
      O => \pll_cs_reg_i_1__0_n_0\
    );
pll_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \pll_cs_reg_i_1__0_n_0\,
      Q => \^pll_sen_write\,
      R => '0'
    );
pll_mosi_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_mosi_write,
      I1 => pll_mosi_read,
      O => pll_mosi
    );
pll_mosi_reg_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(1),
      I1 => pll_write_data(0),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(3),
      I5 => pll_write_data(2),
      O => pll_mosi_reg_i_10_n_0
    );
pll_mosi_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(5),
      I1 => pll_write_data(4),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(7),
      I5 => pll_write_data(6),
      O => pll_mosi_reg_i_11_n_0
    );
pll_mosi_reg_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9555"
    )
        port map (
      I0 => bits_send_reg(3),
      I1 => bits_send_reg(2),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      O => pll_mosi_reg_i_12_n_0
    );
pll_mosi_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(9),
      I1 => pll_write_data(8),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(11),
      I5 => pll_write_data(10),
      O => pll_mosi_reg_i_13_n_0
    );
pll_mosi_reg_i_14: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => bits_send_reg(2),
      I1 => bits_send_reg(0),
      I2 => bits_send_reg(1),
      O => pll_mosi_reg_i_14_n_0
    );
pll_mosi_reg_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(13),
      I1 => pll_write_data(12),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(15),
      I5 => pll_write_data(14),
      O => pll_mosi_reg_i_15_n_0
    );
pll_mosi_reg_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F8000000F000000"
    )
        port map (
      I0 => bits_send_reg(0),
      I1 => bits_send_reg(1),
      I2 => bits_send_reg(2),
      I3 => bits_send_reg(3),
      I4 => bits_send_reg(4),
      I5 => pll_write_address_reg(4),
      O => pll_mosi_reg_i_16_n_0
    );
pll_mosi_reg_i_17: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F3AA0000"
    )
        port map (
      I0 => pll_write_address_reg(2),
      I1 => bits_send_reg(3),
      I2 => pll_write_address_reg(0),
      I3 => bits_send_reg(1),
      I4 => bits_send_reg(0),
      O => pll_mosi_reg_i_17_n_0
    );
\pll_mosi_reg_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFB000B0FF800080"
    )
        port map (
      I0 => pll_mosi_write,
      I1 => \pll_mosi_reg_i_2__0_n_0\,
      I2 => \^is_clk_running\,
      I3 => bits_send0,
      I4 => pll_write_data(23),
      I5 => \pll_mosi_reg_i_3__0_n_0\,
      O => \pll_mosi_reg_i_1__0_n_0\
    );
\pll_mosi_reg_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => bits_send_reg(4),
      I1 => bits_send_reg(3),
      I2 => bits_send_reg(2),
      I3 => bits_send_reg(1),
      I4 => bits_send_reg(0),
      O => \pll_mosi_reg_i_2__0_n_0\
    );
\pll_mosi_reg_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF3E02"
    )
        port map (
      I0 => \pll_mosi_reg_i_4__0_n_0\,
      I1 => bits_send_reg(4),
      I2 => \pll_mosi_reg_i_5__0_n_0\,
      I3 => \pll_mosi_reg_i_6__0_n_0\,
      I4 => \pll_mosi_reg_i_7__0_n_0\,
      O => \pll_mosi_reg_i_3__0_n_0\
    );
\pll_mosi_reg_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EBBB2888"
    )
        port map (
      I0 => pll_mosi_reg_i_8_n_0,
      I1 => bits_send_reg(2),
      I2 => bits_send_reg(0),
      I3 => bits_send_reg(1),
      I4 => pll_mosi_reg_i_9_n_0,
      O => \pll_mosi_reg_i_4__0_n_0\
    );
\pll_mosi_reg_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EAAA"
    )
        port map (
      I0 => bits_send_reg(3),
      I1 => bits_send_reg(2),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      O => \pll_mosi_reg_i_5__0_n_0\
    );
\pll_mosi_reg_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => pll_mosi_reg_i_10_n_0,
      I1 => pll_mosi_reg_i_11_n_0,
      I2 => pll_mosi_reg_i_12_n_0,
      I3 => pll_mosi_reg_i_13_n_0,
      I4 => pll_mosi_reg_i_14_n_0,
      I5 => pll_mosi_reg_i_15_n_0,
      O => \pll_mosi_reg_i_6__0_n_0\
    );
\pll_mosi_reg_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A8A8A8888888A88"
    )
        port map (
      I0 => pll_mosi_reg_i_16_n_0,
      I1 => pll_mosi_reg_i_17_n_0,
      I2 => bits_send_reg(0),
      I3 => pll_write_address_reg(3),
      I4 => bits_send_reg(1),
      I5 => pll_write_address_reg(1),
      O => \pll_mosi_reg_i_7__0_n_0\
    );
pll_mosi_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(17),
      I1 => pll_write_data(16),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(19),
      I5 => pll_write_data(18),
      O => pll_mosi_reg_i_8_n_0
    );
pll_mosi_reg_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => pll_write_data(21),
      I1 => pll_write_data(20),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => pll_write_data(23),
      I5 => pll_write_data(22),
      O => pll_mosi_reg_i_9_n_0
    );
pll_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => \pll_mosi_reg_i_1__0_n_0\,
      Q => pll_mosi_write,
      R => '0'
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_pll_spi,
      CE => '1',
      D => pll_write,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll is
  port (
    pll_read_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_sck : out STD_LOGIC;
    pll_sen : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_data_ready : out STD_LOGIC;
    pll_write : in STD_LOGIC;
    pll_read : in STD_LOGIC;
    clk_pll_spi : in STD_LOGIC;
    pll_ld_sdo : in STD_LOGIC;
    pll_read_address_reg : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pll_write_address_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_write_data : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll is
  signal is_clk_running : STD_LOGIC;
  signal pll_mosi_read : STD_LOGIC;
  signal pll_sen_write : STD_LOGIC;
begin
pll_receive: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive
     port map (
      clk_pll_spi => clk_pll_spi,
      is_clk_running => is_clk_running,
      pll_data_ready => pll_data_ready,
      pll_ld_sdo => pll_ld_sdo,
      pll_mosi_read => pll_mosi_read,
      pll_read => pll_read,
      pll_read_address_reg(7 downto 0) => pll_read_address_reg(7 downto 0),
      pll_read_data(23 downto 0) => pll_read_data(23 downto 0),
      pll_sck => pll_sck,
      pll_sen => pll_sen,
      pll_sen_write => pll_sen_write
    );
pll_send: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send
     port map (
      clk_pll_spi => clk_pll_spi,
      is_clk_running => is_clk_running,
      pll_mosi => pll_mosi,
      pll_mosi_read => pll_mosi_read,
      pll_sen_write => pll_sen_write,
      pll_write => pll_write,
      pll_write_address_reg(4 downto 0) => pll_write_address_reg(4 downto 0),
      pll_write_data(23 downto 0) => pll_write_data(23 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    pll_ld_sdo : in STD_LOGIC;
    clk_pll_spi : in STD_LOGIC;
    pll_read : in STD_LOGIC;
    pll_write : in STD_LOGIC;
    pll_write_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_address_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_address_reg : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pll_sen : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_sck : out STD_LOGIC;
    pll_read_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_data_ready : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_pll_0_1,pll,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "pll,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll
     port map (
      clk_pll_spi => clk_pll_spi,
      pll_data_ready => pll_data_ready,
      pll_ld_sdo => pll_ld_sdo,
      pll_mosi => pll_mosi,
      pll_read => pll_read,
      pll_read_address_reg(7 downto 0) => pll_read_address_reg(7 downto 0),
      pll_read_data(23 downto 0) => pll_read_data(23 downto 0),
      pll_sck => pll_sck,
      pll_sen => pll_sen,
      pll_write => pll_write,
      pll_write_address_reg(4 downto 0) => pll_write_address_reg(4 downto 0),
      pll_write_data(23 downto 0) => pll_write_data(23 downto 0)
    );
end STRUCTURE;

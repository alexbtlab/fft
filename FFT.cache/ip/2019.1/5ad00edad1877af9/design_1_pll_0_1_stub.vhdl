-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 13:15:56 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_pll_0_1_stub.vhdl
-- Design      : design_1_pll_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    pll_ld_sdo : in STD_LOGIC;
    clk_pll_spi : in STD_LOGIC;
    pll_read : in STD_LOGIC;
    pll_write : in STD_LOGIC;
    pll_write_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_address_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_address_reg : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pll_sen : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_sck : out STD_LOGIC;
    pll_read_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_data_ready : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "pll_ld_sdo,clk_pll_spi,pll_read,pll_write,pll_write_data[23:0],pll_write_address_reg[4:0],pll_read_address_reg[7:0],pll_sen,pll_mosi,pll_sck,pll_read_data[23:0],pll_data_ready";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "pll,Vivado 2019.1";
begin
end;

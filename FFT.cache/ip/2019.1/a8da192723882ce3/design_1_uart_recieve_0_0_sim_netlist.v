// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 16:06:18 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_recieve_0_0_sim_netlist.v
// Design      : design_1_uart_recieve_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_uart_recieve_0_0,uart_receive,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "uart_receive,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    uart_rx,
    byte_received,
    uart_data);
  input clk;
  input uart_rx;
  output byte_received;
  output [7:0]uart_data;

  wire byte_received;
  wire clk;
  wire [7:0]uart_data;
  wire uart_rx;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive inst
       (.byte_received(byte_received),
        .clk(clk),
        .uart_data(uart_data),
        .uart_rx(uart_rx));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive
   (uart_data,
    byte_received,
    clk,
    uart_rx);
  output [7:0]uart_data;
  output byte_received;
  input clk;
  input uart_rx;

  wire byte_received;
  wire byte_received_reg_i_1_n_0;
  wire byte_received_reg_i_2_n_0;
  wire byte_received_reg_i_3_n_0;
  wire clk;
  wire is_receiving_going_i_1_n_0;
  wire is_receiving_going_reg_n_0;
  wire [3:0]p_0_in;
  wire start_bit_recieved_i_1_n_0;
  wire start_bit_recieved_reg_n_0;
  wire [6:0]uart_cnt;
  wire \uart_cnt[0]_i_1_n_0 ;
  wire \uart_cnt[1]_i_1_n_0 ;
  wire \uart_cnt[1]_i_2_n_0 ;
  wire \uart_cnt[2]_i_1_n_0 ;
  wire \uart_cnt[2]_i_2_n_0 ;
  wire \uart_cnt[3]_i_1_n_0 ;
  wire \uart_cnt[4]_i_1_n_0 ;
  wire \uart_cnt[4]_i_2_n_0 ;
  wire \uart_cnt[4]_i_3_n_0 ;
  wire \uart_cnt[5]_i_1_n_0 ;
  wire \uart_cnt[5]_i_2_n_0 ;
  wire \uart_cnt[5]_i_3_n_0 ;
  wire \uart_cnt[5]_i_4_n_0 ;
  wire \uart_cnt[6]_i_1_n_0 ;
  wire \uart_cnt[6]_i_2_n_0 ;
  wire \uart_cnt[6]_i_3_n_0 ;
  wire [7:0]uart_data;
  wire \uart_data_reg[0]_i_1_n_0 ;
  wire \uart_data_reg[1]_i_1_n_0 ;
  wire \uart_data_reg[1]_i_2_n_0 ;
  wire \uart_data_reg[2]_i_1_n_0 ;
  wire \uart_data_reg[3]_i_1_n_0 ;
  wire \uart_data_reg[4]_i_1_n_0 ;
  wire \uart_data_reg[5]_i_1_n_0 ;
  wire \uart_data_reg[5]_i_2_n_0 ;
  wire \uart_data_reg[6]_i_1_n_0 ;
  wire \uart_data_reg[6]_i_2_n_0 ;
  wire \uart_data_reg[7]_i_1_n_0 ;
  wire \uart_data_reg[7]_i_2_n_0 ;
  wire \uart_data_reg[7]_i_3_n_0 ;
  wire \uart_num_of_rbit[3]_i_1_n_0 ;
  wire [3:0]uart_num_of_rbit_reg;
  wire uart_rx;

  LUT3 #(
    .INIT(8'hD5)) 
    byte_received_reg_i_1
       (.I0(byte_received_reg_i_2_n_0),
        .I1(byte_received_reg_i_3_n_0),
        .I2(byte_received),
        .O(byte_received_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    byte_received_reg_i_2
       (.I0(uart_num_of_rbit_reg[3]),
        .I1(\uart_data_reg[1]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(\uart_cnt[5]_i_2_n_0 ),
        .I4(\uart_cnt[1]_i_2_n_0 ),
        .I5(\uart_cnt[2]_i_2_n_0 ),
        .O(byte_received_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFDFFFFFFFFF)) 
    byte_received_reg_i_3
       (.I0(start_bit_recieved_reg_n_0),
        .I1(uart_cnt[1]),
        .I2(uart_cnt[0]),
        .I3(\uart_cnt[1]_i_2_n_0 ),
        .I4(uart_cnt[6]),
        .I5(uart_cnt[4]),
        .O(byte_received_reg_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    byte_received_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(byte_received_reg_i_1_n_0),
        .Q(byte_received),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFE000200)) 
    is_receiving_going_i_1
       (.I0(start_bit_recieved_reg_n_0),
        .I1(uart_rx),
        .I2(byte_received_reg_i_3_n_0),
        .I3(byte_received_reg_i_2_n_0),
        .I4(is_receiving_going_reg_n_0),
        .O(is_receiving_going_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_receiving_going_reg
       (.C(clk),
        .CE(1'b1),
        .D(is_receiving_going_i_1_n_0),
        .Q(is_receiving_going_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hF010)) 
    start_bit_recieved_i_1
       (.I0(is_receiving_going_reg_n_0),
        .I1(uart_rx),
        .I2(byte_received_reg_i_3_n_0),
        .I3(start_bit_recieved_reg_n_0),
        .O(start_bit_recieved_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_bit_recieved_reg
       (.C(clk),
        .CE(1'b1),
        .D(start_bit_recieved_i_1_n_0),
        .Q(start_bit_recieved_reg_n_0),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \uart_cnt[0]_i_1 
       (.I0(uart_cnt[0]),
        .O(\uart_cnt[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3C3C3C3C3C3C2C3C)) 
    \uart_cnt[1]_i_1 
       (.I0(is_receiving_going_reg_n_0),
        .I1(uart_cnt[1]),
        .I2(uart_cnt[0]),
        .I3(uart_cnt[4]),
        .I4(uart_cnt[6]),
        .I5(\uart_cnt[1]_i_2_n_0 ),
        .O(\uart_cnt[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \uart_cnt[1]_i_2 
       (.I0(uart_cnt[5]),
        .I1(uart_cnt[3]),
        .I2(uart_cnt[2]),
        .O(\uart_cnt[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00FFFFFFFB000000)) 
    \uart_cnt[2]_i_1 
       (.I0(\uart_cnt[2]_i_2_n_0 ),
        .I1(uart_cnt[5]),
        .I2(uart_cnt[3]),
        .I3(uart_cnt[1]),
        .I4(uart_cnt[0]),
        .I5(uart_cnt[2]),
        .O(\uart_cnt[2]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \uart_cnt[2]_i_2 
       (.I0(is_receiving_going_reg_n_0),
        .I1(uart_cnt[4]),
        .I2(uart_cnt[6]),
        .O(\uart_cnt[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \uart_cnt[3]_i_1 
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[1]),
        .I3(uart_cnt[3]),
        .O(\uart_cnt[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBBFBAAAABBFBEEAA)) 
    \uart_cnt[4]_i_1 
       (.I0(\uart_cnt[4]_i_2_n_0 ),
        .I1(uart_cnt[0]),
        .I2(is_receiving_going_reg_n_0),
        .I3(uart_cnt[1]),
        .I4(uart_cnt[4]),
        .I5(\uart_cnt[4]_i_3_n_0 ),
        .O(\uart_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h77770000FFEF0000)) 
    \uart_cnt[4]_i_2 
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[3]),
        .I2(uart_cnt[5]),
        .I3(uart_cnt[6]),
        .I4(uart_cnt[4]),
        .I5(uart_cnt[1]),
        .O(\uart_cnt[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \uart_cnt[4]_i_3 
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[3]),
        .O(\uart_cnt[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF6E006E80)) 
    \uart_cnt[5]_i_1 
       (.I0(uart_cnt[3]),
        .I1(uart_cnt[2]),
        .I2(uart_cnt[4]),
        .I3(uart_cnt[5]),
        .I4(\uart_cnt[5]_i_2_n_0 ),
        .I5(\uart_cnt[5]_i_3_n_0 ),
        .O(\uart_cnt[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \uart_cnt[5]_i_2 
       (.I0(uart_cnt[0]),
        .I1(uart_cnt[1]),
        .O(\uart_cnt[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAEAAEEEE)) 
    \uart_cnt[5]_i_3 
       (.I0(\uart_cnt[5]_i_4_n_0 ),
        .I1(uart_cnt[5]),
        .I2(uart_cnt[1]),
        .I3(is_receiving_going_reg_n_0),
        .I4(uart_cnt[0]),
        .O(\uart_cnt[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00DFFFF300000000)) 
    \uart_cnt[5]_i_4 
       (.I0(is_receiving_going_reg_n_0),
        .I1(uart_cnt[4]),
        .I2(uart_cnt[6]),
        .I3(uart_cnt[2]),
        .I4(uart_cnt[1]),
        .I5(uart_cnt[5]),
        .O(\uart_cnt[5]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \uart_cnt[6]_i_1 
       (.I0(is_receiving_going_reg_n_0),
        .I1(start_bit_recieved_reg_n_0),
        .O(\uart_cnt[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hC3F0F0F0F0F0F0D0)) 
    \uart_cnt[6]_i_2 
       (.I0(is_receiving_going_reg_n_0),
        .I1(\uart_cnt[6]_i_3_n_0 ),
        .I2(uart_cnt[6]),
        .I3(uart_cnt[4]),
        .I4(uart_cnt[2]),
        .I5(uart_cnt[3]),
        .O(\uart_cnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \uart_cnt[6]_i_3 
       (.I0(uart_cnt[1]),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[5]),
        .O(\uart_cnt[6]_i_3_n_0 ));
  FDRE \uart_cnt_reg[0] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[0]_i_1_n_0 ),
        .Q(uart_cnt[0]),
        .R(1'b0));
  FDRE \uart_cnt_reg[1] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[1]_i_1_n_0 ),
        .Q(uart_cnt[1]),
        .R(1'b0));
  FDRE \uart_cnt_reg[2] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[2]_i_1_n_0 ),
        .Q(uart_cnt[2]),
        .R(1'b0));
  FDRE \uart_cnt_reg[3] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[3]_i_1_n_0 ),
        .Q(uart_cnt[3]),
        .R(1'b0));
  FDRE \uart_cnt_reg[4] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[4]_i_1_n_0 ),
        .Q(uart_cnt[4]),
        .R(1'b0));
  FDRE \uart_cnt_reg[5] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[5]_i_1_n_0 ),
        .Q(uart_cnt[5]),
        .R(1'b0));
  FDRE \uart_cnt_reg[6] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[6]_i_2_n_0 ),
        .Q(uart_cnt[6]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \uart_data_reg[0]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[6]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(uart_num_of_rbit_reg[1]),
        .I4(uart_data[0]),
        .O(\uart_data_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \uart_data_reg[1]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[7]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(uart_num_of_rbit_reg[3]),
        .I4(\uart_data_reg[1]_i_2_n_0 ),
        .I5(uart_data[1]),
        .O(\uart_data_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \uart_data_reg[1]_i_2 
       (.I0(uart_num_of_rbit_reg[1]),
        .I1(uart_num_of_rbit_reg[2]),
        .O(\uart_data_reg[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \uart_data_reg[2]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[2]),
        .I2(uart_num_of_rbit_reg[1]),
        .I3(\uart_data_reg[6]_i_2_n_0 ),
        .I4(uart_data[2]),
        .O(\uart_data_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    \uart_data_reg[3]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[2]),
        .I2(uart_num_of_rbit_reg[1]),
        .I3(\uart_data_reg[7]_i_2_n_0 ),
        .I4(\uart_data_reg[5]_i_2_n_0 ),
        .I5(uart_data[3]),
        .O(\uart_data_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \uart_data_reg[4]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(\uart_data_reg[6]_i_2_n_0 ),
        .I4(uart_data[4]),
        .O(\uart_data_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    \uart_data_reg[5]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(\uart_data_reg[7]_i_2_n_0 ),
        .I4(\uart_data_reg[5]_i_2_n_0 ),
        .I5(uart_data[5]),
        .O(\uart_data_reg[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \uart_data_reg[5]_i_2 
       (.I0(uart_num_of_rbit_reg[0]),
        .I1(uart_num_of_rbit_reg[3]),
        .O(\uart_data_reg[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hBFFF8000)) 
    \uart_data_reg[6]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[6]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(uart_num_of_rbit_reg[1]),
        .I4(uart_data[6]),
        .O(\uart_data_reg[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \uart_data_reg[6]_i_2 
       (.I0(uart_num_of_rbit_reg[0]),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[1]),
        .I3(\uart_cnt[1]_i_2_n_0 ),
        .I4(\uart_cnt[2]_i_2_n_0 ),
        .I5(uart_num_of_rbit_reg[3]),
        .O(\uart_data_reg[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \uart_data_reg[7]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[7]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(uart_num_of_rbit_reg[3]),
        .I4(\uart_data_reg[7]_i_3_n_0 ),
        .I5(uart_data[7]),
        .O(\uart_data_reg[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF7F)) 
    \uart_data_reg[7]_i_2 
       (.I0(uart_cnt[0]),
        .I1(uart_cnt[1]),
        .I2(uart_cnt[5]),
        .I3(uart_cnt[3]),
        .I4(uart_cnt[2]),
        .I5(\uart_cnt[2]_i_2_n_0 ),
        .O(\uart_data_reg[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \uart_data_reg[7]_i_3 
       (.I0(uart_num_of_rbit_reg[1]),
        .I1(uart_num_of_rbit_reg[2]),
        .O(\uart_data_reg[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[0]_i_1_n_0 ),
        .Q(uart_data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[1]_i_1_n_0 ),
        .Q(uart_data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[2]_i_1_n_0 ),
        .Q(uart_data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[3]_i_1_n_0 ),
        .Q(uart_data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[4]_i_1_n_0 ),
        .Q(uart_data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[5]_i_1_n_0 ),
        .Q(uart_data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[6]_i_1_n_0 ),
        .Q(uart_data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[7]_i_1_n_0 ),
        .Q(uart_data[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h00FD)) 
    \uart_num_of_rbit[0]_i_1 
       (.I0(uart_num_of_rbit_reg[3]),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(uart_num_of_rbit_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \uart_num_of_rbit[1]_i_1 
       (.I0(uart_num_of_rbit_reg[0]),
        .I1(uart_num_of_rbit_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \uart_num_of_rbit[2]_i_1 
       (.I0(uart_num_of_rbit_reg[1]),
        .I1(uart_num_of_rbit_reg[0]),
        .I2(uart_num_of_rbit_reg[2]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \uart_num_of_rbit[3]_i_1 
       (.I0(\uart_cnt[2]_i_2_n_0 ),
        .I1(uart_cnt[2]),
        .I2(uart_cnt[3]),
        .I3(uart_cnt[5]),
        .I4(uart_cnt[1]),
        .I5(uart_cnt[0]),
        .O(\uart_num_of_rbit[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h78E0)) 
    \uart_num_of_rbit[3]_i_2 
       (.I0(uart_num_of_rbit_reg[1]),
        .I1(uart_num_of_rbit_reg[2]),
        .I2(uart_num_of_rbit_reg[3]),
        .I3(uart_num_of_rbit_reg[0]),
        .O(p_0_in[3]));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[0] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[0]),
        .Q(uart_num_of_rbit_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[1] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[1]),
        .Q(uart_num_of_rbit_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[2] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[2]),
        .Q(uart_num_of_rbit_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[3] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[3]),
        .Q(uart_num_of_rbit_reg[3]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

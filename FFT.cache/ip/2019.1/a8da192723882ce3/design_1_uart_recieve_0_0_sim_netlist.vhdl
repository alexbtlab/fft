-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 16:06:18 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_recieve_0_0_sim_netlist.vhdl
-- Design      : design_1_uart_recieve_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive is
  port (
    uart_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    byte_received : out STD_LOGIC;
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive is
  signal \^byte_received\ : STD_LOGIC;
  signal byte_received_reg_i_1_n_0 : STD_LOGIC;
  signal byte_received_reg_i_2_n_0 : STD_LOGIC;
  signal byte_received_reg_i_3_n_0 : STD_LOGIC;
  signal is_receiving_going_i_1_n_0 : STD_LOGIC;
  signal is_receiving_going_reg_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal start_bit_recieved_i_1_n_0 : STD_LOGIC;
  signal start_bit_recieved_reg_n_0 : STD_LOGIC;
  signal uart_cnt : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \uart_cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[1]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[2]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[4]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[4]_i_3_n_0\ : STD_LOGIC;
  signal \uart_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[5]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[5]_i_3_n_0\ : STD_LOGIC;
  signal \uart_cnt[5]_i_4_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \^uart_data\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \uart_data_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \uart_num_of_rbit[3]_i_1_n_0\ : STD_LOGIC;
  signal uart_num_of_rbit_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of is_receiving_going_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of start_bit_recieved_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \uart_cnt[1]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \uart_cnt[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \uart_cnt[4]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \uart_cnt[5]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \uart_cnt[5]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \uart_cnt[6]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \uart_data_reg[1]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \uart_data_reg[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \uart_data_reg[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \uart_data_reg[7]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[0]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[1]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[3]_i_2\ : label is "soft_lutpair4";
begin
  byte_received <= \^byte_received\;
  uart_data(7 downto 0) <= \^uart_data\(7 downto 0);
byte_received_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D5"
    )
        port map (
      I0 => byte_received_reg_i_2_n_0,
      I1 => byte_received_reg_i_3_n_0,
      I2 => \^byte_received\,
      O => byte_received_reg_i_1_n_0
    );
byte_received_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => uart_num_of_rbit_reg(3),
      I1 => \uart_data_reg[1]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(0),
      I3 => \uart_cnt[5]_i_2_n_0\,
      I4 => \uart_cnt[1]_i_2_n_0\,
      I5 => \uart_cnt[2]_i_2_n_0\,
      O => byte_received_reg_i_2_n_0
    );
byte_received_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFDFFFFFFFFF"
    )
        port map (
      I0 => start_bit_recieved_reg_n_0,
      I1 => uart_cnt(1),
      I2 => uart_cnt(0),
      I3 => \uart_cnt[1]_i_2_n_0\,
      I4 => uart_cnt(6),
      I5 => uart_cnt(4),
      O => byte_received_reg_i_3_n_0
    );
byte_received_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => byte_received_reg_i_1_n_0,
      Q => \^byte_received\,
      R => '0'
    );
is_receiving_going_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE000200"
    )
        port map (
      I0 => start_bit_recieved_reg_n_0,
      I1 => uart_rx,
      I2 => byte_received_reg_i_3_n_0,
      I3 => byte_received_reg_i_2_n_0,
      I4 => is_receiving_going_reg_n_0,
      O => is_receiving_going_i_1_n_0
    );
is_receiving_going_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => is_receiving_going_i_1_n_0,
      Q => is_receiving_going_reg_n_0,
      R => '0'
    );
start_bit_recieved_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F010"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => uart_rx,
      I2 => byte_received_reg_i_3_n_0,
      I3 => start_bit_recieved_reg_n_0,
      O => start_bit_recieved_i_1_n_0
    );
start_bit_recieved_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => start_bit_recieved_i_1_n_0,
      Q => start_bit_recieved_reg_n_0,
      R => '0'
    );
\uart_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => uart_cnt(0),
      O => \uart_cnt[0]_i_1_n_0\
    );
\uart_cnt[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C3C3C3C3C3C2C3C"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => uart_cnt(1),
      I2 => uart_cnt(0),
      I3 => uart_cnt(4),
      I4 => uart_cnt(6),
      I5 => \uart_cnt[1]_i_2_n_0\,
      O => \uart_cnt[1]_i_1_n_0\
    );
\uart_cnt[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => uart_cnt(5),
      I1 => uart_cnt(3),
      I2 => uart_cnt(2),
      O => \uart_cnt[1]_i_2_n_0\
    );
\uart_cnt[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FFFFFFFB000000"
    )
        port map (
      I0 => \uart_cnt[2]_i_2_n_0\,
      I1 => uart_cnt(5),
      I2 => uart_cnt(3),
      I3 => uart_cnt(1),
      I4 => uart_cnt(0),
      I5 => uart_cnt(2),
      O => \uart_cnt[2]_i_1_n_0\
    );
\uart_cnt[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => uart_cnt(4),
      I2 => uart_cnt(6),
      O => \uart_cnt[2]_i_2_n_0\
    );
\uart_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(0),
      I2 => uart_cnt(1),
      I3 => uart_cnt(3),
      O => \uart_cnt[3]_i_1_n_0\
    );
\uart_cnt[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBFBAAAABBFBEEAA"
    )
        port map (
      I0 => \uart_cnt[4]_i_2_n_0\,
      I1 => uart_cnt(0),
      I2 => is_receiving_going_reg_n_0,
      I3 => uart_cnt(1),
      I4 => uart_cnt(4),
      I5 => \uart_cnt[4]_i_3_n_0\,
      O => \uart_cnt[4]_i_1_n_0\
    );
\uart_cnt[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"77770000FFEF0000"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(3),
      I2 => uart_cnt(5),
      I3 => uart_cnt(6),
      I4 => uart_cnt(4),
      I5 => uart_cnt(1),
      O => \uart_cnt[4]_i_2_n_0\
    );
\uart_cnt[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(3),
      O => \uart_cnt[4]_i_3_n_0\
    );
\uart_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF6E006E80"
    )
        port map (
      I0 => uart_cnt(3),
      I1 => uart_cnt(2),
      I2 => uart_cnt(4),
      I3 => uart_cnt(5),
      I4 => \uart_cnt[5]_i_2_n_0\,
      I5 => \uart_cnt[5]_i_3_n_0\,
      O => \uart_cnt[5]_i_1_n_0\
    );
\uart_cnt[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => uart_cnt(0),
      I1 => uart_cnt(1),
      O => \uart_cnt[5]_i_2_n_0\
    );
\uart_cnt[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAEEEE"
    )
        port map (
      I0 => \uart_cnt[5]_i_4_n_0\,
      I1 => uart_cnt(5),
      I2 => uart_cnt(1),
      I3 => is_receiving_going_reg_n_0,
      I4 => uart_cnt(0),
      O => \uart_cnt[5]_i_3_n_0\
    );
\uart_cnt[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00DFFFF300000000"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => uart_cnt(4),
      I2 => uart_cnt(6),
      I3 => uart_cnt(2),
      I4 => uart_cnt(1),
      I5 => uart_cnt(5),
      O => \uart_cnt[5]_i_4_n_0\
    );
\uart_cnt[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => start_bit_recieved_reg_n_0,
      O => \uart_cnt[6]_i_1_n_0\
    );
\uart_cnt[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3F0F0F0F0F0F0D0"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => \uart_cnt[6]_i_3_n_0\,
      I2 => uart_cnt(6),
      I3 => uart_cnt(4),
      I4 => uart_cnt(2),
      I5 => uart_cnt(3),
      O => \uart_cnt[6]_i_2_n_0\
    );
\uart_cnt[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => uart_cnt(1),
      I1 => uart_cnt(0),
      I2 => uart_cnt(5),
      O => \uart_cnt[6]_i_3_n_0\
    );
\uart_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[0]_i_1_n_0\,
      Q => uart_cnt(0),
      R => '0'
    );
\uart_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[1]_i_1_n_0\,
      Q => uart_cnt(1),
      R => '0'
    );
\uart_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[2]_i_1_n_0\,
      Q => uart_cnt(2),
      R => '0'
    );
\uart_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[3]_i_1_n_0\,
      Q => uart_cnt(3),
      R => '0'
    );
\uart_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[4]_i_1_n_0\,
      Q => uart_cnt(4),
      R => '0'
    );
\uart_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[5]_i_1_n_0\,
      Q => uart_cnt(5),
      R => '0'
    );
\uart_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[6]_i_2_n_0\,
      Q => uart_cnt(6),
      R => '0'
    );
\uart_data_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[6]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(2),
      I3 => uart_num_of_rbit_reg(1),
      I4 => \^uart_data\(0),
      O => \uart_data_reg[0]_i_1_n_0\
    );
\uart_data_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[7]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(0),
      I3 => uart_num_of_rbit_reg(3),
      I4 => \uart_data_reg[1]_i_2_n_0\,
      I5 => \^uart_data\(1),
      O => \uart_data_reg[1]_i_1_n_0\
    );
\uart_data_reg[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => uart_num_of_rbit_reg(1),
      I1 => uart_num_of_rbit_reg(2),
      O => \uart_data_reg[1]_i_2_n_0\
    );
\uart_data_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(2),
      I2 => uart_num_of_rbit_reg(1),
      I3 => \uart_data_reg[6]_i_2_n_0\,
      I4 => \^uart_data\(2),
      O => \uart_data_reg[2]_i_1_n_0\
    );
\uart_data_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(2),
      I2 => uart_num_of_rbit_reg(1),
      I3 => \uart_data_reg[7]_i_2_n_0\,
      I4 => \uart_data_reg[5]_i_2_n_0\,
      I5 => \^uart_data\(3),
      O => \uart_data_reg[3]_i_1_n_0\
    );
\uart_data_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(2),
      I3 => \uart_data_reg[6]_i_2_n_0\,
      I4 => \^uart_data\(4),
      O => \uart_data_reg[4]_i_1_n_0\
    );
\uart_data_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(2),
      I3 => \uart_data_reg[7]_i_2_n_0\,
      I4 => \uart_data_reg[5]_i_2_n_0\,
      I5 => \^uart_data\(5),
      O => \uart_data_reg[5]_i_1_n_0\
    );
\uart_data_reg[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => uart_num_of_rbit_reg(0),
      I1 => uart_num_of_rbit_reg(3),
      O => \uart_data_reg[5]_i_2_n_0\
    );
\uart_data_reg[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF8000"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[6]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(2),
      I3 => uart_num_of_rbit_reg(1),
      I4 => \^uart_data\(6),
      O => \uart_data_reg[6]_i_1_n_0\
    );
\uart_data_reg[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => uart_num_of_rbit_reg(0),
      I1 => uart_cnt(0),
      I2 => uart_cnt(1),
      I3 => \uart_cnt[1]_i_2_n_0\,
      I4 => \uart_cnt[2]_i_2_n_0\,
      I5 => uart_num_of_rbit_reg(3),
      O => \uart_data_reg[6]_i_2_n_0\
    );
\uart_data_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[7]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(0),
      I3 => uart_num_of_rbit_reg(3),
      I4 => \uart_data_reg[7]_i_3_n_0\,
      I5 => \^uart_data\(7),
      O => \uart_data_reg[7]_i_1_n_0\
    );
\uart_data_reg[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF7F"
    )
        port map (
      I0 => uart_cnt(0),
      I1 => uart_cnt(1),
      I2 => uart_cnt(5),
      I3 => uart_cnt(3),
      I4 => uart_cnt(2),
      I5 => \uart_cnt[2]_i_2_n_0\,
      O => \uart_data_reg[7]_i_2_n_0\
    );
\uart_data_reg[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => uart_num_of_rbit_reg(1),
      I1 => uart_num_of_rbit_reg(2),
      O => \uart_data_reg[7]_i_3_n_0\
    );
\uart_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[0]_i_1_n_0\,
      Q => \^uart_data\(0),
      R => '0'
    );
\uart_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[1]_i_1_n_0\,
      Q => \^uart_data\(1),
      R => '0'
    );
\uart_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[2]_i_1_n_0\,
      Q => \^uart_data\(2),
      R => '0'
    );
\uart_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[3]_i_1_n_0\,
      Q => \^uart_data\(3),
      R => '0'
    );
\uart_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[4]_i_1_n_0\,
      Q => \^uart_data\(4),
      R => '0'
    );
\uart_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[5]_i_1_n_0\,
      Q => \^uart_data\(5),
      R => '0'
    );
\uart_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[6]_i_1_n_0\,
      Q => \^uart_data\(6),
      R => '0'
    );
\uart_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[7]_i_1_n_0\,
      Q => \^uart_data\(7),
      R => '0'
    );
\uart_num_of_rbit[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00FD"
    )
        port map (
      I0 => uart_num_of_rbit_reg(3),
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(2),
      I3 => uart_num_of_rbit_reg(0),
      O => p_0_in(0)
    );
\uart_num_of_rbit[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => uart_num_of_rbit_reg(0),
      I1 => uart_num_of_rbit_reg(1),
      O => p_0_in(1)
    );
\uart_num_of_rbit[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => uart_num_of_rbit_reg(1),
      I1 => uart_num_of_rbit_reg(0),
      I2 => uart_num_of_rbit_reg(2),
      O => p_0_in(2)
    );
\uart_num_of_rbit[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => \uart_cnt[2]_i_2_n_0\,
      I1 => uart_cnt(2),
      I2 => uart_cnt(3),
      I3 => uart_cnt(5),
      I4 => uart_cnt(1),
      I5 => uart_cnt(0),
      O => \uart_num_of_rbit[3]_i_1_n_0\
    );
\uart_num_of_rbit[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"78E0"
    )
        port map (
      I0 => uart_num_of_rbit_reg(1),
      I1 => uart_num_of_rbit_reg(2),
      I2 => uart_num_of_rbit_reg(3),
      I3 => uart_num_of_rbit_reg(0),
      O => p_0_in(3)
    );
\uart_num_of_rbit_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(0),
      Q => uart_num_of_rbit_reg(0),
      R => '0'
    );
\uart_num_of_rbit_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(1),
      Q => uart_num_of_rbit_reg(1),
      R => '0'
    );
\uart_num_of_rbit_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(2),
      Q => uart_num_of_rbit_reg(2),
      R => '0'
    );
\uart_num_of_rbit_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(3),
      Q => uart_num_of_rbit_reg(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC;
    byte_received : out STD_LOGIC;
    uart_data : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_uart_recieve_0_0,uart_receive,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "uart_receive,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive
     port map (
      byte_received => byte_received,
      clk => clk,
      uart_data(7 downto 0) => uart_data(7 downto 0),
      uart_rx => uart_rx
    );
end STRUCTURE;

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 12:09:30 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_stub.vhdl
-- Design      : design_1_top_logic_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    in_uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk_100MHz_in : in STD_LOGIC;
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    pll_data_ready : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    pll_read_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 );
    t_sweep : out STD_LOGIC;
    clk_100MHz_out : out STD_LOGIC;
    user_led : out STD_LOGIC;
    pamp_en : out STD_LOGIC;
    sw_ls : out STD_LOGIC;
    if_amp1_stu_en : out STD_LOGIC;
    if_amp2_stu_en : out STD_LOGIC;
    sw_if1 : out STD_LOGIC;
    sw_if2 : out STD_LOGIC;
    sw_if3 : out STD_LOGIC;
    clk_sync : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    sw_ctrl : out STD_LOGIC;
    atten : out STD_LOGIC_VECTOR ( 5 downto 0 );
    start_uart_send : out STD_LOGIC;
    is_adc_data_sending : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    pll_read : out STD_LOGIC;
    pll_write : out STD_LOGIC;
    pll_write_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_addres : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_addres : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "in_uart_data[7:0],clk_100MHz_in,in_adc_data[7:0],uart_adc_start_sending,pll_data_ready,uart_byte_received,pll_read_data[23:0],out_data_uart[7:0],t_sweep,clk_100MHz_out,user_led,pamp_en,sw_ls,if_amp1_stu_en,if_amp2_stu_en,sw_if1,sw_if2,sw_if3,clk_sync,pll_trig,sw_ctrl,atten[5:0],start_uart_send,is_adc_data_sending,clk_10MHz_out,start_init_clk,clk_20MHz_out,pll_read,pll_write,pll_write_data[23:0],pll_write_addres[4:0],pll_read_addres[7:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "top_logic,Vivado 2019.1";
begin
end;

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 22:08:59 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_0_0_sim_netlist.vhdl
-- Design      : design_1_uart_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive is
  port (
    byte_received : out STD_LOGIC;
    uart_data_rx : out STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive is
  signal \^byte_received\ : STD_LOGIC;
  signal byte_received_reg_i_1_n_0 : STD_LOGIC;
  signal byte_received_reg_i_2_n_0 : STD_LOGIC;
  signal byte_received_reg_i_3_n_0 : STD_LOGIC;
  signal byte_received_reg_i_4_n_0 : STD_LOGIC;
  signal is_receiving_going_i_1_n_0 : STD_LOGIC;
  signal is_receiving_going_reg_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal start_bit_recieved_i_1_n_0 : STD_LOGIC;
  signal start_bit_recieved_reg_n_0 : STD_LOGIC;
  signal uart_cnt : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \uart_cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[1]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[2]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \uart_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal \uart_data_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \uart_data_reg[6]_i_3_n_0\ : STD_LOGIC;
  signal \uart_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \uart_data_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \^uart_data_rx\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \uart_num_of_rbit[3]_i_1_n_0\ : STD_LOGIC;
  signal \uart_num_of_rbit[3]_i_2_n_0\ : STD_LOGIC;
  signal \uart_num_of_rbit[3]_i_3_n_0\ : STD_LOGIC;
  signal uart_num_of_rbit_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of byte_received_reg_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of byte_received_reg_i_3 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of is_receiving_going_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of start_bit_recieved_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \uart_cnt[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \uart_cnt[1]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \uart_cnt[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \uart_cnt[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \uart_cnt[6]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \uart_cnt[6]_i_4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \uart_data_reg[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \uart_data_reg[3]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \uart_data_reg[6]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \uart_data_reg[7]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[3]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \uart_num_of_rbit[3]_i_3\ : label is "soft_lutpair2";
begin
  byte_received <= \^byte_received\;
  uart_data_rx(7 downto 0) <= \^uart_data_rx\(7 downto 0);
byte_received_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DC"
    )
        port map (
      I0 => byte_received_reg_i_2_n_0,
      I1 => byte_received_reg_i_3_n_0,
      I2 => \^byte_received\,
      O => byte_received_reg_i_1_n_0
    );
byte_received_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => byte_received_reg_i_4_n_0,
      I1 => uart_cnt(6),
      I2 => start_bit_recieved_reg_n_0,
      I3 => uart_cnt(3),
      I4 => uart_cnt(0),
      O => byte_received_reg_i_2_n_0
    );
byte_received_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => \uart_num_of_rbit[3]_i_1_n_0\,
      I1 => uart_num_of_rbit_reg(2),
      I2 => uart_num_of_rbit_reg(3),
      I3 => uart_num_of_rbit_reg(0),
      I4 => uart_num_of_rbit_reg(1),
      O => byte_received_reg_i_3_n_0
    );
byte_received_reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(4),
      I2 => uart_cnt(5),
      I3 => uart_cnt(1),
      O => byte_received_reg_i_4_n_0
    );
byte_received_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => byte_received_reg_i_1_n_0,
      Q => \^byte_received\,
      R => '0'
    );
is_receiving_going_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0F0B0008"
    )
        port map (
      I0 => start_bit_recieved_reg_n_0,
      I1 => byte_received_reg_i_2_n_0,
      I2 => byte_received_reg_i_3_n_0,
      I3 => uart_rx,
      I4 => is_receiving_going_reg_n_0,
      O => is_receiving_going_i_1_n_0
    );
is_receiving_going_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => is_receiving_going_i_1_n_0,
      Q => is_receiving_going_reg_n_0,
      R => '0'
    );
start_bit_recieved_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F01"
    )
        port map (
      I0 => uart_rx,
      I1 => is_receiving_going_reg_n_0,
      I2 => byte_received_reg_i_2_n_0,
      I3 => start_bit_recieved_reg_n_0,
      O => start_bit_recieved_i_1_n_0
    );
start_bit_recieved_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => start_bit_recieved_i_1_n_0,
      Q => start_bit_recieved_reg_n_0,
      R => '0'
    );
\uart_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => uart_cnt(0),
      O => \uart_cnt[0]_i_1_n_0\
    );
\uart_cnt[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \uart_cnt[1]_i_2_n_0\,
      I1 => uart_cnt(0),
      I2 => uart_cnt(1),
      O => \uart_cnt[1]_i_1_n_0\
    );
\uart_cnt[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFD5FF"
    )
        port map (
      I0 => byte_received_reg_i_4_n_0,
      I1 => uart_cnt(4),
      I2 => uart_cnt(3),
      I3 => uart_cnt(5),
      I4 => uart_cnt(6),
      I5 => is_receiving_going_reg_n_0,
      O => \uart_cnt[1]_i_2_n_0\
    );
\uart_cnt[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2A6A"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(0),
      I2 => uart_cnt(1),
      I3 => \uart_cnt[2]_i_2_n_0\,
      O => \uart_cnt[2]_i_1_n_0\
    );
\uart_cnt[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00400000"
    )
        port map (
      I0 => uart_cnt(3),
      I1 => is_receiving_going_reg_n_0,
      I2 => uart_cnt(6),
      I3 => uart_cnt(4),
      I4 => uart_cnt(5),
      O => \uart_cnt[2]_i_2_n_0\
    );
\uart_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => uart_cnt(3),
      I1 => uart_cnt(2),
      I2 => uart_cnt(1),
      I3 => uart_cnt(0),
      O => \uart_cnt[3]_i_1_n_0\
    );
\uart_cnt[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAAAAA80000000"
    )
        port map (
      I0 => \uart_cnt[6]_i_4_n_0\,
      I1 => uart_cnt(3),
      I2 => uart_cnt(2),
      I3 => uart_cnt(1),
      I4 => uart_cnt(0),
      I5 => uart_cnt(4),
      O => \uart_cnt[4]_i_1_n_0\
    );
\uart_cnt[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2AAA8000"
    )
        port map (
      I0 => \uart_cnt[6]_i_4_n_0\,
      I1 => \uart_cnt[6]_i_3_n_0\,
      I2 => uart_cnt(3),
      I3 => uart_cnt(4),
      I4 => uart_cnt(5),
      O => \uart_cnt[5]_i_1_n_0\
    );
\uart_cnt[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => start_bit_recieved_reg_n_0,
      O => \uart_cnt[6]_i_1_n_0\
    );
\uart_cnt[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFF800000000000"
    )
        port map (
      I0 => uart_cnt(4),
      I1 => uart_cnt(3),
      I2 => \uart_cnt[6]_i_3_n_0\,
      I3 => uart_cnt(5),
      I4 => uart_cnt(6),
      I5 => \uart_cnt[6]_i_4_n_0\,
      O => \uart_cnt[6]_i_2_n_0\
    );
\uart_cnt[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => uart_cnt(0),
      I1 => uart_cnt(1),
      I2 => uart_cnt(2),
      O => \uart_cnt[6]_i_3_n_0\
    );
\uart_cnt[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"45444545"
    )
        port map (
      I0 => \uart_num_of_rbit[3]_i_1_n_0\,
      I1 => \uart_cnt[1]_i_2_n_0\,
      I2 => uart_cnt(2),
      I3 => uart_cnt(1),
      I4 => uart_cnt(0),
      O => \uart_cnt[6]_i_4_n_0\
    );
\uart_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[0]_i_1_n_0\,
      Q => uart_cnt(0),
      R => '0'
    );
\uart_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[1]_i_1_n_0\,
      Q => uart_cnt(1),
      R => '0'
    );
\uart_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[2]_i_1_n_0\,
      Q => uart_cnt(2),
      R => '0'
    );
\uart_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[3]_i_1_n_0\,
      Q => uart_cnt(3),
      R => '0'
    );
\uart_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[4]_i_1_n_0\,
      Q => uart_cnt(4),
      R => '0'
    );
\uart_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[5]_i_1_n_0\,
      Q => uart_cnt(5),
      R => '0'
    );
\uart_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \uart_cnt[6]_i_1_n_0\,
      D => \uart_cnt[6]_i_2_n_0\,
      Q => uart_cnt(6),
      R => '0'
    );
\uart_data_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(0),
      I3 => \uart_data_reg[3]_i_2_n_0\,
      I4 => \^uart_data_rx\(0),
      O => \uart_data_reg[0]_i_1_n_0\
    );
\uart_data_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(0),
      I3 => \uart_data_reg[3]_i_2_n_0\,
      I4 => \^uart_data_rx\(1),
      O => \uart_data_reg[1]_i_1_n_0\
    );
\uart_data_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(0),
      I2 => uart_num_of_rbit_reg(1),
      I3 => \uart_data_reg[3]_i_2_n_0\,
      I4 => \^uart_data_rx\(2),
      O => \uart_data_reg[2]_i_1_n_0\
    );
\uart_data_reg[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => uart_rx,
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(0),
      I3 => \uart_data_reg[3]_i_2_n_0\,
      I4 => \^uart_data_rx\(3),
      O => \uart_data_reg[3]_i_1_n_0\
    );
\uart_data_reg[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => uart_num_of_rbit_reg(3),
      I1 => is_receiving_going_reg_n_0,
      I2 => uart_num_of_rbit_reg(2),
      I3 => \uart_data_reg[6]_i_2_n_0\,
      O => \uart_data_reg[3]_i_2_n_0\
    );
\uart_data_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000008"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[6]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(0),
      I3 => uart_num_of_rbit_reg(1),
      I4 => \uart_data_reg[6]_i_3_n_0\,
      I5 => \^uart_data_rx\(4),
      O => \uart_data_reg[4]_i_1_n_0\
    );
\uart_data_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[6]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(0),
      I3 => uart_num_of_rbit_reg(1),
      I4 => \uart_data_reg[6]_i_3_n_0\,
      I5 => \^uart_data_rx\(5),
      O => \uart_data_reg[5]_i_1_n_0\
    );
\uart_data_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_data_reg[6]_i_2_n_0\,
      I2 => uart_num_of_rbit_reg(1),
      I3 => uart_num_of_rbit_reg(0),
      I4 => \uart_data_reg[6]_i_3_n_0\,
      I5 => \^uart_data_rx\(6),
      O => \uart_data_reg[6]_i_1_n_0\
    );
\uart_data_reg[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00100000"
    )
        port map (
      I0 => \uart_num_of_rbit[3]_i_3_n_0\,
      I1 => uart_cnt(3),
      I2 => uart_cnt(6),
      I3 => uart_cnt(4),
      I4 => uart_cnt(5),
      O => \uart_data_reg[6]_i_2_n_0\
    );
\uart_data_reg[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => is_receiving_going_reg_n_0,
      I1 => uart_num_of_rbit_reg(3),
      I2 => uart_num_of_rbit_reg(2),
      O => \uart_data_reg[6]_i_3_n_0\
    );
\uart_data_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => uart_rx,
      I1 => \uart_num_of_rbit[3]_i_1_n_0\,
      I2 => \uart_data_reg[7]_i_2_n_0\,
      I3 => uart_num_of_rbit_reg(2),
      I4 => uart_num_of_rbit_reg(3),
      I5 => \^uart_data_rx\(7),
      O => \uart_data_reg[7]_i_1_n_0\
    );
\uart_data_reg[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => uart_num_of_rbit_reg(0),
      I1 => uart_num_of_rbit_reg(1),
      O => \uart_data_reg[7]_i_2_n_0\
    );
\uart_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[0]_i_1_n_0\,
      Q => \^uart_data_rx\(0),
      R => '0'
    );
\uart_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[1]_i_1_n_0\,
      Q => \^uart_data_rx\(1),
      R => '0'
    );
\uart_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[2]_i_1_n_0\,
      Q => \^uart_data_rx\(2),
      R => '0'
    );
\uart_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[3]_i_1_n_0\,
      Q => \^uart_data_rx\(3),
      R => '0'
    );
\uart_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[4]_i_1_n_0\,
      Q => \^uart_data_rx\(4),
      R => '0'
    );
\uart_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[5]_i_1_n_0\,
      Q => \^uart_data_rx\(5),
      R => '0'
    );
\uart_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[6]_i_1_n_0\,
      Q => \^uart_data_rx\(6),
      R => '0'
    );
\uart_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \uart_data_reg[7]_i_1_n_0\,
      Q => \^uart_data_rx\(7),
      R => '0'
    );
\uart_num_of_rbit[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0F0B"
    )
        port map (
      I0 => uart_num_of_rbit_reg(2),
      I1 => uart_num_of_rbit_reg(3),
      I2 => uart_num_of_rbit_reg(0),
      I3 => uart_num_of_rbit_reg(1),
      O => p_0_in(0)
    );
\uart_num_of_rbit[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => uart_num_of_rbit_reg(0),
      I1 => uart_num_of_rbit_reg(1),
      O => p_0_in(1)
    );
\uart_num_of_rbit[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => uart_num_of_rbit_reg(2),
      I1 => uart_num_of_rbit_reg(1),
      I2 => uart_num_of_rbit_reg(0),
      O => p_0_in(2)
    );
\uart_num_of_rbit[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => uart_cnt(5),
      I1 => uart_cnt(4),
      I2 => uart_cnt(6),
      I3 => is_receiving_going_reg_n_0,
      I4 => uart_cnt(3),
      I5 => \uart_num_of_rbit[3]_i_3_n_0\,
      O => \uart_num_of_rbit[3]_i_1_n_0\
    );
\uart_num_of_rbit[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7E80"
    )
        port map (
      I0 => uart_num_of_rbit_reg(1),
      I1 => uart_num_of_rbit_reg(0),
      I2 => uart_num_of_rbit_reg(2),
      I3 => uart_num_of_rbit_reg(3),
      O => \uart_num_of_rbit[3]_i_2_n_0\
    );
\uart_num_of_rbit[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => uart_cnt(2),
      I1 => uart_cnt(0),
      I2 => uart_cnt(1),
      O => \uart_num_of_rbit[3]_i_3_n_0\
    );
\uart_num_of_rbit_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(0),
      Q => uart_num_of_rbit_reg(0),
      R => '0'
    );
\uart_num_of_rbit_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(1),
      Q => uart_num_of_rbit_reg(1),
      R => '0'
    );
\uart_num_of_rbit_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => p_0_in(2),
      Q => uart_num_of_rbit_reg(2),
      R => '0'
    );
\uart_num_of_rbit_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \uart_num_of_rbit[3]_i_1_n_0\,
      D => \uart_num_of_rbit[3]_i_2_n_0\,
      Q => uart_num_of_rbit_reg(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send is
  port (
    is_transmission_going_reg_reg_0 : out STD_LOGIC;
    uart_tx : out STD_LOGIC;
    send_byte : in STD_LOGIC;
    clk : in STD_LOGIC;
    uart_data_tx : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send is
  signal bit_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \bit_num[3]_i_2_n_0\ : STD_LOGIC;
  signal bit_num_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal cnt : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \cnt[2]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[2]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[2]_i_4_n_0\ : STD_LOGIC;
  signal \cnt[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[5]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[5]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal cnt_0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal is_transmission_going_reg_i_1_n_0 : STD_LOGIC;
  signal is_transmission_going_reg_i_2_n_0 : STD_LOGIC;
  signal \^is_transmission_going_reg_reg_0\ : STD_LOGIC;
  signal send_byte_prev : STD_LOGIC;
  signal \^uart_tx\ : STD_LOGIC;
  signal uart_tx_reg_i_10_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_11_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_12_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_13_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_14_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_1_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_2_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_3_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_4_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_5_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_6_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_7_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_8_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_9_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bit_num[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \bit_num[2]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cnt[0]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cnt[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cnt[2]_i_3\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \cnt[2]_i_4\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \cnt[4]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cnt[5]_i_3\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cnt[6]_i_3\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \cnt[6]_i_4\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of uart_tx_reg_i_10 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of uart_tx_reg_i_11 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of uart_tx_reg_i_12 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of uart_tx_reg_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of uart_tx_reg_i_3 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of uart_tx_reg_i_9 : label is "soft_lutpair12";
begin
  is_transmission_going_reg_reg_0 <= \^is_transmission_going_reg_reg_0\;
  uart_tx <= \^uart_tx\;
\bit_num[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99999959"
    )
        port map (
      I0 => \cnt[5]_i_3_n_0\,
      I1 => bit_num_reg(0),
      I2 => send_byte,
      I3 => send_byte_prev,
      I4 => \^is_transmission_going_reg_reg_0\,
      O => bit_num(0)
    );
\bit_num[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8AA5455A8AAA8AA"
    )
        port map (
      I0 => bit_num_reg(1),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => \cnt[5]_i_3_n_0\,
      I5 => bit_num_reg(0),
      O => bit_num(1)
    );
\bit_num[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5559AAAA"
    )
        port map (
      I0 => \bit_num[3]_i_2_n_0\,
      I1 => send_byte,
      I2 => send_byte_prev,
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => bit_num_reg(2),
      O => bit_num(2)
    );
\bit_num[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7777770788888808"
    )
        port map (
      I0 => bit_num_reg(2),
      I1 => \bit_num[3]_i_2_n_0\,
      I2 => send_byte,
      I3 => send_byte_prev,
      I4 => \^is_transmission_going_reg_reg_0\,
      I5 => bit_num_reg(3),
      O => bit_num(3)
    );
\bit_num[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000A8AA0000"
    )
        port map (
      I0 => bit_num_reg(1),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => bit_num_reg(0),
      I5 => \cnt[5]_i_3_n_0\,
      O => \bit_num[3]_i_2_n_0\
    );
\bit_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(0),
      Q => bit_num_reg(0),
      R => '0'
    );
\bit_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(1),
      Q => bit_num_reg(1),
      R => '0'
    );
\bit_num_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(2),
      Q => bit_num_reg(2),
      R => '0'
    );
\bit_num_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(3),
      Q => bit_num_reg(3),
      R => '0'
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6466"
    )
        port map (
      I0 => cnt(0),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => cnt_0(0)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6C606C6C"
    )
        port map (
      I0 => cnt(0),
      I1 => cnt(1),
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => send_byte_prev,
      I4 => send_byte,
      O => cnt_0(1)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0220202020202020"
    )
        port map (
      I0 => \cnt[2]_i_2_n_0\,
      I1 => \cnt[6]_i_3_n_0\,
      I2 => cnt(2),
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => cnt(0),
      I5 => cnt(1),
      O => cnt_0(2)
    );
\cnt[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFDFF7FF"
    )
        port map (
      I0 => cnt(6),
      I1 => cnt(5),
      I2 => cnt(4),
      I3 => \cnt[2]_i_3_n_0\,
      I4 => cnt(3),
      I5 => \cnt[2]_i_4_n_0\,
      O => \cnt[2]_i_2_n_0\
    );
\cnt[2]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => cnt(0),
      I2 => cnt(1),
      I3 => cnt(2),
      O => \cnt[2]_i_3_n_0\
    );
\cnt[2]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7E"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => cnt(0),
      I2 => cnt(1),
      O => \cnt[2]_i_4_n_0\
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFF800000000000"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => cnt(0),
      I2 => cnt(1),
      I3 => cnt(2),
      I4 => cnt(3),
      I5 => uart_tx_reg_i_2_n_0,
      O => cnt_0(3)
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => \cnt[4]_i_2_n_0\,
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => cnt_0(4)
    );
\cnt[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => cnt(4),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => cnt(0),
      I3 => cnt(1),
      I4 => cnt(2),
      I5 => cnt(3),
      O => \cnt[4]_i_2_n_0\
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999990900000000"
    )
        port map (
      I0 => cnt(5),
      I1 => \cnt[5]_i_2_n_0\,
      I2 => send_byte,
      I3 => send_byte_prev,
      I4 => \^is_transmission_going_reg_reg_0\,
      I5 => \cnt[5]_i_3_n_0\,
      O => cnt_0(5)
    );
\cnt[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt(4),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => cnt(0),
      I3 => cnt(1),
      I4 => cnt(2),
      I5 => cnt(3),
      O => \cnt[5]_i_2_n_0\
    );
\cnt[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBDFF"
    )
        port map (
      I0 => cnt(5),
      I1 => \cnt[6]_i_4_n_0\,
      I2 => cnt(4),
      I3 => cnt(6),
      I4 => uart_tx_reg_i_12_n_0,
      O => \cnt[5]_i_3_n_0\
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0320303030302030"
    )
        port map (
      I0 => \cnt[6]_i_2_n_0\,
      I1 => \cnt[6]_i_3_n_0\,
      I2 => cnt(6),
      I3 => cnt(5),
      I4 => cnt(4),
      I5 => \cnt[6]_i_4_n_0\,
      O => cnt_0(6)
    );
\cnt[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFD7FFFFFFC3FFF"
    )
        port map (
      I0 => cnt(4),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => cnt(0),
      I3 => cnt(1),
      I4 => cnt(2),
      I5 => cnt(3),
      O => \cnt[6]_i_2_n_0\
    );
\cnt[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => send_byte,
      I1 => send_byte_prev,
      I2 => \^is_transmission_going_reg_reg_0\,
      O => \cnt[6]_i_3_n_0\
    );
\cnt[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => cnt(3),
      I1 => cnt(2),
      I2 => cnt(1),
      I3 => cnt(0),
      I4 => \^is_transmission_going_reg_reg_0\,
      O => \cnt[6]_i_4_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(0),
      Q => cnt(0),
      R => '0'
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(1),
      Q => cnt(1),
      R => '0'
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(2),
      Q => cnt(2),
      R => '0'
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(3),
      Q => cnt(3),
      R => '0'
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(4),
      Q => cnt(4),
      R => '0'
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(5),
      Q => cnt(5),
      R => '0'
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(6),
      Q => cnt(6),
      R => '0'
    );
is_transmission_going_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00BA"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => send_byte_prev,
      I2 => send_byte,
      I3 => is_transmission_going_reg_i_2_n_0,
      O => is_transmission_going_reg_i_1_n_0
    );
is_transmission_going_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000040000"
    )
        port map (
      I0 => \cnt[6]_i_3_n_0\,
      I1 => bit_num_reg(3),
      I2 => bit_num_reg(2),
      I3 => bit_num_reg(1),
      I4 => bit_num_reg(0),
      I5 => \cnt[5]_i_3_n_0\,
      O => is_transmission_going_reg_i_2_n_0
    );
is_transmission_going_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => is_transmission_going_reg_i_1_n_0,
      Q => \^is_transmission_going_reg_reg_0\,
      R => '0'
    );
send_byte_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => send_byte,
      Q => send_byte_prev,
      R => '0'
    );
uart_tx_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFA8ABA8A8"
    )
        port map (
      I0 => \^uart_tx\,
      I1 => uart_tx_reg_i_2_n_0,
      I2 => uart_tx_reg_i_3_n_0,
      I3 => uart_tx_reg_i_4_n_0,
      I4 => uart_tx_reg_i_5_n_0,
      I5 => uart_tx_reg_i_6_n_0,
      O => uart_tx_reg_i_1_n_0
    );
uart_tx_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => bit_num_reg(2),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => uart_tx_reg_i_10_n_0
    );
uart_tx_reg_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BD"
    )
        port map (
      I0 => cnt(5),
      I1 => \cnt[6]_i_4_n_0\,
      I2 => cnt(4),
      O => uart_tx_reg_i_11_n_0
    );
uart_tx_reg_i_12: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEBFFF"
    )
        port map (
      I0 => cnt(3),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => cnt(0),
      I3 => cnt(1),
      I4 => cnt(2),
      O => uart_tx_reg_i_12_n_0
    );
uart_tx_reg_i_13: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0303FF47"
    )
        port map (
      I0 => uart_data_tx(3),
      I1 => bit_num_reg(0),
      I2 => uart_data_tx(2),
      I3 => bit_num_reg(2),
      I4 => \cnt[6]_i_3_n_0\,
      O => uart_tx_reg_i_13_n_0
    );
uart_tx_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFDDDFDFDDDDDDD"
    )
        port map (
      I0 => bit_num_reg(1),
      I1 => \cnt[6]_i_3_n_0\,
      I2 => bit_num_reg(2),
      I3 => bit_num_reg(0),
      I4 => uart_data_tx(7),
      I5 => uart_data_tx(6),
      O => uart_tx_reg_i_14_n_0
    );
uart_tx_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => \cnt[5]_i_3_n_0\,
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => uart_tx_reg_i_2_n_0
    );
uart_tx_reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => bit_num_reg(3),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => uart_tx_reg_i_3_n_0
    );
uart_tx_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABBABABBBBBBBBB"
    )
        port map (
      I0 => uart_tx_reg_i_7_n_0,
      I1 => uart_tx_reg_i_8_n_0,
      I2 => uart_data_tx(5),
      I3 => uart_data_tx(4),
      I4 => uart_tx_reg_i_9_n_0,
      I5 => uart_tx_reg_i_10_n_0,
      O => uart_tx_reg_i_4_n_0
    );
uart_tx_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011101100001011"
    )
        port map (
      I0 => uart_tx_reg_i_11_n_0,
      I1 => uart_tx_reg_i_12_n_0,
      I2 => \cnt[6]_i_3_n_0\,
      I3 => bit_num_reg(3),
      I4 => uart_tx_reg_i_13_n_0,
      I5 => uart_tx_reg_i_14_n_0,
      O => uart_tx_reg_i_5_n_0
    );
uart_tx_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => \cnt[6]_i_3_n_0\,
      I1 => bit_num_reg(3),
      I2 => bit_num_reg(2),
      I3 => bit_num_reg(1),
      I4 => bit_num_reg(0),
      I5 => \cnt[5]_i_3_n_0\,
      O => uart_tx_reg_i_6_n_0
    );
uart_tx_reg_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"59"
    )
        port map (
      I0 => cnt(6),
      I1 => cnt(5),
      I2 => \cnt[5]_i_2_n_0\,
      O => uart_tx_reg_i_7_n_0
    );
uart_tx_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0BBF0BAF0AAF0BA"
    )
        port map (
      I0 => bit_num_reg(1),
      I1 => bit_num_reg(2),
      I2 => uart_data_tx(0),
      I3 => \cnt[6]_i_3_n_0\,
      I4 => bit_num_reg(0),
      I5 => uart_data_tx(1),
      O => uart_tx_reg_i_8_n_0
    );
uart_tx_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"10FF"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => send_byte_prev,
      I2 => send_byte,
      I3 => bit_num_reg(0),
      O => uart_tx_reg_i_9_n_0
    );
uart_tx_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => uart_tx_reg_i_1_n_0,
      Q => \^uart_tx\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart is
  port (
    is_transmission_going_reg_reg : out STD_LOGIC;
    uart_data_rx : out STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_tx : out STD_LOGIC;
    byte_received : out STD_LOGIC;
    send_byte : in STD_LOGIC;
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC;
    uart_data_tx : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart is
begin
uart_receive_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_receive
     port map (
      byte_received => byte_received,
      clk => clk,
      uart_data_rx(7 downto 0) => uart_data_rx(7 downto 0),
      uart_rx => uart_rx
    );
uart_send_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send
     port map (
      clk => clk,
      is_transmission_going_reg_reg_0 => is_transmission_going_reg_reg,
      send_byte => send_byte,
      uart_data_tx(7 downto 0) => uart_data_tx(7 downto 0),
      uart_tx => uart_tx
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC;
    uart_data_tx : in STD_LOGIC_VECTOR ( 7 downto 0 );
    byte_received : out STD_LOGIC;
    uart_data_rx : out STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_tx : out STD_LOGIC;
    is_transmission_going : out STD_LOGIC;
    send_byte : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_uart_0_0,uart,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "uart,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart
     port map (
      byte_received => byte_received,
      clk => clk,
      is_transmission_going_reg_reg => is_transmission_going,
      send_byte => send_byte,
      uart_data_rx(7 downto 0) => uart_data_rx(7 downto 0),
      uart_data_tx(7 downto 0) => uart_data_tx(7 downto 0),
      uart_rx => uart_rx,
      uart_tx => uart_tx
    );
end STRUCTURE;

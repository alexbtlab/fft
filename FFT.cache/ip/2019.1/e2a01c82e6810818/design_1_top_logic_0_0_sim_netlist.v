// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun  3 15:15:20 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_sim_netlist.v
// Design      : design_1_top_logic_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire clk_out4;
  wire locked;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .clk_out4(clk_out4),
        .locked(locked));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_in1_clk_wiz_0;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clk_out4;
  wire clk_out4_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire locked;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout4_buf
       (.I(clk_out4_clk_wiz_0),
        .O(clk_out4));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(10.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(50),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(100),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(125),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(clk_out3_clk_wiz_0),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(clk_out4_clk_wiz_0),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_top_logic_0_0,top_logic,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "top_logic,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (in_uart_data,
    clk_100MHz_in,
    in_adc_data,
    uart_adc_start_sending,
    pll_data_ready,
    uart_byte_received,
    pll_read_data,
    out_data_uart,
    t_sweep,
    clk_100MHz_out,
    user_led,
    pamp_en,
    sw_ls,
    if_amp1_stu_en,
    if_amp2_stu_en,
    sw_if1,
    sw_if2,
    sw_if3,
    clk_sync,
    pll_trig,
    sw_ctrl,
    atten,
    start_uart_send,
    is_adc_data_sending,
    clk_10MHz_out,
    start_init_clk,
    clk_20MHz_out,
    pll_read,
    pll_write,
    pll_write_data,
    pll_write_addres,
    pll_read_addres);
  input [7:0]in_uart_data;
  input clk_100MHz_in;
  input [7:0]in_adc_data;
  input uart_adc_start_sending;
  input pll_data_ready;
  input uart_byte_received;
  input [23:0]pll_read_data;
  output [7:0]out_data_uart;
  output t_sweep;
  output clk_100MHz_out;
  output user_led;
  output pamp_en;
  output sw_ls;
  output if_amp1_stu_en;
  output if_amp2_stu_en;
  output sw_if1;
  output sw_if2;
  output sw_if3;
  output clk_sync;
  output pll_trig;
  output sw_ctrl;
  output [5:0]atten;
  output start_uart_send;
  output is_adc_data_sending;
  output clk_10MHz_out;
  output start_init_clk;
  output clk_20MHz_out;
  output pll_read;
  output pll_write;
  output [23:0]pll_write_data;
  output [4:0]pll_write_addres;
  output [7:0]pll_read_addres;

  wire \<const0> ;
  wire \<const1> ;
  wire [5:0]atten;
  (* IBUF_LOW_PWR *) wire clk_100MHz_in;
  wire clk_100MHz_out;
  wire clk_10MHz_out;
  wire clk_20MHz_out;
  wire if_amp1_stu_en;
  wire if_amp2_stu_en;
  wire [7:0]in_uart_data;
  wire [7:0]out_data_uart;
  wire pamp_en;
  wire pll_data_ready;
  wire pll_read;
  wire [7:0]pll_read_addres;
  wire [23:0]pll_read_data;
  wire pll_trig;
  wire pll_write;
  wire [4:0]pll_write_addres;
  wire [23:0]pll_write_data;
  wire start_init_clk;
  wire start_uart_send;
  wire sw_ctrl;
  wire sw_if1;
  wire sw_if2;
  wire sw_if3;
  wire t_sweep;
  wire uart_adc_start_sending;
  wire uart_byte_received;

  assign clk_sync = start_init_clk;
  assign is_adc_data_sending = \<const0> ;
  assign sw_ls = \<const1> ;
  assign user_led = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic inst
       (.atten(atten),
        .clk_100MHz_in(clk_100MHz_in),
        .clk_100MHz_out(clk_100MHz_out),
        .clk_10MHz_out(clk_10MHz_out),
        .clk_20MHz_out(clk_20MHz_out),
        .if_amp1_stu_en(if_amp1_stu_en),
        .if_amp2_stu_en(if_amp2_stu_en),
        .in_uart_data(in_uart_data),
        .out_data_uart(out_data_uart),
        .pamp_en(pamp_en),
        .pll_data_ready(pll_data_ready),
        .pll_read(pll_read),
        .pll_read_addres(pll_read_addres),
        .pll_read_data(pll_read_data),
        .pll_trig(pll_trig),
        .pll_write(pll_write),
        .pll_write_addres(pll_write_addres),
        .pll_write_data(pll_write_data),
        .start_init_clk(start_init_clk),
        .start_uart_send(start_uart_send),
        .sw_ctrl(sw_ctrl),
        .sw_if1(sw_if1),
        .sw_if2(sw_if2),
        .sw_if3(sw_if3),
        .t_sweep(t_sweep),
        .uart_adc_start_sending(uart_adc_start_sending),
        .uart_byte_received(uart_byte_received));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic
   (clk_100MHz_out,
    clk_20MHz_out,
    clk_10MHz_out,
    out_data_uart,
    atten,
    pll_write_data,
    pll_write_addres,
    pll_read_addres,
    t_sweep,
    start_init_clk,
    pll_write,
    sw_if1,
    sw_if2,
    pamp_en,
    if_amp2_stu_en,
    if_amp1_stu_en,
    sw_ctrl,
    sw_if3,
    start_uart_send,
    pll_trig,
    pll_read,
    clk_100MHz_in,
    pll_data_ready,
    uart_byte_received,
    in_uart_data,
    pll_read_data,
    uart_adc_start_sending);
  output clk_100MHz_out;
  output clk_20MHz_out;
  output clk_10MHz_out;
  output [7:0]out_data_uart;
  output [5:0]atten;
  output [23:0]pll_write_data;
  output [4:0]pll_write_addres;
  output [7:0]pll_read_addres;
  output t_sweep;
  output start_init_clk;
  output pll_write;
  output sw_if1;
  output sw_if2;
  output pamp_en;
  output if_amp2_stu_en;
  output if_amp1_stu_en;
  output sw_ctrl;
  output sw_if3;
  output start_uart_send;
  output pll_trig;
  output pll_read;
  input clk_100MHz_in;
  input pll_data_ready;
  input uart_byte_received;
  input [7:0]in_uart_data;
  input [23:0]pll_read_data;
  input uart_adc_start_sending;

  wire [5:0]atten;
  wire atten_reg;
  wire clk_100MHz_in;
  wire clk_100MHz_out;
  wire clk_10MHz_out;
  wire clk_20MHz_out;
  wire clk_sync_counter_reg;
  wire [31:1]clk_sync_counter_reg0;
  wire clk_sync_counter_reg0_carry__0_n_0;
  wire clk_sync_counter_reg0_carry__0_n_1;
  wire clk_sync_counter_reg0_carry__0_n_2;
  wire clk_sync_counter_reg0_carry__0_n_3;
  wire clk_sync_counter_reg0_carry__1_n_0;
  wire clk_sync_counter_reg0_carry__1_n_1;
  wire clk_sync_counter_reg0_carry__1_n_2;
  wire clk_sync_counter_reg0_carry__1_n_3;
  wire clk_sync_counter_reg0_carry__2_n_0;
  wire clk_sync_counter_reg0_carry__2_n_1;
  wire clk_sync_counter_reg0_carry__2_n_2;
  wire clk_sync_counter_reg0_carry__2_n_3;
  wire clk_sync_counter_reg0_carry__3_n_0;
  wire clk_sync_counter_reg0_carry__3_n_1;
  wire clk_sync_counter_reg0_carry__3_n_2;
  wire clk_sync_counter_reg0_carry__3_n_3;
  wire clk_sync_counter_reg0_carry__4_n_0;
  wire clk_sync_counter_reg0_carry__4_n_1;
  wire clk_sync_counter_reg0_carry__4_n_2;
  wire clk_sync_counter_reg0_carry__4_n_3;
  wire clk_sync_counter_reg0_carry__5_n_0;
  wire clk_sync_counter_reg0_carry__5_n_1;
  wire clk_sync_counter_reg0_carry__5_n_2;
  wire clk_sync_counter_reg0_carry__5_n_3;
  wire clk_sync_counter_reg0_carry__6_n_2;
  wire clk_sync_counter_reg0_carry__6_n_3;
  wire clk_sync_counter_reg0_carry_n_0;
  wire clk_sync_counter_reg0_carry_n_1;
  wire clk_sync_counter_reg0_carry_n_2;
  wire clk_sync_counter_reg0_carry_n_3;
  wire \clk_sync_counter_reg[0]_i_10_n_0 ;
  wire \clk_sync_counter_reg[0]_i_11_n_0 ;
  wire \clk_sync_counter_reg[0]_i_12_n_0 ;
  wire \clk_sync_counter_reg[0]_i_13_n_0 ;
  wire \clk_sync_counter_reg[0]_i_14_n_0 ;
  wire \clk_sync_counter_reg[0]_i_3_n_0 ;
  wire \clk_sync_counter_reg[0]_i_4_n_0 ;
  wire \clk_sync_counter_reg[0]_i_5_n_0 ;
  wire \clk_sync_counter_reg[0]_i_6_n_0 ;
  wire \clk_sync_counter_reg[0]_i_7_n_0 ;
  wire \clk_sync_counter_reg[0]_i_8_n_0 ;
  wire \clk_sync_counter_reg[0]_i_9_n_0 ;
  wire \clk_sync_counter_reg[12]_i_2_n_0 ;
  wire \clk_sync_counter_reg[12]_i_3_n_0 ;
  wire \clk_sync_counter_reg[12]_i_4_n_0 ;
  wire \clk_sync_counter_reg[12]_i_5_n_0 ;
  wire \clk_sync_counter_reg[16]_i_2_n_0 ;
  wire \clk_sync_counter_reg[16]_i_3_n_0 ;
  wire \clk_sync_counter_reg[16]_i_4_n_0 ;
  wire \clk_sync_counter_reg[16]_i_5_n_0 ;
  wire \clk_sync_counter_reg[16]_i_6_n_0 ;
  wire \clk_sync_counter_reg[16]_i_7_n_0 ;
  wire \clk_sync_counter_reg[16]_i_8_n_0 ;
  wire \clk_sync_counter_reg[16]_i_9_n_0 ;
  wire \clk_sync_counter_reg[20]_i_2_n_0 ;
  wire \clk_sync_counter_reg[20]_i_3_n_0 ;
  wire \clk_sync_counter_reg[20]_i_4_n_0 ;
  wire \clk_sync_counter_reg[20]_i_5_n_0 ;
  wire \clk_sync_counter_reg[24]_i_2_n_0 ;
  wire \clk_sync_counter_reg[24]_i_3_n_0 ;
  wire \clk_sync_counter_reg[24]_i_4_n_0 ;
  wire \clk_sync_counter_reg[24]_i_5_n_0 ;
  wire \clk_sync_counter_reg[28]_i_2_n_0 ;
  wire \clk_sync_counter_reg[28]_i_3_n_0 ;
  wire \clk_sync_counter_reg[28]_i_4_n_0 ;
  wire \clk_sync_counter_reg[28]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_2_n_0 ;
  wire \clk_sync_counter_reg[4]_i_3_n_0 ;
  wire \clk_sync_counter_reg[4]_i_4_n_0 ;
  wire \clk_sync_counter_reg[4]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_6_n_0 ;
  wire \clk_sync_counter_reg[4]_i_7_n_0 ;
  wire \clk_sync_counter_reg[8]_i_2_n_0 ;
  wire \clk_sync_counter_reg[8]_i_3_n_0 ;
  wire \clk_sync_counter_reg[8]_i_4_n_0 ;
  wire \clk_sync_counter_reg[8]_i_5_n_0 ;
  wire [31:0]clk_sync_counter_reg_reg;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_0 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_1 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_2 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_3 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_4 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_5 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_6 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_7 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_7 ;
  wire clk_wizard_main_n_1;
  wire clk_wizard_main_n_4;
  wire [3:0]cnt_uart;
  wire \cnt_uart[0]_i_1_n_0 ;
  wire \cnt_uart[1]_i_1_n_0 ;
  wire \cnt_uart[2]_i_1_n_0 ;
  wire \cnt_uart[3]_i_1_n_0 ;
  wire \cnt_uart[3]_i_2_n_0 ;
  wire if_amp1_en_reg_i_1_n_0;
  wire if_amp1_en_reg_i_2_n_0;
  wire if_amp1_stu_en;
  wire if_amp2_en_reg_i_1_n_0;
  wire if_amp2_stu_en;
  wire [7:0]in_uart_data;
  wire is_clk_initialized_reg;
  wire is_clk_initialized_reg_i_1_n_0;
  wire is_counting;
  wire is_counting_i_1_n_0;
  wire is_uart_receiving_atten_value_i_1_n_0;
  wire is_uart_receiving_atten_value_reg_n_0;
  wire is_uart_receiving_pll_data_i_1_n_0;
  wire is_uart_receiving_pll_data_i_2_n_0;
  wire is_uart_receiving_pll_data_reg_n_0;
  wire is_uart_receiving_pll_read_address;
  wire is_uart_receiving_pll_read_address_i_1_n_0;
  wire is_uart_receiving_pll_write_address;
  wire is_uart_receiving_pll_write_address67_out;
  wire is_uart_receiving_pll_write_address_i_1_n_0;
  wire is_uart_receiving_pll_write_address_i_2_n_0;
  wire [7:0]out_data_uart;
  wire [7:0]p_1_in;
  wire [16:1]p_2_in;
  wire pamp_en;
  wire pamp_en_reg_i_1_n_0;
  wire pamp_en_reg_i_2_n_0;
  wire pamp_en_reg_i_3_n_0;
  wire pamp_en_reg_i_4_n_0;
  wire pamp_en_reg_i_5_n_0;
  wire pll_address_reg;
  wire pll_data_ready;
  wire pll_data_ready_prev;
  wire pll_read;
  wire [7:0]pll_read_addres;
  wire [23:0]pll_read_data;
  wire pll_read_i_1_n_0;
  wire pll_read_i_2_n_0;
  wire pll_trig;
  wire pll_trig_reg_i_1_n_0;
  wire pll_write;
  wire [4:0]pll_write_addres;
  wire pll_write_address_reg;
  wire [23:0]pll_write_data;
  wire \pll_write_data[0]_i_1_n_0 ;
  wire \pll_write_data[10]_i_1_n_0 ;
  wire \pll_write_data[11]_i_1_n_0 ;
  wire \pll_write_data[12]_i_1_n_0 ;
  wire \pll_write_data[13]_i_1_n_0 ;
  wire \pll_write_data[14]_i_1_n_0 ;
  wire \pll_write_data[15]_i_1_n_0 ;
  wire \pll_write_data[16]_i_1_n_0 ;
  wire \pll_write_data[17]_i_1_n_0 ;
  wire \pll_write_data[18]_i_1_n_0 ;
  wire \pll_write_data[19]_i_1_n_0 ;
  wire \pll_write_data[1]_i_1_n_0 ;
  wire \pll_write_data[20]_i_1_n_0 ;
  wire \pll_write_data[21]_i_1_n_0 ;
  wire \pll_write_data[22]_i_1_n_0 ;
  wire \pll_write_data[23]_i_2_n_0 ;
  wire \pll_write_data[23]_i_3_n_0 ;
  wire \pll_write_data[23]_i_4_n_0 ;
  wire \pll_write_data[2]_i_1_n_0 ;
  wire \pll_write_data[3]_i_1_n_0 ;
  wire \pll_write_data[4]_i_1_n_0 ;
  wire \pll_write_data[5]_i_1_n_0 ;
  wire \pll_write_data[6]_i_1_n_0 ;
  wire \pll_write_data[7]_i_1_n_0 ;
  wire \pll_write_data[8]_i_1_n_0 ;
  wire \pll_write_data[9]_i_1_n_0 ;
  wire pll_write_i_1_n_0;
  wire pll_write_i_2_n_0;
  wire pll_write_i_3_n_0;
  wire \received_byte_num[0]_i_1_n_0 ;
  wire \received_byte_num[0]_i_2_n_0 ;
  wire \received_byte_num[0]_i_3_n_0 ;
  wire \received_byte_num[0]_i_4_n_0 ;
  wire \received_byte_num[0]_i_5_n_0 ;
  wire \received_byte_num[0]_i_6_n_0 ;
  wire \received_byte_num[1]_i_1_n_0 ;
  wire \received_byte_num[1]_i_2_n_0 ;
  wire \received_byte_num_reg_n_0_[0] ;
  wire \received_byte_num_reg_n_0_[1] ;
  wire start_init_clk;
  wire start_init_clk_reg_i_10_n_0;
  wire start_init_clk_reg_i_11_n_0;
  wire start_init_clk_reg_i_12_n_0;
  wire start_init_clk_reg_i_13_n_0;
  wire start_init_clk_reg_i_14_n_0;
  wire start_init_clk_reg_i_15_n_0;
  wire start_init_clk_reg_i_16_n_0;
  wire start_init_clk_reg_i_17_n_0;
  wire start_init_clk_reg_i_18_n_0;
  wire start_init_clk_reg_i_1_n_0;
  wire start_init_clk_reg_i_2_n_0;
  wire start_init_clk_reg_i_3_n_0;
  wire start_init_clk_reg_i_4_n_0;
  wire start_init_clk_reg_i_5_n_0;
  wire start_init_clk_reg_i_6_n_0;
  wire start_init_clk_reg_i_7_n_0;
  wire start_init_clk_reg_i_8_n_0;
  wire start_init_clk_reg_i_9_n_0;
  wire start_uart_send;
  wire sw_ctrl;
  wire sw_ctrl_reg_i_1_n_0;
  wire sw_ctrl_reg_i_2_n_0;
  wire sw_if1;
  wire sw_if1_reg_i_1_n_0;
  wire sw_if2;
  wire sw_if2_reg_i_1_n_0;
  wire sw_if2_reg_i_2_n_0;
  wire sw_if3;
  wire sw_if3_reg_i_1_n_0;
  wire sw_if3_reg_i_2_n_0;
  wire t_sweep;
  wire t_sweep_INST_0_i_1_n_0;
  wire t_sweep_INST_0_i_2_n_0;
  wire t_sweep_INST_0_i_3_n_0;
  wire t_sweep_INST_0_i_4_n_0;
  wire trig_tguard_counter;
  wire \trig_tguard_counter[0]_i_1_n_0 ;
  wire \trig_tguard_counter[0]_i_2_n_0 ;
  wire \trig_tguard_counter[0]_i_3_n_0 ;
  wire \trig_tguard_counter[16]_i_10_n_0 ;
  wire \trig_tguard_counter[16]_i_11_n_0 ;
  wire \trig_tguard_counter[16]_i_12_n_0 ;
  wire \trig_tguard_counter[16]_i_13_n_0 ;
  wire \trig_tguard_counter[16]_i_1_n_0 ;
  wire \trig_tguard_counter[16]_i_4_n_0 ;
  wire \trig_tguard_counter[16]_i_5_n_0 ;
  wire \trig_tguard_counter[16]_i_6_n_0 ;
  wire \trig_tguard_counter[16]_i_7_n_0 ;
  wire \trig_tguard_counter[16]_i_8_n_0 ;
  wire \trig_tguard_counter[16]_i_9_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_1 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_2 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_3 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_3 ;
  wire \trig_tguard_counter_reg_n_0_[0] ;
  wire \trig_tguard_counter_reg_n_0_[10] ;
  wire \trig_tguard_counter_reg_n_0_[11] ;
  wire \trig_tguard_counter_reg_n_0_[12] ;
  wire \trig_tguard_counter_reg_n_0_[13] ;
  wire \trig_tguard_counter_reg_n_0_[14] ;
  wire \trig_tguard_counter_reg_n_0_[15] ;
  wire \trig_tguard_counter_reg_n_0_[16] ;
  wire \trig_tguard_counter_reg_n_0_[1] ;
  wire \trig_tguard_counter_reg_n_0_[2] ;
  wire \trig_tguard_counter_reg_n_0_[3] ;
  wire \trig_tguard_counter_reg_n_0_[4] ;
  wire \trig_tguard_counter_reg_n_0_[5] ;
  wire \trig_tguard_counter_reg_n_0_[6] ;
  wire \trig_tguard_counter_reg_n_0_[7] ;
  wire \trig_tguard_counter_reg_n_0_[8] ;
  wire \trig_tguard_counter_reg_n_0_[9] ;
  wire trig_tsweep_counter074_out;
  wire \trig_tsweep_counter[0]_i_1_n_0 ;
  wire \trig_tsweep_counter[0]_i_4_n_0 ;
  wire \trig_tsweep_counter[0]_i_5_n_0 ;
  wire \trig_tsweep_counter[0]_i_6_n_0 ;
  wire \trig_tsweep_counter[0]_i_7_n_0 ;
  wire \trig_tsweep_counter[0]_i_8_n_0 ;
  wire [16:0]trig_tsweep_counter_reg;
  wire \trig_tsweep_counter_reg[0]_i_3_n_0 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_1 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_2 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_3 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_4 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_5 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_6 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_7 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[16]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_7 ;
  wire uart_adc_start_sending;
  wire uart_byte_received;
  wire uart_byte_received_prev;
  wire uart_byte_received_prev0;
  wire uart_start_sending_i_1_n_0;
  wire uart_start_sending_i_2_n_0;
  wire uart_start_sending_i_3_n_0;
  wire uart_start_sending_reg_n_0;
  wire \uart_tx_data[0]_i_10_n_0 ;
  wire \uart_tx_data[0]_i_2_n_0 ;
  wire \uart_tx_data[0]_i_3_n_0 ;
  wire \uart_tx_data[0]_i_4_n_0 ;
  wire \uart_tx_data[0]_i_5_n_0 ;
  wire \uart_tx_data[0]_i_6_n_0 ;
  wire \uart_tx_data[0]_i_7_n_0 ;
  wire \uart_tx_data[0]_i_8_n_0 ;
  wire \uart_tx_data[0]_i_9_n_0 ;
  wire \uart_tx_data[1]_i_2_n_0 ;
  wire \uart_tx_data[1]_i_3_n_0 ;
  wire \uart_tx_data[1]_i_4_n_0 ;
  wire \uart_tx_data[1]_i_5_n_0 ;
  wire \uart_tx_data[1]_i_6_n_0 ;
  wire \uart_tx_data[1]_i_7_n_0 ;
  wire \uart_tx_data[1]_i_8_n_0 ;
  wire \uart_tx_data[1]_i_9_n_0 ;
  wire \uart_tx_data[2]_i_2_n_0 ;
  wire \uart_tx_data[2]_i_3_n_0 ;
  wire \uart_tx_data[2]_i_4_n_0 ;
  wire \uart_tx_data[2]_i_5_n_0 ;
  wire \uart_tx_data[2]_i_6_n_0 ;
  wire \uart_tx_data[2]_i_7_n_0 ;
  wire \uart_tx_data[3]_i_2_n_0 ;
  wire \uart_tx_data[4]_i_2_n_0 ;
  wire \uart_tx_data[4]_i_3_n_0 ;
  wire \uart_tx_data[4]_i_4_n_0 ;
  wire \uart_tx_data[4]_i_5_n_0 ;
  wire \uart_tx_data[4]_i_6_n_0 ;
  wire \uart_tx_data[7]_i_1_n_0 ;
  wire \uart_tx_data[7]_i_4_n_0 ;
  wire \uart_tx_data[7]_i_5_n_0 ;
  wire \uart_tx_data[7]_i_6_n_0 ;
  wire \uart_tx_data[7]_i_7_n_0 ;
  wire \uart_tx_data[7]_i_8_n_0 ;
  wire \uart_tx_data[7]_i_9_n_0 ;
  wire [3:2]NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED;
  wire [3:3]\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h08080800)) 
    \atten_reg[5]_i_1 
       (.I0(is_uart_receiving_atten_value_reg_n_0),
        .I1(uart_byte_received),
        .I2(uart_byte_received_prev),
        .I3(\received_byte_num_reg_n_0_[1] ),
        .I4(\received_byte_num_reg_n_0_[0] ),
        .O(atten_reg));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[0]),
        .Q(atten[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[1]),
        .Q(atten[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[2]),
        .Q(atten[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[3]),
        .Q(atten[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[4]),
        .Q(atten[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(atten_reg),
        .D(in_uart_data[5]),
        .Q(atten[5]),
        .R(1'b0));
  CARRY4 clk_sync_counter_reg0_carry
       (.CI(1'b0),
        .CO({clk_sync_counter_reg0_carry_n_0,clk_sync_counter_reg0_carry_n_1,clk_sync_counter_reg0_carry_n_2,clk_sync_counter_reg0_carry_n_3}),
        .CYINIT(clk_sync_counter_reg_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[4:1]),
        .S(clk_sync_counter_reg_reg[4:1]));
  CARRY4 clk_sync_counter_reg0_carry__0
       (.CI(clk_sync_counter_reg0_carry_n_0),
        .CO({clk_sync_counter_reg0_carry__0_n_0,clk_sync_counter_reg0_carry__0_n_1,clk_sync_counter_reg0_carry__0_n_2,clk_sync_counter_reg0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[8:5]),
        .S(clk_sync_counter_reg_reg[8:5]));
  CARRY4 clk_sync_counter_reg0_carry__1
       (.CI(clk_sync_counter_reg0_carry__0_n_0),
        .CO({clk_sync_counter_reg0_carry__1_n_0,clk_sync_counter_reg0_carry__1_n_1,clk_sync_counter_reg0_carry__1_n_2,clk_sync_counter_reg0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[12:9]),
        .S(clk_sync_counter_reg_reg[12:9]));
  CARRY4 clk_sync_counter_reg0_carry__2
       (.CI(clk_sync_counter_reg0_carry__1_n_0),
        .CO({clk_sync_counter_reg0_carry__2_n_0,clk_sync_counter_reg0_carry__2_n_1,clk_sync_counter_reg0_carry__2_n_2,clk_sync_counter_reg0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[16:13]),
        .S(clk_sync_counter_reg_reg[16:13]));
  CARRY4 clk_sync_counter_reg0_carry__3
       (.CI(clk_sync_counter_reg0_carry__2_n_0),
        .CO({clk_sync_counter_reg0_carry__3_n_0,clk_sync_counter_reg0_carry__3_n_1,clk_sync_counter_reg0_carry__3_n_2,clk_sync_counter_reg0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[20:17]),
        .S(clk_sync_counter_reg_reg[20:17]));
  CARRY4 clk_sync_counter_reg0_carry__4
       (.CI(clk_sync_counter_reg0_carry__3_n_0),
        .CO({clk_sync_counter_reg0_carry__4_n_0,clk_sync_counter_reg0_carry__4_n_1,clk_sync_counter_reg0_carry__4_n_2,clk_sync_counter_reg0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[24:21]),
        .S(clk_sync_counter_reg_reg[24:21]));
  CARRY4 clk_sync_counter_reg0_carry__5
       (.CI(clk_sync_counter_reg0_carry__4_n_0),
        .CO({clk_sync_counter_reg0_carry__5_n_0,clk_sync_counter_reg0_carry__5_n_1,clk_sync_counter_reg0_carry__5_n_2,clk_sync_counter_reg0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[28:25]),
        .S(clk_sync_counter_reg_reg[28:25]));
  CARRY4 clk_sync_counter_reg0_carry__6
       (.CI(clk_sync_counter_reg0_carry__5_n_0),
        .CO({NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED[3:2],clk_sync_counter_reg0_carry__6_n_2,clk_sync_counter_reg0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED[3],clk_sync_counter_reg0[31:29]}),
        .S({1'b0,clk_sync_counter_reg_reg[31:29]}));
  LUT2 #(
    .INIT(4'h1)) 
    \clk_sync_counter_reg[0]_i_1 
       (.I0(is_clk_initialized_reg),
        .I1(start_init_clk_reg_i_3_n_0),
        .O(clk_sync_counter_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000002)) 
    \clk_sync_counter_reg[0]_i_10 
       (.I0(start_init_clk_reg_i_14_n_0),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[14]),
        .I4(clk_sync_counter_reg_reg[13]),
        .I5(\clk_sync_counter_reg[0]_i_14_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    \clk_sync_counter_reg[0]_i_11 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_sync_counter_reg_reg[12]),
        .I5(clk_sync_counter_reg_reg[10]),
        .O(\clk_sync_counter_reg[0]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    \clk_sync_counter_reg[0]_i_12 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'h0660)) 
    \clk_sync_counter_reg[0]_i_13 
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[7]),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[0]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \clk_sync_counter_reg[0]_i_14 
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .O(\clk_sync_counter_reg[0]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_3 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(clk_sync_counter_reg0[3]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_4 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[2]),
        .I2(clk_sync_counter_reg0[2]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_5 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[1]),
        .I2(clk_sync_counter_reg0[1]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clk_sync_counter_reg[0]_i_6 
       (.I0(clk_sync_counter_reg_reg[0]),
        .O(\clk_sync_counter_reg[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFF77FF07)) 
    \clk_sync_counter_reg[0]_i_7 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I2(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I3(start_init_clk_reg_i_5_n_0),
        .I4(clk_sync_counter_reg_reg[17]),
        .O(\clk_sync_counter_reg[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h08200000)) 
    \clk_sync_counter_reg[0]_i_8 
       (.I0(\clk_sync_counter_reg[0]_i_11_n_0 ),
        .I1(clk_sync_counter_reg_reg[10]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[9]),
        .I4(\clk_sync_counter_reg[0]_i_12_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000101000)) 
    \clk_sync_counter_reg[0]_i_9 
       (.I0(start_init_clk_reg_i_12_n_0),
        .I1(start_init_clk_reg_i_10_n_0),
        .I2(\clk_sync_counter_reg[0]_i_13_n_0 ),
        .I3(clk_sync_counter_reg_reg[17]),
        .I4(clk_sync_counter_reg_reg[16]),
        .I5(clk_sync_counter_reg_reg[3]),
        .O(\clk_sync_counter_reg[0]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[12]_i_2 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg0[15]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_3 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[14]),
        .O(\clk_sync_counter_reg[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[13]),
        .O(\clk_sync_counter_reg[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[12]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[12]),
        .O(\clk_sync_counter_reg[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[16]_i_2 
       (.I0(clk_sync_counter_reg0[19]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[16]_i_3 
       (.I0(clk_sync_counter_reg0[18]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hC8)) 
    \clk_sync_counter_reg[16]_i_4 
       (.I0(clk_sync_counter_reg0[17]),
        .I1(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I2(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFF004E4E)) 
    \clk_sync_counter_reg[16]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(clk_sync_counter_reg0[16]),
        .I2(\clk_sync_counter_reg[16]_i_6_n_0 ),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(start_init_clk_reg_i_2_n_0),
        .O(\clk_sync_counter_reg[16]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \clk_sync_counter_reg[16]_i_6 
       (.I0(\clk_sync_counter_reg[16]_i_7_n_0 ),
        .I1(\clk_sync_counter_reg[16]_i_8_n_0 ),
        .I2(\clk_sync_counter_reg[16]_i_9_n_0 ),
        .I3(start_init_clk_reg_i_16_n_0),
        .I4(start_init_clk_reg_i_10_n_0),
        .O(\clk_sync_counter_reg[16]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \clk_sync_counter_reg[16]_i_7 
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[16]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \clk_sync_counter_reg[16]_i_8 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[10]),
        .O(\clk_sync_counter_reg[16]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0100)) 
    \clk_sync_counter_reg[16]_i_9 
       (.I0(clk_sync_counter_reg_reg[4]),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_sync_counter_reg_reg[8]),
        .O(\clk_sync_counter_reg[16]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_2 
       (.I0(clk_sync_counter_reg0[23]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_3 
       (.I0(clk_sync_counter_reg0[22]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_4 
       (.I0(clk_sync_counter_reg0[21]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_5 
       (.I0(clk_sync_counter_reg0[20]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_2 
       (.I0(clk_sync_counter_reg0[27]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_3 
       (.I0(clk_sync_counter_reg0[26]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_4 
       (.I0(clk_sync_counter_reg0[25]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_5 
       (.I0(clk_sync_counter_reg0[24]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_2 
       (.I0(clk_sync_counter_reg0[31]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_3 
       (.I0(clk_sync_counter_reg0[30]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_4 
       (.I0(clk_sync_counter_reg0[29]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_5 
       (.I0(clk_sync_counter_reg0[28]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[4]_i_2 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[7]),
        .I3(clk_sync_counter_reg0[7]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[4]_i_3 
       (.I0(clk_sync_counter_reg_reg[6]),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg0[6]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[4]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg0[5]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[4]_i_5 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(clk_sync_counter_reg0[4]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF8FAFCFFFCFFFCFF)) 
    \clk_sync_counter_reg[4]_i_6 
       (.I0(\clk_sync_counter_reg[16]_i_6_n_0 ),
        .I1(clk_sync_counter_reg_reg[17]),
        .I2(start_init_clk_reg_i_5_n_0),
        .I3(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I4(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I5(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \clk_sync_counter_reg[4]_i_7 
       (.I0(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I2(start_init_clk_reg_i_5_n_0),
        .O(\clk_sync_counter_reg[4]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[8]_i_2 
       (.I0(clk_sync_counter_reg_reg[11]),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg0[11]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[8]_i_3 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[10]),
        .I3(clk_sync_counter_reg0[10]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[8]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_sync_counter_reg0[9]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[8]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[8]),
        .O(\clk_sync_counter_reg[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_7 ),
        .Q(clk_sync_counter_reg_reg[0]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\clk_sync_counter_reg_reg[0]_i_2_n_0 ,\clk_sync_counter_reg_reg[0]_i_2_n_1 ,\clk_sync_counter_reg_reg[0]_i_2_n_2 ,\clk_sync_counter_reg_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,start_init_clk_reg_i_2_n_0}),
        .O({\clk_sync_counter_reg_reg[0]_i_2_n_4 ,\clk_sync_counter_reg_reg[0]_i_2_n_5 ,\clk_sync_counter_reg_reg[0]_i_2_n_6 ,\clk_sync_counter_reg_reg[0]_i_2_n_7 }),
        .S({\clk_sync_counter_reg[0]_i_3_n_0 ,\clk_sync_counter_reg[0]_i_4_n_0 ,\clk_sync_counter_reg[0]_i_5_n_0 ,\clk_sync_counter_reg[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[12]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[12]_i_1 
       (.CI(\clk_sync_counter_reg_reg[8]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[12]_i_1_n_0 ,\clk_sync_counter_reg_reg[12]_i_1_n_1 ,\clk_sync_counter_reg_reg[12]_i_1_n_2 ,\clk_sync_counter_reg_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[12]_i_1_n_4 ,\clk_sync_counter_reg_reg[12]_i_1_n_5 ,\clk_sync_counter_reg_reg[12]_i_1_n_6 ,\clk_sync_counter_reg_reg[12]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[12]_i_2_n_0 ,\clk_sync_counter_reg[12]_i_3_n_0 ,\clk_sync_counter_reg[12]_i_4_n_0 ,\clk_sync_counter_reg[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[16]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[16]_i_1 
       (.CI(\clk_sync_counter_reg_reg[12]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[16]_i_1_n_0 ,\clk_sync_counter_reg_reg[16]_i_1_n_1 ,\clk_sync_counter_reg_reg[16]_i_1_n_2 ,\clk_sync_counter_reg_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[16]_i_1_n_4 ,\clk_sync_counter_reg_reg[16]_i_1_n_5 ,\clk_sync_counter_reg_reg[16]_i_1_n_6 ,\clk_sync_counter_reg_reg[16]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[16]_i_2_n_0 ,\clk_sync_counter_reg[16]_i_3_n_0 ,\clk_sync_counter_reg[16]_i_4_n_0 ,\clk_sync_counter_reg[16]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[17] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[18] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[19] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_6 ),
        .Q(clk_sync_counter_reg_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[20] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[20]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[20]_i_1 
       (.CI(\clk_sync_counter_reg_reg[16]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[20]_i_1_n_0 ,\clk_sync_counter_reg_reg[20]_i_1_n_1 ,\clk_sync_counter_reg_reg[20]_i_1_n_2 ,\clk_sync_counter_reg_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[20]_i_1_n_4 ,\clk_sync_counter_reg_reg[20]_i_1_n_5 ,\clk_sync_counter_reg_reg[20]_i_1_n_6 ,\clk_sync_counter_reg_reg[20]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[20]_i_2_n_0 ,\clk_sync_counter_reg[20]_i_3_n_0 ,\clk_sync_counter_reg[20]_i_4_n_0 ,\clk_sync_counter_reg[20]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[21] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[22] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[23] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[24] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[24]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[24]_i_1 
       (.CI(\clk_sync_counter_reg_reg[20]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[24]_i_1_n_0 ,\clk_sync_counter_reg_reg[24]_i_1_n_1 ,\clk_sync_counter_reg_reg[24]_i_1_n_2 ,\clk_sync_counter_reg_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[24]_i_1_n_4 ,\clk_sync_counter_reg_reg[24]_i_1_n_5 ,\clk_sync_counter_reg_reg[24]_i_1_n_6 ,\clk_sync_counter_reg_reg[24]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[24]_i_2_n_0 ,\clk_sync_counter_reg[24]_i_3_n_0 ,\clk_sync_counter_reg[24]_i_4_n_0 ,\clk_sync_counter_reg[24]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[25] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[26] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[27] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[28] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[28]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[28]_i_1 
       (.CI(\clk_sync_counter_reg_reg[24]_i_1_n_0 ),
        .CO({\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED [3],\clk_sync_counter_reg_reg[28]_i_1_n_1 ,\clk_sync_counter_reg_reg[28]_i_1_n_2 ,\clk_sync_counter_reg_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[28]_i_1_n_4 ,\clk_sync_counter_reg_reg[28]_i_1_n_5 ,\clk_sync_counter_reg_reg[28]_i_1_n_6 ,\clk_sync_counter_reg_reg[28]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[28]_i_2_n_0 ,\clk_sync_counter_reg[28]_i_3_n_0 ,\clk_sync_counter_reg[28]_i_4_n_0 ,\clk_sync_counter_reg[28]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[29] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_5 ),
        .Q(clk_sync_counter_reg_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[30] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[31] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_4 ),
        .Q(clk_sync_counter_reg_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[4]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[4]_i_1 
       (.CI(\clk_sync_counter_reg_reg[0]_i_2_n_0 ),
        .CO({\clk_sync_counter_reg_reg[4]_i_1_n_0 ,\clk_sync_counter_reg_reg[4]_i_1_n_1 ,\clk_sync_counter_reg_reg[4]_i_1_n_2 ,\clk_sync_counter_reg_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[4]_i_1_n_4 ,\clk_sync_counter_reg_reg[4]_i_1_n_5 ,\clk_sync_counter_reg_reg[4]_i_1_n_6 ,\clk_sync_counter_reg_reg[4]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[4]_i_2_n_0 ,\clk_sync_counter_reg[4]_i_3_n_0 ,\clk_sync_counter_reg[4]_i_4_n_0 ,\clk_sync_counter_reg[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[8]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[8]_i_1 
       (.CI(\clk_sync_counter_reg_reg[4]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[8]_i_1_n_0 ,\clk_sync_counter_reg_reg[8]_i_1_n_1 ,\clk_sync_counter_reg_reg[8]_i_1_n_2 ,\clk_sync_counter_reg_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[8]_i_1_n_4 ,\clk_sync_counter_reg_reg[8]_i_1_n_5 ,\clk_sync_counter_reg_reg[8]_i_1_n_6 ,\clk_sync_counter_reg_reg[8]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[8]_i_2_n_0 ,\clk_sync_counter_reg[8]_i_3_n_0 ,\clk_sync_counter_reg[8]_i_4_n_0 ,\clk_sync_counter_reg[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 clk_wizard_main
       (.clk_in1(clk_100MHz_in),
        .clk_out1(clk_100MHz_out),
        .clk_out2(clk_wizard_main_n_1),
        .clk_out3(clk_20MHz_out),
        .clk_out4(clk_10MHz_out),
        .locked(clk_wizard_main_n_4));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_uart[0]_i_1 
       (.I0(cnt_uart[0]),
        .O(\cnt_uart[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h6266)) 
    \cnt_uart[1]_i_1 
       (.I0(cnt_uart[1]),
        .I1(cnt_uart[0]),
        .I2(cnt_uart[3]),
        .I3(cnt_uart[2]),
        .O(\cnt_uart[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h4FA0)) 
    \cnt_uart[2]_i_1 
       (.I0(cnt_uart[1]),
        .I1(cnt_uart[3]),
        .I2(cnt_uart[0]),
        .I3(cnt_uart[2]),
        .O(\cnt_uart[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAABAAAAA)) 
    \cnt_uart[3]_i_1 
       (.I0(is_counting),
        .I1(cnt_uart[3]),
        .I2(cnt_uart[2]),
        .I3(cnt_uart[1]),
        .I4(cnt_uart[0]),
        .O(\cnt_uart[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h6CCC)) 
    \cnt_uart[3]_i_2 
       (.I0(cnt_uart[2]),
        .I1(cnt_uart[3]),
        .I2(cnt_uart[0]),
        .I3(cnt_uart[1]),
        .O(\cnt_uart[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[0]_i_1_n_0 ),
        .Q(cnt_uart[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[1]_i_1_n_0 ),
        .Q(cnt_uart[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[2]_i_1_n_0 ),
        .Q(cnt_uart[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[3]_i_2_n_0 ),
        .Q(cnt_uart[3]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF4FFFFFF44444444)) 
    if_amp1_en_reg_i_1
       (.I0(if_amp1_en_reg_i_2_n_0),
        .I1(pamp_en_reg_i_3_n_0),
        .I2(in_uart_data[0]),
        .I3(pamp_en_reg_i_4_n_0),
        .I4(\uart_tx_data[4]_i_4_n_0 ),
        .I5(if_amp1_stu_en),
        .O(if_amp1_en_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    if_amp1_en_reg_i_2
       (.I0(in_uart_data[4]),
        .I1(in_uart_data[7]),
        .I2(in_uart_data[6]),
        .I3(in_uart_data[5]),
        .I4(in_uart_data[3]),
        .O(if_amp1_en_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    if_amp1_en_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(if_amp1_en_reg_i_1_n_0),
        .Q(if_amp1_stu_en),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hEFFF0400)) 
    if_amp2_en_reg_i_1
       (.I0(in_uart_data[2]),
        .I1(in_uart_data[0]),
        .I2(in_uart_data[1]),
        .I3(\uart_tx_data[4]_i_4_n_0 ),
        .I4(if_amp2_stu_en),
        .O(if_amp2_en_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    if_amp2_en_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(if_amp2_en_reg_i_1_n_0),
        .Q(if_amp2_stu_en),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hF2)) 
    is_clk_initialized_reg_i_1
       (.I0(start_init_clk_reg_i_3_n_0),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(is_clk_initialized_reg),
        .O(is_clk_initialized_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_clk_initialized_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_clk_initialized_reg_i_1_n_0),
        .Q(is_clk_initialized_reg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h88A888A8AAAA88A8)) 
    is_counting_i_1
       (.I0(pll_read_i_2_n_0),
        .I1(is_counting),
        .I2(pll_data_ready),
        .I3(pll_data_ready_prev),
        .I4(uart_byte_received),
        .I5(uart_byte_received_prev),
        .O(is_counting_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_counting_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_counting_i_1_n_0),
        .Q(is_counting),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000010)) 
    is_uart_receiving_atten_value_i_1
       (.I0(sw_ctrl_reg_i_2_n_0),
        .I1(in_uart_data[4]),
        .I2(in_uart_data[1]),
        .I3(in_uart_data[0]),
        .I4(sw_if3_reg_i_2_n_0),
        .I5(is_uart_receiving_atten_value_reg_n_0),
        .O(is_uart_receiving_atten_value_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_uart_receiving_atten_value_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_uart_receiving_atten_value_i_1_n_0),
        .Q(is_uart_receiving_atten_value_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEF0000)) 
    is_uart_receiving_pll_data_i_1
       (.I0(is_uart_receiving_pll_read_address),
        .I1(is_uart_receiving_atten_value_reg_n_0),
        .I2(is_uart_receiving_pll_data_i_2_n_0),
        .I3(pll_write_i_2_n_0),
        .I4(is_uart_receiving_pll_data_reg_n_0),
        .I5(pll_write_address_reg),
        .O(is_uart_receiving_pll_data_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    is_uart_receiving_pll_data_i_2
       (.I0(\received_byte_num_reg_n_0_[0] ),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .O(is_uart_receiving_pll_data_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_uart_receiving_pll_data_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_uart_receiving_pll_data_i_1_n_0),
        .Q(is_uart_receiving_pll_data_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0080FFFF00800080)) 
    is_uart_receiving_pll_read_address_i_1
       (.I0(\uart_tx_data[4]_i_4_n_0 ),
        .I1(in_uart_data[1]),
        .I2(in_uart_data[0]),
        .I3(in_uart_data[2]),
        .I4(pll_address_reg),
        .I5(is_uart_receiving_pll_read_address),
        .O(is_uart_receiving_pll_read_address_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_uart_receiving_pll_read_address_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_uart_receiving_pll_read_address_i_1_n_0),
        .Q(is_uart_receiving_pll_read_address),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h222F222222222222)) 
    is_uart_receiving_pll_write_address_i_1
       (.I0(is_uart_receiving_pll_write_address),
        .I1(pll_write_address_reg),
        .I2(is_uart_receiving_pll_write_address_i_2_n_0),
        .I3(sw_if3_reg_i_2_n_0),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(is_uart_receiving_pll_write_address_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    is_uart_receiving_pll_write_address_i_2
       (.I0(in_uart_data[3]),
        .I1(in_uart_data[7]),
        .I2(in_uart_data[6]),
        .I3(in_uart_data[5]),
        .I4(in_uart_data[4]),
        .O(is_uart_receiving_pll_write_address_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_uart_receiving_pll_write_address_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_uart_receiving_pll_write_address_i_1_n_0),
        .Q(is_uart_receiving_pll_write_address),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hF4FFFFFF44444444)) 
    pamp_en_reg_i_1
       (.I0(pamp_en_reg_i_2_n_0),
        .I1(pamp_en_reg_i_3_n_0),
        .I2(in_uart_data[0]),
        .I3(pamp_en_reg_i_4_n_0),
        .I4(pamp_en_reg_i_5_n_0),
        .I5(pamp_en),
        .O(pamp_en_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    pamp_en_reg_i_2
       (.I0(in_uart_data[3]),
        .I1(in_uart_data[7]),
        .I2(in_uart_data[6]),
        .I3(in_uart_data[5]),
        .I4(in_uart_data[4]),
        .O(pamp_en_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    pamp_en_reg_i_3
       (.I0(in_uart_data[2]),
        .I1(uart_byte_received),
        .I2(uart_byte_received_prev),
        .I3(\pll_write_data[23]_i_4_n_0 ),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(pamp_en_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h1)) 
    pamp_en_reg_i_4
       (.I0(in_uart_data[1]),
        .I1(in_uart_data[2]),
        .O(pamp_en_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    pamp_en_reg_i_5
       (.I0(if_amp1_en_reg_i_2_n_0),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(uart_byte_received_prev),
        .I4(uart_byte_received),
        .O(pamp_en_reg_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pamp_en_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pamp_en_reg_i_1_n_0),
        .Q(pamp_en),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000444000000000)) 
    \pll_address_reg[7]_i_1 
       (.I0(is_uart_receiving_atten_value_reg_n_0),
        .I1(is_uart_receiving_pll_read_address),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(\received_byte_num_reg_n_0_[1] ),
        .I4(uart_byte_received_prev),
        .I5(uart_byte_received),
        .O(pll_address_reg));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[0]),
        .Q(pll_read_addres[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[1]),
        .Q(pll_read_addres[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[2]),
        .Q(pll_read_addres[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[3]),
        .Q(pll_read_addres[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[4]),
        .Q(pll_read_addres[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[5]),
        .Q(pll_read_addres[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[6]),
        .Q(pll_read_addres[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_address_reg_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(pll_address_reg),
        .D(in_uart_data[7]),
        .Q(pll_read_addres[7]),
        .R(1'b0));
  FDRE pll_data_ready_prev_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_data_ready),
        .Q(pll_data_ready_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hF0B0F000)) 
    pll_read_i_1
       (.I0(pll_data_ready_prev),
        .I1(pll_data_ready),
        .I2(pll_read_i_2_n_0),
        .I3(pll_address_reg),
        .I4(pll_read),
        .O(pll_read_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    pll_read_i_2
       (.I0(cnt_uart[0]),
        .I1(cnt_uart[1]),
        .I2(cnt_uart[2]),
        .I3(cnt_uart[3]),
        .O(pll_read_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_read_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_read_i_1_n_0),
        .Q(pll_read),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00CE)) 
    pll_trig_reg_i_1
       (.I0(pll_trig),
        .I1(\trig_tguard_counter[16]_i_1_n_0 ),
        .I2(trig_tguard_counter),
        .I3(trig_tsweep_counter074_out),
        .O(pll_trig_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_trig_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_trig_reg_i_1_n_0),
        .Q(pll_trig),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_write_address_reg_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(pll_write_address_reg),
        .D(in_uart_data[0]),
        .Q(pll_write_addres[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_write_address_reg_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(pll_write_address_reg),
        .D(in_uart_data[1]),
        .Q(pll_write_addres[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_write_address_reg_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(pll_write_address_reg),
        .D(in_uart_data[2]),
        .Q(pll_write_addres[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_write_address_reg_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(pll_write_address_reg),
        .D(in_uart_data[3]),
        .Q(pll_write_addres[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \pll_write_address_reg_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(pll_write_address_reg),
        .D(in_uart_data[4]),
        .Q(pll_write_addres[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[0]_i_1 
       (.I0(in_uart_data[0]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[0]),
        .O(\pll_write_data[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[10]_i_1 
       (.I0(in_uart_data[2]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[10]),
        .O(\pll_write_data[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[11]_i_1 
       (.I0(in_uart_data[3]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[11]),
        .O(\pll_write_data[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[12]_i_1 
       (.I0(in_uart_data[4]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[12]),
        .O(\pll_write_data[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[13]_i_1 
       (.I0(in_uart_data[5]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[13]),
        .O(\pll_write_data[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[14]_i_1 
       (.I0(in_uart_data[6]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[14]),
        .O(\pll_write_data[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[15]_i_1 
       (.I0(in_uart_data[7]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[15]),
        .O(\pll_write_data[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[16]_i_1 
       (.I0(in_uart_data[0]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[16]),
        .O(\pll_write_data[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[17]_i_1 
       (.I0(in_uart_data[1]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[17]),
        .O(\pll_write_data[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[18]_i_1 
       (.I0(in_uart_data[2]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[18]),
        .O(\pll_write_data[18]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[19]_i_1 
       (.I0(in_uart_data[3]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[19]),
        .O(\pll_write_data[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[1]_i_1 
       (.I0(in_uart_data[1]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[1]),
        .O(\pll_write_data[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[20]_i_1 
       (.I0(in_uart_data[4]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[20]),
        .O(\pll_write_data[20]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[21]_i_1 
       (.I0(in_uart_data[5]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[21]),
        .O(\pll_write_data[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[22]_i_1 
       (.I0(in_uart_data[6]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[22]),
        .O(\pll_write_data[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \pll_write_data[23]_i_1 
       (.I0(uart_byte_received),
        .I1(uart_byte_received_prev),
        .I2(\pll_write_data[23]_i_4_n_0 ),
        .I3(is_uart_receiving_pll_read_address),
        .I4(is_uart_receiving_atten_value_reg_n_0),
        .I5(is_uart_receiving_pll_write_address),
        .O(pll_write_address_reg));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \pll_write_data[23]_i_2 
       (.I0(uart_byte_received),
        .I1(uart_byte_received_prev),
        .I2(\pll_write_data[23]_i_4_n_0 ),
        .I3(is_uart_receiving_pll_read_address),
        .I4(is_uart_receiving_atten_value_reg_n_0),
        .I5(is_uart_receiving_pll_data_reg_n_0),
        .O(\pll_write_data[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'hFF80)) 
    \pll_write_data[23]_i_3 
       (.I0(in_uart_data[7]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[23]),
        .O(\pll_write_data[23]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pll_write_data[23]_i_4 
       (.I0(\received_byte_num_reg_n_0_[0] ),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .O(\pll_write_data[23]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[2]_i_1 
       (.I0(in_uart_data[2]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[2]),
        .O(\pll_write_data[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[3]_i_1 
       (.I0(in_uart_data[3]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[3]),
        .O(\pll_write_data[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[4]_i_1 
       (.I0(in_uart_data[4]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[4]),
        .O(\pll_write_data[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[5]_i_1 
       (.I0(in_uart_data[5]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[5]),
        .O(\pll_write_data[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[6]_i_1 
       (.I0(in_uart_data[6]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[6]),
        .O(\pll_write_data[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[7]_i_1 
       (.I0(in_uart_data[7]),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(pll_write_data[7]),
        .O(\pll_write_data[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[8]_i_1 
       (.I0(in_uart_data[0]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[8]),
        .O(\pll_write_data[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \pll_write_data[9]_i_1 
       (.I0(in_uart_data[1]),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num_reg_n_0_[0] ),
        .I3(pll_write_data[9]),
        .O(\pll_write_data[9]_i_1_n_0 ));
  FDRE \pll_write_data_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[0]_i_1_n_0 ),
        .Q(pll_write_data[0]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[10]_i_1_n_0 ),
        .Q(pll_write_data[10]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[11]_i_1_n_0 ),
        .Q(pll_write_data[11]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[12]_i_1_n_0 ),
        .Q(pll_write_data[12]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[13]_i_1_n_0 ),
        .Q(pll_write_data[13]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[14]_i_1_n_0 ),
        .Q(pll_write_data[14]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[15]_i_1_n_0 ),
        .Q(pll_write_data[15]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[16]_i_1_n_0 ),
        .Q(pll_write_data[16]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[17] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[17]_i_1_n_0 ),
        .Q(pll_write_data[17]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[18] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[18]_i_1_n_0 ),
        .Q(pll_write_data[18]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[19] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[19]_i_1_n_0 ),
        .Q(pll_write_data[19]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[1]_i_1_n_0 ),
        .Q(pll_write_data[1]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[20] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[20]_i_1_n_0 ),
        .Q(pll_write_data[20]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[21] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[21]_i_1_n_0 ),
        .Q(pll_write_data[21]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[22] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[22]_i_1_n_0 ),
        .Q(pll_write_data[22]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[23] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[23]_i_3_n_0 ),
        .Q(pll_write_data[23]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[2]_i_1_n_0 ),
        .Q(pll_write_data[2]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[3]_i_1_n_0 ),
        .Q(pll_write_data[3]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[4]_i_1_n_0 ),
        .Q(pll_write_data[4]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[5]_i_1_n_0 ),
        .Q(pll_write_data[5]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[6]_i_1_n_0 ),
        .Q(pll_write_data[6]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[7]_i_1_n_0 ),
        .Q(pll_write_data[7]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[8]_i_1_n_0 ),
        .Q(pll_write_data[8]),
        .R(pll_write_address_reg));
  FDRE \pll_write_data_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(\pll_write_data[23]_i_2_n_0 ),
        .D(\pll_write_data[9]_i_1_n_0 ),
        .Q(pll_write_data[9]),
        .R(pll_write_address_reg));
  LUT6 #(
    .INIT(64'h0400FFFF04000400)) 
    pll_write_i_1
       (.I0(is_uart_receiving_pll_write_address),
        .I1(is_uart_receiving_pll_data_reg_n_0),
        .I2(pll_write_i_2_n_0),
        .I3(pll_write_i_3_n_0),
        .I4(is_uart_receiving_pll_write_address67_out),
        .I5(pll_write),
        .O(pll_write_i_1_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    pll_write_i_2
       (.I0(uart_byte_received_prev),
        .I1(uart_byte_received),
        .O(pll_write_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    pll_write_i_3
       (.I0(\received_byte_num_reg_n_0_[1] ),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(is_uart_receiving_atten_value_reg_n_0),
        .I3(is_uart_receiving_pll_read_address),
        .O(pll_write_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    pll_write_i_4
       (.I0(in_uart_data[0]),
        .I1(in_uart_data[1]),
        .I2(\pll_write_data[23]_i_4_n_0 ),
        .I3(pll_write_i_2_n_0),
        .I4(in_uart_data[2]),
        .I5(is_uart_receiving_pll_write_address_i_2_n_0),
        .O(is_uart_receiving_pll_write_address67_out));
  FDRE #(
    .INIT(1'b0)) 
    pll_write_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_write_i_1_n_0),
        .Q(pll_write),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBBBBBBCCB0B0B0C0)) 
    \received_byte_num[0]_i_1 
       (.I0(is_uart_receiving_pll_write_address),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num[0]_i_2_n_0 ),
        .I3(\received_byte_num[0]_i_3_n_0 ),
        .I4(\received_byte_num[0]_i_4_n_0 ),
        .I5(\received_byte_num[0]_i_5_n_0 ),
        .O(\received_byte_num[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hDDDF)) 
    \received_byte_num[0]_i_2 
       (.I0(uart_byte_received),
        .I1(uart_byte_received_prev),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(\received_byte_num_reg_n_0_[0] ),
        .O(\received_byte_num[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h10FF10FF10FF1010)) 
    \received_byte_num[0]_i_3 
       (.I0(sw_ctrl_reg_i_2_n_0),
        .I1(in_uart_data[4]),
        .I2(\received_byte_num[0]_i_6_n_0 ),
        .I3(\received_byte_num[0]_i_2_n_0 ),
        .I4(is_uart_receiving_pll_data_reg_n_0),
        .I5(is_uart_receiving_pll_write_address),
        .O(\received_byte_num[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \received_byte_num[0]_i_4 
       (.I0(\uart_tx_data[4]_i_4_n_0 ),
        .I1(in_uart_data[1]),
        .I2(in_uart_data[0]),
        .O(\received_byte_num[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \received_byte_num[0]_i_5 
       (.I0(is_uart_receiving_pll_read_address),
        .I1(is_uart_receiving_atten_value_reg_n_0),
        .O(\received_byte_num[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \received_byte_num[0]_i_6 
       (.I0(in_uart_data[1]),
        .I1(in_uart_data[0]),
        .I2(in_uart_data[2]),
        .I3(uart_byte_received),
        .I4(uart_byte_received_prev),
        .I5(\pll_write_data[23]_i_4_n_0 ),
        .O(\received_byte_num[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hF8)) 
    \received_byte_num[1]_i_1 
       (.I0(pll_write_i_2_n_0),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(\received_byte_num[1]_i_2_n_0 ),
        .O(\received_byte_num[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000004C00000C0C0)) 
    \received_byte_num[1]_i_2 
       (.I0(pll_write_i_2_n_0),
        .I1(\received_byte_num[0]_i_5_n_0 ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(\received_byte_num_reg_n_0_[0] ),
        .I4(is_uart_receiving_pll_write_address),
        .I5(is_uart_receiving_pll_data_reg_n_0),
        .O(\received_byte_num[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \received_byte_num_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(\received_byte_num[0]_i_1_n_0 ),
        .Q(\received_byte_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \received_byte_num_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(\received_byte_num[1]_i_1_n_0 ),
        .Q(\received_byte_num_reg_n_0_[1] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFFEF00)) 
    start_init_clk_reg_i_1
       (.I0(is_clk_initialized_reg),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(start_init_clk_reg_i_3_n_0),
        .I3(start_init_clk),
        .I4(start_init_clk_reg_i_4_n_0),
        .O(start_init_clk_reg_i_1_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_10
       (.I0(clk_sync_counter_reg_reg[0]),
        .I1(clk_sync_counter_reg_reg[1]),
        .I2(clk_sync_counter_reg_reg[2]),
        .O(start_init_clk_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    start_init_clk_reg_i_11
       (.I0(clk_sync_counter_reg_reg[18]),
        .I1(clk_sync_counter_reg_reg[19]),
        .I2(clk_sync_counter_reg_reg[20]),
        .I3(clk_sync_counter_reg_reg[21]),
        .I4(clk_sync_counter_reg_reg[23]),
        .I5(clk_sync_counter_reg_reg[22]),
        .O(start_init_clk_reg_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_12
       (.I0(clk_sync_counter_reg_reg[6]),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(clk_sync_counter_reg_reg[11]),
        .O(start_init_clk_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_init_clk_reg_i_13
       (.I0(clk_sync_counter_reg_reg[27]),
        .I1(clk_sync_counter_reg_reg[28]),
        .I2(clk_sync_counter_reg_reg[29]),
        .I3(clk_sync_counter_reg_reg[26]),
        .I4(clk_sync_counter_reg_reg[24]),
        .I5(clk_sync_counter_reg_reg[25]),
        .O(start_init_clk_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'h0155FFFFFFFFFFFF)) 
    start_init_clk_reg_i_14
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[6]),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_sync_counter_reg_reg[10]),
        .I5(clk_sync_counter_reg_reg[9]),
        .O(start_init_clk_reg_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h7)) 
    start_init_clk_reg_i_15
       (.I0(clk_sync_counter_reg_reg[9]),
        .I1(clk_sync_counter_reg_reg[10]),
        .O(start_init_clk_reg_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_16
       (.I0(clk_sync_counter_reg_reg[7]),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[6]),
        .O(start_init_clk_reg_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h1)) 
    start_init_clk_reg_i_17
       (.I0(clk_sync_counter_reg_reg[30]),
        .I1(clk_sync_counter_reg_reg[31]),
        .O(start_init_clk_reg_i_17_n_0));
  LUT4 #(
    .INIT(16'h0100)) 
    start_init_clk_reg_i_18
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[7]),
        .O(start_init_clk_reg_i_18_n_0));
  LUT5 #(
    .INIT(32'h11110111)) 
    start_init_clk_reg_i_2
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(start_init_clk_reg_i_5_n_0),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(start_init_clk_reg_i_6_n_0),
        .O(start_init_clk_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEAA0000)) 
    start_init_clk_reg_i_3
       (.I0(clk_sync_counter_reg_reg[16]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(start_init_clk_reg_i_7_n_0),
        .I3(clk_sync_counter_reg_reg[15]),
        .I4(clk_sync_counter_reg_reg[17]),
        .I5(start_init_clk_reg_i_5_n_0),
        .O(start_init_clk_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    start_init_clk_reg_i_4
       (.I0(start_init_clk_reg_i_8_n_0),
        .I1(start_init_clk_reg_i_9_n_0),
        .I2(start_init_clk_reg_i_10_n_0),
        .I3(start_init_clk_reg_i_11_n_0),
        .I4(start_init_clk_reg_i_12_n_0),
        .I5(start_init_clk_reg_i_13_n_0),
        .O(start_init_clk_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hFEFF)) 
    start_init_clk_reg_i_5
       (.I0(clk_sync_counter_reg_reg[30]),
        .I1(clk_sync_counter_reg_reg[31]),
        .I2(start_init_clk_reg_i_11_n_0),
        .I3(start_init_clk_reg_i_13_n_0),
        .O(start_init_clk_reg_i_5_n_0));
  LUT5 #(
    .INIT(32'h00010000)) 
    start_init_clk_reg_i_6
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[12]),
        .I4(start_init_clk_reg_i_14_n_0),
        .O(start_init_clk_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    start_init_clk_reg_i_7
       (.I0(start_init_clk_reg_i_15_n_0),
        .I1(clk_sync_counter_reg_reg[11]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[13]),
        .I4(clk_sync_counter_reg_reg[12]),
        .I5(start_init_clk_reg_i_16_n_0),
        .O(start_init_clk_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    start_init_clk_reg_i_8
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(start_init_clk_reg_i_15_n_0),
        .I3(clk_sync_counter_reg_reg[14]),
        .I4(clk_sync_counter_reg_reg[17]),
        .I5(start_init_clk_reg_i_17_n_0),
        .O(start_init_clk_reg_i_8_n_0));
  LUT4 #(
    .INIT(16'h0400)) 
    start_init_clk_reg_i_9
       (.I0(is_clk_initialized_reg),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[3]),
        .I3(start_init_clk_reg_i_18_n_0),
        .O(start_init_clk_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_init_clk_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(start_init_clk_reg_i_1_n_0),
        .Q(start_init_clk),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    start_uart_send_INST_0
       (.I0(uart_start_sending_reg_n_0),
        .I1(uart_adc_start_sending),
        .O(start_uart_send));
  LUT6 #(
    .INIT(64'hFFFFFFFE00010000)) 
    sw_ctrl_reg_i_1
       (.I0(sw_ctrl_reg_i_2_n_0),
        .I1(in_uart_data[4]),
        .I2(in_uart_data[1]),
        .I3(sw_if3_reg_i_2_n_0),
        .I4(in_uart_data[0]),
        .I5(sw_ctrl),
        .O(sw_ctrl_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sw_ctrl_reg_i_2
       (.I0(in_uart_data[5]),
        .I1(in_uart_data[6]),
        .I2(in_uart_data[7]),
        .I3(in_uart_data[3]),
        .O(sw_ctrl_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sw_ctrl_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(sw_ctrl_reg_i_1_n_0),
        .Q(sw_ctrl),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hEFFF0400)) 
    sw_if1_reg_i_1
       (.I0(in_uart_data[2]),
        .I1(in_uart_data[0]),
        .I2(in_uart_data[1]),
        .I3(pamp_en_reg_i_5_n_0),
        .I4(sw_if1),
        .O(sw_if1_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    sw_if1_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(sw_if1_reg_i_1_n_0),
        .Q(sw_if1),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000200)) 
    sw_if2_reg_i_1
       (.I0(in_uart_data[0]),
        .I1(in_uart_data[2]),
        .I2(sw_if2_reg_i_2_n_0),
        .I3(in_uart_data[1]),
        .I4(if_amp1_en_reg_i_2_n_0),
        .I5(sw_if2),
        .O(sw_if2_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    sw_if2_reg_i_2
       (.I0(uart_byte_received),
        .I1(uart_byte_received_prev),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(\received_byte_num_reg_n_0_[0] ),
        .O(sw_if2_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sw_if2_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(sw_if2_reg_i_1_n_0),
        .Q(sw_if2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFEF0004)) 
    sw_if3_reg_i_1
       (.I0(if_amp1_en_reg_i_2_n_0),
        .I1(in_uart_data[1]),
        .I2(in_uart_data[0]),
        .I3(sw_if3_reg_i_2_n_0),
        .I4(sw_if3),
        .O(sw_if3_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFEFFFFFF)) 
    sw_if3_reg_i_2
       (.I0(\received_byte_num_reg_n_0_[0] ),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(uart_byte_received_prev),
        .I3(uart_byte_received),
        .I4(in_uart_data[2]),
        .O(sw_if3_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sw_if3_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(sw_if3_reg_i_1_n_0),
        .Q(sw_if3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    t_sweep_INST_0
       (.I0(t_sweep_INST_0_i_1_n_0),
        .O(t_sweep));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    t_sweep_INST_0_i_1
       (.I0(trig_tsweep_counter_reg[2]),
        .I1(trig_tsweep_counter_reg[1]),
        .I2(trig_tsweep_counter_reg[0]),
        .I3(t_sweep_INST_0_i_2_n_0),
        .I4(t_sweep_INST_0_i_3_n_0),
        .I5(t_sweep_INST_0_i_4_n_0),
        .O(t_sweep_INST_0_i_1_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    t_sweep_INST_0_i_2
       (.I0(trig_tsweep_counter_reg[6]),
        .I1(trig_tsweep_counter_reg[5]),
        .I2(trig_tsweep_counter_reg[4]),
        .I3(trig_tsweep_counter_reg[3]),
        .O(t_sweep_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    t_sweep_INST_0_i_3
       (.I0(trig_tsweep_counter_reg[10]),
        .I1(trig_tsweep_counter_reg[9]),
        .I2(trig_tsweep_counter_reg[8]),
        .I3(trig_tsweep_counter_reg[7]),
        .O(t_sweep_INST_0_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    t_sweep_INST_0_i_4
       (.I0(trig_tsweep_counter_reg[11]),
        .I1(trig_tsweep_counter_reg[12]),
        .I2(trig_tsweep_counter_reg[13]),
        .I3(trig_tsweep_counter_reg[14]),
        .I4(trig_tsweep_counter_reg[16]),
        .I5(trig_tsweep_counter_reg[15]),
        .O(t_sweep_INST_0_i_4_n_0));
  LUT5 #(
    .INIT(32'h888F88F8)) 
    \trig_tguard_counter[0]_i_1 
       (.I0(\trig_tguard_counter[16]_i_6_n_0 ),
        .I1(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .I3(\trig_tguard_counter[0]_i_2_n_0 ),
        .I4(trig_tguard_counter),
        .O(\trig_tguard_counter[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000040000000000)) 
    \trig_tguard_counter[0]_i_2 
       (.I0(\trig_tguard_counter[16]_i_8_n_0 ),
        .I1(\trig_tguard_counter[0]_i_3_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .I3(\trig_tguard_counter_reg_n_0_[2] ),
        .I4(\trig_tguard_counter_reg_n_0_[1] ),
        .I5(t_sweep_INST_0_i_1_n_0),
        .O(\trig_tguard_counter[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \trig_tguard_counter[0]_i_3 
       (.I0(\trig_tguard_counter_reg_n_0_[6] ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[4] ),
        .I3(\trig_tguard_counter_reg_n_0_[3] ),
        .O(\trig_tguard_counter[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FF404040)) 
    \trig_tguard_counter[16]_i_1 
       (.I0(\trig_tguard_counter[16]_i_4_n_0 ),
        .I1(\trig_tguard_counter[16]_i_5_n_0 ),
        .I2(\trig_tguard_counter[16]_i_6_n_0 ),
        .I3(t_sweep_INST_0_i_1_n_0),
        .I4(\trig_tguard_counter[16]_i_7_n_0 ),
        .I5(\trig_tguard_counter[16]_i_8_n_0 ),
        .O(\trig_tguard_counter[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tguard_counter[16]_i_10 
       (.I0(trig_tsweep_counter_reg[1]),
        .I1(trig_tsweep_counter_reg[0]),
        .I2(trig_tsweep_counter_reg[3]),
        .I3(trig_tsweep_counter_reg[2]),
        .O(\trig_tguard_counter[16]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tguard_counter[16]_i_11 
       (.I0(\trig_tguard_counter_reg_n_0_[3] ),
        .I1(\trig_tguard_counter_reg_n_0_[4] ),
        .O(\trig_tguard_counter[16]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_12 
       (.I0(\trig_tguard_counter_reg_n_0_[12] ),
        .I1(\trig_tguard_counter_reg_n_0_[15] ),
        .I2(\trig_tguard_counter_reg_n_0_[11] ),
        .I3(\trig_tguard_counter_reg_n_0_[9] ),
        .O(\trig_tguard_counter[16]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_13 
       (.I0(\trig_tguard_counter_reg_n_0_[10] ),
        .I1(\trig_tguard_counter_reg_n_0_[14] ),
        .I2(\trig_tguard_counter_reg_n_0_[13] ),
        .I3(\trig_tguard_counter_reg_n_0_[16] ),
        .O(\trig_tguard_counter[16]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h000000002A002AAA)) 
    \trig_tguard_counter[16]_i_2 
       (.I0(t_sweep_INST_0_i_1_n_0),
        .I1(\trig_tguard_counter_reg_n_0_[6] ),
        .I2(\trig_tguard_counter_reg_n_0_[5] ),
        .I3(\trig_tguard_counter[16]_i_4_n_0 ),
        .I4(\trig_tguard_counter[16]_i_9_n_0 ),
        .I5(\trig_tguard_counter[16]_i_8_n_0 ),
        .O(trig_tguard_counter));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \trig_tguard_counter[16]_i_4 
       (.I0(\trig_tguard_counter_reg_n_0_[4] ),
        .I1(\trig_tguard_counter_reg_n_0_[3] ),
        .I2(\trig_tguard_counter_reg_n_0_[2] ),
        .O(\trig_tguard_counter[16]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \trig_tguard_counter[16]_i_5 
       (.I0(\trig_tguard_counter[16]_i_9_n_0 ),
        .I1(trig_tsweep_counter_reg[15]),
        .I2(trig_tsweep_counter_reg[16]),
        .I3(trig_tsweep_counter_reg[13]),
        .I4(trig_tsweep_counter_reg[14]),
        .O(\trig_tguard_counter[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \trig_tguard_counter[16]_i_6 
       (.I0(\trig_tguard_counter[16]_i_10_n_0 ),
        .I1(trig_tsweep_counter_reg[7]),
        .I2(trig_tsweep_counter_reg[6]),
        .I3(trig_tsweep_counter_reg[5]),
        .I4(trig_tsweep_counter_reg[4]),
        .I5(\trig_tsweep_counter[0]_i_5_n_0 ),
        .O(\trig_tguard_counter[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \trig_tguard_counter[16]_i_7 
       (.I0(\trig_tguard_counter[16]_i_11_n_0 ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[6] ),
        .I3(\trig_tguard_counter_reg_n_0_[0] ),
        .I4(\trig_tguard_counter_reg_n_0_[2] ),
        .I5(\trig_tguard_counter_reg_n_0_[1] ),
        .O(\trig_tguard_counter[16]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_8 
       (.I0(\trig_tguard_counter_reg_n_0_[8] ),
        .I1(\trig_tguard_counter_reg_n_0_[7] ),
        .I2(\trig_tguard_counter[16]_i_12_n_0 ),
        .I3(\trig_tguard_counter[16]_i_13_n_0 ),
        .O(\trig_tguard_counter[16]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tguard_counter[16]_i_9 
       (.I0(\trig_tguard_counter_reg_n_0_[6] ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[1] ),
        .I3(\trig_tguard_counter_reg_n_0_[0] ),
        .O(\trig_tguard_counter[16]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(\trig_tguard_counter[0]_i_1_n_0 ),
        .Q(\trig_tguard_counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[10]),
        .Q(\trig_tguard_counter_reg_n_0_[10] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[11]),
        .Q(\trig_tguard_counter_reg_n_0_[11] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[12]),
        .Q(\trig_tguard_counter_reg_n_0_[12] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[12]_i_1 
       (.CI(\trig_tguard_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[12]_i_1_n_0 ,\trig_tguard_counter_reg[12]_i_1_n_1 ,\trig_tguard_counter_reg[12]_i_1_n_2 ,\trig_tguard_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S({\trig_tguard_counter_reg_n_0_[12] ,\trig_tguard_counter_reg_n_0_[11] ,\trig_tguard_counter_reg_n_0_[10] ,\trig_tguard_counter_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[13]),
        .Q(\trig_tguard_counter_reg_n_0_[13] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[14]),
        .Q(\trig_tguard_counter_reg_n_0_[14] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[15]),
        .Q(\trig_tguard_counter_reg_n_0_[15] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[16]),
        .Q(\trig_tguard_counter_reg_n_0_[16] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[16]_i_3 
       (.CI(\trig_tguard_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED [3],\trig_tguard_counter_reg[16]_i_3_n_1 ,\trig_tguard_counter_reg[16]_i_3_n_2 ,\trig_tguard_counter_reg[16]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[16:13]),
        .S({\trig_tguard_counter_reg_n_0_[16] ,\trig_tguard_counter_reg_n_0_[15] ,\trig_tguard_counter_reg_n_0_[14] ,\trig_tguard_counter_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[1]),
        .Q(\trig_tguard_counter_reg_n_0_[1] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[2]),
        .Q(\trig_tguard_counter_reg_n_0_[2] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[3]),
        .Q(\trig_tguard_counter_reg_n_0_[3] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[4]),
        .Q(\trig_tguard_counter_reg_n_0_[4] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trig_tguard_counter_reg[4]_i_1_n_0 ,\trig_tguard_counter_reg[4]_i_1_n_1 ,\trig_tguard_counter_reg[4]_i_1_n_2 ,\trig_tguard_counter_reg[4]_i_1_n_3 }),
        .CYINIT(\trig_tguard_counter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S({\trig_tguard_counter_reg_n_0_[4] ,\trig_tguard_counter_reg_n_0_[3] ,\trig_tguard_counter_reg_n_0_[2] ,\trig_tguard_counter_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[5]),
        .Q(\trig_tguard_counter_reg_n_0_[5] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[6]),
        .Q(\trig_tguard_counter_reg_n_0_[6] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[7]),
        .Q(\trig_tguard_counter_reg_n_0_[7] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[8]),
        .Q(\trig_tguard_counter_reg_n_0_[8] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[8]_i_1 
       (.CI(\trig_tguard_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[8]_i_1_n_0 ,\trig_tguard_counter_reg[8]_i_1_n_1 ,\trig_tguard_counter_reg[8]_i_1_n_2 ,\trig_tguard_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S({\trig_tguard_counter_reg_n_0_[8] ,\trig_tguard_counter_reg_n_0_[7] ,\trig_tguard_counter_reg_n_0_[6] ,\trig_tguard_counter_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[9]),
        .Q(\trig_tguard_counter_reg_n_0_[9] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tsweep_counter[0]_i_1 
       (.I0(trig_tguard_counter),
        .I1(\trig_tguard_counter[16]_i_1_n_0 ),
        .O(\trig_tsweep_counter[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF010)) 
    \trig_tsweep_counter[0]_i_2 
       (.I0(trig_tsweep_counter_reg[6]),
        .I1(trig_tsweep_counter_reg[7]),
        .I2(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I3(\trig_tsweep_counter[0]_i_5_n_0 ),
        .O(trig_tsweep_counter074_out));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \trig_tsweep_counter[0]_i_4 
       (.I0(\trig_tguard_counter[16]_i_4_n_0 ),
        .I1(\trig_tsweep_counter[0]_i_7_n_0 ),
        .I2(\trig_tguard_counter[16]_i_12_n_0 ),
        .I3(\trig_tguard_counter[16]_i_13_n_0 ),
        .I4(\trig_tsweep_counter[0]_i_8_n_0 ),
        .I5(\trig_tguard_counter[16]_i_9_n_0 ),
        .O(\trig_tsweep_counter[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \trig_tsweep_counter[0]_i_5 
       (.I0(trig_tsweep_counter_reg[9]),
        .I1(trig_tsweep_counter_reg[10]),
        .I2(trig_tsweep_counter_reg[8]),
        .I3(trig_tsweep_counter_reg[11]),
        .I4(trig_tsweep_counter_reg[12]),
        .O(\trig_tsweep_counter[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trig_tsweep_counter[0]_i_6 
       (.I0(trig_tsweep_counter_reg[0]),
        .O(\trig_tsweep_counter[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tsweep_counter[0]_i_7 
       (.I0(\trig_tguard_counter_reg_n_0_[7] ),
        .I1(\trig_tguard_counter_reg_n_0_[8] ),
        .O(\trig_tsweep_counter[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tsweep_counter[0]_i_8 
       (.I0(trig_tsweep_counter_reg[14]),
        .I1(trig_tsweep_counter_reg[13]),
        .I2(trig_tsweep_counter_reg[16]),
        .I3(trig_tsweep_counter_reg[15]),
        .O(\trig_tsweep_counter[0]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_7 ),
        .Q(trig_tsweep_counter_reg[0]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\trig_tsweep_counter_reg[0]_i_3_n_0 ,\trig_tsweep_counter_reg[0]_i_3_n_1 ,\trig_tsweep_counter_reg[0]_i_3_n_2 ,\trig_tsweep_counter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\trig_tsweep_counter_reg[0]_i_3_n_4 ,\trig_tsweep_counter_reg[0]_i_3_n_5 ,\trig_tsweep_counter_reg[0]_i_3_n_6 ,\trig_tsweep_counter_reg[0]_i_3_n_7 }),
        .S({trig_tsweep_counter_reg[3:1],\trig_tsweep_counter[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[10]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[11]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[12]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[12]_i_1 
       (.CI(\trig_tsweep_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[12]_i_1_n_0 ,\trig_tsweep_counter_reg[12]_i_1_n_1 ,\trig_tsweep_counter_reg[12]_i_1_n_2 ,\trig_tsweep_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[12]_i_1_n_4 ,\trig_tsweep_counter_reg[12]_i_1_n_5 ,\trig_tsweep_counter_reg[12]_i_1_n_6 ,\trig_tsweep_counter_reg[12]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[13]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[14]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[15]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[16]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[16]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[16]_i_1 
       (.CI(\trig_tsweep_counter_reg[12]_i_1_n_0 ),
        .CO(\NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED [3:1],\trig_tsweep_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,trig_tsweep_counter_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_6 ),
        .Q(trig_tsweep_counter_reg[1]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_5 ),
        .Q(trig_tsweep_counter_reg[2]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_4 ),
        .Q(trig_tsweep_counter_reg[3]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[4]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[4]_i_1 
       (.CI(\trig_tsweep_counter_reg[0]_i_3_n_0 ),
        .CO({\trig_tsweep_counter_reg[4]_i_1_n_0 ,\trig_tsweep_counter_reg[4]_i_1_n_1 ,\trig_tsweep_counter_reg[4]_i_1_n_2 ,\trig_tsweep_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[4]_i_1_n_4 ,\trig_tsweep_counter_reg[4]_i_1_n_5 ,\trig_tsweep_counter_reg[4]_i_1_n_6 ,\trig_tsweep_counter_reg[4]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[5]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[6]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[7]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[8]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[8]_i_1 
       (.CI(\trig_tsweep_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[8]_i_1_n_0 ,\trig_tsweep_counter_reg[8]_i_1_n_1 ,\trig_tsweep_counter_reg[8]_i_1_n_2 ,\trig_tsweep_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[8]_i_1_n_4 ,\trig_tsweep_counter_reg[8]_i_1_n_5 ,\trig_tsweep_counter_reg[8]_i_1_n_6 ,\trig_tsweep_counter_reg[8]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter074_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[9]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    uart_byte_received_prev_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(uart_byte_received),
        .Q(uart_byte_received_prev),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAA8A888888)) 
    uart_start_sending_i_1
       (.I0(pll_read_i_2_n_0),
        .I1(\uart_tx_data[7]_i_4_n_0 ),
        .I2(pll_data_ready_prev),
        .I3(pll_data_ready),
        .I4(uart_start_sending_i_2_n_0),
        .I5(uart_start_sending_reg_n_0),
        .O(uart_start_sending_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    uart_start_sending_i_2
       (.I0(out_data_uart[2]),
        .I1(out_data_uart[3]),
        .I2(out_data_uart[0]),
        .I3(out_data_uart[1]),
        .I4(uart_start_sending_i_3_n_0),
        .O(uart_start_sending_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    uart_start_sending_i_3
       (.I0(out_data_uart[4]),
        .I1(out_data_uart[5]),
        .I2(out_data_uart[7]),
        .I3(out_data_uart[6]),
        .O(uart_start_sending_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    uart_start_sending_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(uart_start_sending_i_1_n_0),
        .Q(uart_start_sending_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \uart_tx_data[0]_i_1 
       (.I0(\uart_tx_data[0]_i_2_n_0 ),
        .I1(\uart_tx_data[0]_i_3_n_0 ),
        .I2(\uart_tx_data[0]_i_4_n_0 ),
        .I3(\uart_tx_data[0]_i_5_n_0 ),
        .I4(\uart_tx_data[0]_i_6_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'h0000555500000300)) 
    \uart_tx_data[0]_i_10 
       (.I0(is_uart_receiving_atten_value_reg_n_0),
        .I1(in_uart_data[1]),
        .I2(in_uart_data[0]),
        .I3(pll_read_data[0]),
        .I4(\uart_tx_data[0]_i_7_n_0 ),
        .I5(\pll_write_data[23]_i_4_n_0 ),
        .O(\uart_tx_data[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h3F3F003F22220000)) 
    \uart_tx_data[0]_i_2 
       (.I0(pll_read_data[8]),
        .I1(\pll_write_data[23]_i_4_n_0 ),
        .I2(is_uart_receiving_atten_value_reg_n_0),
        .I3(\uart_tx_data[2]_i_5_n_0 ),
        .I4(in_uart_data[0]),
        .I5(\uart_tx_data[0]_i_7_n_0 ),
        .O(\uart_tx_data[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A0A0FF101010)) 
    \uart_tx_data[0]_i_3 
       (.I0(in_uart_data[4]),
        .I1(in_uart_data[2]),
        .I2(\uart_tx_data[2]_i_3_n_0 ),
        .I3(\uart_tx_data[0]_i_8_n_0 ),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[3]),
        .O(\uart_tx_data[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF111111111)) 
    \uart_tx_data[0]_i_4 
       (.I0(\pll_write_data[23]_i_4_n_0 ),
        .I1(\uart_tx_data[0]_i_9_n_0 ),
        .I2(in_uart_data[7]),
        .I3(in_uart_data[6]),
        .I4(in_uart_data[5]),
        .I5(\uart_tx_data[2]_i_3_n_0 ),
        .O(\uart_tx_data[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000000E)) 
    \uart_tx_data[0]_i_5 
       (.I0(is_uart_receiving_pll_data_reg_n_0),
        .I1(is_uart_receiving_pll_write_address),
        .I2(is_uart_receiving_atten_value_reg_n_0),
        .I3(is_uart_receiving_pll_read_address),
        .I4(\received_byte_num[0]_i_2_n_0 ),
        .I5(\uart_tx_data[0]_i_10_n_0 ),
        .O(\uart_tx_data[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFBBB0000FBBBFBBB)) 
    \uart_tx_data[0]_i_6 
       (.I0(in_uart_data[0]),
        .I1(\uart_tx_data[2]_i_5_n_0 ),
        .I2(in_uart_data[2]),
        .I3(in_uart_data[4]),
        .I4(uart_byte_received_prev),
        .I5(uart_byte_received),
        .O(\uart_tx_data[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \uart_tx_data[0]_i_7 
       (.I0(in_uart_data[2]),
        .I1(in_uart_data[4]),
        .O(\uart_tx_data[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A8FFA0FFA8FF)) 
    \uart_tx_data[0]_i_8 
       (.I0(in_uart_data[2]),
        .I1(pll_read_data[16]),
        .I2(pll_write_i_2_n_0),
        .I3(in_uart_data[4]),
        .I4(\pll_write_data[23]_i_4_n_0 ),
        .I5(is_uart_receiving_atten_value_reg_n_0),
        .O(\uart_tx_data[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \uart_tx_data[0]_i_9 
       (.I0(in_uart_data[0]),
        .I1(in_uart_data[1]),
        .O(\uart_tx_data[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEAFFEAFFEA)) 
    \uart_tx_data[1]_i_1 
       (.I0(\uart_tx_data[1]_i_2_n_0 ),
        .I1(\uart_tx_data[7]_i_6_n_0 ),
        .I2(pll_read_data[9]),
        .I3(\uart_tx_data[1]_i_3_n_0 ),
        .I4(\uart_tx_data[7]_i_5_n_0 ),
        .I5(pll_read_data[1]),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hAAFAAAAAAAEAAAAA)) 
    \uart_tx_data[1]_i_2 
       (.I0(\uart_tx_data[1]_i_4_n_0 ),
        .I1(in_uart_data[3]),
        .I2(\uart_tx_data[2]_i_6_n_0 ),
        .I3(\uart_tx_data[1]_i_5_n_0 ),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(\uart_tx_data[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0E04FFFF)) 
    \uart_tx_data[1]_i_3 
       (.I0(\pll_write_data[23]_i_4_n_0 ),
        .I1(pamp_en_reg_i_4_n_0),
        .I2(\uart_tx_data[2]_i_5_n_0 ),
        .I3(\uart_tx_data[1]_i_6_n_0 ),
        .I4(\uart_tx_data[2]_i_3_n_0 ),
        .I5(\uart_tx_data[1]_i_7_n_0 ),
        .O(\uart_tx_data[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h3030303230003032)) 
    \uart_tx_data[1]_i_4 
       (.I0(\uart_tx_data[1]_i_8_n_0 ),
        .I1(is_uart_receiving_pll_write_address_i_2_n_0),
        .I2(\uart_tx_data[1]_i_9_n_0 ),
        .I3(pll_write_i_2_n_0),
        .I4(\pll_write_data[23]_i_4_n_0 ),
        .I5(\uart_tx_data[1]_i_6_n_0 ),
        .O(\uart_tx_data[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \uart_tx_data[1]_i_5 
       (.I0(in_uart_data[5]),
        .I1(in_uart_data[6]),
        .I2(in_uart_data[7]),
        .I3(in_uart_data[4]),
        .O(\uart_tx_data[1]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hAB)) 
    \uart_tx_data[1]_i_6 
       (.I0(is_uart_receiving_pll_read_address),
        .I1(is_uart_receiving_pll_data_reg_n_0),
        .I2(is_uart_receiving_pll_write_address),
        .O(\uart_tx_data[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000BBFB0000)) 
    \uart_tx_data[1]_i_7 
       (.I0(uart_byte_received_prev),
        .I1(uart_byte_received),
        .I2(is_uart_receiving_pll_write_address_i_2_n_0),
        .I3(\pll_write_data[23]_i_4_n_0 ),
        .I4(pll_data_ready),
        .I5(pll_data_ready_prev),
        .O(\uart_tx_data[1]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \uart_tx_data[1]_i_8 
       (.I0(pll_read_data[17]),
        .I1(in_uart_data[1]),
        .I2(in_uart_data[0]),
        .O(\uart_tx_data[1]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \uart_tx_data[1]_i_9 
       (.I0(in_uart_data[1]),
        .I1(in_uart_data[0]),
        .I2(in_uart_data[2]),
        .O(\uart_tx_data[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEAFFEAFFEAFF)) 
    \uart_tx_data[2]_i_1 
       (.I0(\uart_tx_data[2]_i_2_n_0 ),
        .I1(\uart_tx_data[7]_i_6_n_0 ),
        .I2(pll_read_data[10]),
        .I3(\uart_tx_data[2]_i_3_n_0 ),
        .I4(\uart_tx_data[7]_i_7_n_0 ),
        .I5(pll_read_data[18]),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hF888888888888888)) 
    \uart_tx_data[2]_i_2 
       (.I0(pll_read_data[2]),
        .I1(\uart_tx_data[7]_i_5_n_0 ),
        .I2(\uart_tx_data[2]_i_4_n_0 ),
        .I3(in_uart_data[2]),
        .I4(\uart_tx_data[2]_i_5_n_0 ),
        .I5(\uart_tx_data[2]_i_6_n_0 ),
        .O(\uart_tx_data[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hF1FFFFFF)) 
    \uart_tx_data[2]_i_3 
       (.I0(\received_byte_num_reg_n_0_[0] ),
        .I1(\received_byte_num_reg_n_0_[1] ),
        .I2(uart_byte_received_prev),
        .I3(uart_byte_received),
        .I4(is_uart_receiving_atten_value_reg_n_0),
        .O(\uart_tx_data[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    \uart_tx_data[2]_i_4 
       (.I0(\uart_tx_data[2]_i_7_n_0 ),
        .I1(in_uart_data[3]),
        .I2(in_uart_data[5]),
        .I3(in_uart_data[6]),
        .I4(in_uart_data[7]),
        .I5(in_uart_data[4]),
        .O(\uart_tx_data[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \uart_tx_data[2]_i_5 
       (.I0(pll_data_ready_prev),
        .I1(pll_data_ready),
        .O(\uart_tx_data[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF1FFFFFFFFFF)) 
    \uart_tx_data[2]_i_6 
       (.I0(is_uart_receiving_pll_write_address),
        .I1(is_uart_receiving_pll_data_reg_n_0),
        .I2(is_uart_receiving_pll_read_address),
        .I3(\pll_write_data[23]_i_4_n_0 ),
        .I4(uart_byte_received_prev),
        .I5(uart_byte_received),
        .O(\uart_tx_data[2]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \uart_tx_data[2]_i_7 
       (.I0(in_uart_data[0]),
        .I1(in_uart_data[1]),
        .O(\uart_tx_data[2]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \uart_tx_data[3]_i_1 
       (.I0(\uart_tx_data[3]_i_2_n_0 ),
        .I1(\uart_tx_data[7]_i_6_n_0 ),
        .I2(pll_read_data[11]),
        .I3(pll_read_data[19]),
        .I4(\uart_tx_data[7]_i_7_n_0 ),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'h88888888F8F888F8)) 
    \uart_tx_data[3]_i_2 
       (.I0(pll_read_data[3]),
        .I1(\uart_tx_data[7]_i_5_n_0 ),
        .I2(\uart_tx_data[4]_i_5_n_0 ),
        .I3(pll_data_ready),
        .I4(pll_data_ready_prev),
        .I5(if_amp1_en_reg_i_2_n_0),
        .O(\uart_tx_data[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEAEAEAEAFAEAEAEA)) 
    \uart_tx_data[4]_i_1 
       (.I0(\uart_tx_data[4]_i_2_n_0 ),
        .I1(\uart_tx_data[4]_i_3_n_0 ),
        .I2(\uart_tx_data[4]_i_4_n_0 ),
        .I3(pll_read_data[20]),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000222)) 
    \uart_tx_data[4]_i_2 
       (.I0(\uart_tx_data[4]_i_5_n_0 ),
        .I1(in_uart_data[2]),
        .I2(in_uart_data[0]),
        .I3(in_uart_data[1]),
        .I4(is_uart_receiving_pll_write_address_i_2_n_0),
        .I5(\uart_tx_data[4]_i_6_n_0 ),
        .O(\uart_tx_data[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    \uart_tx_data[4]_i_3 
       (.I0(pll_read_data[4]),
        .I1(in_uart_data[0]),
        .I2(pll_read_data[12]),
        .I3(in_uart_data[1]),
        .O(\uart_tx_data[4]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \uart_tx_data[4]_i_4 
       (.I0(is_uart_receiving_pll_write_address_i_2_n_0),
        .I1(\received_byte_num_reg_n_0_[0] ),
        .I2(\received_byte_num_reg_n_0_[1] ),
        .I3(uart_byte_received_prev),
        .I4(uart_byte_received),
        .O(\uart_tx_data[4]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hFFFF4445)) 
    \uart_tx_data[4]_i_5 
       (.I0(is_uart_receiving_atten_value_reg_n_0),
        .I1(is_uart_receiving_pll_read_address),
        .I2(is_uart_receiving_pll_data_reg_n_0),
        .I3(is_uart_receiving_pll_write_address),
        .I4(\received_byte_num[0]_i_2_n_0 ),
        .O(\uart_tx_data[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCFCEEEE)) 
    \uart_tx_data[4]_i_6 
       (.I0(is_uart_receiving_pll_write_address_i_2_n_0),
        .I1(pll_write_i_2_n_0),
        .I2(\uart_tx_data[1]_i_6_n_0 ),
        .I3(is_uart_receiving_atten_value_reg_n_0),
        .I4(\pll_write_data[23]_i_4_n_0 ),
        .I5(\uart_tx_data[2]_i_5_n_0 ),
        .O(\uart_tx_data[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \uart_tx_data[5]_i_1 
       (.I0(\uart_tx_data[7]_i_5_n_0 ),
        .I1(pll_read_data[5]),
        .I2(\uart_tx_data[7]_i_6_n_0 ),
        .I3(pll_read_data[13]),
        .I4(pll_read_data[21]),
        .I5(\uart_tx_data[7]_i_7_n_0 ),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \uart_tx_data[6]_i_1 
       (.I0(\uart_tx_data[7]_i_5_n_0 ),
        .I1(pll_read_data[6]),
        .I2(\uart_tx_data[7]_i_6_n_0 ),
        .I3(pll_read_data[14]),
        .I4(pll_read_data[22]),
        .I5(\uart_tx_data[7]_i_7_n_0 ),
        .O(p_1_in[6]));
  LUT3 #(
    .INIT(8'hAE)) 
    \uart_tx_data[7]_i_1 
       (.I0(\uart_tx_data[7]_i_4_n_0 ),
        .I1(pll_data_ready),
        .I2(pll_data_ready_prev),
        .O(\uart_tx_data[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \uart_tx_data[7]_i_2 
       (.I0(\uart_tx_data[7]_i_5_n_0 ),
        .I1(pll_read_data[7]),
        .I2(\uart_tx_data[7]_i_6_n_0 ),
        .I3(pll_read_data[15]),
        .I4(pll_read_data[23]),
        .I5(\uart_tx_data[7]_i_7_n_0 ),
        .O(p_1_in[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \uart_tx_data[7]_i_3 
       (.I0(clk_wizard_main_n_4),
        .I1(clk_wizard_main_n_1),
        .O(uart_byte_received_prev0));
  LUT6 #(
    .INIT(64'hFFFFFFFF03020F0F)) 
    \uart_tx_data[7]_i_4 
       (.I0(in_uart_data[4]),
        .I1(sw_ctrl_reg_i_2_n_0),
        .I2(sw_if2_reg_i_2_n_0),
        .I3(in_uart_data[2]),
        .I4(if_amp1_en_reg_i_2_n_0),
        .I5(\uart_tx_data[7]_i_8_n_0 ),
        .O(\uart_tx_data[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \uart_tx_data[7]_i_5 
       (.I0(is_uart_receiving_pll_write_address_i_2_n_0),
        .I1(in_uart_data[2]),
        .I2(pll_write_i_2_n_0),
        .I3(\pll_write_data[23]_i_4_n_0 ),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(\uart_tx_data[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    \uart_tx_data[7]_i_6 
       (.I0(is_uart_receiving_pll_write_address_i_2_n_0),
        .I1(in_uart_data[2]),
        .I2(pll_write_i_2_n_0),
        .I3(\pll_write_data[23]_i_4_n_0 ),
        .I4(in_uart_data[1]),
        .I5(in_uart_data[0]),
        .O(\uart_tx_data[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    \uart_tx_data[7]_i_7 
       (.I0(\pll_write_data[23]_i_4_n_0 ),
        .I1(pll_write_i_2_n_0),
        .I2(in_uart_data[2]),
        .I3(in_uart_data[0]),
        .I4(in_uart_data[1]),
        .I5(is_uart_receiving_pll_write_address_i_2_n_0),
        .O(\uart_tx_data[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFAA00000303)) 
    \uart_tx_data[7]_i_8 
       (.I0(\uart_tx_data[7]_i_9_n_0 ),
        .I1(\uart_tx_data[0]_i_9_n_0 ),
        .I2(sw_ctrl_reg_i_2_n_0),
        .I3(is_uart_receiving_atten_value_reg_n_0),
        .I4(pll_write_i_2_n_0),
        .I5(\pll_write_data[23]_i_4_n_0 ),
        .O(\uart_tx_data[7]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \uart_tx_data[7]_i_9 
       (.I0(is_uart_receiving_pll_write_address),
        .I1(is_uart_receiving_pll_data_reg_n_0),
        .I2(is_uart_receiving_pll_read_address),
        .O(\uart_tx_data[7]_i_9_n_0 ));
  FDRE \uart_tx_data_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(out_data_uart[0]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(out_data_uart[1]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(out_data_uart[2]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(out_data_uart[3]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(out_data_uart[4]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(out_data_uart[5]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(out_data_uart[6]),
        .R(1'b0));
  FDRE \uart_tx_data_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(\uart_tx_data[7]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(out_data_uart[7]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 22:24:22 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_stub.vhdl
-- Design      : design_1_top_logic_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    in_uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk_100MHz_in : in STD_LOGIC;
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    data_ready_from_pll : in STD_LOGIC;
    pll_read_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 );
    in_sending : out STD_LOGIC;
    t_sweep : out STD_LOGIC;
    clk_100MHz_out : out STD_LOGIC;
    start_uart_send : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    pll_read : out STD_LOGIC;
    pll_write : out STD_LOGIC;
    pll_write_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_addres : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_addres : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "in_uart_data[7:0],clk_100MHz_in,in_adc_data[7:0],uart_adc_start_sending,uart_byte_received,data_ready_from_pll,pll_read_data[23:0],out_data_uart[7:0],in_sending,t_sweep,clk_100MHz_out,start_uart_send,clk_10MHz_out,start_init_clk,clk_20MHz_out,pll_read,pll_write,pll_write_data[23:0],pll_write_addres[4:0],pll_read_addres[7:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "top_logic,Vivado 2019.1";
begin
end;

// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 22:24:22 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_stub.v
// Design      : design_1_top_logic_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "top_logic,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(in_uart_data, clk_100MHz_in, in_adc_data, 
  uart_adc_start_sending, uart_byte_received, data_ready_from_pll, pll_read_data, 
  out_data_uart, in_sending, t_sweep, clk_100MHz_out, start_uart_send, clk_10MHz_out, 
  start_init_clk, clk_20MHz_out, pll_read, pll_write, pll_write_data, pll_write_addres, 
  pll_read_addres)
/* synthesis syn_black_box black_box_pad_pin="in_uart_data[7:0],clk_100MHz_in,in_adc_data[7:0],uart_adc_start_sending,uart_byte_received,data_ready_from_pll,pll_read_data[23:0],out_data_uart[7:0],in_sending,t_sweep,clk_100MHz_out,start_uart_send,clk_10MHz_out,start_init_clk,clk_20MHz_out,pll_read,pll_write,pll_write_data[23:0],pll_write_addres[4:0],pll_read_addres[7:0]" */;
  input [7:0]in_uart_data;
  input clk_100MHz_in;
  input [7:0]in_adc_data;
  input uart_adc_start_sending;
  input uart_byte_received;
  input data_ready_from_pll;
  input [23:0]pll_read_data;
  output [7:0]out_data_uart;
  output in_sending;
  output t_sweep;
  output clk_100MHz_out;
  output start_uart_send;
  output clk_10MHz_out;
  output start_init_clk;
  output clk_20MHz_out;
  output pll_read;
  output pll_write;
  output [23:0]pll_write_data;
  output [4:0]pll_write_addres;
  output [7:0]pll_read_addres;
endmodule

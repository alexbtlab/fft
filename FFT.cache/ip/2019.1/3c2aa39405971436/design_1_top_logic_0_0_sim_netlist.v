// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun  3 10:41:14 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_sim_netlist.v
// Design      : design_1_top_logic_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire clk_out4;
  wire locked;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .clk_out4(clk_out4),
        .locked(locked));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_out4,
    locked,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output clk_out4;
  output locked;
  input clk_in1;

  wire clk_in1;
  wire clk_in1_clk_wiz_0;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clk_out4;
  wire clk_out4_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire locked;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout4_buf
       (.I(clk_out4_clk_wiz_0),
        .O(clk_out4));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(10.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(50),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(100),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(125),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(clk_out3_clk_wiz_0),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(clk_out4_clk_wiz_0),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_top_logic_0_0,top_logic,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "top_logic,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (in_uart_data,
    clk_100MHz_in,
    in_adc_data,
    uart_adc_start_sending,
    pll_data_ready,
    uart_byte_received,
    pll_read_data,
    out_data_uart,
    t_sweep,
    clk_100MHz_out,
    user_led,
    pamp_en,
    sw_ls,
    if_amp1_stu_en,
    if_amp2_stu_en,
    sw_if1,
    sw_if2,
    sw_if3,
    clk_sync,
    pll_trig,
    sw_ctrl,
    atten,
    start_uart_send,
    is_adc_data_sending,
    clk_10MHz_out,
    start_init_clk,
    clk_20MHz_out,
    pll_read,
    pll_write,
    pll_write_data,
    pll_write_addres,
    pll_read_addres);
  input [7:0]in_uart_data;
  input clk_100MHz_in;
  input [7:0]in_adc_data;
  input uart_adc_start_sending;
  input pll_data_ready;
  input uart_byte_received;
  input [23:0]pll_read_data;
  output [7:0]out_data_uart;
  output t_sweep;
  output clk_100MHz_out;
  output user_led;
  output pamp_en;
  output sw_ls;
  output if_amp1_stu_en;
  output if_amp2_stu_en;
  output sw_if1;
  output sw_if2;
  output sw_if3;
  output clk_sync;
  output pll_trig;
  output sw_ctrl;
  output [5:0]atten;
  output start_uart_send;
  output is_adc_data_sending;
  output clk_10MHz_out;
  output start_init_clk;
  output clk_20MHz_out;
  output pll_read;
  output pll_write;
  output [23:0]pll_write_data;
  output [4:0]pll_write_addres;
  output [7:0]pll_read_addres;

  wire \<const0> ;
  wire \<const1> ;
  wire [4:4]\^atten ;
  (* IBUF_LOW_PWR *) wire clk_100MHz_in;
  wire clk_100MHz_out;
  wire clk_10MHz_out;
  wire clk_20MHz_out;
  wire [4:4]\^out_data_uart ;
  wire pll_data_ready;
  wire pll_trig;
  wire start_init_clk;
  wire start_uart_send;
  wire t_sweep;
  wire uart_adc_start_sending;
  wire uart_byte_received;

  assign atten[5] = \^atten [4];
  assign atten[4] = \^atten [4];
  assign atten[3] = \^atten [4];
  assign atten[2] = \^atten [4];
  assign atten[1] = \^atten [4];
  assign atten[0] = \^atten [4];
  assign clk_sync = start_init_clk;
  assign if_amp1_stu_en = \<const0> ;
  assign if_amp2_stu_en = \<const0> ;
  assign is_adc_data_sending = \<const0> ;
  assign out_data_uart[7] = \<const0> ;
  assign out_data_uart[6] = \<const0> ;
  assign out_data_uart[5] = \<const0> ;
  assign out_data_uart[4] = \^out_data_uart [4];
  assign out_data_uart[3] = \<const0> ;
  assign out_data_uart[2] = \<const0> ;
  assign out_data_uart[1] = \^out_data_uart [4];
  assign out_data_uart[0] = \<const1> ;
  assign pamp_en = \<const0> ;
  assign pll_read = \<const0> ;
  assign pll_read_addres[7] = \<const0> ;
  assign pll_read_addres[6] = \<const0> ;
  assign pll_read_addres[5] = \<const0> ;
  assign pll_read_addres[4] = \<const0> ;
  assign pll_read_addres[3] = \<const0> ;
  assign pll_read_addres[2] = \<const0> ;
  assign pll_read_addres[1] = \<const0> ;
  assign pll_read_addres[0] = \<const0> ;
  assign pll_write = \<const0> ;
  assign pll_write_addres[4] = \<const0> ;
  assign pll_write_addres[3] = \<const0> ;
  assign pll_write_addres[2] = \<const0> ;
  assign pll_write_addres[1] = \<const0> ;
  assign pll_write_addres[0] = \<const0> ;
  assign pll_write_data[23] = \<const0> ;
  assign pll_write_data[22] = \<const0> ;
  assign pll_write_data[21] = \<const0> ;
  assign pll_write_data[20] = \<const0> ;
  assign pll_write_data[19] = \<const0> ;
  assign pll_write_data[18] = \<const0> ;
  assign pll_write_data[17] = \<const0> ;
  assign pll_write_data[16] = \<const0> ;
  assign pll_write_data[15] = \<const0> ;
  assign pll_write_data[14] = \<const0> ;
  assign pll_write_data[13] = \<const0> ;
  assign pll_write_data[12] = \<const0> ;
  assign pll_write_data[11] = \<const0> ;
  assign pll_write_data[10] = \<const0> ;
  assign pll_write_data[9] = \<const0> ;
  assign pll_write_data[8] = \<const0> ;
  assign pll_write_data[7] = \<const0> ;
  assign pll_write_data[6] = \<const0> ;
  assign pll_write_data[5] = \<const0> ;
  assign pll_write_data[4] = \<const0> ;
  assign pll_write_data[3] = \<const0> ;
  assign pll_write_data[2] = \<const0> ;
  assign pll_write_data[1] = \<const0> ;
  assign pll_write_data[0] = \<const0> ;
  assign sw_ctrl = \<const0> ;
  assign sw_if1 = \<const1> ;
  assign sw_if2 = \<const0> ;
  assign sw_if3 = \<const0> ;
  assign sw_ls = \<const1> ;
  assign user_led = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic inst
       (.atten(\^atten ),
        .clk_100MHz_in(clk_100MHz_in),
        .clk_100MHz_out(clk_100MHz_out),
        .clk_10MHz_out(clk_10MHz_out),
        .clk_20MHz_out(clk_20MHz_out),
        .out_data_uart(\^out_data_uart ),
        .pll_data_ready(pll_data_ready),
        .pll_trig(pll_trig),
        .start_init_clk(start_init_clk),
        .start_uart_send(start_uart_send),
        .t_sweep(t_sweep),
        .uart_adc_start_sending(uart_adc_start_sending),
        .uart_byte_received(uart_byte_received));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic
   (clk_100MHz_out,
    clk_20MHz_out,
    clk_10MHz_out,
    atten,
    t_sweep,
    start_init_clk,
    start_uart_send,
    out_data_uart,
    pll_trig,
    clk_100MHz_in,
    uart_byte_received,
    pll_data_ready,
    uart_adc_start_sending);
  output clk_100MHz_out;
  output clk_20MHz_out;
  output clk_10MHz_out;
  output [0:0]atten;
  output t_sweep;
  output start_init_clk;
  output start_uart_send;
  output [0:0]out_data_uart;
  output pll_trig;
  input clk_100MHz_in;
  input uart_byte_received;
  input pll_data_ready;
  input uart_adc_start_sending;

  wire [0:0]atten;
  wire clk_100MHz_in;
  wire clk_100MHz_out;
  wire clk_10MHz_out;
  wire clk_20MHz_out;
  wire clk_sync_counter_reg;
  wire [31:1]clk_sync_counter_reg0;
  wire clk_sync_counter_reg0_carry__0_n_0;
  wire clk_sync_counter_reg0_carry__0_n_1;
  wire clk_sync_counter_reg0_carry__0_n_2;
  wire clk_sync_counter_reg0_carry__0_n_3;
  wire clk_sync_counter_reg0_carry__1_n_0;
  wire clk_sync_counter_reg0_carry__1_n_1;
  wire clk_sync_counter_reg0_carry__1_n_2;
  wire clk_sync_counter_reg0_carry__1_n_3;
  wire clk_sync_counter_reg0_carry__2_n_0;
  wire clk_sync_counter_reg0_carry__2_n_1;
  wire clk_sync_counter_reg0_carry__2_n_2;
  wire clk_sync_counter_reg0_carry__2_n_3;
  wire clk_sync_counter_reg0_carry__3_n_0;
  wire clk_sync_counter_reg0_carry__3_n_1;
  wire clk_sync_counter_reg0_carry__3_n_2;
  wire clk_sync_counter_reg0_carry__3_n_3;
  wire clk_sync_counter_reg0_carry__4_n_0;
  wire clk_sync_counter_reg0_carry__4_n_1;
  wire clk_sync_counter_reg0_carry__4_n_2;
  wire clk_sync_counter_reg0_carry__4_n_3;
  wire clk_sync_counter_reg0_carry__5_n_0;
  wire clk_sync_counter_reg0_carry__5_n_1;
  wire clk_sync_counter_reg0_carry__5_n_2;
  wire clk_sync_counter_reg0_carry__5_n_3;
  wire clk_sync_counter_reg0_carry__6_n_2;
  wire clk_sync_counter_reg0_carry__6_n_3;
  wire clk_sync_counter_reg0_carry_n_0;
  wire clk_sync_counter_reg0_carry_n_1;
  wire clk_sync_counter_reg0_carry_n_2;
  wire clk_sync_counter_reg0_carry_n_3;
  wire \clk_sync_counter_reg[0]_i_10_n_0 ;
  wire \clk_sync_counter_reg[0]_i_11_n_0 ;
  wire \clk_sync_counter_reg[0]_i_12_n_0 ;
  wire \clk_sync_counter_reg[0]_i_13_n_0 ;
  wire \clk_sync_counter_reg[0]_i_14_n_0 ;
  wire \clk_sync_counter_reg[0]_i_3_n_0 ;
  wire \clk_sync_counter_reg[0]_i_4_n_0 ;
  wire \clk_sync_counter_reg[0]_i_5_n_0 ;
  wire \clk_sync_counter_reg[0]_i_6_n_0 ;
  wire \clk_sync_counter_reg[0]_i_7_n_0 ;
  wire \clk_sync_counter_reg[0]_i_8_n_0 ;
  wire \clk_sync_counter_reg[0]_i_9_n_0 ;
  wire \clk_sync_counter_reg[12]_i_2_n_0 ;
  wire \clk_sync_counter_reg[12]_i_3_n_0 ;
  wire \clk_sync_counter_reg[12]_i_4_n_0 ;
  wire \clk_sync_counter_reg[12]_i_5_n_0 ;
  wire \clk_sync_counter_reg[16]_i_2_n_0 ;
  wire \clk_sync_counter_reg[16]_i_3_n_0 ;
  wire \clk_sync_counter_reg[16]_i_4_n_0 ;
  wire \clk_sync_counter_reg[16]_i_5_n_0 ;
  wire \clk_sync_counter_reg[16]_i_6_n_0 ;
  wire \clk_sync_counter_reg[16]_i_7_n_0 ;
  wire \clk_sync_counter_reg[16]_i_8_n_0 ;
  wire \clk_sync_counter_reg[16]_i_9_n_0 ;
  wire \clk_sync_counter_reg[20]_i_2_n_0 ;
  wire \clk_sync_counter_reg[20]_i_3_n_0 ;
  wire \clk_sync_counter_reg[20]_i_4_n_0 ;
  wire \clk_sync_counter_reg[20]_i_5_n_0 ;
  wire \clk_sync_counter_reg[24]_i_2_n_0 ;
  wire \clk_sync_counter_reg[24]_i_3_n_0 ;
  wire \clk_sync_counter_reg[24]_i_4_n_0 ;
  wire \clk_sync_counter_reg[24]_i_5_n_0 ;
  wire \clk_sync_counter_reg[28]_i_2_n_0 ;
  wire \clk_sync_counter_reg[28]_i_3_n_0 ;
  wire \clk_sync_counter_reg[28]_i_4_n_0 ;
  wire \clk_sync_counter_reg[28]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_2_n_0 ;
  wire \clk_sync_counter_reg[4]_i_3_n_0 ;
  wire \clk_sync_counter_reg[4]_i_4_n_0 ;
  wire \clk_sync_counter_reg[4]_i_5_n_0 ;
  wire \clk_sync_counter_reg[4]_i_6_n_0 ;
  wire \clk_sync_counter_reg[4]_i_7_n_0 ;
  wire \clk_sync_counter_reg[8]_i_2_n_0 ;
  wire \clk_sync_counter_reg[8]_i_3_n_0 ;
  wire \clk_sync_counter_reg[8]_i_4_n_0 ;
  wire \clk_sync_counter_reg[8]_i_5_n_0 ;
  wire [31:0]clk_sync_counter_reg_reg;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_0 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_1 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_2 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_3 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_4 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_5 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_6 ;
  wire \clk_sync_counter_reg_reg[0]_i_2_n_7 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[12]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[16]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[20]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[24]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[28]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[4]_i_1_n_7 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_0 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_1 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_2 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_3 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_4 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_5 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_6 ;
  wire \clk_sync_counter_reg_reg[8]_i_1_n_7 ;
  wire clk_wizard_main_n_1;
  wire clk_wizard_main_n_4;
  wire [3:0]cnt_uart;
  wire \cnt_uart[0]_i_1_n_0 ;
  wire \cnt_uart[1]_i_1_n_0 ;
  wire \cnt_uart[2]_i_1_n_0 ;
  wire \cnt_uart[3]_i_1_n_0 ;
  wire \cnt_uart[3]_i_2_n_0 ;
  wire is_clk_initialized_reg;
  wire is_clk_initialized_reg_i_1_n_0;
  wire is_counting;
  wire is_counting_i_1_n_0;
  wire is_counting_i_2_n_0;
  wire [0:0]out_data_uart;
  wire [16:1]p_2_in;
  wire pll_data_ready;
  wire pll_data_ready_prev;
  wire pll_trig;
  wire pll_trig_reg_i_1_n_0;
  wire start_init_clk;
  wire start_init_clk_reg_i_10_n_0;
  wire start_init_clk_reg_i_11_n_0;
  wire start_init_clk_reg_i_12_n_0;
  wire start_init_clk_reg_i_13_n_0;
  wire start_init_clk_reg_i_14_n_0;
  wire start_init_clk_reg_i_15_n_0;
  wire start_init_clk_reg_i_16_n_0;
  wire start_init_clk_reg_i_17_n_0;
  wire start_init_clk_reg_i_18_n_0;
  wire start_init_clk_reg_i_1_n_0;
  wire start_init_clk_reg_i_2_n_0;
  wire start_init_clk_reg_i_3_n_0;
  wire start_init_clk_reg_i_4_n_0;
  wire start_init_clk_reg_i_5_n_0;
  wire start_init_clk_reg_i_6_n_0;
  wire start_init_clk_reg_i_7_n_0;
  wire start_init_clk_reg_i_8_n_0;
  wire start_init_clk_reg_i_9_n_0;
  wire start_uart_send;
  wire t_sweep;
  wire t_sweep_INST_0_i_1_n_0;
  wire t_sweep_INST_0_i_2_n_0;
  wire t_sweep_INST_0_i_3_n_0;
  wire t_sweep_INST_0_i_4_n_0;
  wire trig_tguard_counter;
  wire \trig_tguard_counter[0]_i_1_n_0 ;
  wire \trig_tguard_counter[0]_i_2_n_0 ;
  wire \trig_tguard_counter[0]_i_3_n_0 ;
  wire \trig_tguard_counter[16]_i_10_n_0 ;
  wire \trig_tguard_counter[16]_i_11_n_0 ;
  wire \trig_tguard_counter[16]_i_12_n_0 ;
  wire \trig_tguard_counter[16]_i_13_n_0 ;
  wire \trig_tguard_counter[16]_i_1_n_0 ;
  wire \trig_tguard_counter[16]_i_4_n_0 ;
  wire \trig_tguard_counter[16]_i_5_n_0 ;
  wire \trig_tguard_counter[16]_i_6_n_0 ;
  wire \trig_tguard_counter[16]_i_7_n_0 ;
  wire \trig_tguard_counter[16]_i_8_n_0 ;
  wire \trig_tguard_counter[16]_i_9_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_1 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_2 ;
  wire \trig_tguard_counter_reg[16]_i_3_n_3 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_3 ;
  wire \trig_tguard_counter_reg_n_0_[0] ;
  wire \trig_tguard_counter_reg_n_0_[10] ;
  wire \trig_tguard_counter_reg_n_0_[11] ;
  wire \trig_tguard_counter_reg_n_0_[12] ;
  wire \trig_tguard_counter_reg_n_0_[13] ;
  wire \trig_tguard_counter_reg_n_0_[14] ;
  wire \trig_tguard_counter_reg_n_0_[15] ;
  wire \trig_tguard_counter_reg_n_0_[16] ;
  wire \trig_tguard_counter_reg_n_0_[1] ;
  wire \trig_tguard_counter_reg_n_0_[2] ;
  wire \trig_tguard_counter_reg_n_0_[3] ;
  wire \trig_tguard_counter_reg_n_0_[4] ;
  wire \trig_tguard_counter_reg_n_0_[5] ;
  wire \trig_tguard_counter_reg_n_0_[6] ;
  wire \trig_tguard_counter_reg_n_0_[7] ;
  wire \trig_tguard_counter_reg_n_0_[8] ;
  wire \trig_tguard_counter_reg_n_0_[9] ;
  wire trig_tsweep_counter079_out;
  wire \trig_tsweep_counter[0]_i_1_n_0 ;
  wire \trig_tsweep_counter[0]_i_4_n_0 ;
  wire \trig_tsweep_counter[0]_i_5_n_0 ;
  wire \trig_tsweep_counter[0]_i_6_n_0 ;
  wire \trig_tsweep_counter[0]_i_7_n_0 ;
  wire \trig_tsweep_counter[0]_i_8_n_0 ;
  wire [16:0]trig_tsweep_counter_reg;
  wire \trig_tsweep_counter_reg[0]_i_3_n_0 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_1 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_2 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_3 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_4 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_5 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_6 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_7 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[16]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_7 ;
  wire uart_adc_start_sending;
  wire uart_byte_received;
  wire uart_byte_received_prev;
  wire uart_byte_received_prev0;
  wire uart_start_sending_i_1_n_0;
  wire uart_start_sending_reg_n_0;
  wire \uart_tx_data[4]_i_1_n_0 ;
  wire [3:2]NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED;
  wire [3:3]\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED ;
  wire [3:0]\NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED ;

  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(1'b0),
        .D(1'b0),
        .Q(atten),
        .R(1'b0));
  CARRY4 clk_sync_counter_reg0_carry
       (.CI(1'b0),
        .CO({clk_sync_counter_reg0_carry_n_0,clk_sync_counter_reg0_carry_n_1,clk_sync_counter_reg0_carry_n_2,clk_sync_counter_reg0_carry_n_3}),
        .CYINIT(clk_sync_counter_reg_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[4:1]),
        .S(clk_sync_counter_reg_reg[4:1]));
  CARRY4 clk_sync_counter_reg0_carry__0
       (.CI(clk_sync_counter_reg0_carry_n_0),
        .CO({clk_sync_counter_reg0_carry__0_n_0,clk_sync_counter_reg0_carry__0_n_1,clk_sync_counter_reg0_carry__0_n_2,clk_sync_counter_reg0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[8:5]),
        .S(clk_sync_counter_reg_reg[8:5]));
  CARRY4 clk_sync_counter_reg0_carry__1
       (.CI(clk_sync_counter_reg0_carry__0_n_0),
        .CO({clk_sync_counter_reg0_carry__1_n_0,clk_sync_counter_reg0_carry__1_n_1,clk_sync_counter_reg0_carry__1_n_2,clk_sync_counter_reg0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[12:9]),
        .S(clk_sync_counter_reg_reg[12:9]));
  CARRY4 clk_sync_counter_reg0_carry__2
       (.CI(clk_sync_counter_reg0_carry__1_n_0),
        .CO({clk_sync_counter_reg0_carry__2_n_0,clk_sync_counter_reg0_carry__2_n_1,clk_sync_counter_reg0_carry__2_n_2,clk_sync_counter_reg0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[16:13]),
        .S(clk_sync_counter_reg_reg[16:13]));
  CARRY4 clk_sync_counter_reg0_carry__3
       (.CI(clk_sync_counter_reg0_carry__2_n_0),
        .CO({clk_sync_counter_reg0_carry__3_n_0,clk_sync_counter_reg0_carry__3_n_1,clk_sync_counter_reg0_carry__3_n_2,clk_sync_counter_reg0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[20:17]),
        .S(clk_sync_counter_reg_reg[20:17]));
  CARRY4 clk_sync_counter_reg0_carry__4
       (.CI(clk_sync_counter_reg0_carry__3_n_0),
        .CO({clk_sync_counter_reg0_carry__4_n_0,clk_sync_counter_reg0_carry__4_n_1,clk_sync_counter_reg0_carry__4_n_2,clk_sync_counter_reg0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[24:21]),
        .S(clk_sync_counter_reg_reg[24:21]));
  CARRY4 clk_sync_counter_reg0_carry__5
       (.CI(clk_sync_counter_reg0_carry__4_n_0),
        .CO({clk_sync_counter_reg0_carry__5_n_0,clk_sync_counter_reg0_carry__5_n_1,clk_sync_counter_reg0_carry__5_n_2,clk_sync_counter_reg0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(clk_sync_counter_reg0[28:25]),
        .S(clk_sync_counter_reg_reg[28:25]));
  CARRY4 clk_sync_counter_reg0_carry__6
       (.CI(clk_sync_counter_reg0_carry__5_n_0),
        .CO({NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED[3:2],clk_sync_counter_reg0_carry__6_n_2,clk_sync_counter_reg0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED[3],clk_sync_counter_reg0[31:29]}),
        .S({1'b0,clk_sync_counter_reg_reg[31:29]}));
  LUT2 #(
    .INIT(4'h1)) 
    \clk_sync_counter_reg[0]_i_1 
       (.I0(is_clk_initialized_reg),
        .I1(start_init_clk_reg_i_3_n_0),
        .O(clk_sync_counter_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000002)) 
    \clk_sync_counter_reg[0]_i_10 
       (.I0(start_init_clk_reg_i_14_n_0),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[14]),
        .I4(clk_sync_counter_reg_reg[13]),
        .I5(\clk_sync_counter_reg[0]_i_14_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    \clk_sync_counter_reg[0]_i_11 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_sync_counter_reg_reg[12]),
        .I5(clk_sync_counter_reg_reg[10]),
        .O(\clk_sync_counter_reg[0]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \clk_sync_counter_reg[0]_i_12 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0660)) 
    \clk_sync_counter_reg[0]_i_13 
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[7]),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[0]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \clk_sync_counter_reg[0]_i_14 
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .O(\clk_sync_counter_reg[0]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_3 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(clk_sync_counter_reg0[3]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_4 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[2]),
        .I2(clk_sync_counter_reg0[2]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[0]_i_5 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[1]),
        .I2(clk_sync_counter_reg0[1]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \clk_sync_counter_reg[0]_i_6 
       (.I0(clk_sync_counter_reg_reg[0]),
        .O(\clk_sync_counter_reg[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFF77FF07)) 
    \clk_sync_counter_reg[0]_i_7 
       (.I0(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I2(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I3(start_init_clk_reg_i_5_n_0),
        .I4(clk_sync_counter_reg_reg[17]),
        .O(\clk_sync_counter_reg[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h08200000)) 
    \clk_sync_counter_reg[0]_i_8 
       (.I0(\clk_sync_counter_reg[0]_i_11_n_0 ),
        .I1(clk_sync_counter_reg_reg[10]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[9]),
        .I4(\clk_sync_counter_reg[0]_i_12_n_0 ),
        .O(\clk_sync_counter_reg[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000101000)) 
    \clk_sync_counter_reg[0]_i_9 
       (.I0(start_init_clk_reg_i_12_n_0),
        .I1(start_init_clk_reg_i_10_n_0),
        .I2(\clk_sync_counter_reg[0]_i_13_n_0 ),
        .I3(clk_sync_counter_reg_reg[17]),
        .I4(clk_sync_counter_reg_reg[16]),
        .I5(clk_sync_counter_reg_reg[3]),
        .O(\clk_sync_counter_reg[0]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[12]_i_2 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg0[15]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_3 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[14]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[14]),
        .O(\clk_sync_counter_reg[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[13]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[13]),
        .O(\clk_sync_counter_reg[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[12]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[12]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[12]),
        .O(\clk_sync_counter_reg[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[16]_i_2 
       (.I0(clk_sync_counter_reg0[19]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[16]_i_3 
       (.I0(clk_sync_counter_reg0[18]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hC8)) 
    \clk_sync_counter_reg[16]_i_4 
       (.I0(clk_sync_counter_reg0[17]),
        .I1(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I2(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFF004E4E)) 
    \clk_sync_counter_reg[16]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(clk_sync_counter_reg0[16]),
        .I2(\clk_sync_counter_reg[16]_i_6_n_0 ),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(start_init_clk_reg_i_2_n_0),
        .O(\clk_sync_counter_reg[16]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \clk_sync_counter_reg[16]_i_6 
       (.I0(\clk_sync_counter_reg[16]_i_7_n_0 ),
        .I1(\clk_sync_counter_reg[16]_i_8_n_0 ),
        .I2(\clk_sync_counter_reg[16]_i_9_n_0 ),
        .I3(start_init_clk_reg_i_16_n_0),
        .I4(start_init_clk_reg_i_10_n_0),
        .O(\clk_sync_counter_reg[16]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \clk_sync_counter_reg[16]_i_7 
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[16]),
        .I3(clk_sync_counter_reg_reg[15]),
        .O(\clk_sync_counter_reg[16]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \clk_sync_counter_reg[16]_i_8 
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[10]),
        .O(\clk_sync_counter_reg[16]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0100)) 
    \clk_sync_counter_reg[16]_i_9 
       (.I0(clk_sync_counter_reg_reg[4]),
        .I1(clk_sync_counter_reg_reg[3]),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_sync_counter_reg_reg[8]),
        .O(\clk_sync_counter_reg[16]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_2 
       (.I0(clk_sync_counter_reg0[23]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_3 
       (.I0(clk_sync_counter_reg0[22]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_4 
       (.I0(clk_sync_counter_reg0[21]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[20]_i_5 
       (.I0(clk_sync_counter_reg0[20]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_2 
       (.I0(clk_sync_counter_reg0[27]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_3 
       (.I0(clk_sync_counter_reg0[26]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_4 
       (.I0(clk_sync_counter_reg0[25]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[24]_i_5 
       (.I0(clk_sync_counter_reg0[24]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_2 
       (.I0(clk_sync_counter_reg0[31]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_3 
       (.I0(clk_sync_counter_reg0[30]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_4 
       (.I0(clk_sync_counter_reg0[29]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \clk_sync_counter_reg[28]_i_5 
       (.I0(clk_sync_counter_reg0[28]),
        .I1(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[28]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[4]_i_2 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[7]),
        .I3(clk_sync_counter_reg0[7]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[4]_i_3 
       (.I0(clk_sync_counter_reg_reg[6]),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg0[6]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[4]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg0[5]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[4]_i_5 
       (.I0(start_init_clk_reg_i_2_n_0),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(clk_sync_counter_reg0[4]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF8FAFCFFFCFFFCFF)) 
    \clk_sync_counter_reg[4]_i_6 
       (.I0(\clk_sync_counter_reg[16]_i_6_n_0 ),
        .I1(clk_sync_counter_reg_reg[17]),
        .I2(start_init_clk_reg_i_5_n_0),
        .I3(\clk_sync_counter_reg[0]_i_10_n_0 ),
        .I4(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I5(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .O(\clk_sync_counter_reg[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \clk_sync_counter_reg[4]_i_7 
       (.I0(\clk_sync_counter_reg[0]_i_9_n_0 ),
        .I1(\clk_sync_counter_reg[0]_i_8_n_0 ),
        .I2(start_init_clk_reg_i_5_n_0),
        .O(\clk_sync_counter_reg[4]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \clk_sync_counter_reg[8]_i_2 
       (.I0(clk_sync_counter_reg_reg[11]),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg0[11]),
        .I3(\clk_sync_counter_reg[0]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[8]_i_3 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[10]),
        .I3(clk_sync_counter_reg0[10]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h5151F351)) 
    \clk_sync_counter_reg[8]_i_4 
       (.I0(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[9]),
        .I3(clk_sync_counter_reg0[9]),
        .I4(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .O(\clk_sync_counter_reg[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFC0EAC0)) 
    \clk_sync_counter_reg[8]_i_5 
       (.I0(\clk_sync_counter_reg[4]_i_7_n_0 ),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(\clk_sync_counter_reg[4]_i_6_n_0 ),
        .I4(clk_sync_counter_reg0[8]),
        .O(\clk_sync_counter_reg[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_7 ),
        .Q(clk_sync_counter_reg_reg[0]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\clk_sync_counter_reg_reg[0]_i_2_n_0 ,\clk_sync_counter_reg_reg[0]_i_2_n_1 ,\clk_sync_counter_reg_reg[0]_i_2_n_2 ,\clk_sync_counter_reg_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,start_init_clk_reg_i_2_n_0}),
        .O({\clk_sync_counter_reg_reg[0]_i_2_n_4 ,\clk_sync_counter_reg_reg[0]_i_2_n_5 ,\clk_sync_counter_reg_reg[0]_i_2_n_6 ,\clk_sync_counter_reg_reg[0]_i_2_n_7 }),
        .S({\clk_sync_counter_reg[0]_i_3_n_0 ,\clk_sync_counter_reg[0]_i_4_n_0 ,\clk_sync_counter_reg[0]_i_5_n_0 ,\clk_sync_counter_reg[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[12]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[12]_i_1 
       (.CI(\clk_sync_counter_reg_reg[8]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[12]_i_1_n_0 ,\clk_sync_counter_reg_reg[12]_i_1_n_1 ,\clk_sync_counter_reg_reg[12]_i_1_n_2 ,\clk_sync_counter_reg_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[12]_i_1_n_4 ,\clk_sync_counter_reg_reg[12]_i_1_n_5 ,\clk_sync_counter_reg_reg[12]_i_1_n_6 ,\clk_sync_counter_reg_reg[12]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[12]_i_2_n_0 ,\clk_sync_counter_reg[12]_i_3_n_0 ,\clk_sync_counter_reg[12]_i_4_n_0 ,\clk_sync_counter_reg[12]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[12]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[16]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[16]_i_1 
       (.CI(\clk_sync_counter_reg_reg[12]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[16]_i_1_n_0 ,\clk_sync_counter_reg_reg[16]_i_1_n_1 ,\clk_sync_counter_reg_reg[16]_i_1_n_2 ,\clk_sync_counter_reg_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[16]_i_1_n_4 ,\clk_sync_counter_reg_reg[16]_i_1_n_5 ,\clk_sync_counter_reg_reg[16]_i_1_n_6 ,\clk_sync_counter_reg_reg[16]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[16]_i_2_n_0 ,\clk_sync_counter_reg[16]_i_3_n_0 ,\clk_sync_counter_reg[16]_i_4_n_0 ,\clk_sync_counter_reg[16]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[17] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[18] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[19] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[16]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_6 ),
        .Q(clk_sync_counter_reg_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[20] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[20]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[20]_i_1 
       (.CI(\clk_sync_counter_reg_reg[16]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[20]_i_1_n_0 ,\clk_sync_counter_reg_reg[20]_i_1_n_1 ,\clk_sync_counter_reg_reg[20]_i_1_n_2 ,\clk_sync_counter_reg_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[20]_i_1_n_4 ,\clk_sync_counter_reg_reg[20]_i_1_n_5 ,\clk_sync_counter_reg_reg[20]_i_1_n_6 ,\clk_sync_counter_reg_reg[20]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[20]_i_2_n_0 ,\clk_sync_counter_reg[20]_i_3_n_0 ,\clk_sync_counter_reg[20]_i_4_n_0 ,\clk_sync_counter_reg[20]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[21] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[22] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[23] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[20]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[24] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[24]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[24]_i_1 
       (.CI(\clk_sync_counter_reg_reg[20]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[24]_i_1_n_0 ,\clk_sync_counter_reg_reg[24]_i_1_n_1 ,\clk_sync_counter_reg_reg[24]_i_1_n_2 ,\clk_sync_counter_reg_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[24]_i_1_n_4 ,\clk_sync_counter_reg_reg[24]_i_1_n_5 ,\clk_sync_counter_reg_reg[24]_i_1_n_6 ,\clk_sync_counter_reg_reg[24]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[24]_i_2_n_0 ,\clk_sync_counter_reg[24]_i_3_n_0 ,\clk_sync_counter_reg[24]_i_4_n_0 ,\clk_sync_counter_reg[24]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[25] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[26] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[27] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[24]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[28] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[28]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[28]_i_1 
       (.CI(\clk_sync_counter_reg_reg[24]_i_1_n_0 ),
        .CO({\NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED [3],\clk_sync_counter_reg_reg[28]_i_1_n_1 ,\clk_sync_counter_reg_reg[28]_i_1_n_2 ,\clk_sync_counter_reg_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[28]_i_1_n_4 ,\clk_sync_counter_reg_reg[28]_i_1_n_5 ,\clk_sync_counter_reg_reg[28]_i_1_n_6 ,\clk_sync_counter_reg_reg[28]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[28]_i_2_n_0 ,\clk_sync_counter_reg[28]_i_3_n_0 ,\clk_sync_counter_reg[28]_i_4_n_0 ,\clk_sync_counter_reg[28]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[29] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_5 ),
        .Q(clk_sync_counter_reg_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[30] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[31] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[28]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[0]_i_2_n_4 ),
        .Q(clk_sync_counter_reg_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[4]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[4]_i_1 
       (.CI(\clk_sync_counter_reg_reg[0]_i_2_n_0 ),
        .CO({\clk_sync_counter_reg_reg[4]_i_1_n_0 ,\clk_sync_counter_reg_reg[4]_i_1_n_1 ,\clk_sync_counter_reg_reg[4]_i_1_n_2 ,\clk_sync_counter_reg_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[4]_i_1_n_4 ,\clk_sync_counter_reg_reg[4]_i_1_n_5 ,\clk_sync_counter_reg_reg[4]_i_1_n_6 ,\clk_sync_counter_reg_reg[4]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[4]_i_2_n_0 ,\clk_sync_counter_reg[4]_i_3_n_0 ,\clk_sync_counter_reg[4]_i_4_n_0 ,\clk_sync_counter_reg[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_5 ),
        .Q(clk_sync_counter_reg_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[4]_i_1_n_4 ),
        .Q(clk_sync_counter_reg_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_7 ),
        .Q(clk_sync_counter_reg_reg[8]),
        .R(1'b0));
  CARRY4 \clk_sync_counter_reg_reg[8]_i_1 
       (.CI(\clk_sync_counter_reg_reg[4]_i_1_n_0 ),
        .CO({\clk_sync_counter_reg_reg[8]_i_1_n_0 ,\clk_sync_counter_reg_reg[8]_i_1_n_1 ,\clk_sync_counter_reg_reg[8]_i_1_n_2 ,\clk_sync_counter_reg_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\clk_sync_counter_reg_reg[8]_i_1_n_4 ,\clk_sync_counter_reg_reg[8]_i_1_n_5 ,\clk_sync_counter_reg_reg[8]_i_1_n_6 ,\clk_sync_counter_reg_reg[8]_i_1_n_7 }),
        .S({\clk_sync_counter_reg[8]_i_2_n_0 ,\clk_sync_counter_reg[8]_i_3_n_0 ,\clk_sync_counter_reg[8]_i_4_n_0 ,\clk_sync_counter_reg[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \clk_sync_counter_reg_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(clk_sync_counter_reg),
        .D(\clk_sync_counter_reg_reg[8]_i_1_n_6 ),
        .Q(clk_sync_counter_reg_reg[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 clk_wizard_main
       (.clk_in1(clk_100MHz_in),
        .clk_out1(clk_100MHz_out),
        .clk_out2(clk_wizard_main_n_1),
        .clk_out3(clk_20MHz_out),
        .clk_out4(clk_10MHz_out),
        .locked(clk_wizard_main_n_4));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_uart[0]_i_1 
       (.I0(cnt_uart[0]),
        .O(\cnt_uart[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0FB0)) 
    \cnt_uart[1]_i_1 
       (.I0(cnt_uart[3]),
        .I1(cnt_uart[2]),
        .I2(cnt_uart[0]),
        .I3(cnt_uart[1]),
        .O(\cnt_uart[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h2FC0)) 
    \cnt_uart[2]_i_1 
       (.I0(cnt_uart[3]),
        .I1(cnt_uart[1]),
        .I2(cnt_uart[0]),
        .I3(cnt_uart[2]),
        .O(\cnt_uart[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hABAAAAAA)) 
    \cnt_uart[3]_i_1 
       (.I0(is_counting),
        .I1(cnt_uart[3]),
        .I2(cnt_uart[1]),
        .I3(cnt_uart[0]),
        .I4(cnt_uart[2]),
        .O(\cnt_uart[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \cnt_uart[3]_i_2 
       (.I0(cnt_uart[3]),
        .I1(cnt_uart[2]),
        .I2(cnt_uart[1]),
        .I3(cnt_uart[0]),
        .O(\cnt_uart[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[0]_i_1_n_0 ),
        .Q(cnt_uart[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[1]_i_1_n_0 ),
        .Q(cnt_uart[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[2]_i_1_n_0 ),
        .Q(cnt_uart[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_uart_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(\cnt_uart[3]_i_1_n_0 ),
        .D(\cnt_uart[3]_i_2_n_0 ),
        .Q(cnt_uart[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF2)) 
    is_clk_initialized_reg_i_1
       (.I0(start_init_clk_reg_i_3_n_0),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(is_clk_initialized_reg),
        .O(is_clk_initialized_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_clk_initialized_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_clk_initialized_reg_i_1_n_0),
        .Q(is_clk_initialized_reg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    is_counting_i_1
       (.I0(is_counting),
        .I1(pll_data_ready_prev),
        .I2(pll_data_ready),
        .I3(uart_byte_received),
        .I4(uart_byte_received_prev),
        .I5(is_counting_i_2_n_0),
        .O(is_counting_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    is_counting_i_2
       (.I0(cnt_uart[2]),
        .I1(cnt_uart[0]),
        .I2(cnt_uart[1]),
        .I3(cnt_uart[3]),
        .O(is_counting_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_counting_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(is_counting_i_1_n_0),
        .Q(is_counting),
        .R(1'b0));
  FDRE pll_data_ready_prev_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_data_ready),
        .Q(pll_data_ready_prev),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00CE)) 
    pll_trig_reg_i_1
       (.I0(pll_trig),
        .I1(\trig_tguard_counter[16]_i_1_n_0 ),
        .I2(trig_tguard_counter),
        .I3(trig_tsweep_counter079_out),
        .O(pll_trig_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_trig_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(pll_trig_reg_i_1_n_0),
        .Q(pll_trig),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFFEF00)) 
    start_init_clk_reg_i_1
       (.I0(is_clk_initialized_reg),
        .I1(start_init_clk_reg_i_2_n_0),
        .I2(start_init_clk_reg_i_3_n_0),
        .I3(start_init_clk),
        .I4(start_init_clk_reg_i_4_n_0),
        .O(start_init_clk_reg_i_1_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_10
       (.I0(clk_sync_counter_reg_reg[0]),
        .I1(clk_sync_counter_reg_reg[1]),
        .I2(clk_sync_counter_reg_reg[2]),
        .O(start_init_clk_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    start_init_clk_reg_i_11
       (.I0(clk_sync_counter_reg_reg[18]),
        .I1(clk_sync_counter_reg_reg[19]),
        .I2(clk_sync_counter_reg_reg[20]),
        .I3(clk_sync_counter_reg_reg[21]),
        .I4(clk_sync_counter_reg_reg[23]),
        .I5(clk_sync_counter_reg_reg[22]),
        .O(start_init_clk_reg_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_12
       (.I0(clk_sync_counter_reg_reg[6]),
        .I1(clk_sync_counter_reg_reg[4]),
        .I2(clk_sync_counter_reg_reg[11]),
        .O(start_init_clk_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_init_clk_reg_i_13
       (.I0(clk_sync_counter_reg_reg[27]),
        .I1(clk_sync_counter_reg_reg[28]),
        .I2(clk_sync_counter_reg_reg[29]),
        .I3(clk_sync_counter_reg_reg[26]),
        .I4(clk_sync_counter_reg_reg[24]),
        .I5(clk_sync_counter_reg_reg[25]),
        .O(start_init_clk_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'h0155FFFFFFFFFFFF)) 
    start_init_clk_reg_i_14
       (.I0(clk_sync_counter_reg_reg[8]),
        .I1(clk_sync_counter_reg_reg[6]),
        .I2(clk_sync_counter_reg_reg[5]),
        .I3(clk_sync_counter_reg_reg[7]),
        .I4(clk_sync_counter_reg_reg[10]),
        .I5(clk_sync_counter_reg_reg[9]),
        .O(start_init_clk_reg_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h7)) 
    start_init_clk_reg_i_15
       (.I0(clk_sync_counter_reg_reg[9]),
        .I1(clk_sync_counter_reg_reg[10]),
        .O(start_init_clk_reg_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    start_init_clk_reg_i_16
       (.I0(clk_sync_counter_reg_reg[7]),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[6]),
        .O(start_init_clk_reg_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    start_init_clk_reg_i_17
       (.I0(clk_sync_counter_reg_reg[30]),
        .I1(clk_sync_counter_reg_reg[31]),
        .O(start_init_clk_reg_i_17_n_0));
  LUT4 #(
    .INIT(16'h0100)) 
    start_init_clk_reg_i_18
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[12]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[7]),
        .O(start_init_clk_reg_i_18_n_0));
  LUT5 #(
    .INIT(32'h11110111)) 
    start_init_clk_reg_i_2
       (.I0(clk_sync_counter_reg_reg[17]),
        .I1(start_init_clk_reg_i_5_n_0),
        .I2(clk_sync_counter_reg_reg[15]),
        .I3(clk_sync_counter_reg_reg[16]),
        .I4(start_init_clk_reg_i_6_n_0),
        .O(start_init_clk_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEAA0000)) 
    start_init_clk_reg_i_3
       (.I0(clk_sync_counter_reg_reg[16]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(start_init_clk_reg_i_7_n_0),
        .I3(clk_sync_counter_reg_reg[15]),
        .I4(clk_sync_counter_reg_reg[17]),
        .I5(start_init_clk_reg_i_5_n_0),
        .O(start_init_clk_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    start_init_clk_reg_i_4
       (.I0(start_init_clk_reg_i_8_n_0),
        .I1(start_init_clk_reg_i_9_n_0),
        .I2(start_init_clk_reg_i_10_n_0),
        .I3(start_init_clk_reg_i_11_n_0),
        .I4(start_init_clk_reg_i_12_n_0),
        .I5(start_init_clk_reg_i_13_n_0),
        .O(start_init_clk_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFEFF)) 
    start_init_clk_reg_i_5
       (.I0(clk_sync_counter_reg_reg[30]),
        .I1(clk_sync_counter_reg_reg[31]),
        .I2(start_init_clk_reg_i_11_n_0),
        .I3(start_init_clk_reg_i_13_n_0),
        .O(start_init_clk_reg_i_5_n_0));
  LUT5 #(
    .INIT(32'h00010000)) 
    start_init_clk_reg_i_6
       (.I0(clk_sync_counter_reg_reg[13]),
        .I1(clk_sync_counter_reg_reg[14]),
        .I2(clk_sync_counter_reg_reg[11]),
        .I3(clk_sync_counter_reg_reg[12]),
        .I4(start_init_clk_reg_i_14_n_0),
        .O(start_init_clk_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    start_init_clk_reg_i_7
       (.I0(start_init_clk_reg_i_15_n_0),
        .I1(clk_sync_counter_reg_reg[11]),
        .I2(clk_sync_counter_reg_reg[8]),
        .I3(clk_sync_counter_reg_reg[13]),
        .I4(clk_sync_counter_reg_reg[12]),
        .I5(start_init_clk_reg_i_16_n_0),
        .O(start_init_clk_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    start_init_clk_reg_i_8
       (.I0(clk_sync_counter_reg_reg[15]),
        .I1(clk_sync_counter_reg_reg[16]),
        .I2(start_init_clk_reg_i_15_n_0),
        .I3(clk_sync_counter_reg_reg[14]),
        .I4(clk_sync_counter_reg_reg[17]),
        .I5(start_init_clk_reg_i_17_n_0),
        .O(start_init_clk_reg_i_8_n_0));
  LUT4 #(
    .INIT(16'h0400)) 
    start_init_clk_reg_i_9
       (.I0(is_clk_initialized_reg),
        .I1(clk_sync_counter_reg_reg[5]),
        .I2(clk_sync_counter_reg_reg[3]),
        .I3(start_init_clk_reg_i_18_n_0),
        .O(start_init_clk_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_init_clk_reg_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(start_init_clk_reg_i_1_n_0),
        .Q(start_init_clk),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    start_uart_send_INST_0
       (.I0(uart_start_sending_reg_n_0),
        .I1(uart_adc_start_sending),
        .O(start_uart_send));
  LUT1 #(
    .INIT(2'h1)) 
    t_sweep_INST_0
       (.I0(t_sweep_INST_0_i_1_n_0),
        .O(t_sweep));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    t_sweep_INST_0_i_1
       (.I0(trig_tsweep_counter_reg[2]),
        .I1(trig_tsweep_counter_reg[1]),
        .I2(trig_tsweep_counter_reg[0]),
        .I3(t_sweep_INST_0_i_2_n_0),
        .I4(t_sweep_INST_0_i_3_n_0),
        .I5(t_sweep_INST_0_i_4_n_0),
        .O(t_sweep_INST_0_i_1_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    t_sweep_INST_0_i_2
       (.I0(trig_tsweep_counter_reg[6]),
        .I1(trig_tsweep_counter_reg[5]),
        .I2(trig_tsweep_counter_reg[4]),
        .I3(trig_tsweep_counter_reg[3]),
        .O(t_sweep_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    t_sweep_INST_0_i_3
       (.I0(trig_tsweep_counter_reg[10]),
        .I1(trig_tsweep_counter_reg[9]),
        .I2(trig_tsweep_counter_reg[8]),
        .I3(trig_tsweep_counter_reg[7]),
        .O(t_sweep_INST_0_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    t_sweep_INST_0_i_4
       (.I0(trig_tsweep_counter_reg[11]),
        .I1(trig_tsweep_counter_reg[12]),
        .I2(trig_tsweep_counter_reg[13]),
        .I3(trig_tsweep_counter_reg[14]),
        .I4(trig_tsweep_counter_reg[16]),
        .I5(trig_tsweep_counter_reg[15]),
        .O(t_sweep_INST_0_i_4_n_0));
  LUT5 #(
    .INIT(32'h888F88F8)) 
    \trig_tguard_counter[0]_i_1 
       (.I0(\trig_tguard_counter[16]_i_6_n_0 ),
        .I1(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .I3(\trig_tguard_counter[0]_i_2_n_0 ),
        .I4(trig_tguard_counter),
        .O(\trig_tguard_counter[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000040000000000)) 
    \trig_tguard_counter[0]_i_2 
       (.I0(\trig_tguard_counter[16]_i_8_n_0 ),
        .I1(\trig_tguard_counter[0]_i_3_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .I3(\trig_tguard_counter_reg_n_0_[2] ),
        .I4(\trig_tguard_counter_reg_n_0_[1] ),
        .I5(t_sweep_INST_0_i_1_n_0),
        .O(\trig_tguard_counter[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \trig_tguard_counter[0]_i_3 
       (.I0(\trig_tguard_counter_reg_n_0_[6] ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[4] ),
        .I3(\trig_tguard_counter_reg_n_0_[3] ),
        .O(\trig_tguard_counter[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FF404040)) 
    \trig_tguard_counter[16]_i_1 
       (.I0(\trig_tguard_counter[16]_i_4_n_0 ),
        .I1(\trig_tguard_counter[16]_i_5_n_0 ),
        .I2(\trig_tguard_counter[16]_i_6_n_0 ),
        .I3(t_sweep_INST_0_i_1_n_0),
        .I4(\trig_tguard_counter[16]_i_7_n_0 ),
        .I5(\trig_tguard_counter[16]_i_8_n_0 ),
        .O(\trig_tguard_counter[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tguard_counter[16]_i_10 
       (.I0(trig_tsweep_counter_reg[1]),
        .I1(trig_tsweep_counter_reg[0]),
        .I2(trig_tsweep_counter_reg[3]),
        .I3(trig_tsweep_counter_reg[2]),
        .O(\trig_tguard_counter[16]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tguard_counter[16]_i_11 
       (.I0(\trig_tguard_counter_reg_n_0_[3] ),
        .I1(\trig_tguard_counter_reg_n_0_[4] ),
        .O(\trig_tguard_counter[16]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_12 
       (.I0(\trig_tguard_counter_reg_n_0_[12] ),
        .I1(\trig_tguard_counter_reg_n_0_[15] ),
        .I2(\trig_tguard_counter_reg_n_0_[11] ),
        .I3(\trig_tguard_counter_reg_n_0_[9] ),
        .O(\trig_tguard_counter[16]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_13 
       (.I0(\trig_tguard_counter_reg_n_0_[10] ),
        .I1(\trig_tguard_counter_reg_n_0_[14] ),
        .I2(\trig_tguard_counter_reg_n_0_[13] ),
        .I3(\trig_tguard_counter_reg_n_0_[16] ),
        .O(\trig_tguard_counter[16]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h000000002A002AAA)) 
    \trig_tguard_counter[16]_i_2 
       (.I0(t_sweep_INST_0_i_1_n_0),
        .I1(\trig_tguard_counter_reg_n_0_[6] ),
        .I2(\trig_tguard_counter_reg_n_0_[5] ),
        .I3(\trig_tguard_counter[16]_i_4_n_0 ),
        .I4(\trig_tguard_counter[16]_i_9_n_0 ),
        .I5(\trig_tguard_counter[16]_i_8_n_0 ),
        .O(trig_tguard_counter));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \trig_tguard_counter[16]_i_4 
       (.I0(\trig_tguard_counter_reg_n_0_[4] ),
        .I1(\trig_tguard_counter_reg_n_0_[3] ),
        .I2(\trig_tguard_counter_reg_n_0_[2] ),
        .O(\trig_tguard_counter[16]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \trig_tguard_counter[16]_i_5 
       (.I0(\trig_tguard_counter[16]_i_9_n_0 ),
        .I1(trig_tsweep_counter_reg[15]),
        .I2(trig_tsweep_counter_reg[16]),
        .I3(trig_tsweep_counter_reg[13]),
        .I4(trig_tsweep_counter_reg[14]),
        .O(\trig_tguard_counter[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \trig_tguard_counter[16]_i_6 
       (.I0(\trig_tguard_counter[16]_i_10_n_0 ),
        .I1(trig_tsweep_counter_reg[7]),
        .I2(trig_tsweep_counter_reg[6]),
        .I3(trig_tsweep_counter_reg[5]),
        .I4(trig_tsweep_counter_reg[4]),
        .I5(\trig_tsweep_counter[0]_i_5_n_0 ),
        .O(\trig_tguard_counter[16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000400000)) 
    \trig_tguard_counter[16]_i_7 
       (.I0(\trig_tguard_counter[16]_i_11_n_0 ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[6] ),
        .I3(\trig_tguard_counter_reg_n_0_[0] ),
        .I4(\trig_tguard_counter_reg_n_0_[2] ),
        .I5(\trig_tguard_counter_reg_n_0_[1] ),
        .O(\trig_tguard_counter[16]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[16]_i_8 
       (.I0(\trig_tguard_counter_reg_n_0_[8] ),
        .I1(\trig_tguard_counter_reg_n_0_[7] ),
        .I2(\trig_tguard_counter[16]_i_12_n_0 ),
        .I3(\trig_tguard_counter[16]_i_13_n_0 ),
        .O(\trig_tguard_counter[16]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tguard_counter[16]_i_9 
       (.I0(\trig_tguard_counter_reg_n_0_[6] ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .I2(\trig_tguard_counter_reg_n_0_[1] ),
        .I3(\trig_tguard_counter_reg_n_0_[0] ),
        .O(\trig_tguard_counter[16]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(\trig_tguard_counter[0]_i_1_n_0 ),
        .Q(\trig_tguard_counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[10]),
        .Q(\trig_tguard_counter_reg_n_0_[10] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[11]),
        .Q(\trig_tguard_counter_reg_n_0_[11] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[12]),
        .Q(\trig_tguard_counter_reg_n_0_[12] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[12]_i_1 
       (.CI(\trig_tguard_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[12]_i_1_n_0 ,\trig_tguard_counter_reg[12]_i_1_n_1 ,\trig_tguard_counter_reg[12]_i_1_n_2 ,\trig_tguard_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S({\trig_tguard_counter_reg_n_0_[12] ,\trig_tguard_counter_reg_n_0_[11] ,\trig_tguard_counter_reg_n_0_[10] ,\trig_tguard_counter_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[13]),
        .Q(\trig_tguard_counter_reg_n_0_[13] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[14]),
        .Q(\trig_tguard_counter_reg_n_0_[14] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[15]),
        .Q(\trig_tguard_counter_reg_n_0_[15] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[16]),
        .Q(\trig_tguard_counter_reg_n_0_[16] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[16]_i_3 
       (.CI(\trig_tguard_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED [3],\trig_tguard_counter_reg[16]_i_3_n_1 ,\trig_tguard_counter_reg[16]_i_3_n_2 ,\trig_tguard_counter_reg[16]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[16:13]),
        .S({\trig_tguard_counter_reg_n_0_[16] ,\trig_tguard_counter_reg_n_0_[15] ,\trig_tguard_counter_reg_n_0_[14] ,\trig_tguard_counter_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[1]),
        .Q(\trig_tguard_counter_reg_n_0_[1] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[2]),
        .Q(\trig_tguard_counter_reg_n_0_[2] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[3]),
        .Q(\trig_tguard_counter_reg_n_0_[3] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[4]),
        .Q(\trig_tguard_counter_reg_n_0_[4] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trig_tguard_counter_reg[4]_i_1_n_0 ,\trig_tguard_counter_reg[4]_i_1_n_1 ,\trig_tguard_counter_reg[4]_i_1_n_2 ,\trig_tguard_counter_reg[4]_i_1_n_3 }),
        .CYINIT(\trig_tguard_counter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S({\trig_tguard_counter_reg_n_0_[4] ,\trig_tguard_counter_reg_n_0_[3] ,\trig_tguard_counter_reg_n_0_[2] ,\trig_tguard_counter_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[5]),
        .Q(\trig_tguard_counter_reg_n_0_[5] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[6]),
        .Q(\trig_tguard_counter_reg_n_0_[6] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[7]),
        .Q(\trig_tguard_counter_reg_n_0_[7] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[8]),
        .Q(\trig_tguard_counter_reg_n_0_[8] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[8]_i_1 
       (.CI(\trig_tguard_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[8]_i_1_n_0 ,\trig_tguard_counter_reg[8]_i_1_n_1 ,\trig_tguard_counter_reg[8]_i_1_n_2 ,\trig_tguard_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S({\trig_tguard_counter_reg_n_0_[8] ,\trig_tguard_counter_reg_n_0_[7] ,\trig_tguard_counter_reg_n_0_[6] ,\trig_tguard_counter_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tguard_counter),
        .D(p_2_in[9]),
        .Q(\trig_tguard_counter_reg_n_0_[9] ),
        .R(\trig_tguard_counter[16]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tsweep_counter[0]_i_1 
       (.I0(trig_tguard_counter),
        .I1(\trig_tguard_counter[16]_i_1_n_0 ),
        .O(\trig_tsweep_counter[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF010)) 
    \trig_tsweep_counter[0]_i_2 
       (.I0(trig_tsweep_counter_reg[6]),
        .I1(trig_tsweep_counter_reg[7]),
        .I2(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I3(\trig_tsweep_counter[0]_i_5_n_0 ),
        .O(trig_tsweep_counter079_out));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \trig_tsweep_counter[0]_i_4 
       (.I0(\trig_tguard_counter[16]_i_4_n_0 ),
        .I1(\trig_tsweep_counter[0]_i_7_n_0 ),
        .I2(\trig_tguard_counter[16]_i_12_n_0 ),
        .I3(\trig_tguard_counter[16]_i_13_n_0 ),
        .I4(\trig_tsweep_counter[0]_i_8_n_0 ),
        .I5(\trig_tguard_counter[16]_i_9_n_0 ),
        .O(\trig_tsweep_counter[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \trig_tsweep_counter[0]_i_5 
       (.I0(trig_tsweep_counter_reg[9]),
        .I1(trig_tsweep_counter_reg[10]),
        .I2(trig_tsweep_counter_reg[8]),
        .I3(trig_tsweep_counter_reg[11]),
        .I4(trig_tsweep_counter_reg[12]),
        .O(\trig_tsweep_counter[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trig_tsweep_counter[0]_i_6 
       (.I0(trig_tsweep_counter_reg[0]),
        .O(\trig_tsweep_counter[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tsweep_counter[0]_i_7 
       (.I0(\trig_tguard_counter_reg_n_0_[7] ),
        .I1(\trig_tguard_counter_reg_n_0_[8] ),
        .O(\trig_tsweep_counter[0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tsweep_counter[0]_i_8 
       (.I0(trig_tsweep_counter_reg[14]),
        .I1(trig_tsweep_counter_reg[13]),
        .I2(trig_tsweep_counter_reg[16]),
        .I3(trig_tsweep_counter_reg[15]),
        .O(\trig_tsweep_counter[0]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[0] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_7 ),
        .Q(trig_tsweep_counter_reg[0]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\trig_tsweep_counter_reg[0]_i_3_n_0 ,\trig_tsweep_counter_reg[0]_i_3_n_1 ,\trig_tsweep_counter_reg[0]_i_3_n_2 ,\trig_tsweep_counter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\trig_tsweep_counter_reg[0]_i_3_n_4 ,\trig_tsweep_counter_reg[0]_i_3_n_5 ,\trig_tsweep_counter_reg[0]_i_3_n_6 ,\trig_tsweep_counter_reg[0]_i_3_n_7 }),
        .S({trig_tsweep_counter_reg[3:1],\trig_tsweep_counter[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[10] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[10]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[11] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[11]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[12] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[12]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[12]_i_1 
       (.CI(\trig_tsweep_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[12]_i_1_n_0 ,\trig_tsweep_counter_reg[12]_i_1_n_1 ,\trig_tsweep_counter_reg[12]_i_1_n_2 ,\trig_tsweep_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[12]_i_1_n_4 ,\trig_tsweep_counter_reg[12]_i_1_n_5 ,\trig_tsweep_counter_reg[12]_i_1_n_6 ,\trig_tsweep_counter_reg[12]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[13] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[13]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[14] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[14]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[15] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[15]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[16] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[16]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[16]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[16]_i_1 
       (.CI(\trig_tsweep_counter_reg[12]_i_1_n_0 ),
        .CO(\NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED [3:1],\trig_tsweep_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,trig_tsweep_counter_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[1] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_6 ),
        .Q(trig_tsweep_counter_reg[1]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[2] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_5 ),
        .Q(trig_tsweep_counter_reg[2]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[3] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_4 ),
        .Q(trig_tsweep_counter_reg[3]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[4]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[4]_i_1 
       (.CI(\trig_tsweep_counter_reg[0]_i_3_n_0 ),
        .CO({\trig_tsweep_counter_reg[4]_i_1_n_0 ,\trig_tsweep_counter_reg[4]_i_1_n_1 ,\trig_tsweep_counter_reg[4]_i_1_n_2 ,\trig_tsweep_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[4]_i_1_n_4 ,\trig_tsweep_counter_reg[4]_i_1_n_5 ,\trig_tsweep_counter_reg[4]_i_1_n_6 ,\trig_tsweep_counter_reg[4]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[5] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[5]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[6] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[6]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[7] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[7]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[8] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[8]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[8]_i_1 
       (.CI(\trig_tsweep_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[8]_i_1_n_0 ,\trig_tsweep_counter_reg[8]_i_1_n_1 ,\trig_tsweep_counter_reg[8]_i_1_n_2 ,\trig_tsweep_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[8]_i_1_n_4 ,\trig_tsweep_counter_reg[8]_i_1_n_5 ,\trig_tsweep_counter_reg[8]_i_1_n_6 ,\trig_tsweep_counter_reg[8]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[9] 
       (.C(uart_byte_received_prev0),
        .CE(trig_tsweep_counter079_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[9]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    uart_byte_received_prev_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(uart_byte_received),
        .Q(uart_byte_received_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h55550010)) 
    uart_start_sending_i_1
       (.I0(is_counting_i_2_n_0),
        .I1(out_data_uart),
        .I2(pll_data_ready),
        .I3(pll_data_ready_prev),
        .I4(uart_start_sending_reg_n_0),
        .O(uart_start_sending_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    uart_start_sending_reg
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(uart_start_sending_i_1_n_0),
        .Q(uart_start_sending_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \uart_tx_data[4]_i_1 
       (.I0(pll_data_ready_prev),
        .I1(pll_data_ready),
        .I2(out_data_uart),
        .O(\uart_tx_data[4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \uart_tx_data[4]_i_2 
       (.I0(clk_wizard_main_n_4),
        .I1(clk_wizard_main_n_1),
        .O(uart_byte_received_prev0));
  FDRE \uart_tx_data_reg[4] 
       (.C(uart_byte_received_prev0),
        .CE(1'b1),
        .D(\uart_tx_data[4]_i_1_n_0 ),
        .Q(out_data_uart),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

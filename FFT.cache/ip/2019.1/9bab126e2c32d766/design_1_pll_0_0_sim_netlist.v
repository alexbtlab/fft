// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 18:24:56 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_pll_0_0_sim_netlist.v
// Design      : design_1_pll_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_pll_0_0,pll,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pll,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_pll_spi,
    pll_ld_sdo,
    pll_read,
    pll_write,
    pll_write_data,
    pll_write_address_reg,
    pll_read_address_reg,
    pll_sen,
    pll_mosi,
    pll_sck,
    pll_read_data,
    pll_data_ready);
  input clk_pll_spi;
  input pll_ld_sdo;
  input pll_read;
  input pll_write;
  input [23:0]pll_write_data;
  input [4:0]pll_write_address_reg;
  input [7:0]pll_read_address_reg;
  output pll_sen;
  output pll_mosi;
  output pll_sck;
  output [23:0]pll_read_data;
  output pll_data_ready;

  wire clk_pll_spi;
  wire pll_data_ready;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_read;
  wire [7:0]pll_read_address_reg;
  wire [23:0]pll_read_data;
  wire pll_sck;
  wire pll_sen;
  wire pll_write;
  wire [4:0]pll_write_address_reg;
  wire [23:0]pll_write_data;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll inst
       (.clk_pll_spi(clk_pll_spi),
        .pll_data_ready(pll_data_ready),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi(pll_mosi),
        .pll_read(pll_read),
        .pll_read_address_reg(pll_read_address_reg),
        .pll_read_data(pll_read_data),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_write(pll_write),
        .pll_write_address_reg(pll_write_address_reg),
        .pll_write_data(pll_write_data));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll
   (pll_read_data,
    pll_sck,
    pll_sen,
    pll_mosi,
    pll_data_ready,
    pll_write,
    pll_read,
    clk_pll_spi,
    pll_ld_sdo,
    pll_read_address_reg,
    pll_write_address_reg,
    pll_write_data);
  output [23:0]pll_read_data;
  output pll_sck;
  output pll_sen;
  output pll_mosi;
  output pll_data_ready;
  input pll_write;
  input pll_read;
  input clk_pll_spi;
  input pll_ld_sdo;
  input [7:0]pll_read_address_reg;
  input [4:0]pll_write_address_reg;
  input [23:0]pll_write_data;

  wire clk_pll_spi;
  wire is_clk_running;
  wire pll_data_ready;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_mosi_read;
  wire pll_read;
  wire [7:0]pll_read_address_reg;
  wire [23:0]pll_read_data;
  wire pll_sck;
  wire pll_sen;
  wire pll_sen_write;
  wire pll_write;
  wire [4:0]pll_write_address_reg;
  wire [23:0]pll_write_data;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive pll_receive
       (.clk_pll_spi(clk_pll_spi),
        .is_clk_running(is_clk_running),
        .pll_data_ready(pll_data_ready),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi_read(pll_mosi_read),
        .pll_read(pll_read),
        .pll_read_address_reg(pll_read_address_reg),
        .pll_read_data(pll_read_data),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_sen_write(pll_sen_write));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send pll_send
       (.clk_pll_spi(clk_pll_spi),
        .is_clk_running(is_clk_running),
        .pll_mosi(pll_mosi),
        .pll_mosi_read(pll_mosi_read),
        .pll_sen_write(pll_sen_write),
        .pll_write(pll_write),
        .pll_write_address_reg(pll_write_address_reg),
        .pll_write_data(pll_write_data));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive
   (pll_mosi_read,
    pll_data_ready,
    pll_read_data,
    pll_sck,
    pll_sen,
    pll_read,
    clk_pll_spi,
    pll_ld_sdo,
    pll_read_address_reg,
    is_clk_running,
    pll_sen_write);
  output pll_mosi_read;
  output pll_data_ready;
  output [23:0]pll_read_data;
  output pll_sck;
  output pll_sen;
  input pll_read;
  input clk_pll_spi;
  input pll_ld_sdo;
  input [7:0]pll_read_address_reg;
  input is_clk_running;
  input pll_sen_write;

  wire \bits_send[0]_i_1_n_0 ;
  wire \bits_send[1]_i_1_n_0 ;
  wire \bits_send[2]_i_1_n_0 ;
  wire \bits_send[3]_i_1_n_0 ;
  wire \bits_send[4]_i_1_n_0 ;
  wire \bits_send[4]_i_2_n_0 ;
  wire \bits_send_reg_n_0_[0] ;
  wire \bits_send_reg_n_0_[1] ;
  wire \bits_send_reg_n_0_[2] ;
  wire \bits_send_reg_n_0_[3] ;
  wire \bits_send_reg_n_0_[4] ;
  wire clk_pll_spi;
  wire data_ready_reg7_out;
  wire data_ready_reg_i_1_n_0;
  wire is_clk_running;
  wire is_first_cycle_going;
  wire is_first_cycle_going_i_1_n_0;
  wire is_second_cycle_going_i_1_n_0;
  wire is_second_cycle_going_reg_n_0;
  wire pll_cs_reg_i_1_n_0;
  wire pll_data_ready;
  wire pll_ld_sdo;
  wire pll_mosi_read;
  wire pll_mosi_reg_i_1_n_0;
  wire pll_mosi_reg_i_2_n_0;
  wire pll_mosi_reg_i_3_n_0;
  wire pll_mosi_reg_i_4_n_0;
  wire pll_mosi_reg_i_6_n_0;
  wire pll_mosi_reg_i_7_n_0;
  wire pll_read;
  wire [7:0]pll_read_address_reg;
  wire [23:0]pll_read_data;
  wire pll_sck;
  wire pll_sen;
  wire pll_sen_read;
  wire pll_sen_write;
  wire read_data_reg;
  wire read_data_reg025_out;
  wire \read_data_reg[0]_i_1_n_0 ;
  wire \read_data_reg[10]_i_1_n_0 ;
  wire \read_data_reg[11]_i_1_n_0 ;
  wire \read_data_reg[12]_i_1_n_0 ;
  wire \read_data_reg[13]_i_1_n_0 ;
  wire \read_data_reg[14]_i_1_n_0 ;
  wire \read_data_reg[15]_i_1_n_0 ;
  wire \read_data_reg[15]_i_2_n_0 ;
  wire \read_data_reg[16]_i_1_n_0 ;
  wire \read_data_reg[16]_i_2_n_0 ;
  wire \read_data_reg[17]_i_1_n_0 ;
  wire \read_data_reg[17]_i_2_n_0 ;
  wire \read_data_reg[18]_i_1_n_0 ;
  wire \read_data_reg[18]_i_2_n_0 ;
  wire \read_data_reg[19]_i_1_n_0 ;
  wire \read_data_reg[19]_i_2_n_0 ;
  wire \read_data_reg[1]_i_1_n_0 ;
  wire \read_data_reg[20]_i_1_n_0 ;
  wire \read_data_reg[20]_i_2_n_0 ;
  wire \read_data_reg[21]_i_1_n_0 ;
  wire \read_data_reg[22]_i_1_n_0 ;
  wire \read_data_reg[22]_i_2_n_0 ;
  wire \read_data_reg[23]_i_3_n_0 ;
  wire \read_data_reg[23]_i_4_n_0 ;
  wire \read_data_reg[2]_i_1_n_0 ;
  wire \read_data_reg[3]_i_1_n_0 ;
  wire \read_data_reg[4]_i_1_n_0 ;
  wire \read_data_reg[5]_i_1_n_0 ;
  wire \read_data_reg[6]_i_1_n_0 ;
  wire \read_data_reg[7]_i_1_n_0 ;
  wire \read_data_reg[8]_i_1_n_0 ;
  wire \read_data_reg[9]_i_1_n_0 ;
  wire start_prev;

  LUT1 #(
    .INIT(2'h1)) 
    \bits_send[0]_i_1 
       (.I0(\bits_send_reg_n_0_[0] ),
        .O(\bits_send[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bits_send[1]_i_1 
       (.I0(\bits_send_reg_n_0_[1] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .O(\bits_send[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bits_send[2]_i_1 
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .O(\bits_send[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \bits_send[3]_i_1 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(\bits_send_reg_n_0_[0] ),
        .O(\bits_send[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \bits_send[4]_i_1 
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(is_first_cycle_going),
        .O(\bits_send[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \bits_send[4]_i_2 
       (.I0(\bits_send_reg_n_0_[4] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(\bits_send_reg_n_0_[3] ),
        .O(\bits_send[4]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[0] 
       (.C(clk_pll_spi),
        .CE(\bits_send[4]_i_1_n_0 ),
        .D(\bits_send[0]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[0] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[1] 
       (.C(clk_pll_spi),
        .CE(\bits_send[4]_i_1_n_0 ),
        .D(\bits_send[1]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[1] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[2] 
       (.C(clk_pll_spi),
        .CE(\bits_send[4]_i_1_n_0 ),
        .D(\bits_send[2]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[2] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[3] 
       (.C(clk_pll_spi),
        .CE(\bits_send[4]_i_1_n_0 ),
        .D(\bits_send[3]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[3] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[4] 
       (.C(clk_pll_spi),
        .CE(\bits_send[4]_i_1_n_0 ),
        .D(\bits_send[4]_i_2_n_0 ),
        .Q(\bits_send_reg_n_0_[4] ),
        .R(read_data_reg025_out));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAB00ABAB)) 
    data_ready_reg_i_1
       (.I0(pll_data_ready),
        .I1(is_first_cycle_going),
        .I2(is_second_cycle_going_reg_n_0),
        .I3(start_prev),
        .I4(pll_read),
        .O(data_ready_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_ready_reg_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(data_ready_reg_i_1_n_0),
        .Q(pll_data_ready),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2AAAFFFF2AAA2AAA)) 
    is_first_cycle_going_i_1
       (.I0(is_first_cycle_going),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[3] ),
        .I3(\read_data_reg[16]_i_2_n_0 ),
        .I4(start_prev),
        .I5(pll_read),
        .O(is_first_cycle_going_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_first_cycle_going_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(is_first_cycle_going_i_1_n_0),
        .Q(is_first_cycle_going),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAEAAA2AAA)) 
    is_second_cycle_going_i_1
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[3] ),
        .I3(\read_data_reg[16]_i_2_n_0 ),
        .I4(is_first_cycle_going),
        .I5(read_data_reg025_out),
        .O(is_second_cycle_going_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_second_cycle_going_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(is_second_cycle_going_i_1_n_0),
        .Q(is_second_cycle_going_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hDCDCFFDC54540054)) 
    pll_cs_reg_i_1
       (.I0(pll_mosi_reg_i_3_n_0),
        .I1(is_first_cycle_going),
        .I2(is_second_cycle_going_reg_n_0),
        .I3(pll_read),
        .I4(start_prev),
        .I5(pll_sen_read),
        .O(pll_cs_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_cs_reg_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_cs_reg_i_1_n_0),
        .Q(pll_sen_read),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000022200020)) 
    pll_mosi_reg_i_1
       (.I0(pll_mosi_reg_i_2_n_0),
        .I1(read_data_reg025_out),
        .I2(pll_mosi_read),
        .I3(pll_mosi_reg_i_3_n_0),
        .I4(pll_mosi_reg_i_4_n_0),
        .I5(data_ready_reg7_out),
        .O(pll_mosi_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFEEE0000)) 
    pll_mosi_reg_i_2
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(pll_mosi_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    pll_mosi_reg_i_3
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[3] ),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(pll_mosi_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'h008B000000008B00)) 
    pll_mosi_reg_i_4
       (.I0(pll_mosi_reg_i_6_n_0),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(pll_mosi_reg_i_7_n_0),
        .I3(\bits_send_reg_n_0_[4] ),
        .I4(\read_data_reg[16]_i_2_n_0 ),
        .I5(\bits_send_reg_n_0_[3] ),
        .O(pll_mosi_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h1)) 
    pll_mosi_reg_i_5
       (.I0(is_first_cycle_going),
        .I1(is_second_cycle_going_reg_n_0),
        .O(data_ready_reg7_out));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    pll_mosi_reg_i_6
       (.I0(pll_read_address_reg[1]),
        .I1(pll_read_address_reg[5]),
        .I2(\bits_send[1]_i_1_n_0 ),
        .I3(pll_read_address_reg[3]),
        .I4(\bits_send[2]_i_1_n_0 ),
        .I5(pll_read_address_reg[7]),
        .O(pll_mosi_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    pll_mosi_reg_i_7
       (.I0(pll_read_address_reg[0]),
        .I1(pll_read_address_reg[4]),
        .I2(\bits_send[1]_i_1_n_0 ),
        .I3(pll_read_address_reg[2]),
        .I4(\bits_send[2]_i_1_n_0 ),
        .I5(pll_read_address_reg[6]),
        .O(pll_mosi_reg_i_7_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_mosi_reg_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_mosi_reg_i_1_n_0),
        .Q(pll_mosi_read),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hAAA8)) 
    pll_sck_INST_0
       (.I0(clk_pll_spi),
        .I1(is_clk_running),
        .I2(is_first_cycle_going),
        .I3(is_second_cycle_going_reg_n_0),
        .O(pll_sck));
  LUT2 #(
    .INIT(4'hE)) 
    pll_sen_INST_0
       (.I0(pll_sen_read),
        .I1(pll_sen_write),
        .O(pll_sen));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \read_data_reg[0]_i_1 
       (.I0(pll_read_data[0]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[16]_i_2_n_0 ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[10]_i_1 
       (.I0(pll_read_data[10]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAABAAAAAAAAAAAAA)) 
    \read_data_reg[11]_i_1 
       (.I0(pll_read_data[11]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[12]_i_1 
       (.I0(pll_read_data[12]),
        .I1(pll_ld_sdo),
        .I2(\bits_send_reg_n_0_[2] ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAEAAAAAAAAAA)) 
    \read_data_reg[13]_i_1 
       (.I0(pll_read_data[13]),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAEAAAAAAAAAA)) 
    \read_data_reg[14]_i_1 
       (.I0(pll_read_data[14]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAABAAAAAAAAAA)) 
    \read_data_reg[15]_i_1 
       (.I0(pll_read_data[15]),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \read_data_reg[15]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hAAAABAAA)) 
    \read_data_reg[16]_i_1 
       (.I0(pll_read_data[16]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[16]_i_2_n_0 ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \read_data_reg[16]_i_2 
       (.I0(\bits_send_reg_n_0_[0] ),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[2] ),
        .O(\read_data_reg[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[17]_i_1 
       (.I0(pll_read_data[17]),
        .I1(\read_data_reg[17]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \read_data_reg[17]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .O(\read_data_reg[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[18]_i_1 
       (.I0(pll_read_data[18]),
        .I1(\read_data_reg[18]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \read_data_reg[18]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[19]_i_1 
       (.I0(pll_read_data[19]),
        .I1(\read_data_reg[19]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \read_data_reg[19]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[1]_i_1 
       (.I0(pll_read_data[1]),
        .I1(\read_data_reg[17]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAABAAAAA)) 
    \read_data_reg[20]_i_1 
       (.I0(pll_read_data[20]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[20]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(pll_ld_sdo),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \read_data_reg[20]_i_2 
       (.I0(\bits_send_reg_n_0_[1] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAAA)) 
    \read_data_reg[21]_i_1 
       (.I0(pll_read_data[21]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAAA)) 
    \read_data_reg[22]_i_1 
       (.I0(pll_read_data[22]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[22]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \read_data_reg[22]_i_2 
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(pll_ld_sdo),
        .O(\read_data_reg[22]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \read_data_reg[23]_i_1 
       (.I0(pll_read),
        .I1(start_prev),
        .O(read_data_reg025_out));
  LUT4 #(
    .INIT(16'h002A)) 
    \read_data_reg[23]_i_2 
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[3] ),
        .I3(is_first_cycle_going),
        .O(read_data_reg));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[23]_i_3 
       (.I0(pll_read_data[23]),
        .I1(\read_data_reg[23]_i_4_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[23]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \read_data_reg[23]_i_4 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .O(\read_data_reg[23]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[2]_i_1 
       (.I0(pll_read_data[2]),
        .I1(\read_data_reg[18]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[3]_i_1 
       (.I0(pll_read_data[3]),
        .I1(\read_data_reg[19]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAABAAAAAAAAAAAAA)) 
    \read_data_reg[4]_i_1 
       (.I0(pll_read_data[4]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[20]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(pll_ld_sdo),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAABAAAAAAAAAAAA)) 
    \read_data_reg[5]_i_1 
       (.I0(pll_read_data[5]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAABAAAAAAAAAAAA)) 
    \read_data_reg[6]_i_1 
       (.I0(pll_read_data[6]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[7]_i_1 
       (.I0(pll_read_data[7]),
        .I1(\read_data_reg[23]_i_4_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEAAAAAAAAAAAAAAA)) 
    \read_data_reg[8]_i_1 
       (.I0(pll_read_data[8]),
        .I1(pll_ld_sdo),
        .I2(\bits_send_reg_n_0_[2] ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[9]_i_1 
       (.I0(pll_read_data[9]),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[0] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[0]_i_1_n_0 ),
        .Q(pll_read_data[0]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[10] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[10]_i_1_n_0 ),
        .Q(pll_read_data[10]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[11] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[11]_i_1_n_0 ),
        .Q(pll_read_data[11]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[12] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[12]_i_1_n_0 ),
        .Q(pll_read_data[12]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[13] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[13]_i_1_n_0 ),
        .Q(pll_read_data[13]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[14] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[14]_i_1_n_0 ),
        .Q(pll_read_data[14]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[15] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[15]_i_1_n_0 ),
        .Q(pll_read_data[15]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[16] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[16]_i_1_n_0 ),
        .Q(pll_read_data[16]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[17] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[17]_i_1_n_0 ),
        .Q(pll_read_data[17]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[18] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[18]_i_1_n_0 ),
        .Q(pll_read_data[18]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[19] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[19]_i_1_n_0 ),
        .Q(pll_read_data[19]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[1] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[1]_i_1_n_0 ),
        .Q(pll_read_data[1]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[20] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[20]_i_1_n_0 ),
        .Q(pll_read_data[20]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[21] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[21]_i_1_n_0 ),
        .Q(pll_read_data[21]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[22] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[22]_i_1_n_0 ),
        .Q(pll_read_data[22]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[23] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[23]_i_3_n_0 ),
        .Q(pll_read_data[23]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[2] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[2]_i_1_n_0 ),
        .Q(pll_read_data[2]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[3] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[3]_i_1_n_0 ),
        .Q(pll_read_data[3]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[4] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[4]_i_1_n_0 ),
        .Q(pll_read_data[4]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[5] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[5]_i_1_n_0 ),
        .Q(pll_read_data[5]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[6] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[6]_i_1_n_0 ),
        .Q(pll_read_data[6]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[7] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[7]_i_1_n_0 ),
        .Q(pll_read_data[7]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[8] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[8]_i_1_n_0 ),
        .Q(pll_read_data[8]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[9] 
       (.C(clk_pll_spi),
        .CE(read_data_reg),
        .D(\read_data_reg[9]_i_1_n_0 ),
        .Q(pll_read_data[9]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_read),
        .Q(start_prev),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send
   (is_clk_running,
    pll_sen_write,
    pll_mosi,
    pll_write,
    clk_pll_spi,
    pll_write_address_reg,
    pll_write_data,
    pll_mosi_read);
  output is_clk_running;
  output pll_sen_write;
  output pll_mosi;
  input pll_write;
  input clk_pll_spi;
  input [4:0]pll_write_address_reg;
  input [23:0]pll_write_data;
  input pll_mosi_read;

  wire bits_send0;
  wire \bits_send[0]_i_1__0_n_0 ;
  wire \bits_send[1]_i_1__0_n_0 ;
  wire \bits_send[2]_i_1__0_n_0 ;
  wire \bits_send[3]_i_1__0_n_0 ;
  wire \bits_send[4]_i_1__0_n_0 ;
  wire \bits_send[4]_i_3_n_0 ;
  wire [4:0]bits_send_reg;
  wire clk_pll_spi;
  wire is_clk_running;
  wire is_clk_running_i_1_n_0;
  wire pll_cs_reg_i_1__0_n_0;
  wire pll_mosi;
  wire pll_mosi_read;
  wire pll_mosi_reg_i_10_n_0;
  wire pll_mosi_reg_i_11_n_0;
  wire pll_mosi_reg_i_12_n_0;
  wire pll_mosi_reg_i_13_n_0;
  wire pll_mosi_reg_i_14_n_0;
  wire pll_mosi_reg_i_15_n_0;
  wire pll_mosi_reg_i_16_n_0;
  wire pll_mosi_reg_i_17_n_0;
  wire pll_mosi_reg_i_1__0_n_0;
  wire pll_mosi_reg_i_2__0_n_0;
  wire pll_mosi_reg_i_3__0_n_0;
  wire pll_mosi_reg_i_4__0_n_0;
  wire pll_mosi_reg_i_5__0_n_0;
  wire pll_mosi_reg_i_6__0_n_0;
  wire pll_mosi_reg_i_7__0_n_0;
  wire pll_mosi_reg_i_8_n_0;
  wire pll_mosi_reg_i_9_n_0;
  wire pll_mosi_write;
  wire pll_sen_write;
  wire pll_write;
  wire [4:0]pll_write_address_reg;
  wire [23:0]pll_write_data;
  wire start_prev;

  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h6066)) 
    \bits_send[0]_i_1__0 
       (.I0(bits_send_reg[0]),
        .I1(is_clk_running),
        .I2(start_prev),
        .I3(pll_write),
        .O(\bits_send[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h0BBBB000)) 
    \bits_send[1]_i_1__0 
       (.I0(start_prev),
        .I1(pll_write),
        .I2(is_clk_running),
        .I3(bits_send_reg[0]),
        .I4(bits_send_reg[1]),
        .O(\bits_send[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0BB0BB00BB00BB00)) 
    \bits_send[2]_i_1__0 
       (.I0(start_prev),
        .I1(pll_write),
        .I2(is_clk_running),
        .I3(bits_send_reg[2]),
        .I4(bits_send_reg[0]),
        .I5(bits_send_reg[1]),
        .O(\bits_send[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h1450505050505050)) 
    \bits_send[3]_i_1__0 
       (.I0(bits_send0),
        .I1(is_clk_running),
        .I2(bits_send_reg[3]),
        .I3(bits_send_reg[2]),
        .I4(bits_send_reg[1]),
        .I5(bits_send_reg[0]),
        .O(\bits_send[3]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h1555555540000000)) 
    \bits_send[4]_i_1__0 
       (.I0(bits_send0),
        .I1(is_clk_running),
        .I2(bits_send_reg[3]),
        .I3(bits_send_reg[2]),
        .I4(\bits_send[4]_i_3_n_0 ),
        .I5(bits_send_reg[4]),
        .O(\bits_send[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bits_send[4]_i_2__0 
       (.I0(pll_write),
        .I1(start_prev),
        .O(bits_send0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \bits_send[4]_i_3 
       (.I0(bits_send_reg[1]),
        .I1(bits_send_reg[0]),
        .O(\bits_send[4]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[0] 
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(\bits_send[0]_i_1__0_n_0 ),
        .Q(bits_send_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[1] 
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(\bits_send[1]_i_1__0_n_0 ),
        .Q(bits_send_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[2] 
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(\bits_send[2]_i_1__0_n_0 ),
        .Q(bits_send_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[3] 
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(\bits_send[3]_i_1__0_n_0 ),
        .Q(bits_send_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[4] 
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(\bits_send[4]_i_1__0_n_0 ),
        .Q(bits_send_reg[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF2AAAAAAA)) 
    is_clk_running_i_1
       (.I0(is_clk_running),
        .I1(\bits_send[4]_i_3_n_0 ),
        .I2(bits_send_reg[2]),
        .I3(bits_send_reg[3]),
        .I4(bits_send_reg[4]),
        .I5(bits_send0),
        .O(is_clk_running_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_clk_running_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(is_clk_running_i_1_n_0),
        .Q(is_clk_running),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hEF20AA20)) 
    pll_cs_reg_i_1__0
       (.I0(pll_sen_write),
        .I1(start_prev),
        .I2(pll_write),
        .I3(is_clk_running),
        .I4(pll_mosi_reg_i_2__0_n_0),
        .O(pll_cs_reg_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_cs_reg_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_cs_reg_i_1__0_n_0),
        .Q(pll_sen_write),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    pll_mosi_INST_0
       (.I0(pll_mosi_write),
        .I1(pll_mosi_read),
        .O(pll_mosi));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_10
       (.I0(pll_write_data[1]),
        .I1(pll_write_data[0]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[3]),
        .I5(pll_write_data[2]),
        .O(pll_mosi_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_11
       (.I0(pll_write_data[5]),
        .I1(pll_write_data[4]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[7]),
        .I5(pll_write_data[6]),
        .O(pll_mosi_reg_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h9555)) 
    pll_mosi_reg_i_12
       (.I0(bits_send_reg[3]),
        .I1(bits_send_reg[2]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .O(pll_mosi_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_13
       (.I0(pll_write_data[9]),
        .I1(pll_write_data[8]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[11]),
        .I5(pll_write_data[10]),
        .O(pll_mosi_reg_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    pll_mosi_reg_i_14
       (.I0(bits_send_reg[2]),
        .I1(bits_send_reg[0]),
        .I2(bits_send_reg[1]),
        .O(pll_mosi_reg_i_14_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_15
       (.I0(pll_write_data[13]),
        .I1(pll_write_data[12]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[15]),
        .I5(pll_write_data[14]),
        .O(pll_mosi_reg_i_15_n_0));
  LUT6 #(
    .INIT(64'h0F8000000F000000)) 
    pll_mosi_reg_i_16
       (.I0(bits_send_reg[0]),
        .I1(bits_send_reg[1]),
        .I2(bits_send_reg[2]),
        .I3(bits_send_reg[3]),
        .I4(bits_send_reg[4]),
        .I5(pll_write_address_reg[4]),
        .O(pll_mosi_reg_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hF3AA0000)) 
    pll_mosi_reg_i_17
       (.I0(pll_write_address_reg[2]),
        .I1(bits_send_reg[3]),
        .I2(pll_write_address_reg[0]),
        .I3(bits_send_reg[1]),
        .I4(bits_send_reg[0]),
        .O(pll_mosi_reg_i_17_n_0));
  LUT6 #(
    .INIT(64'hFFB000B0FF800080)) 
    pll_mosi_reg_i_1__0
       (.I0(pll_mosi_write),
        .I1(pll_mosi_reg_i_2__0_n_0),
        .I2(is_clk_running),
        .I3(bits_send0),
        .I4(pll_write_data[23]),
        .I5(pll_mosi_reg_i_3__0_n_0),
        .O(pll_mosi_reg_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    pll_mosi_reg_i_2__0
       (.I0(bits_send_reg[4]),
        .I1(bits_send_reg[3]),
        .I2(bits_send_reg[2]),
        .I3(bits_send_reg[1]),
        .I4(bits_send_reg[0]),
        .O(pll_mosi_reg_i_2__0_n_0));
  LUT5 #(
    .INIT(32'hFFFF3E02)) 
    pll_mosi_reg_i_3__0
       (.I0(pll_mosi_reg_i_4__0_n_0),
        .I1(bits_send_reg[4]),
        .I2(pll_mosi_reg_i_5__0_n_0),
        .I3(pll_mosi_reg_i_6__0_n_0),
        .I4(pll_mosi_reg_i_7__0_n_0),
        .O(pll_mosi_reg_i_3__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hEBBB2888)) 
    pll_mosi_reg_i_4__0
       (.I0(pll_mosi_reg_i_8_n_0),
        .I1(bits_send_reg[2]),
        .I2(bits_send_reg[0]),
        .I3(bits_send_reg[1]),
        .I4(pll_mosi_reg_i_9_n_0),
        .O(pll_mosi_reg_i_4__0_n_0));
  LUT4 #(
    .INIT(16'hEAAA)) 
    pll_mosi_reg_i_5__0
       (.I0(bits_send_reg[3]),
        .I1(bits_send_reg[2]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .O(pll_mosi_reg_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    pll_mosi_reg_i_6__0
       (.I0(pll_mosi_reg_i_10_n_0),
        .I1(pll_mosi_reg_i_11_n_0),
        .I2(pll_mosi_reg_i_12_n_0),
        .I3(pll_mosi_reg_i_13_n_0),
        .I4(pll_mosi_reg_i_14_n_0),
        .I5(pll_mosi_reg_i_15_n_0),
        .O(pll_mosi_reg_i_6__0_n_0));
  LUT6 #(
    .INIT(64'h8A8A8A8888888A88)) 
    pll_mosi_reg_i_7__0
       (.I0(pll_mosi_reg_i_16_n_0),
        .I1(pll_mosi_reg_i_17_n_0),
        .I2(bits_send_reg[0]),
        .I3(pll_write_address_reg[3]),
        .I4(bits_send_reg[1]),
        .I5(pll_write_address_reg[1]),
        .O(pll_mosi_reg_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_8
       (.I0(pll_write_data[17]),
        .I1(pll_write_data[16]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[19]),
        .I5(pll_write_data[18]),
        .O(pll_mosi_reg_i_8_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_9
       (.I0(pll_write_data[21]),
        .I1(pll_write_data[20]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(pll_write_data[23]),
        .I5(pll_write_data[22]),
        .O(pll_mosi_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_mosi_reg_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_mosi_reg_i_1__0_n_0),
        .Q(pll_mosi_write),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_pll_spi),
        .CE(1'b1),
        .D(pll_write),
        .Q(start_prev),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

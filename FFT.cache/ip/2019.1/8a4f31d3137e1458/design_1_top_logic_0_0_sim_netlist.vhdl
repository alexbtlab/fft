-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 16:06:57 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_sim_netlist.vhdl
-- Design      : design_1_top_logic_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    uart_start : out STD_LOGIC;
    in_sending : out STD_LOGIC;
    t_sweep : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_top_logic_0_0,top_logic_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "top_logic_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic_0 is
  port (
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    uart_start : out STD_LOGIC;
    in_sending : out STD_LOGIC;
    t_sweep : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic_0;
begin
inst: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic_0
     port map (
      in_adc_data(7 downto 0) => in_adc_data(7 downto 0),
      in_sending => in_sending,
      out_data_uart(7 downto 0) => out_data_uart(7 downto 0),
      start_init_clk => start_init_clk,
      t_sweep => t_sweep,
      uart_adc_start_sending => uart_adc_start_sending,
      uart_start => uart_start
    );
end STRUCTURE;

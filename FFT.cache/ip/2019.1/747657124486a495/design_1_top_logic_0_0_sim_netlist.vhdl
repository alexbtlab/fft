-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 13:57:08 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_top_logic_0_0_sim_netlist.vhdl
-- Design      : design_1_top_logic_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  signal clk_in1_clk_wiz_0 : STD_LOGIC;
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clk_out4_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufg : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
clkout4_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out4_clk_wiz_0,
      O => clk_out4
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 10.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 50,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 100,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 125,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => clk_out3_clk_wiz_0,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => clk_out4_clk_wiz_0,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3,
      clk_out4 => clk_out4,
      locked => locked
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic is
  port (
    clk_100MHz_out : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    atten : out STD_LOGIC_VECTOR ( 0 to 0 );
    t_sweep : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    start_uart_send : out STD_LOGIC;
    out_data_uart : out STD_LOGIC_VECTOR ( 0 to 0 );
    pll_trig : out STD_LOGIC;
    clk_100MHz_in : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    pll_data_ready : in STD_LOGIC;
    uart_adc_start_sending : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic is
  signal clk_sync_counter_reg : STD_LOGIC;
  signal clk_sync_counter_reg0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \clk_sync_counter_reg0_carry__0_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__6_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__6_n_3\ : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_0 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_1 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_2 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_3 : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_11_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_12_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_14_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal clk_sync_counter_reg_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \clk_sync_counter_reg_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal clk_wizard_main_n_1 : STD_LOGIC;
  signal clk_wizard_main_n_4 : STD_LOGIC;
  signal cnt_uart : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \cnt_uart[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[3]_i_2_n_0\ : STD_LOGIC;
  signal is_clk_initialized_reg : STD_LOGIC;
  signal is_clk_initialized_reg_i_1_n_0 : STD_LOGIC;
  signal is_counting : STD_LOGIC;
  signal is_counting_i_1_n_0 : STD_LOGIC;
  signal is_counting_i_2_n_0 : STD_LOGIC;
  signal \^out_data_uart\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal p_2_in : STD_LOGIC_VECTOR ( 16 downto 1 );
  signal pll_data_ready_prev : STD_LOGIC;
  signal \^pll_trig\ : STD_LOGIC;
  signal pll_trig_reg_i_1_n_0 : STD_LOGIC;
  signal \^start_init_clk\ : STD_LOGIC;
  signal start_init_clk_reg_i_10_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_11_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_12_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_13_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_14_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_15_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_16_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_17_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_18_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_1_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_2_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_3_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_4_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_5_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_6_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_7_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_8_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_9_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_1_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_2_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_3_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_4_n_0 : STD_LOGIC;
  signal trig_tguard_counter : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_10_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_11_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_12_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_13_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_6_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_7_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_8_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_9_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[10]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[11]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[12]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[13]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[14]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[15]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[16]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[3]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[4]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[5]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[6]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[7]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[8]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[9]\ : STD_LOGIC;
  signal trig_tsweep_counter079_out : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_7_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_8_n_0\ : STD_LOGIC;
  signal trig_tsweep_counter_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \trig_tsweep_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal uart_byte_received_prev : STD_LOGIC;
  signal uart_byte_received_prev0 : STD_LOGIC;
  signal uart_start_sending_i_1_n_0 : STD_LOGIC;
  signal uart_start_sending_reg_n_0 : STD_LOGIC;
  signal \uart_tx_data[4]_i_1_n_0\ : STD_LOGIC;
  signal \NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_13\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_14\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_7\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_8\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[4]_i_7\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_uart[1]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cnt_uart[2]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cnt_uart[3]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of is_clk_initialized_reg_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of is_counting_i_2 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_12 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_15 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_16 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_17 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_5 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \trig_tguard_counter[0]_i_3\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_5\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_8\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \trig_tsweep_counter[0]_i_7\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \trig_tsweep_counter[0]_i_8\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of uart_start_sending_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \uart_tx_data[4]_i_1\ : label is "soft_lutpair1";
begin
  out_data_uart(0) <= \^out_data_uart\(0);
  pll_trig <= \^pll_trig\;
  start_init_clk <= \^start_init_clk\;
\atten_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '0',
      D => '0',
      Q => atten(0),
      R => '0'
    );
clk_sync_counter_reg0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => clk_sync_counter_reg0_carry_n_0,
      CO(2) => clk_sync_counter_reg0_carry_n_1,
      CO(1) => clk_sync_counter_reg0_carry_n_2,
      CO(0) => clk_sync_counter_reg0_carry_n_3,
      CYINIT => clk_sync_counter_reg_reg(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(4 downto 1),
      S(3 downto 0) => clk_sync_counter_reg_reg(4 downto 1)
    );
\clk_sync_counter_reg0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => clk_sync_counter_reg0_carry_n_0,
      CO(3) => \clk_sync_counter_reg0_carry__0_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__0_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__0_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(8 downto 5),
      S(3 downto 0) => clk_sync_counter_reg_reg(8 downto 5)
    );
\clk_sync_counter_reg0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__0_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__1_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__1_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__1_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(12 downto 9),
      S(3 downto 0) => clk_sync_counter_reg_reg(12 downto 9)
    );
\clk_sync_counter_reg0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__1_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__2_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__2_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__2_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(16 downto 13),
      S(3 downto 0) => clk_sync_counter_reg_reg(16 downto 13)
    );
\clk_sync_counter_reg0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__2_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__3_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__3_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__3_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(20 downto 17),
      S(3 downto 0) => clk_sync_counter_reg_reg(20 downto 17)
    );
\clk_sync_counter_reg0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__3_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__4_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__4_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__4_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(24 downto 21),
      S(3 downto 0) => clk_sync_counter_reg_reg(24 downto 21)
    );
\clk_sync_counter_reg0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__4_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__5_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__5_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__5_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(28 downto 25),
      S(3 downto 0) => clk_sync_counter_reg_reg(28 downto 25)
    );
\clk_sync_counter_reg0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \clk_sync_counter_reg0_carry__6_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => clk_sync_counter_reg0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => clk_sync_counter_reg_reg(31 downto 29)
    );
\clk_sync_counter_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => start_init_clk_reg_i_3_n_0,
      O => clk_sync_counter_reg
    );
\clk_sync_counter_reg[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000002"
    )
        port map (
      I0 => start_init_clk_reg_i_14_n_0,
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(14),
      I4 => clk_sync_counter_reg_reg(13),
      I5 => \clk_sync_counter_reg[0]_i_14_n_0\,
      O => \clk_sync_counter_reg[0]_i_10_n_0\
    );
\clk_sync_counter_reg[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000900990090000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_sync_counter_reg_reg(12),
      I5 => clk_sync_counter_reg_reg(10),
      O => \clk_sync_counter_reg[0]_i_11_n_0\
    );
\clk_sync_counter_reg[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[0]_i_12_n_0\
    );
\clk_sync_counter_reg[0]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(7),
      I2 => clk_sync_counter_reg_reg(14),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[0]_i_13_n_0\
    );
\clk_sync_counter_reg[0]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      O => \clk_sync_counter_reg[0]_i_14_n_0\
    );
\clk_sync_counter_reg[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(3),
      I2 => clk_sync_counter_reg0(3),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_3_n_0\
    );
\clk_sync_counter_reg[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(2),
      I2 => clk_sync_counter_reg0(2),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_4_n_0\
    );
\clk_sync_counter_reg[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(1),
      I2 => clk_sync_counter_reg0(1),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_5_n_0\
    );
\clk_sync_counter_reg[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(0),
      O => \clk_sync_counter_reg[0]_i_6_n_0\
    );
\clk_sync_counter_reg[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF77FF07"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I2 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I3 => start_init_clk_reg_i_5_n_0,
      I4 => clk_sync_counter_reg_reg(17),
      O => \clk_sync_counter_reg[0]_i_7_n_0\
    );
\clk_sync_counter_reg[0]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08200000"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_11_n_0\,
      I1 => clk_sync_counter_reg_reg(10),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(9),
      I4 => \clk_sync_counter_reg[0]_i_12_n_0\,
      O => \clk_sync_counter_reg[0]_i_8_n_0\
    );
\clk_sync_counter_reg[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000101000"
    )
        port map (
      I0 => start_init_clk_reg_i_12_n_0,
      I1 => start_init_clk_reg_i_10_n_0,
      I2 => \clk_sync_counter_reg[0]_i_13_n_0\,
      I3 => clk_sync_counter_reg_reg(17),
      I4 => clk_sync_counter_reg_reg(16),
      I5 => clk_sync_counter_reg_reg(3),
      O => \clk_sync_counter_reg[0]_i_9_n_0\
    );
\clk_sync_counter_reg[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg0(15),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[12]_i_2_n_0\
    );
\clk_sync_counter_reg[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(14),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(14),
      O => \clk_sync_counter_reg[12]_i_3_n_0\
    );
\clk_sync_counter_reg[12]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(13),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(13),
      O => \clk_sync_counter_reg[12]_i_4_n_0\
    );
\clk_sync_counter_reg[12]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(12),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(12),
      O => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(19),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_2_n_0\
    );
\clk_sync_counter_reg[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(18),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_3_n_0\
    );
\clk_sync_counter_reg[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => clk_sync_counter_reg0(17),
      I1 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I2 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_4_n_0\
    );
\clk_sync_counter_reg[16]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF004E4E"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => clk_sync_counter_reg0(16),
      I2 => \clk_sync_counter_reg[16]_i_6_n_0\,
      I3 => clk_sync_counter_reg_reg(16),
      I4 => start_init_clk_reg_i_2_n_0,
      O => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg[16]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => \clk_sync_counter_reg[16]_i_7_n_0\,
      I1 => \clk_sync_counter_reg[16]_i_8_n_0\,
      I2 => \clk_sync_counter_reg[16]_i_9_n_0\,
      I3 => start_init_clk_reg_i_16_n_0,
      I4 => start_init_clk_reg_i_10_n_0,
      O => \clk_sync_counter_reg[16]_i_6_n_0\
    );
\clk_sync_counter_reg[16]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[16]_i_7_n_0\
    );
\clk_sync_counter_reg[16]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(10),
      O => \clk_sync_counter_reg[16]_i_8_n_0\
    );
\clk_sync_counter_reg[16]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(4),
      I1 => clk_sync_counter_reg_reg(3),
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_sync_counter_reg_reg(8),
      O => \clk_sync_counter_reg[16]_i_9_n_0\
    );
\clk_sync_counter_reg[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(23),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_2_n_0\
    );
\clk_sync_counter_reg[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(22),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_3_n_0\
    );
\clk_sync_counter_reg[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(21),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_4_n_0\
    );
\clk_sync_counter_reg[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(20),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(27),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_2_n_0\
    );
\clk_sync_counter_reg[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(26),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_3_n_0\
    );
\clk_sync_counter_reg[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(25),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_4_n_0\
    );
\clk_sync_counter_reg[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(24),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(31),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_2_n_0\
    );
\clk_sync_counter_reg[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(30),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_3_n_0\
    );
\clk_sync_counter_reg[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(29),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_4_n_0\
    );
\clk_sync_counter_reg[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(28),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(7),
      I3 => clk_sync_counter_reg0(7),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_2_n_0\
    );
\clk_sync_counter_reg[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(6),
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg0(6),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_3_n_0\
    );
\clk_sync_counter_reg[4]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg0(5),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_4_n_0\
    );
\clk_sync_counter_reg[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(4),
      I2 => clk_sync_counter_reg0(4),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FAFCFFFCFFFCFF"
    )
        port map (
      I0 => \clk_sync_counter_reg[16]_i_6_n_0\,
      I1 => clk_sync_counter_reg_reg(17),
      I2 => start_init_clk_reg_i_5_n_0,
      I3 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I4 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I5 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[4]_i_6_n_0\
    );
\clk_sync_counter_reg[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I2 => start_init_clk_reg_i_5_n_0,
      O => \clk_sync_counter_reg[4]_i_7_n_0\
    );
\clk_sync_counter_reg[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(11),
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg0(11),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_2_n_0\
    );
\clk_sync_counter_reg[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(10),
      I3 => clk_sync_counter_reg0(10),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_3_n_0\
    );
\clk_sync_counter_reg[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_sync_counter_reg0(9),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_4_n_0\
    );
\clk_sync_counter_reg[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(8),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(8),
      O => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      Q => clk_sync_counter_reg_reg(0),
      R => '0'
    );
\clk_sync_counter_reg_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[0]_i_2_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[0]_i_2_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => start_init_clk_reg_i_2_n_0,
      O(3) => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      O(2) => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      O(1) => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      O(0) => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      S(3) => \clk_sync_counter_reg[0]_i_3_n_0\,
      S(2) => \clk_sync_counter_reg[0]_i_4_n_0\,
      S(1) => \clk_sync_counter_reg[0]_i_5_n_0\,
      S(0) => \clk_sync_counter_reg[0]_i_6_n_0\
    );
\clk_sync_counter_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(10),
      R => '0'
    );
\clk_sync_counter_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(11),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(12),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[12]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[12]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[12]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[12]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[12]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(13),
      R => '0'
    );
\clk_sync_counter_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(14),
      R => '0'
    );
\clk_sync_counter_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(15),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(16),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[16]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[16]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[16]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[16]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[16]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(17),
      R => '0'
    );
\clk_sync_counter_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(18),
      R => '0'
    );
\clk_sync_counter_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(19),
      R => '0'
    );
\clk_sync_counter_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      Q => clk_sync_counter_reg_reg(1),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(20),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[20]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[20]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[20]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[20]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[20]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(21),
      R => '0'
    );
\clk_sync_counter_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(22),
      R => '0'
    );
\clk_sync_counter_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(23),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(24),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[24]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[24]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[24]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[24]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[24]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(25),
      R => '0'
    );
\clk_sync_counter_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(26),
      R => '0'
    );
\clk_sync_counter_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(27),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(28),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(3) => \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \clk_sync_counter_reg_reg[28]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[28]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[28]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[28]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[28]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(29),
      R => '0'
    );
\clk_sync_counter_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      Q => clk_sync_counter_reg_reg(2),
      R => '0'
    );
\clk_sync_counter_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(30),
      R => '0'
    );
\clk_sync_counter_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(31),
      R => '0'
    );
\clk_sync_counter_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      Q => clk_sync_counter_reg_reg(3),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(4),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[4]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[4]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[4]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[4]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[4]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(5),
      R => '0'
    );
\clk_sync_counter_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(6),
      R => '0'
    );
\clk_sync_counter_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(7),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(8),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[8]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[8]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[8]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[8]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[8]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(9),
      R => '0'
    );
clk_wizard_main: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
     port map (
      clk_in1 => clk_100MHz_in,
      clk_out1 => clk_100MHz_out,
      clk_out2 => clk_wizard_main_n_1,
      clk_out3 => clk_20MHz_out,
      clk_out4 => clk_10MHz_out,
      locked => clk_wizard_main_n_4
    );
\cnt_uart[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_uart(0),
      O => \cnt_uart[0]_i_1_n_0\
    );
\cnt_uart[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0FB0"
    )
        port map (
      I0 => cnt_uart(3),
      I1 => cnt_uart(2),
      I2 => cnt_uart(0),
      I3 => cnt_uart(1),
      O => \cnt_uart[1]_i_1_n_0\
    );
\cnt_uart[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2FC0"
    )
        port map (
      I0 => cnt_uart(3),
      I1 => cnt_uart(1),
      I2 => cnt_uart(0),
      I3 => cnt_uart(2),
      O => \cnt_uart[2]_i_1_n_0\
    );
\cnt_uart[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ABAAAAAA"
    )
        port map (
      I0 => is_counting,
      I1 => cnt_uart(3),
      I2 => cnt_uart(1),
      I3 => cnt_uart(0),
      I4 => cnt_uart(2),
      O => \cnt_uart[3]_i_1_n_0\
    );
\cnt_uart[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => cnt_uart(3),
      I1 => cnt_uart(2),
      I2 => cnt_uart(1),
      I3 => cnt_uart(0),
      O => \cnt_uart[3]_i_2_n_0\
    );
\cnt_uart_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[0]_i_1_n_0\,
      Q => cnt_uart(0),
      R => '0'
    );
\cnt_uart_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[1]_i_1_n_0\,
      Q => cnt_uart(1),
      R => '0'
    );
\cnt_uart_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[2]_i_1_n_0\,
      Q => cnt_uart(2),
      R => '0'
    );
\cnt_uart_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[3]_i_2_n_0\,
      Q => cnt_uart(3),
      R => '0'
    );
is_clk_initialized_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F2"
    )
        port map (
      I0 => start_init_clk_reg_i_3_n_0,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => is_clk_initialized_reg,
      O => is_clk_initialized_reg_i_1_n_0
    );
is_clk_initialized_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_clk_initialized_reg_i_1_n_0,
      Q => is_clk_initialized_reg,
      R => '0'
    );
is_counting_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => is_counting,
      I1 => pll_data_ready_prev,
      I2 => pll_data_ready,
      I3 => uart_byte_received,
      I4 => uart_byte_received_prev,
      I5 => is_counting_i_2_n_0,
      O => is_counting_i_1_n_0
    );
is_counting_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => cnt_uart(2),
      I1 => cnt_uart(0),
      I2 => cnt_uart(1),
      I3 => cnt_uart(3),
      O => is_counting_i_2_n_0
    );
is_counting_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_counting_i_1_n_0,
      Q => is_counting,
      R => '0'
    );
pll_data_ready_prev_reg: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_data_ready,
      Q => pll_data_ready_prev,
      R => '0'
    );
pll_trig_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00CE"
    )
        port map (
      I0 => \^pll_trig\,
      I1 => \trig_tguard_counter[16]_i_1_n_0\,
      I2 => trig_tguard_counter,
      I3 => trig_tsweep_counter079_out,
      O => pll_trig_reg_i_1_n_0
    );
pll_trig_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_trig_reg_i_1_n_0,
      Q => \^pll_trig\,
      R => '0'
    );
start_init_clk_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEF00"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => start_init_clk_reg_i_3_n_0,
      I3 => \^start_init_clk\,
      I4 => start_init_clk_reg_i_4_n_0,
      O => start_init_clk_reg_i_1_n_0
    );
start_init_clk_reg_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(0),
      I1 => clk_sync_counter_reg_reg(1),
      I2 => clk_sync_counter_reg_reg(2),
      O => start_init_clk_reg_i_10_n_0
    );
start_init_clk_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(18),
      I1 => clk_sync_counter_reg_reg(19),
      I2 => clk_sync_counter_reg_reg(20),
      I3 => clk_sync_counter_reg_reg(21),
      I4 => clk_sync_counter_reg_reg(23),
      I5 => clk_sync_counter_reg_reg(22),
      O => start_init_clk_reg_i_11_n_0
    );
start_init_clk_reg_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(6),
      I1 => clk_sync_counter_reg_reg(4),
      I2 => clk_sync_counter_reg_reg(11),
      O => start_init_clk_reg_i_12_n_0
    );
start_init_clk_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(27),
      I1 => clk_sync_counter_reg_reg(28),
      I2 => clk_sync_counter_reg_reg(29),
      I3 => clk_sync_counter_reg_reg(26),
      I4 => clk_sync_counter_reg_reg(24),
      I5 => clk_sync_counter_reg_reg(25),
      O => start_init_clk_reg_i_13_n_0
    );
start_init_clk_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0155FFFFFFFFFFFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(6),
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_sync_counter_reg_reg(10),
      I5 => clk_sync_counter_reg_reg(9),
      O => start_init_clk_reg_i_14_n_0
    );
start_init_clk_reg_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(9),
      I1 => clk_sync_counter_reg_reg(10),
      O => start_init_clk_reg_i_15_n_0
    );
start_init_clk_reg_i_16: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(7),
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(6),
      O => start_init_clk_reg_i_16_n_0
    );
start_init_clk_reg_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(30),
      I1 => clk_sync_counter_reg_reg(31),
      O => start_init_clk_reg_i_17_n_0
    );
start_init_clk_reg_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(7),
      O => start_init_clk_reg_i_18_n_0
    );
start_init_clk_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11110111"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => start_init_clk_reg_i_5_n_0,
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg_reg(16),
      I4 => start_init_clk_reg_i_6_n_0,
      O => start_init_clk_reg_i_2_n_0
    );
start_init_clk_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEAA0000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(16),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => start_init_clk_reg_i_7_n_0,
      I3 => clk_sync_counter_reg_reg(15),
      I4 => clk_sync_counter_reg_reg(17),
      I5 => start_init_clk_reg_i_5_n_0,
      O => start_init_clk_reg_i_3_n_0
    );
start_init_clk_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => start_init_clk_reg_i_8_n_0,
      I1 => start_init_clk_reg_i_9_n_0,
      I2 => start_init_clk_reg_i_10_n_0,
      I3 => start_init_clk_reg_i_11_n_0,
      I4 => start_init_clk_reg_i_12_n_0,
      I5 => start_init_clk_reg_i_13_n_0,
      O => start_init_clk_reg_i_4_n_0
    );
start_init_clk_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(30),
      I1 => clk_sync_counter_reg_reg(31),
      I2 => start_init_clk_reg_i_11_n_0,
      I3 => start_init_clk_reg_i_13_n_0,
      O => start_init_clk_reg_i_5_n_0
    );
start_init_clk_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(12),
      I4 => start_init_clk_reg_i_14_n_0,
      O => start_init_clk_reg_i_6_n_0
    );
start_init_clk_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => start_init_clk_reg_i_15_n_0,
      I1 => clk_sync_counter_reg_reg(11),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(13),
      I4 => clk_sync_counter_reg_reg(12),
      I5 => start_init_clk_reg_i_16_n_0,
      O => start_init_clk_reg_i_7_n_0
    );
start_init_clk_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => start_init_clk_reg_i_15_n_0,
      I3 => clk_sync_counter_reg_reg(14),
      I4 => clk_sync_counter_reg_reg(17),
      I5 => start_init_clk_reg_i_17_n_0,
      O => start_init_clk_reg_i_8_n_0
    );
start_init_clk_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(3),
      I3 => start_init_clk_reg_i_18_n_0,
      O => start_init_clk_reg_i_9_n_0
    );
start_init_clk_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => start_init_clk_reg_i_1_n_0,
      Q => \^start_init_clk\,
      R => '0'
    );
start_uart_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => uart_start_sending_reg_n_0,
      I1 => uart_adc_start_sending,
      O => start_uart_send
    );
t_sweep_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_sweep_INST_0_i_1_n_0,
      O => t_sweep
    );
t_sweep_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => trig_tsweep_counter_reg(2),
      I1 => trig_tsweep_counter_reg(1),
      I2 => trig_tsweep_counter_reg(0),
      I3 => t_sweep_INST_0_i_2_n_0,
      I4 => t_sweep_INST_0_i_3_n_0,
      I5 => t_sweep_INST_0_i_4_n_0,
      O => t_sweep_INST_0_i_1_n_0
    );
t_sweep_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(6),
      I1 => trig_tsweep_counter_reg(5),
      I2 => trig_tsweep_counter_reg(4),
      I3 => trig_tsweep_counter_reg(3),
      O => t_sweep_INST_0_i_2_n_0
    );
t_sweep_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(10),
      I1 => trig_tsweep_counter_reg(9),
      I2 => trig_tsweep_counter_reg(8),
      I3 => trig_tsweep_counter_reg(7),
      O => t_sweep_INST_0_i_3_n_0
    );
t_sweep_INST_0_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(11),
      I1 => trig_tsweep_counter_reg(12),
      I2 => trig_tsweep_counter_reg(13),
      I3 => trig_tsweep_counter_reg(14),
      I4 => trig_tsweep_counter_reg(16),
      I5 => trig_tsweep_counter_reg(15),
      O => t_sweep_INST_0_i_4_n_0
    );
\trig_tguard_counter[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"888F88F8"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_6_n_0\,
      I1 => \trig_tsweep_counter[0]_i_4_n_0\,
      I2 => \trig_tguard_counter_reg_n_0_[0]\,
      I3 => \trig_tguard_counter[0]_i_2_n_0\,
      I4 => trig_tguard_counter,
      O => \trig_tguard_counter[0]_i_1_n_0\
    );
\trig_tguard_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000040000000000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_8_n_0\,
      I1 => \trig_tguard_counter[0]_i_3_n_0\,
      I2 => \trig_tguard_counter_reg_n_0_[0]\,
      I3 => \trig_tguard_counter_reg_n_0_[2]\,
      I4 => \trig_tguard_counter_reg_n_0_[1]\,
      I5 => t_sweep_INST_0_i_1_n_0,
      O => \trig_tguard_counter[0]_i_2_n_0\
    );
\trig_tguard_counter[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[6]\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[4]\,
      I3 => \trig_tguard_counter_reg_n_0_[3]\,
      O => \trig_tguard_counter[0]_i_3_n_0\
    );
\trig_tguard_counter[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF404040"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_4_n_0\,
      I1 => \trig_tguard_counter[16]_i_5_n_0\,
      I2 => \trig_tguard_counter[16]_i_6_n_0\,
      I3 => t_sweep_INST_0_i_1_n_0,
      I4 => \trig_tguard_counter[16]_i_7_n_0\,
      I5 => \trig_tguard_counter[16]_i_8_n_0\,
      O => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter[16]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(1),
      I1 => trig_tsweep_counter_reg(0),
      I2 => trig_tsweep_counter_reg(3),
      I3 => trig_tsweep_counter_reg(2),
      O => \trig_tguard_counter[16]_i_10_n_0\
    );
\trig_tguard_counter[16]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[3]\,
      I1 => \trig_tguard_counter_reg_n_0_[4]\,
      O => \trig_tguard_counter[16]_i_11_n_0\
    );
\trig_tguard_counter[16]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[12]\,
      I1 => \trig_tguard_counter_reg_n_0_[15]\,
      I2 => \trig_tguard_counter_reg_n_0_[11]\,
      I3 => \trig_tguard_counter_reg_n_0_[9]\,
      O => \trig_tguard_counter[16]_i_12_n_0\
    );
\trig_tguard_counter[16]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[10]\,
      I1 => \trig_tguard_counter_reg_n_0_[14]\,
      I2 => \trig_tguard_counter_reg_n_0_[13]\,
      I3 => \trig_tguard_counter_reg_n_0_[16]\,
      O => \trig_tguard_counter[16]_i_13_n_0\
    );
\trig_tguard_counter[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000002A002AAA"
    )
        port map (
      I0 => t_sweep_INST_0_i_1_n_0,
      I1 => \trig_tguard_counter_reg_n_0_[6]\,
      I2 => \trig_tguard_counter_reg_n_0_[5]\,
      I3 => \trig_tguard_counter[16]_i_4_n_0\,
      I4 => \trig_tguard_counter[16]_i_9_n_0\,
      I5 => \trig_tguard_counter[16]_i_8_n_0\,
      O => trig_tguard_counter
    );
\trig_tguard_counter[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[4]\,
      I1 => \trig_tguard_counter_reg_n_0_[3]\,
      I2 => \trig_tguard_counter_reg_n_0_[2]\,
      O => \trig_tguard_counter[16]_i_4_n_0\
    );
\trig_tguard_counter[16]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_9_n_0\,
      I1 => trig_tsweep_counter_reg(15),
      I2 => trig_tsweep_counter_reg(16),
      I3 => trig_tsweep_counter_reg(13),
      I4 => trig_tsweep_counter_reg(14),
      O => \trig_tguard_counter[16]_i_5_n_0\
    );
\trig_tguard_counter[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_10_n_0\,
      I1 => trig_tsweep_counter_reg(7),
      I2 => trig_tsweep_counter_reg(6),
      I3 => trig_tsweep_counter_reg(5),
      I4 => trig_tsweep_counter_reg(4),
      I5 => \trig_tsweep_counter[0]_i_5_n_0\,
      O => \trig_tguard_counter[16]_i_6_n_0\
    );
\trig_tguard_counter[16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_11_n_0\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[6]\,
      I3 => \trig_tguard_counter_reg_n_0_[0]\,
      I4 => \trig_tguard_counter_reg_n_0_[2]\,
      I5 => \trig_tguard_counter_reg_n_0_[1]\,
      O => \trig_tguard_counter[16]_i_7_n_0\
    );
\trig_tguard_counter[16]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[8]\,
      I1 => \trig_tguard_counter_reg_n_0_[7]\,
      I2 => \trig_tguard_counter[16]_i_12_n_0\,
      I3 => \trig_tguard_counter[16]_i_13_n_0\,
      O => \trig_tguard_counter[16]_i_8_n_0\
    );
\trig_tguard_counter[16]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[6]\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[1]\,
      I3 => \trig_tguard_counter_reg_n_0_[0]\,
      O => \trig_tguard_counter[16]_i_9_n_0\
    );
\trig_tguard_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => \trig_tguard_counter[0]_i_1_n_0\,
      Q => \trig_tguard_counter_reg_n_0_[0]\,
      R => '0'
    );
\trig_tguard_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(10),
      Q => \trig_tguard_counter_reg_n_0_[10]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(11),
      Q => \trig_tguard_counter_reg_n_0_[11]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(12),
      Q => \trig_tguard_counter_reg_n_0_[12]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(12 downto 9),
      S(3) => \trig_tguard_counter_reg_n_0_[12]\,
      S(2) => \trig_tguard_counter_reg_n_0_[11]\,
      S(1) => \trig_tguard_counter_reg_n_0_[10]\,
      S(0) => \trig_tguard_counter_reg_n_0_[9]\
    );
\trig_tguard_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(13),
      Q => \trig_tguard_counter_reg_n_0_[13]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(14),
      Q => \trig_tguard_counter_reg_n_0_[14]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(15),
      Q => \trig_tguard_counter_reg_n_0_[15]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(16),
      Q => \trig_tguard_counter_reg_n_0_[16]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[16]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(3) => \NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \trig_tguard_counter_reg[16]_i_3_n_1\,
      CO(1) => \trig_tguard_counter_reg[16]_i_3_n_2\,
      CO(0) => \trig_tguard_counter_reg[16]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(16 downto 13),
      S(3) => \trig_tguard_counter_reg_n_0_[16]\,
      S(2) => \trig_tguard_counter_reg_n_0_[15]\,
      S(1) => \trig_tguard_counter_reg_n_0_[14]\,
      S(0) => \trig_tguard_counter_reg_n_0_[13]\
    );
\trig_tguard_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(1),
      Q => \trig_tguard_counter_reg_n_0_[1]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(2),
      Q => \trig_tguard_counter_reg_n_0_[2]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(3),
      Q => \trig_tguard_counter_reg_n_0_[3]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(4),
      Q => \trig_tguard_counter_reg_n_0_[4]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[4]_i_1_n_3\,
      CYINIT => \trig_tguard_counter_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(4 downto 1),
      S(3) => \trig_tguard_counter_reg_n_0_[4]\,
      S(2) => \trig_tguard_counter_reg_n_0_[3]\,
      S(1) => \trig_tguard_counter_reg_n_0_[2]\,
      S(0) => \trig_tguard_counter_reg_n_0_[1]\
    );
\trig_tguard_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(5),
      Q => \trig_tguard_counter_reg_n_0_[5]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(6),
      Q => \trig_tguard_counter_reg_n_0_[6]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(7),
      Q => \trig_tguard_counter_reg_n_0_[7]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(8),
      Q => \trig_tguard_counter_reg_n_0_[8]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(8 downto 5),
      S(3) => \trig_tguard_counter_reg_n_0_[8]\,
      S(2) => \trig_tguard_counter_reg_n_0_[7]\,
      S(1) => \trig_tguard_counter_reg_n_0_[6]\,
      S(0) => \trig_tguard_counter_reg_n_0_[5]\
    );
\trig_tguard_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(9),
      Q => \trig_tguard_counter_reg_n_0_[9]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => trig_tguard_counter,
      I1 => \trig_tguard_counter[16]_i_1_n_0\,
      O => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F010"
    )
        port map (
      I0 => trig_tsweep_counter_reg(6),
      I1 => trig_tsweep_counter_reg(7),
      I2 => \trig_tsweep_counter[0]_i_4_n_0\,
      I3 => \trig_tsweep_counter[0]_i_5_n_0\,
      O => trig_tsweep_counter079_out
    );
\trig_tsweep_counter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_4_n_0\,
      I1 => \trig_tsweep_counter[0]_i_7_n_0\,
      I2 => \trig_tguard_counter[16]_i_12_n_0\,
      I3 => \trig_tguard_counter[16]_i_13_n_0\,
      I4 => \trig_tsweep_counter[0]_i_8_n_0\,
      I5 => \trig_tguard_counter[16]_i_9_n_0\,
      O => \trig_tsweep_counter[0]_i_4_n_0\
    );
\trig_tsweep_counter[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => trig_tsweep_counter_reg(9),
      I1 => trig_tsweep_counter_reg(10),
      I2 => trig_tsweep_counter_reg(8),
      I3 => trig_tsweep_counter_reg(11),
      I4 => trig_tsweep_counter_reg(12),
      O => \trig_tsweep_counter[0]_i_5_n_0\
    );
\trig_tsweep_counter[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => trig_tsweep_counter_reg(0),
      O => \trig_tsweep_counter[0]_i_6_n_0\
    );
\trig_tsweep_counter[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[7]\,
      I1 => \trig_tguard_counter_reg_n_0_[8]\,
      O => \trig_tsweep_counter[0]_i_7_n_0\
    );
\trig_tsweep_counter[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(14),
      I1 => trig_tsweep_counter_reg(13),
      I2 => trig_tsweep_counter_reg(16),
      I3 => trig_tsweep_counter_reg(15),
      O => \trig_tsweep_counter[0]_i_8_n_0\
    );
\trig_tsweep_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      Q => trig_tsweep_counter_reg(0),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(2) => \trig_tsweep_counter_reg[0]_i_3_n_1\,
      CO(1) => \trig_tsweep_counter_reg[0]_i_3_n_2\,
      CO(0) => \trig_tsweep_counter_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      O(2) => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      O(1) => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      O(0) => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      S(3 downto 1) => trig_tsweep_counter_reg(3 downto 1),
      S(0) => \trig_tsweep_counter[0]_i_6_n_0\
    );
\trig_tsweep_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(10),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(11),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(12),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(3) => \trig_tsweep_counter_reg[12]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(15 downto 12)
    );
\trig_tsweep_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(13),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(14),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(15),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[16]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(16),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \trig_tsweep_counter_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => trig_tsweep_counter_reg(16)
    );
\trig_tsweep_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      Q => trig_tsweep_counter_reg(1),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      Q => trig_tsweep_counter_reg(2),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      Q => trig_tsweep_counter_reg(3),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(4),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(3) => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(7 downto 4)
    );
\trig_tsweep_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(5),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(6),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(7),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(8),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(11 downto 8)
    );
\trig_tsweep_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter079_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(9),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
uart_byte_received_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => uart_byte_received,
      Q => uart_byte_received_prev,
      R => '0'
    );
uart_start_sending_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55550010"
    )
        port map (
      I0 => is_counting_i_2_n_0,
      I1 => \^out_data_uart\(0),
      I2 => pll_data_ready,
      I3 => pll_data_ready_prev,
      I4 => uart_start_sending_reg_n_0,
      O => uart_start_sending_i_1_n_0
    );
uart_start_sending_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => uart_start_sending_i_1_n_0,
      Q => uart_start_sending_reg_n_0,
      R => '0'
    );
\uart_tx_data[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => pll_data_ready_prev,
      I1 => pll_data_ready,
      I2 => \^out_data_uart\(0),
      O => \uart_tx_data[4]_i_1_n_0\
    );
\uart_tx_data[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_wizard_main_n_4,
      I1 => clk_wizard_main_n_1,
      O => uart_byte_received_prev0
    );
\uart_tx_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => \uart_tx_data[4]_i_1_n_0\,
      Q => \^out_data_uart\(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    in_uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk_100MHz_in : in STD_LOGIC;
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    pll_data_ready : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    pll_read_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 );
    t_sweep : out STD_LOGIC;
    clk_100MHz_out : out STD_LOGIC;
    user_led : out STD_LOGIC;
    pamp_en : out STD_LOGIC;
    sw_ls : out STD_LOGIC;
    if_amp1_stu_en : out STD_LOGIC;
    if_amp2_stu_en : out STD_LOGIC;
    sw_if1 : out STD_LOGIC;
    sw_if2 : out STD_LOGIC;
    sw_if3 : out STD_LOGIC;
    clk_sync : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    sw_ctrl : out STD_LOGIC;
    atten : out STD_LOGIC_VECTOR ( 5 downto 0 );
    start_uart_send : out STD_LOGIC;
    is_adc_data_sending : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    pll_read : out STD_LOGIC;
    pll_write : out STD_LOGIC;
    pll_write_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_addres : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_addres : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_top_logic_0_0,top_logic,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "top_logic,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^atten\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \^out_data_uart\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \^start_init_clk\ : STD_LOGIC;
begin
  atten(5) <= \^atten\(4);
  atten(4) <= \^atten\(4);
  atten(3) <= \^atten\(4);
  atten(2) <= \^atten\(4);
  atten(1) <= \^atten\(4);
  atten(0) <= \^atten\(4);
  clk_sync <= \^start_init_clk\;
  if_amp1_stu_en <= \<const0>\;
  if_amp2_stu_en <= \<const0>\;
  is_adc_data_sending <= \<const0>\;
  out_data_uart(7) <= \<const0>\;
  out_data_uart(6) <= \<const0>\;
  out_data_uart(5) <= \<const0>\;
  out_data_uart(4) <= \^out_data_uart\(4);
  out_data_uart(3) <= \<const0>\;
  out_data_uart(2) <= \<const0>\;
  out_data_uart(1) <= \^out_data_uart\(4);
  out_data_uart(0) <= \<const1>\;
  pamp_en <= \<const0>\;
  pll_read <= \<const0>\;
  pll_read_addres(7) <= \<const0>\;
  pll_read_addres(6) <= \<const0>\;
  pll_read_addres(5) <= \<const0>\;
  pll_read_addres(4) <= \<const0>\;
  pll_read_addres(3) <= \<const0>\;
  pll_read_addres(2) <= \<const0>\;
  pll_read_addres(1) <= \<const0>\;
  pll_read_addres(0) <= \<const0>\;
  pll_write <= \<const0>\;
  pll_write_addres(4) <= \<const0>\;
  pll_write_addres(3) <= \<const0>\;
  pll_write_addres(2) <= \<const0>\;
  pll_write_addres(1) <= \<const0>\;
  pll_write_addres(0) <= \<const0>\;
  pll_write_data(23) <= \<const0>\;
  pll_write_data(22) <= \<const0>\;
  pll_write_data(21) <= \<const0>\;
  pll_write_data(20) <= \<const0>\;
  pll_write_data(19) <= \<const0>\;
  pll_write_data(18) <= \<const0>\;
  pll_write_data(17) <= \<const0>\;
  pll_write_data(16) <= \<const0>\;
  pll_write_data(15) <= \<const0>\;
  pll_write_data(14) <= \<const0>\;
  pll_write_data(13) <= \<const0>\;
  pll_write_data(12) <= \<const0>\;
  pll_write_data(11) <= \<const0>\;
  pll_write_data(10) <= \<const0>\;
  pll_write_data(9) <= \<const0>\;
  pll_write_data(8) <= \<const0>\;
  pll_write_data(7) <= \<const0>\;
  pll_write_data(6) <= \<const0>\;
  pll_write_data(5) <= \<const0>\;
  pll_write_data(4) <= \<const0>\;
  pll_write_data(3) <= \<const0>\;
  pll_write_data(2) <= \<const0>\;
  pll_write_data(1) <= \<const0>\;
  pll_write_data(0) <= \<const0>\;
  start_init_clk <= \^start_init_clk\;
  sw_ctrl <= \<const0>\;
  sw_if1 <= \<const1>\;
  sw_if2 <= \<const0>\;
  sw_if3 <= \<const0>\;
  sw_ls <= \<const1>\;
  user_led <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_logic
     port map (
      atten(0) => \^atten\(4),
      clk_100MHz_in => clk_100MHz_in,
      clk_100MHz_out => clk_100MHz_out,
      clk_10MHz_out => clk_10MHz_out,
      clk_20MHz_out => clk_20MHz_out,
      out_data_uart(0) => \^out_data_uart\(4),
      pll_data_ready => pll_data_ready,
      pll_trig => pll_trig,
      start_init_clk => \^start_init_clk\,
      start_uart_send => start_uart_send,
      t_sweep => t_sweep,
      uart_adc_start_sending => uart_adc_start_sending,
      uart_byte_received => uart_byte_received
    );
end STRUCTURE;

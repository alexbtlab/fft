-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 11:48:55 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_adc_control_0_0_stub.vhdl
-- Design      : design_1_adc_control_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    test1 : in STD_LOGIC;
    test2 : in STD_LOGIC;
    test3 : in STD_LOGIC;
    test4 : in STD_LOGIC;
    tsweep : in STD_LOGIC;
    uart_clk : in STD_LOGIC;
    output_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    is_sending : in STD_LOGIC;
    send_uart_byte : out STD_LOGIC;
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clkx_p : in STD_LOGIC;
    fpga_clkx_n : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "test1,test2,test3,test4,tsweep,uart_clk,output_data[7:0],is_sending,send_uart_byte,adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,fpga_clkx_p,fpga_clkx_n,adc_clkx_p,adc_clkx_n";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "adc_control,Vivado 2019.1";
begin
end;

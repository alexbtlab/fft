// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 16:06:19 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_send_0_0_sim_netlist.v
// Design      : design_1_uart_send_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_uart_send_0_0,uart_send,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "uart_send,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (uart_data,
    clk,
    uart_tx,
    is_transmission_going,
    send_byte);
  input [7:0]uart_data;
  input clk;
  output uart_tx;
  output is_transmission_going;
  input send_byte;

  wire clk;
  wire is_transmission_going;
  wire send_byte;
  wire [7:0]uart_data;
  wire uart_tx;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send inst
       (.clk(clk),
        .is_transmission_going_reg_reg_0(is_transmission_going),
        .send_byte(send_byte),
        .uart_data(uart_data),
        .uart_tx(uart_tx));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send
   (is_transmission_going_reg_reg_0,
    uart_tx,
    send_byte,
    clk,
    uart_data);
  output is_transmission_going_reg_reg_0;
  output uart_tx;
  input send_byte;
  input clk;
  input [7:0]uart_data;

  wire [3:1]bit_num;
  wire \bit_num[0]_i_1_n_0 ;
  wire [3:0]bit_num_reg;
  wire clk;
  wire [6:0]cnt;
  wire \cnt[2]_i_2_n_0 ;
  wire \cnt[2]_i_3_n_0 ;
  wire \cnt[3]_i_2_n_0 ;
  wire \cnt[4]_i_2_n_0 ;
  wire \cnt[5]_i_2_n_0 ;
  wire \cnt[6]_i_2_n_0 ;
  wire \cnt[6]_i_3_n_0 ;
  wire \cnt[6]_i_4_n_0 ;
  wire \cnt[6]_i_5_n_0 ;
  wire [6:0]cnt_0;
  wire is_transmission_going_reg_i_1_n_0;
  wire is_transmission_going_reg_i_2_n_0;
  wire is_transmission_going_reg_reg_0;
  wire send_byte;
  wire send_byte_prev;
  wire [7:0]uart_data;
  wire uart_tx;
  wire uart_tx_reg_i_10_n_0;
  wire uart_tx_reg_i_11_n_0;
  wire uart_tx_reg_i_12_n_0;
  wire uart_tx_reg_i_1_n_0;
  wire uart_tx_reg_i_2_n_0;
  wire uart_tx_reg_i_3_n_0;
  wire uart_tx_reg_i_4_n_0;
  wire uart_tx_reg_i_5_n_0;
  wire uart_tx_reg_i_6_n_0;
  wire uart_tx_reg_i_7_n_0;
  wire uart_tx_reg_i_8_n_0;
  wire uart_tx_reg_i_9_n_0;

  LUT5 #(
    .INIT(32'h666666A6)) 
    \bit_num[0]_i_1 
       (.I0(uart_tx_reg_i_5_n_0),
        .I1(bit_num_reg[0]),
        .I2(send_byte),
        .I3(send_byte_prev),
        .I4(is_transmission_going_reg_reg_0),
        .O(\bit_num[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7878787878780078)) 
    \bit_num[1]_i_1 
       (.I0(uart_tx_reg_i_5_n_0),
        .I1(bit_num_reg[0]),
        .I2(bit_num_reg[1]),
        .I3(send_byte),
        .I4(send_byte_prev),
        .I5(is_transmission_going_reg_reg_0),
        .O(bit_num[1]));
  LUT5 #(
    .INIT(32'h007F0080)) 
    \bit_num[2]_i_1 
       (.I0(bit_num_reg[0]),
        .I1(uart_tx_reg_i_5_n_0),
        .I2(bit_num_reg[1]),
        .I3(uart_tx_reg_i_2_n_0),
        .I4(bit_num_reg[2]),
        .O(bit_num[2]));
  LUT6 #(
    .INIT(64'h00007FFF00008000)) 
    \bit_num[3]_i_1 
       (.I0(bit_num_reg[2]),
        .I1(bit_num_reg[1]),
        .I2(uart_tx_reg_i_5_n_0),
        .I3(bit_num_reg[0]),
        .I4(uart_tx_reg_i_2_n_0),
        .I5(bit_num_reg[3]),
        .O(bit_num[3]));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\bit_num[0]_i_1_n_0 ),
        .Q(bit_num_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[1]),
        .Q(bit_num_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[2]),
        .Q(bit_num_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[3]),
        .Q(bit_num_reg[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h6466)) 
    \cnt[0]_i_1 
       (.I0(cnt[0]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(cnt_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6C606C6C)) 
    \cnt[1]_i_1 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .I2(is_transmission_going_reg_reg_0),
        .I3(send_byte_prev),
        .I4(send_byte),
        .O(cnt_0[1]));
  LUT6 #(
    .INIT(64'h000000000000F6FF)) 
    \cnt[2]_i_1 
       (.I0(\cnt[5]_i_2_n_0 ),
        .I1(cnt[5]),
        .I2(\cnt[2]_i_2_n_0 ),
        .I3(cnt[6]),
        .I4(uart_tx_reg_i_2_n_0),
        .I5(\cnt[2]_i_3_n_0 ),
        .O(cnt_0[2]));
  LUT6 #(
    .INIT(64'h7FFFFFFFEFFFFFFA)) 
    \cnt[2]_i_2 
       (.I0(cnt[4]),
        .I1(cnt[2]),
        .I2(is_transmission_going_reg_reg_0),
        .I3(cnt[0]),
        .I4(cnt[1]),
        .I5(cnt[3]),
        .O(\cnt[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h9555)) 
    \cnt[2]_i_3 
       (.I0(cnt[2]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(cnt[0]),
        .I3(cnt[1]),
        .O(\cnt[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0000AAA2)) 
    \cnt[3]_i_1 
       (.I0(\cnt[3]_i_2_n_0 ),
        .I1(send_byte),
        .I2(send_byte_prev),
        .I3(is_transmission_going_reg_reg_0),
        .I4(uart_tx_reg_i_5_n_0),
        .O(cnt_0[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \cnt[3]_i_2 
       (.I0(cnt[3]),
        .I1(cnt[1]),
        .I2(cnt[0]),
        .I3(is_transmission_going_reg_reg_0),
        .I4(cnt[2]),
        .O(\cnt[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    \cnt[4]_i_1 
       (.I0(\cnt[4]_i_2_n_0 ),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(cnt_0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \cnt[4]_i_2 
       (.I0(cnt[4]),
        .I1(cnt[2]),
        .I2(is_transmission_going_reg_reg_0),
        .I3(cnt[0]),
        .I4(cnt[1]),
        .I5(cnt[3]),
        .O(\cnt[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000099999909)) 
    \cnt[5]_i_1 
       (.I0(cnt[5]),
        .I1(\cnt[5]_i_2_n_0 ),
        .I2(send_byte),
        .I3(send_byte_prev),
        .I4(is_transmission_going_reg_reg_0),
        .I5(uart_tx_reg_i_5_n_0),
        .O(cnt_0[5]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt[5]_i_2 
       (.I0(cnt[4]),
        .I1(cnt[2]),
        .I2(is_transmission_going_reg_reg_0),
        .I3(cnt[0]),
        .I4(cnt[1]),
        .I5(cnt[3]),
        .O(\cnt[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0000AAA2)) 
    \cnt[6]_i_1 
       (.I0(\cnt[6]_i_2_n_0 ),
        .I1(send_byte),
        .I2(send_byte_prev),
        .I3(is_transmission_going_reg_reg_0),
        .I4(\cnt[6]_i_3_n_0 ),
        .O(cnt_0[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFDFFD)) 
    \cnt[6]_i_2 
       (.I0(cnt[5]),
        .I1(cnt[4]),
        .I2(cnt[2]),
        .I3(\cnt[6]_i_4_n_0 ),
        .I4(cnt[3]),
        .I5(\cnt[6]_i_5_n_0 ),
        .O(\cnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h59)) 
    \cnt[6]_i_3 
       (.I0(cnt[6]),
        .I1(cnt[5]),
        .I2(\cnt[5]_i_2_n_0 ),
        .O(\cnt[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt[6]_i_4 
       (.I0(cnt[1]),
        .I1(cnt[0]),
        .I2(is_transmission_going_reg_reg_0),
        .O(\cnt[6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h7E)) 
    \cnt[6]_i_5 
       (.I0(is_transmission_going_reg_reg_0),
        .I1(cnt[0]),
        .I2(cnt[1]),
        .O(\cnt[6]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[0]),
        .Q(cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[1]),
        .Q(cnt[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[2]),
        .Q(cnt[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[3]),
        .Q(cnt[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[4]),
        .Q(cnt[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[5]),
        .Q(cnt[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[6]),
        .Q(cnt[6]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBA00BABABABABABA)) 
    is_transmission_going_reg_i_1
       (.I0(is_transmission_going_reg_reg_0),
        .I1(send_byte_prev),
        .I2(send_byte),
        .I3(is_transmission_going_reg_i_2_n_0),
        .I4(bit_num_reg[0]),
        .I5(uart_tx_reg_i_5_n_0),
        .O(is_transmission_going_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF02FF)) 
    is_transmission_going_reg_i_2
       (.I0(send_byte),
        .I1(send_byte_prev),
        .I2(is_transmission_going_reg_reg_0),
        .I3(bit_num_reg[3]),
        .I4(bit_num_reg[2]),
        .I5(bit_num_reg[1]),
        .O(is_transmission_going_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_transmission_going_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(is_transmission_going_reg_i_1_n_0),
        .Q(is_transmission_going_reg_reg_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    send_byte_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(send_byte),
        .Q(send_byte_prev),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h20EFFFFF22222222)) 
    uart_tx_reg_i_1
       (.I0(uart_tx),
        .I1(uart_tx_reg_i_2_n_0),
        .I2(bit_num_reg[3]),
        .I3(uart_tx_reg_i_3_n_0),
        .I4(uart_tx_reg_i_4_n_0),
        .I5(uart_tx_reg_i_5_n_0),
        .O(uart_tx_reg_i_1_n_0));
  LUT4 #(
    .INIT(16'h10FF)) 
    uart_tx_reg_i_10
       (.I0(is_transmission_going_reg_reg_0),
        .I1(send_byte_prev),
        .I2(send_byte),
        .I3(bit_num_reg[1]),
        .O(uart_tx_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hFEFFAAAA0200AAAA)) 
    uart_tx_reg_i_11
       (.I0(uart_data[2]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(bit_num_reg[0]),
        .I5(uart_data[3]),
        .O(uart_tx_reg_i_11_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEBFFF)) 
    uart_tx_reg_i_12
       (.I0(cnt[3]),
        .I1(cnt[1]),
        .I2(cnt[0]),
        .I3(is_transmission_going_reg_reg_0),
        .I4(cnt[2]),
        .I5(cnt[4]),
        .O(uart_tx_reg_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h02)) 
    uart_tx_reg_i_2
       (.I0(send_byte),
        .I1(send_byte_prev),
        .I2(is_transmission_going_reg_reg_0),
        .O(uart_tx_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    uart_tx_reg_i_3
       (.I0(uart_tx_reg_i_6_n_0),
        .I1(uart_tx_reg_i_7_n_0),
        .I2(uart_tx_reg_i_8_n_0),
        .I3(uart_tx_reg_i_9_n_0),
        .I4(uart_tx_reg_i_10_n_0),
        .I5(uart_tx_reg_i_11_n_0),
        .O(uart_tx_reg_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    uart_tx_reg_i_4
       (.I0(bit_num_reg[0]),
        .I1(bit_num_reg[1]),
        .I2(bit_num_reg[2]),
        .I3(bit_num_reg[3]),
        .I4(uart_tx_reg_i_2_n_0),
        .O(uart_tx_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h2002)) 
    uart_tx_reg_i_5
       (.I0(cnt[6]),
        .I1(uart_tx_reg_i_12_n_0),
        .I2(cnt[5]),
        .I3(\cnt[5]_i_2_n_0 ),
        .O(uart_tx_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'hFEFFAAAA0200AAAA)) 
    uart_tx_reg_i_6
       (.I0(uart_data[4]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(bit_num_reg[0]),
        .I5(uart_data[5]),
        .O(uart_tx_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'hFEFFAAAA0200AAAA)) 
    uart_tx_reg_i_7
       (.I0(uart_data[6]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(bit_num_reg[0]),
        .I5(uart_data[7]),
        .O(uart_tx_reg_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    uart_tx_reg_i_8
       (.I0(bit_num_reg[2]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(uart_tx_reg_i_8_n_0));
  LUT6 #(
    .INIT(64'hFEFFAAAA0200AAAA)) 
    uart_tx_reg_i_9
       (.I0(uart_data[0]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(bit_num_reg[0]),
        .I5(uart_data[1]),
        .O(uart_tx_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b1)) 
    uart_tx_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(uart_tx_reg_i_1_n_0),
        .Q(uart_tx),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 16:06:19 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_send_0_0_sim_netlist.vhdl
-- Design      : design_1_uart_send_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send is
  port (
    is_transmission_going_reg_reg_0 : out STD_LOGIC;
    uart_tx : out STD_LOGIC;
    send_byte : in STD_LOGIC;
    clk : in STD_LOGIC;
    uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send is
  signal bit_num : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \bit_num[0]_i_1_n_0\ : STD_LOGIC;
  signal bit_num_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal cnt : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \cnt[2]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[2]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[5]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal \cnt[6]_i_5_n_0\ : STD_LOGIC;
  signal cnt_0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal is_transmission_going_reg_i_1_n_0 : STD_LOGIC;
  signal is_transmission_going_reg_i_2_n_0 : STD_LOGIC;
  signal \^is_transmission_going_reg_reg_0\ : STD_LOGIC;
  signal send_byte_prev : STD_LOGIC;
  signal \^uart_tx\ : STD_LOGIC;
  signal uart_tx_reg_i_10_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_11_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_12_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_1_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_2_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_3_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_4_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_5_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_6_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_7_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_8_n_0 : STD_LOGIC;
  signal uart_tx_reg_i_9_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt[2]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt[3]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cnt[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt[6]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt[6]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cnt[6]_i_5\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of uart_tx_reg_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of uart_tx_reg_i_5 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of uart_tx_reg_i_8 : label is "soft_lutpair4";
begin
  is_transmission_going_reg_reg_0 <= \^is_transmission_going_reg_reg_0\;
  uart_tx <= \^uart_tx\;
\bit_num[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"666666A6"
    )
        port map (
      I0 => uart_tx_reg_i_5_n_0,
      I1 => bit_num_reg(0),
      I2 => send_byte,
      I3 => send_byte_prev,
      I4 => \^is_transmission_going_reg_reg_0\,
      O => \bit_num[0]_i_1_n_0\
    );
\bit_num[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7878787878780078"
    )
        port map (
      I0 => uart_tx_reg_i_5_n_0,
      I1 => bit_num_reg(0),
      I2 => bit_num_reg(1),
      I3 => send_byte,
      I4 => send_byte_prev,
      I5 => \^is_transmission_going_reg_reg_0\,
      O => bit_num(1)
    );
\bit_num[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"007F0080"
    )
        port map (
      I0 => bit_num_reg(0),
      I1 => uart_tx_reg_i_5_n_0,
      I2 => bit_num_reg(1),
      I3 => uart_tx_reg_i_2_n_0,
      I4 => bit_num_reg(2),
      O => bit_num(2)
    );
\bit_num[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00007FFF00008000"
    )
        port map (
      I0 => bit_num_reg(2),
      I1 => bit_num_reg(1),
      I2 => uart_tx_reg_i_5_n_0,
      I3 => bit_num_reg(0),
      I4 => uart_tx_reg_i_2_n_0,
      I5 => bit_num_reg(3),
      O => bit_num(3)
    );
\bit_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \bit_num[0]_i_1_n_0\,
      Q => bit_num_reg(0),
      R => '0'
    );
\bit_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(1),
      Q => bit_num_reg(1),
      R => '0'
    );
\bit_num_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(2),
      Q => bit_num_reg(2),
      R => '0'
    );
\bit_num_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => bit_num(3),
      Q => bit_num_reg(3),
      R => '0'
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6466"
    )
        port map (
      I0 => cnt(0),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => cnt_0(0)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6C606C6C"
    )
        port map (
      I0 => cnt(0),
      I1 => cnt(1),
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => send_byte_prev,
      I4 => send_byte,
      O => cnt_0(1)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000F6FF"
    )
        port map (
      I0 => \cnt[5]_i_2_n_0\,
      I1 => cnt(5),
      I2 => \cnt[2]_i_2_n_0\,
      I3 => cnt(6),
      I4 => uart_tx_reg_i_2_n_0,
      I5 => \cnt[2]_i_3_n_0\,
      O => cnt_0(2)
    );
\cnt[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFEFFFFFFA"
    )
        port map (
      I0 => cnt(4),
      I1 => cnt(2),
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => cnt(0),
      I4 => cnt(1),
      I5 => cnt(3),
      O => \cnt[2]_i_2_n_0\
    );
\cnt[2]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9555"
    )
        port map (
      I0 => cnt(2),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => cnt(0),
      I3 => cnt(1),
      O => \cnt[2]_i_3_n_0\
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAA2"
    )
        port map (
      I0 => \cnt[3]_i_2_n_0\,
      I1 => send_byte,
      I2 => send_byte_prev,
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => uart_tx_reg_i_5_n_0,
      O => cnt_0(3)
    );
\cnt[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => cnt(3),
      I1 => cnt(1),
      I2 => cnt(0),
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => cnt(2),
      O => \cnt[3]_i_2_n_0\
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => \cnt[4]_i_2_n_0\,
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => cnt_0(4)
    );
\cnt[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => cnt(4),
      I1 => cnt(2),
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => cnt(0),
      I4 => cnt(1),
      I5 => cnt(3),
      O => \cnt[4]_i_2_n_0\
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000099999909"
    )
        port map (
      I0 => cnt(5),
      I1 => \cnt[5]_i_2_n_0\,
      I2 => send_byte,
      I3 => send_byte_prev,
      I4 => \^is_transmission_going_reg_reg_0\,
      I5 => uart_tx_reg_i_5_n_0,
      O => cnt_0(5)
    );
\cnt[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt(4),
      I1 => cnt(2),
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => cnt(0),
      I4 => cnt(1),
      I5 => cnt(3),
      O => \cnt[5]_i_2_n_0\
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAA2"
    )
        port map (
      I0 => \cnt[6]_i_2_n_0\,
      I1 => send_byte,
      I2 => send_byte_prev,
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => \cnt[6]_i_3_n_0\,
      O => cnt_0(6)
    );
\cnt[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFDFFD"
    )
        port map (
      I0 => cnt(5),
      I1 => cnt(4),
      I2 => cnt(2),
      I3 => \cnt[6]_i_4_n_0\,
      I4 => cnt(3),
      I5 => \cnt[6]_i_5_n_0\,
      O => \cnt[6]_i_2_n_0\
    );
\cnt[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"59"
    )
        port map (
      I0 => cnt(6),
      I1 => cnt(5),
      I2 => \cnt[5]_i_2_n_0\,
      O => \cnt[6]_i_3_n_0\
    );
\cnt[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => cnt(1),
      I1 => cnt(0),
      I2 => \^is_transmission_going_reg_reg_0\,
      O => \cnt[6]_i_4_n_0\
    );
\cnt[6]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7E"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => cnt(0),
      I2 => cnt(1),
      O => \cnt[6]_i_5_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(0),
      Q => cnt(0),
      R => '0'
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(1),
      Q => cnt(1),
      R => '0'
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(2),
      Q => cnt(2),
      R => '0'
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(3),
      Q => cnt(3),
      R => '0'
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(4),
      Q => cnt(4),
      R => '0'
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(5),
      Q => cnt(5),
      R => '0'
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => cnt_0(6),
      Q => cnt(6),
      R => '0'
    );
is_transmission_going_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BA00BABABABABABA"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => send_byte_prev,
      I2 => send_byte,
      I3 => is_transmission_going_reg_i_2_n_0,
      I4 => bit_num_reg(0),
      I5 => uart_tx_reg_i_5_n_0,
      O => is_transmission_going_reg_i_1_n_0
    );
is_transmission_going_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF02FF"
    )
        port map (
      I0 => send_byte,
      I1 => send_byte_prev,
      I2 => \^is_transmission_going_reg_reg_0\,
      I3 => bit_num_reg(3),
      I4 => bit_num_reg(2),
      I5 => bit_num_reg(1),
      O => is_transmission_going_reg_i_2_n_0
    );
is_transmission_going_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => is_transmission_going_reg_i_1_n_0,
      Q => \^is_transmission_going_reg_reg_0\,
      R => '0'
    );
send_byte_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => send_byte,
      Q => send_byte_prev,
      R => '0'
    );
uart_tx_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20EFFFFF22222222"
    )
        port map (
      I0 => \^uart_tx\,
      I1 => uart_tx_reg_i_2_n_0,
      I2 => bit_num_reg(3),
      I3 => uart_tx_reg_i_3_n_0,
      I4 => uart_tx_reg_i_4_n_0,
      I5 => uart_tx_reg_i_5_n_0,
      O => uart_tx_reg_i_1_n_0
    );
uart_tx_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"10FF"
    )
        port map (
      I0 => \^is_transmission_going_reg_reg_0\,
      I1 => send_byte_prev,
      I2 => send_byte,
      I3 => bit_num_reg(1),
      O => uart_tx_reg_i_10_n_0
    );
uart_tx_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFAAAA0200AAAA"
    )
        port map (
      I0 => uart_data(2),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => bit_num_reg(0),
      I5 => uart_data(3),
      O => uart_tx_reg_i_11_n_0
    );
uart_tx_reg_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEBFFF"
    )
        port map (
      I0 => cnt(3),
      I1 => cnt(1),
      I2 => cnt(0),
      I3 => \^is_transmission_going_reg_reg_0\,
      I4 => cnt(2),
      I5 => cnt(4),
      O => uart_tx_reg_i_12_n_0
    );
uart_tx_reg_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => send_byte,
      I1 => send_byte_prev,
      I2 => \^is_transmission_going_reg_reg_0\,
      O => uart_tx_reg_i_2_n_0
    );
uart_tx_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => uart_tx_reg_i_6_n_0,
      I1 => uart_tx_reg_i_7_n_0,
      I2 => uart_tx_reg_i_8_n_0,
      I3 => uart_tx_reg_i_9_n_0,
      I4 => uart_tx_reg_i_10_n_0,
      I5 => uart_tx_reg_i_11_n_0,
      O => uart_tx_reg_i_3_n_0
    );
uart_tx_reg_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => bit_num_reg(0),
      I1 => bit_num_reg(1),
      I2 => bit_num_reg(2),
      I3 => bit_num_reg(3),
      I4 => uart_tx_reg_i_2_n_0,
      O => uart_tx_reg_i_4_n_0
    );
uart_tx_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2002"
    )
        port map (
      I0 => cnt(6),
      I1 => uart_tx_reg_i_12_n_0,
      I2 => cnt(5),
      I3 => \cnt[5]_i_2_n_0\,
      O => uart_tx_reg_i_5_n_0
    );
uart_tx_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFAAAA0200AAAA"
    )
        port map (
      I0 => uart_data(4),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => bit_num_reg(0),
      I5 => uart_data(5),
      O => uart_tx_reg_i_6_n_0
    );
uart_tx_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFAAAA0200AAAA"
    )
        port map (
      I0 => uart_data(6),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => bit_num_reg(0),
      I5 => uart_data(7),
      O => uart_tx_reg_i_7_n_0
    );
uart_tx_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A8AA"
    )
        port map (
      I0 => bit_num_reg(2),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      O => uart_tx_reg_i_8_n_0
    );
uart_tx_reg_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFAAAA0200AAAA"
    )
        port map (
      I0 => uart_data(0),
      I1 => \^is_transmission_going_reg_reg_0\,
      I2 => send_byte_prev,
      I3 => send_byte,
      I4 => bit_num_reg(0),
      I5 => uart_data(1),
      O => uart_tx_reg_i_9_n_0
    );
uart_tx_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => uart_tx_reg_i_1_n_0,
      Q => \^uart_tx\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC;
    uart_tx : out STD_LOGIC;
    is_transmission_going : out STD_LOGIC;
    send_byte : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_uart_send_0_0,uart_send,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "uart_send,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_send
     port map (
      clk => clk,
      is_transmission_going_reg_reg_0 => is_transmission_going,
      send_byte => send_byte,
      uart_data(7 downto 0) => uart_data(7 downto 0),
      uart_tx => uart_tx
    );
end STRUCTURE;

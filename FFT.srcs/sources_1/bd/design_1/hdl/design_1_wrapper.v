//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Wed Jun  3 15:14:35 2020
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (adc_clk_n,
    adc_clk_p,
    adc_da_n,
    adc_da_p,
    adc_db_n,
    adc_db_p,
    adc_dco_n,
    adc_dco_p,
    atten,
    clk_100MHz,
    clk_cs,
    clk_mosi,
    clk_sclk,
    clk_sync,
    fpga_clk_n,
    fpga_clk_p,
    if_amp1_stu_en,
    if_amp2_stu_en,
    pamp_en,
    pll_ld_sdo,
    pll_mosi,
    pll_sck,
    pll_sen,
    pll_trig,
    sw_ctrl,
    sw_if1,
    sw_if2,
    sw_if3,
    sw_ls,
    user_io_10,
    user_io_9,
    user_led);
  output adc_clk_n;
  output adc_clk_p;
  input adc_da_n;
  input adc_da_p;
  input adc_db_n;
  input adc_db_p;
  input adc_dco_n;
  input adc_dco_p;
  output [5:0]atten;
  input clk_100MHz;
  output clk_cs;
  output clk_mosi;
  output clk_sclk;
  output clk_sync;
  input fpga_clk_n;
  input fpga_clk_p;
  output if_amp1_stu_en;
  output if_amp2_stu_en;
  output pamp_en;
  input pll_ld_sdo;
  output pll_mosi;
  output pll_sck;
  output pll_sen;
  output pll_trig;
  output sw_ctrl;
  output sw_if1;
  output sw_if2;
  output sw_if3;
  output sw_ls;
  output user_io_10;
  input user_io_9;
  output user_led;

  wire adc_clk_n;
  wire adc_clk_p;
  wire adc_da_n;
  wire adc_da_p;
  wire adc_db_n;
  wire adc_db_p;
  wire adc_dco_n;
  wire adc_dco_p;
  wire [5:0]atten;
  wire clk_100MHz;
  wire clk_cs;
  wire clk_mosi;
  wire clk_sclk;
  wire clk_sync;
  wire fpga_clk_n;
  wire fpga_clk_p;
  wire if_amp1_stu_en;
  wire if_amp2_stu_en;
  wire pamp_en;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_sck;
  wire pll_sen;
  wire pll_trig;
  wire sw_ctrl;
  wire sw_if1;
  wire sw_if2;
  wire sw_if3;
  wire sw_ls;
  wire user_io_10;
  wire user_io_9;
  wire user_led;

  design_1 design_1_i
       (.adc_clk_n(adc_clk_n),
        .adc_clk_p(adc_clk_p),
        .adc_da_n(adc_da_n),
        .adc_da_p(adc_da_p),
        .adc_db_n(adc_db_n),
        .adc_db_p(adc_db_p),
        .adc_dco_n(adc_dco_n),
        .adc_dco_p(adc_dco_p),
        .atten(atten),
        .clk_100MHz(clk_100MHz),
        .clk_cs(clk_cs),
        .clk_mosi(clk_mosi),
        .clk_sclk(clk_sclk),
        .clk_sync(clk_sync),
        .fpga_clk_n(fpga_clk_n),
        .fpga_clk_p(fpga_clk_p),
        .if_amp1_stu_en(if_amp1_stu_en),
        .if_amp2_stu_en(if_amp2_stu_en),
        .pamp_en(pamp_en),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi(pll_mosi),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_trig(pll_trig),
        .sw_ctrl(sw_ctrl),
        .sw_if1(sw_if1),
        .sw_if2(sw_if2),
        .sw_if3(sw_if3),
        .sw_ls(sw_ls),
        .user_io_10(user_io_10),
        .user_io_9(user_io_9),
        .user_led(user_led));
endmodule

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 15:15:20 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               C:/project/fft/FFT.srcs/sources_1/bd/design_1/ip/design_1_top_logic_0_0/design_1_top_logic_0_0_sim_netlist.vhdl
-- Design      : design_1_top_logic_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_top_logic_0_0_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_top_logic_0_0_clk_wiz_0_clk_wiz : entity is "clk_wiz_0_clk_wiz";
end design_1_top_logic_0_0_clk_wiz_0_clk_wiz;

architecture STRUCTURE of design_1_top_logic_0_0_clk_wiz_0_clk_wiz is
  signal clk_in1_clk_wiz_0 : STD_LOGIC;
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clk_out4_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufg : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout4_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
clkout4_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out4_clk_wiz_0,
      O => clk_out4
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 10.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 50,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 100,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 125,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => clk_out3_clk_wiz_0,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => clk_out4_clk_wiz_0,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_top_logic_0_0_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_out4 : out STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_top_logic_0_0_clk_wiz_0 : entity is "clk_wiz_0";
end design_1_top_logic_0_0_clk_wiz_0;

architecture STRUCTURE of design_1_top_logic_0_0_clk_wiz_0 is
begin
inst: entity work.design_1_top_logic_0_0_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3,
      clk_out4 => clk_out4,
      locked => locked
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_top_logic_0_0_top_logic is
  port (
    clk_100MHz_out : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 );
    atten : out STD_LOGIC_VECTOR ( 5 downto 0 );
    pll_write_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_addres : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_addres : out STD_LOGIC_VECTOR ( 7 downto 0 );
    t_sweep : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    pll_write : out STD_LOGIC;
    sw_if1 : out STD_LOGIC;
    sw_if2 : out STD_LOGIC;
    pamp_en : out STD_LOGIC;
    if_amp2_stu_en : out STD_LOGIC;
    if_amp1_stu_en : out STD_LOGIC;
    sw_ctrl : out STD_LOGIC;
    sw_if3 : out STD_LOGIC;
    start_uart_send : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    pll_read : out STD_LOGIC;
    clk_100MHz_in : in STD_LOGIC;
    pll_data_ready : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    in_uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pll_read_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    uart_adc_start_sending : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_top_logic_0_0_top_logic : entity is "top_logic";
end design_1_top_logic_0_0_top_logic;

architecture STRUCTURE of design_1_top_logic_0_0_top_logic is
  signal atten_reg : STD_LOGIC;
  signal clk_sync_counter_reg : STD_LOGIC;
  signal clk_sync_counter_reg0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \clk_sync_counter_reg0_carry__0_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__0_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__3_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__4_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__5_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__6_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg0_carry__6_n_3\ : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_0 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_1 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_2 : STD_LOGIC;
  signal clk_sync_counter_reg0_carry_n_3 : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_10_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_11_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_12_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_14_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[12]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_8_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[16]_i_9_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[20]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[24]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[28]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_6_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[4]_i_7_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_4_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg[8]_i_5_n_0\ : STD_LOGIC;
  signal clk_sync_counter_reg_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \clk_sync_counter_reg_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \clk_sync_counter_reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal clk_wizard_main_n_1 : STD_LOGIC;
  signal clk_wizard_main_n_4 : STD_LOGIC;
  signal cnt_uart : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \cnt_uart[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_uart[3]_i_2_n_0\ : STD_LOGIC;
  signal if_amp1_en_reg_i_1_n_0 : STD_LOGIC;
  signal if_amp1_en_reg_i_2_n_0 : STD_LOGIC;
  signal \^if_amp1_stu_en\ : STD_LOGIC;
  signal if_amp2_en_reg_i_1_n_0 : STD_LOGIC;
  signal \^if_amp2_stu_en\ : STD_LOGIC;
  signal is_clk_initialized_reg : STD_LOGIC;
  signal is_clk_initialized_reg_i_1_n_0 : STD_LOGIC;
  signal is_counting : STD_LOGIC;
  signal is_counting_i_1_n_0 : STD_LOGIC;
  signal is_uart_receiving_atten_value_i_1_n_0 : STD_LOGIC;
  signal is_uart_receiving_atten_value_reg_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_data_i_1_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_data_i_2_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_data_reg_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_read_address : STD_LOGIC;
  signal is_uart_receiving_pll_read_address_i_1_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_write_address : STD_LOGIC;
  signal is_uart_receiving_pll_write_address67_out : STD_LOGIC;
  signal is_uart_receiving_pll_write_address_i_1_n_0 : STD_LOGIC;
  signal is_uart_receiving_pll_write_address_i_2_n_0 : STD_LOGIC;
  signal \^out_data_uart\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_2_in : STD_LOGIC_VECTOR ( 16 downto 1 );
  signal \^pamp_en\ : STD_LOGIC;
  signal pamp_en_reg_i_1_n_0 : STD_LOGIC;
  signal pamp_en_reg_i_2_n_0 : STD_LOGIC;
  signal pamp_en_reg_i_3_n_0 : STD_LOGIC;
  signal pamp_en_reg_i_4_n_0 : STD_LOGIC;
  signal pamp_en_reg_i_5_n_0 : STD_LOGIC;
  signal pll_address_reg : STD_LOGIC;
  signal pll_data_ready_prev : STD_LOGIC;
  signal \^pll_read\ : STD_LOGIC;
  signal pll_read_i_1_n_0 : STD_LOGIC;
  signal pll_read_i_2_n_0 : STD_LOGIC;
  signal \^pll_trig\ : STD_LOGIC;
  signal pll_trig_reg_i_1_n_0 : STD_LOGIC;
  signal \^pll_write\ : STD_LOGIC;
  signal pll_write_address_reg : STD_LOGIC;
  signal \^pll_write_data\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \pll_write_data[0]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[10]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[11]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[12]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[13]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[14]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[15]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[16]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[17]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[18]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[19]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[1]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[20]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[21]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[22]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[23]_i_2_n_0\ : STD_LOGIC;
  signal \pll_write_data[23]_i_3_n_0\ : STD_LOGIC;
  signal \pll_write_data[23]_i_4_n_0\ : STD_LOGIC;
  signal \pll_write_data[2]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[3]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[4]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[5]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[6]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[7]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[8]_i_1_n_0\ : STD_LOGIC;
  signal \pll_write_data[9]_i_1_n_0\ : STD_LOGIC;
  signal pll_write_i_1_n_0 : STD_LOGIC;
  signal pll_write_i_2_n_0 : STD_LOGIC;
  signal pll_write_i_3_n_0 : STD_LOGIC;
  signal \received_byte_num[0]_i_1_n_0\ : STD_LOGIC;
  signal \received_byte_num[0]_i_2_n_0\ : STD_LOGIC;
  signal \received_byte_num[0]_i_3_n_0\ : STD_LOGIC;
  signal \received_byte_num[0]_i_4_n_0\ : STD_LOGIC;
  signal \received_byte_num[0]_i_5_n_0\ : STD_LOGIC;
  signal \received_byte_num[0]_i_6_n_0\ : STD_LOGIC;
  signal \received_byte_num[1]_i_1_n_0\ : STD_LOGIC;
  signal \received_byte_num[1]_i_2_n_0\ : STD_LOGIC;
  signal \received_byte_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \received_byte_num_reg_n_0_[1]\ : STD_LOGIC;
  signal \^start_init_clk\ : STD_LOGIC;
  signal start_init_clk_reg_i_10_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_11_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_12_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_13_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_14_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_15_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_16_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_17_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_18_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_1_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_2_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_3_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_4_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_5_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_6_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_7_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_8_n_0 : STD_LOGIC;
  signal start_init_clk_reg_i_9_n_0 : STD_LOGIC;
  signal \^sw_ctrl\ : STD_LOGIC;
  signal sw_ctrl_reg_i_1_n_0 : STD_LOGIC;
  signal sw_ctrl_reg_i_2_n_0 : STD_LOGIC;
  signal \^sw_if1\ : STD_LOGIC;
  signal sw_if1_reg_i_1_n_0 : STD_LOGIC;
  signal \^sw_if2\ : STD_LOGIC;
  signal sw_if2_reg_i_1_n_0 : STD_LOGIC;
  signal sw_if2_reg_i_2_n_0 : STD_LOGIC;
  signal \^sw_if3\ : STD_LOGIC;
  signal sw_if3_reg_i_1_n_0 : STD_LOGIC;
  signal sw_if3_reg_i_2_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_1_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_2_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_3_n_0 : STD_LOGIC;
  signal t_sweep_INST_0_i_4_n_0 : STD_LOGIC;
  signal trig_tguard_counter : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_10_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_11_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_12_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_13_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_6_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_7_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_8_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[16]_i_9_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[16]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[10]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[11]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[12]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[13]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[14]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[15]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[16]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[3]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[4]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[5]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[6]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[7]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[8]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[9]\ : STD_LOGIC;
  signal trig_tsweep_counter074_out : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_7_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_8_n_0\ : STD_LOGIC;
  signal trig_tsweep_counter_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \trig_tsweep_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal uart_byte_received_prev : STD_LOGIC;
  signal uart_byte_received_prev0 : STD_LOGIC;
  signal uart_start_sending_i_1_n_0 : STD_LOGIC;
  signal uart_start_sending_i_2_n_0 : STD_LOGIC;
  signal uart_start_sending_i_3_n_0 : STD_LOGIC;
  signal uart_start_sending_reg_n_0 : STD_LOGIC;
  signal \uart_tx_data[0]_i_10_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_2_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_3_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_4_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_5_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_6_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_7_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_8_n_0\ : STD_LOGIC;
  signal \uart_tx_data[0]_i_9_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_2_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_3_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_4_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_5_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_6_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_7_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_8_n_0\ : STD_LOGIC;
  signal \uart_tx_data[1]_i_9_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_2_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_3_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_4_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_5_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_6_n_0\ : STD_LOGIC;
  signal \uart_tx_data[2]_i_7_n_0\ : STD_LOGIC;
  signal \uart_tx_data[3]_i_2_n_0\ : STD_LOGIC;
  signal \uart_tx_data[4]_i_2_n_0\ : STD_LOGIC;
  signal \uart_tx_data[4]_i_3_n_0\ : STD_LOGIC;
  signal \uart_tx_data[4]_i_4_n_0\ : STD_LOGIC;
  signal \uart_tx_data[4]_i_5_n_0\ : STD_LOGIC;
  signal \uart_tx_data[4]_i_6_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_1_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_4_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_5_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_6_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_7_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_8_n_0\ : STD_LOGIC;
  signal \uart_tx_data[7]_i_9_n_0\ : STD_LOGIC;
  signal \NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_12\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_14\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_7\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[0]_i_8\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \clk_sync_counter_reg[4]_i_7\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_uart[1]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \cnt_uart[2]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \cnt_uart[3]_i_2\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of if_amp1_en_reg_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of if_amp2_en_reg_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of is_clk_initialized_reg_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of is_uart_receiving_pll_data_i_2 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of is_uart_receiving_pll_write_address_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of pamp_en_reg_i_2 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of pamp_en_reg_i_4 : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of pamp_en_reg_i_5 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of pll_read_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of pll_read_i_2 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \pll_write_data[0]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \pll_write_data[10]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \pll_write_data[11]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \pll_write_data[12]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \pll_write_data[13]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \pll_write_data[14]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \pll_write_data[15]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \pll_write_data[18]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \pll_write_data[1]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \pll_write_data[22]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \pll_write_data[23]_i_3\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \pll_write_data[23]_i_4\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \pll_write_data[2]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \pll_write_data[3]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \pll_write_data[4]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \pll_write_data[5]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \pll_write_data[8]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \pll_write_data[9]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of pll_write_i_3 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \received_byte_num[0]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \received_byte_num[0]_i_4\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \received_byte_num[0]_i_5\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_12 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_15 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_16 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_17 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of start_init_clk_reg_i_5 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of sw_ctrl_reg_i_2 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of sw_if1_reg_i_1 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of sw_if2_reg_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of sw_if3_reg_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of sw_if3_reg_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \trig_tguard_counter[0]_i_3\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_4\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_5\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \trig_tguard_counter[16]_i_8\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \trig_tsweep_counter[0]_i_7\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \trig_tsweep_counter[0]_i_8\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \uart_tx_data[0]_i_7\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \uart_tx_data[0]_i_9\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \uart_tx_data[1]_i_5\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \uart_tx_data[1]_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \uart_tx_data[2]_i_3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \uart_tx_data[2]_i_5\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \uart_tx_data[2]_i_7\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \uart_tx_data[4]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \uart_tx_data[4]_i_4\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \uart_tx_data[4]_i_5\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \uart_tx_data[7]_i_9\ : label is "soft_lutpair12";
begin
  if_amp1_stu_en <= \^if_amp1_stu_en\;
  if_amp2_stu_en <= \^if_amp2_stu_en\;
  out_data_uart(7 downto 0) <= \^out_data_uart\(7 downto 0);
  pamp_en <= \^pamp_en\;
  pll_read <= \^pll_read\;
  pll_trig <= \^pll_trig\;
  pll_write <= \^pll_write\;
  pll_write_data(23 downto 0) <= \^pll_write_data\(23 downto 0);
  start_init_clk <= \^start_init_clk\;
  sw_ctrl <= \^sw_ctrl\;
  sw_if1 <= \^sw_if1\;
  sw_if2 <= \^sw_if2\;
  sw_if3 <= \^sw_if3\;
\atten_reg[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08080800"
    )
        port map (
      I0 => is_uart_receiving_atten_value_reg_n_0,
      I1 => uart_byte_received,
      I2 => uart_byte_received_prev,
      I3 => \received_byte_num_reg_n_0_[1]\,
      I4 => \received_byte_num_reg_n_0_[0]\,
      O => atten_reg
    );
\atten_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(0),
      Q => atten(0),
      R => '0'
    );
\atten_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(1),
      Q => atten(1),
      R => '0'
    );
\atten_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(2),
      Q => atten(2),
      R => '0'
    );
\atten_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(3),
      Q => atten(3),
      R => '0'
    );
\atten_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(4),
      Q => atten(4),
      R => '0'
    );
\atten_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => atten_reg,
      D => in_uart_data(5),
      Q => atten(5),
      R => '0'
    );
clk_sync_counter_reg0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => clk_sync_counter_reg0_carry_n_0,
      CO(2) => clk_sync_counter_reg0_carry_n_1,
      CO(1) => clk_sync_counter_reg0_carry_n_2,
      CO(0) => clk_sync_counter_reg0_carry_n_3,
      CYINIT => clk_sync_counter_reg_reg(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(4 downto 1),
      S(3 downto 0) => clk_sync_counter_reg_reg(4 downto 1)
    );
\clk_sync_counter_reg0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => clk_sync_counter_reg0_carry_n_0,
      CO(3) => \clk_sync_counter_reg0_carry__0_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__0_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__0_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(8 downto 5),
      S(3 downto 0) => clk_sync_counter_reg_reg(8 downto 5)
    );
\clk_sync_counter_reg0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__0_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__1_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__1_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__1_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(12 downto 9),
      S(3 downto 0) => clk_sync_counter_reg_reg(12 downto 9)
    );
\clk_sync_counter_reg0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__1_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__2_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__2_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__2_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(16 downto 13),
      S(3 downto 0) => clk_sync_counter_reg_reg(16 downto 13)
    );
\clk_sync_counter_reg0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__2_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__3_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__3_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__3_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(20 downto 17),
      S(3 downto 0) => clk_sync_counter_reg_reg(20 downto 17)
    );
\clk_sync_counter_reg0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__3_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__4_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__4_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__4_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(24 downto 21),
      S(3 downto 0) => clk_sync_counter_reg_reg(24 downto 21)
    );
\clk_sync_counter_reg0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__4_n_0\,
      CO(3) => \clk_sync_counter_reg0_carry__5_n_0\,
      CO(2) => \clk_sync_counter_reg0_carry__5_n_1\,
      CO(1) => \clk_sync_counter_reg0_carry__5_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => clk_sync_counter_reg0(28 downto 25),
      S(3 downto 0) => clk_sync_counter_reg_reg(28 downto 25)
    );
\clk_sync_counter_reg0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_clk_sync_counter_reg0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \clk_sync_counter_reg0_carry__6_n_2\,
      CO(0) => \clk_sync_counter_reg0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_clk_sync_counter_reg0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => clk_sync_counter_reg0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => clk_sync_counter_reg_reg(31 downto 29)
    );
\clk_sync_counter_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => start_init_clk_reg_i_3_n_0,
      O => clk_sync_counter_reg
    );
\clk_sync_counter_reg[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000002"
    )
        port map (
      I0 => start_init_clk_reg_i_14_n_0,
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(14),
      I4 => clk_sync_counter_reg_reg(13),
      I5 => \clk_sync_counter_reg[0]_i_14_n_0\,
      O => \clk_sync_counter_reg[0]_i_10_n_0\
    );
\clk_sync_counter_reg[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000900990090000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_sync_counter_reg_reg(12),
      I5 => clk_sync_counter_reg_reg(10),
      O => \clk_sync_counter_reg[0]_i_11_n_0\
    );
\clk_sync_counter_reg[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[0]_i_12_n_0\
    );
\clk_sync_counter_reg[0]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0660"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(7),
      I2 => clk_sync_counter_reg_reg(14),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[0]_i_13_n_0\
    );
\clk_sync_counter_reg[0]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      O => \clk_sync_counter_reg[0]_i_14_n_0\
    );
\clk_sync_counter_reg[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(3),
      I2 => clk_sync_counter_reg0(3),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_3_n_0\
    );
\clk_sync_counter_reg[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(2),
      I2 => clk_sync_counter_reg0(2),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_4_n_0\
    );
\clk_sync_counter_reg[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(1),
      I2 => clk_sync_counter_reg0(1),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[0]_i_5_n_0\
    );
\clk_sync_counter_reg[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(0),
      O => \clk_sync_counter_reg[0]_i_6_n_0\
    );
\clk_sync_counter_reg[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF77FF07"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I2 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I3 => start_init_clk_reg_i_5_n_0,
      I4 => clk_sync_counter_reg_reg(17),
      O => \clk_sync_counter_reg[0]_i_7_n_0\
    );
\clk_sync_counter_reg[0]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08200000"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_11_n_0\,
      I1 => clk_sync_counter_reg_reg(10),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(9),
      I4 => \clk_sync_counter_reg[0]_i_12_n_0\,
      O => \clk_sync_counter_reg[0]_i_8_n_0\
    );
\clk_sync_counter_reg[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000101000"
    )
        port map (
      I0 => start_init_clk_reg_i_12_n_0,
      I1 => start_init_clk_reg_i_10_n_0,
      I2 => \clk_sync_counter_reg[0]_i_13_n_0\,
      I3 => clk_sync_counter_reg_reg(17),
      I4 => clk_sync_counter_reg_reg(16),
      I5 => clk_sync_counter_reg_reg(3),
      O => \clk_sync_counter_reg[0]_i_9_n_0\
    );
\clk_sync_counter_reg[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg0(15),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[12]_i_2_n_0\
    );
\clk_sync_counter_reg[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(14),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(14),
      O => \clk_sync_counter_reg[12]_i_3_n_0\
    );
\clk_sync_counter_reg[12]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(13),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(13),
      O => \clk_sync_counter_reg[12]_i_4_n_0\
    );
\clk_sync_counter_reg[12]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(12),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(12),
      O => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(19),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_2_n_0\
    );
\clk_sync_counter_reg[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(18),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_3_n_0\
    );
\clk_sync_counter_reg[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => clk_sync_counter_reg0(17),
      I1 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I2 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[16]_i_4_n_0\
    );
\clk_sync_counter_reg[16]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF004E4E"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => clk_sync_counter_reg0(16),
      I2 => \clk_sync_counter_reg[16]_i_6_n_0\,
      I3 => clk_sync_counter_reg_reg(16),
      I4 => start_init_clk_reg_i_2_n_0,
      O => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg[16]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => \clk_sync_counter_reg[16]_i_7_n_0\,
      I1 => \clk_sync_counter_reg[16]_i_8_n_0\,
      I2 => \clk_sync_counter_reg[16]_i_9_n_0\,
      I3 => start_init_clk_reg_i_16_n_0,
      I4 => start_init_clk_reg_i_10_n_0,
      O => \clk_sync_counter_reg[16]_i_6_n_0\
    );
\clk_sync_counter_reg[16]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(16),
      I3 => clk_sync_counter_reg_reg(15),
      O => \clk_sync_counter_reg[16]_i_7_n_0\
    );
\clk_sync_counter_reg[16]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(10),
      O => \clk_sync_counter_reg[16]_i_8_n_0\
    );
\clk_sync_counter_reg[16]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(4),
      I1 => clk_sync_counter_reg_reg(3),
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_sync_counter_reg_reg(8),
      O => \clk_sync_counter_reg[16]_i_9_n_0\
    );
\clk_sync_counter_reg[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(23),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_2_n_0\
    );
\clk_sync_counter_reg[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(22),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_3_n_0\
    );
\clk_sync_counter_reg[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(21),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_4_n_0\
    );
\clk_sync_counter_reg[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(20),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(27),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_2_n_0\
    );
\clk_sync_counter_reg[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(26),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_3_n_0\
    );
\clk_sync_counter_reg[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(25),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_4_n_0\
    );
\clk_sync_counter_reg[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(24),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(31),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_2_n_0\
    );
\clk_sync_counter_reg[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(30),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_3_n_0\
    );
\clk_sync_counter_reg[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(29),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_4_n_0\
    );
\clk_sync_counter_reg[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_sync_counter_reg0(28),
      I1 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(7),
      I3 => clk_sync_counter_reg0(7),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_2_n_0\
    );
\clk_sync_counter_reg[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(6),
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg0(6),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_3_n_0\
    );
\clk_sync_counter_reg[4]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg0(5),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_4_n_0\
    );
\clk_sync_counter_reg[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => start_init_clk_reg_i_2_n_0,
      I1 => clk_sync_counter_reg_reg(4),
      I2 => clk_sync_counter_reg0(4),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FAFCFFFCFFFCFF"
    )
        port map (
      I0 => \clk_sync_counter_reg[16]_i_6_n_0\,
      I1 => clk_sync_counter_reg_reg(17),
      I2 => start_init_clk_reg_i_5_n_0,
      I3 => \clk_sync_counter_reg[0]_i_10_n_0\,
      I4 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I5 => \clk_sync_counter_reg[0]_i_8_n_0\,
      O => \clk_sync_counter_reg[4]_i_6_n_0\
    );
\clk_sync_counter_reg[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \clk_sync_counter_reg[0]_i_9_n_0\,
      I1 => \clk_sync_counter_reg[0]_i_8_n_0\,
      I2 => start_init_clk_reg_i_5_n_0,
      O => \clk_sync_counter_reg[4]_i_7_n_0\
    );
\clk_sync_counter_reg[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(11),
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg0(11),
      I3 => \clk_sync_counter_reg[0]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_2_n_0\
    );
\clk_sync_counter_reg[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(10),
      I3 => clk_sync_counter_reg0(10),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_3_n_0\
    );
\clk_sync_counter_reg[8]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5151F351"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(9),
      I3 => clk_sync_counter_reg0(9),
      I4 => \clk_sync_counter_reg[4]_i_7_n_0\,
      O => \clk_sync_counter_reg[8]_i_4_n_0\
    );
\clk_sync_counter_reg[8]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0EAC0"
    )
        port map (
      I0 => \clk_sync_counter_reg[4]_i_7_n_0\,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => clk_sync_counter_reg_reg(8),
      I3 => \clk_sync_counter_reg[4]_i_6_n_0\,
      I4 => clk_sync_counter_reg0(8),
      O => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      Q => clk_sync_counter_reg_reg(0),
      R => '0'
    );
\clk_sync_counter_reg_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[0]_i_2_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[0]_i_2_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => start_init_clk_reg_i_2_n_0,
      O(3) => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      O(2) => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      O(1) => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      O(0) => \clk_sync_counter_reg_reg[0]_i_2_n_7\,
      S(3) => \clk_sync_counter_reg[0]_i_3_n_0\,
      S(2) => \clk_sync_counter_reg[0]_i_4_n_0\,
      S(1) => \clk_sync_counter_reg[0]_i_5_n_0\,
      S(0) => \clk_sync_counter_reg[0]_i_6_n_0\
    );
\clk_sync_counter_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(10),
      R => '0'
    );
\clk_sync_counter_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(11),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(12),
      R => '0'
    );
\clk_sync_counter_reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[12]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[12]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[12]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[12]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[12]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[12]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[12]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(13),
      R => '0'
    );
\clk_sync_counter_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(14),
      R => '0'
    );
\clk_sync_counter_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[12]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(15),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(16),
      R => '0'
    );
\clk_sync_counter_reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[12]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[16]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[16]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[16]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[16]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[16]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[16]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[16]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(17),
      R => '0'
    );
\clk_sync_counter_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(18),
      R => '0'
    );
\clk_sync_counter_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[16]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(19),
      R => '0'
    );
\clk_sync_counter_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_6\,
      Q => clk_sync_counter_reg_reg(1),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(20),
      R => '0'
    );
\clk_sync_counter_reg_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[16]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[20]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[20]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[20]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[20]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[20]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[20]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[20]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(21),
      R => '0'
    );
\clk_sync_counter_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(22),
      R => '0'
    );
\clk_sync_counter_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[20]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(23),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(24),
      R => '0'
    );
\clk_sync_counter_reg_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[20]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[24]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[24]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[24]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[24]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[24]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[24]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[24]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(25),
      R => '0'
    );
\clk_sync_counter_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(26),
      R => '0'
    );
\clk_sync_counter_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[24]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(27),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(28),
      R => '0'
    );
\clk_sync_counter_reg_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[24]_i_1_n_0\,
      CO(3) => \NLW_clk_sync_counter_reg_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \clk_sync_counter_reg_reg[28]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[28]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[28]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[28]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[28]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[28]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[28]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(29),
      R => '0'
    );
\clk_sync_counter_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_5\,
      Q => clk_sync_counter_reg_reg(2),
      R => '0'
    );
\clk_sync_counter_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(30),
      R => '0'
    );
\clk_sync_counter_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[28]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(31),
      R => '0'
    );
\clk_sync_counter_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[0]_i_2_n_4\,
      Q => clk_sync_counter_reg_reg(3),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(4),
      R => '0'
    );
\clk_sync_counter_reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[0]_i_2_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[4]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[4]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[4]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[4]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[4]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[4]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[4]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(5),
      R => '0'
    );
\clk_sync_counter_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_5\,
      Q => clk_sync_counter_reg_reg(6),
      R => '0'
    );
\clk_sync_counter_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[4]_i_1_n_4\,
      Q => clk_sync_counter_reg_reg(7),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      Q => clk_sync_counter_reg_reg(8),
      R => '0'
    );
\clk_sync_counter_reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \clk_sync_counter_reg_reg[4]_i_1_n_0\,
      CO(3) => \clk_sync_counter_reg_reg[8]_i_1_n_0\,
      CO(2) => \clk_sync_counter_reg_reg[8]_i_1_n_1\,
      CO(1) => \clk_sync_counter_reg_reg[8]_i_1_n_2\,
      CO(0) => \clk_sync_counter_reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \clk_sync_counter_reg_reg[8]_i_1_n_4\,
      O(2) => \clk_sync_counter_reg_reg[8]_i_1_n_5\,
      O(1) => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      O(0) => \clk_sync_counter_reg_reg[8]_i_1_n_7\,
      S(3) => \clk_sync_counter_reg[8]_i_2_n_0\,
      S(2) => \clk_sync_counter_reg[8]_i_3_n_0\,
      S(1) => \clk_sync_counter_reg[8]_i_4_n_0\,
      S(0) => \clk_sync_counter_reg[8]_i_5_n_0\
    );
\clk_sync_counter_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => clk_sync_counter_reg,
      D => \clk_sync_counter_reg_reg[8]_i_1_n_6\,
      Q => clk_sync_counter_reg_reg(9),
      R => '0'
    );
clk_wizard_main: entity work.design_1_top_logic_0_0_clk_wiz_0
     port map (
      clk_in1 => clk_100MHz_in,
      clk_out1 => clk_100MHz_out,
      clk_out2 => clk_wizard_main_n_1,
      clk_out3 => clk_20MHz_out,
      clk_out4 => clk_10MHz_out,
      locked => clk_wizard_main_n_4
    );
\cnt_uart[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_uart(0),
      O => \cnt_uart[0]_i_1_n_0\
    );
\cnt_uart[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6266"
    )
        port map (
      I0 => cnt_uart(1),
      I1 => cnt_uart(0),
      I2 => cnt_uart(3),
      I3 => cnt_uart(2),
      O => \cnt_uart[1]_i_1_n_0\
    );
\cnt_uart[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FA0"
    )
        port map (
      I0 => cnt_uart(1),
      I1 => cnt_uart(3),
      I2 => cnt_uart(0),
      I3 => cnt_uart(2),
      O => \cnt_uart[2]_i_1_n_0\
    );
\cnt_uart[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AABAAAAA"
    )
        port map (
      I0 => is_counting,
      I1 => cnt_uart(3),
      I2 => cnt_uart(2),
      I3 => cnt_uart(1),
      I4 => cnt_uart(0),
      O => \cnt_uart[3]_i_1_n_0\
    );
\cnt_uart[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6CCC"
    )
        port map (
      I0 => cnt_uart(2),
      I1 => cnt_uart(3),
      I2 => cnt_uart(0),
      I3 => cnt_uart(1),
      O => \cnt_uart[3]_i_2_n_0\
    );
\cnt_uart_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[0]_i_1_n_0\,
      Q => cnt_uart(0),
      R => '0'
    );
\cnt_uart_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[1]_i_1_n_0\,
      Q => cnt_uart(1),
      R => '0'
    );
\cnt_uart_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[2]_i_1_n_0\,
      Q => cnt_uart(2),
      R => '0'
    );
\cnt_uart_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => \cnt_uart[3]_i_1_n_0\,
      D => \cnt_uart[3]_i_2_n_0\,
      Q => cnt_uart(3),
      R => '0'
    );
if_amp1_en_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FFFFFF44444444"
    )
        port map (
      I0 => if_amp1_en_reg_i_2_n_0,
      I1 => pamp_en_reg_i_3_n_0,
      I2 => in_uart_data(0),
      I3 => pamp_en_reg_i_4_n_0,
      I4 => \uart_tx_data[4]_i_4_n_0\,
      I5 => \^if_amp1_stu_en\,
      O => if_amp1_en_reg_i_1_n_0
    );
if_amp1_en_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => in_uart_data(7),
      I2 => in_uart_data(6),
      I3 => in_uart_data(5),
      I4 => in_uart_data(3),
      O => if_amp1_en_reg_i_2_n_0
    );
if_amp1_en_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => if_amp1_en_reg_i_1_n_0,
      Q => \^if_amp1_stu_en\,
      R => '0'
    );
if_amp2_en_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF0400"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => in_uart_data(0),
      I2 => in_uart_data(1),
      I3 => \uart_tx_data[4]_i_4_n_0\,
      I4 => \^if_amp2_stu_en\,
      O => if_amp2_en_reg_i_1_n_0
    );
if_amp2_en_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => if_amp2_en_reg_i_1_n_0,
      Q => \^if_amp2_stu_en\,
      R => '0'
    );
is_clk_initialized_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F2"
    )
        port map (
      I0 => start_init_clk_reg_i_3_n_0,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => is_clk_initialized_reg,
      O => is_clk_initialized_reg_i_1_n_0
    );
is_clk_initialized_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_clk_initialized_reg_i_1_n_0,
      Q => is_clk_initialized_reg,
      R => '0'
    );
is_counting_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888A8AAAA88A8"
    )
        port map (
      I0 => pll_read_i_2_n_0,
      I1 => is_counting,
      I2 => pll_data_ready,
      I3 => pll_data_ready_prev,
      I4 => uart_byte_received,
      I5 => uart_byte_received_prev,
      O => is_counting_i_1_n_0
    );
is_counting_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_counting_i_1_n_0,
      Q => is_counting,
      R => '0'
    );
is_uart_receiving_atten_value_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000010"
    )
        port map (
      I0 => sw_ctrl_reg_i_2_n_0,
      I1 => in_uart_data(4),
      I2 => in_uart_data(1),
      I3 => in_uart_data(0),
      I4 => sw_if3_reg_i_2_n_0,
      I5 => is_uart_receiving_atten_value_reg_n_0,
      O => is_uart_receiving_atten_value_i_1_n_0
    );
is_uart_receiving_atten_value_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_uart_receiving_atten_value_i_1_n_0,
      Q => is_uart_receiving_atten_value_reg_n_0,
      R => '0'
    );
is_uart_receiving_pll_data_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFEF0000"
    )
        port map (
      I0 => is_uart_receiving_pll_read_address,
      I1 => is_uart_receiving_atten_value_reg_n_0,
      I2 => is_uart_receiving_pll_data_i_2_n_0,
      I3 => pll_write_i_2_n_0,
      I4 => is_uart_receiving_pll_data_reg_n_0,
      I5 => pll_write_address_reg,
      O => is_uart_receiving_pll_data_i_1_n_0
    );
is_uart_receiving_pll_data_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \received_byte_num_reg_n_0_[0]\,
      I1 => \received_byte_num_reg_n_0_[1]\,
      O => is_uart_receiving_pll_data_i_2_n_0
    );
is_uart_receiving_pll_data_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_uart_receiving_pll_data_i_1_n_0,
      Q => is_uart_receiving_pll_data_reg_n_0,
      R => '0'
    );
is_uart_receiving_pll_read_address_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080FFFF00800080"
    )
        port map (
      I0 => \uart_tx_data[4]_i_4_n_0\,
      I1 => in_uart_data(1),
      I2 => in_uart_data(0),
      I3 => in_uart_data(2),
      I4 => pll_address_reg,
      I5 => is_uart_receiving_pll_read_address,
      O => is_uart_receiving_pll_read_address_i_1_n_0
    );
is_uart_receiving_pll_read_address_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_uart_receiving_pll_read_address_i_1_n_0,
      Q => is_uart_receiving_pll_read_address,
      R => '0'
    );
is_uart_receiving_pll_write_address_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222F222222222222"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address,
      I1 => pll_write_address_reg,
      I2 => is_uart_receiving_pll_write_address_i_2_n_0,
      I3 => sw_if3_reg_i_2_n_0,
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => is_uart_receiving_pll_write_address_i_1_n_0
    );
is_uart_receiving_pll_write_address_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => in_uart_data(3),
      I1 => in_uart_data(7),
      I2 => in_uart_data(6),
      I3 => in_uart_data(5),
      I4 => in_uart_data(4),
      O => is_uart_receiving_pll_write_address_i_2_n_0
    );
is_uart_receiving_pll_write_address_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => is_uart_receiving_pll_write_address_i_1_n_0,
      Q => is_uart_receiving_pll_write_address,
      R => '0'
    );
pamp_en_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4FFFFFF44444444"
    )
        port map (
      I0 => pamp_en_reg_i_2_n_0,
      I1 => pamp_en_reg_i_3_n_0,
      I2 => in_uart_data(0),
      I3 => pamp_en_reg_i_4_n_0,
      I4 => pamp_en_reg_i_5_n_0,
      I5 => \^pamp_en\,
      O => pamp_en_reg_i_1_n_0
    );
pamp_en_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => in_uart_data(3),
      I1 => in_uart_data(7),
      I2 => in_uart_data(6),
      I3 => in_uart_data(5),
      I4 => in_uart_data(4),
      O => pamp_en_reg_i_2_n_0
    );
pamp_en_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000000000"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => uart_byte_received,
      I2 => uart_byte_received_prev,
      I3 => \pll_write_data[23]_i_4_n_0\,
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => pamp_en_reg_i_3_n_0
    );
pamp_en_reg_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => in_uart_data(2),
      O => pamp_en_reg_i_4_n_0
    );
pamp_en_reg_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => if_amp1_en_reg_i_2_n_0,
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => uart_byte_received_prev,
      I4 => uart_byte_received,
      O => pamp_en_reg_i_5_n_0
    );
pamp_en_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pamp_en_reg_i_1_n_0,
      Q => \^pamp_en\,
      R => '0'
    );
\pll_address_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000444000000000"
    )
        port map (
      I0 => is_uart_receiving_atten_value_reg_n_0,
      I1 => is_uart_receiving_pll_read_address,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \received_byte_num_reg_n_0_[1]\,
      I4 => uart_byte_received_prev,
      I5 => uart_byte_received,
      O => pll_address_reg
    );
\pll_address_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(0),
      Q => pll_read_addres(0),
      R => '0'
    );
\pll_address_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(1),
      Q => pll_read_addres(1),
      R => '0'
    );
\pll_address_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(2),
      Q => pll_read_addres(2),
      R => '0'
    );
\pll_address_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(3),
      Q => pll_read_addres(3),
      R => '0'
    );
\pll_address_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(4),
      Q => pll_read_addres(4),
      R => '0'
    );
\pll_address_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(5),
      Q => pll_read_addres(5),
      R => '0'
    );
\pll_address_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(6),
      Q => pll_read_addres(6),
      R => '0'
    );
\pll_address_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_address_reg,
      D => in_uart_data(7),
      Q => pll_read_addres(7),
      R => '0'
    );
pll_data_ready_prev_reg: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_data_ready,
      Q => pll_data_ready_prev,
      R => '0'
    );
pll_read_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0B0F000"
    )
        port map (
      I0 => pll_data_ready_prev,
      I1 => pll_data_ready,
      I2 => pll_read_i_2_n_0,
      I3 => pll_address_reg,
      I4 => \^pll_read\,
      O => pll_read_i_1_n_0
    );
pll_read_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => cnt_uart(0),
      I1 => cnt_uart(1),
      I2 => cnt_uart(2),
      I3 => cnt_uart(3),
      O => pll_read_i_2_n_0
    );
pll_read_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_read_i_1_n_0,
      Q => \^pll_read\,
      R => '0'
    );
pll_trig_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00CE"
    )
        port map (
      I0 => \^pll_trig\,
      I1 => \trig_tguard_counter[16]_i_1_n_0\,
      I2 => trig_tguard_counter,
      I3 => trig_tsweep_counter074_out,
      O => pll_trig_reg_i_1_n_0
    );
pll_trig_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_trig_reg_i_1_n_0,
      Q => \^pll_trig\,
      R => '0'
    );
\pll_write_address_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_write_address_reg,
      D => in_uart_data(0),
      Q => pll_write_addres(0),
      R => '0'
    );
\pll_write_address_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_write_address_reg,
      D => in_uart_data(1),
      Q => pll_write_addres(1),
      R => '0'
    );
\pll_write_address_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_write_address_reg,
      D => in_uart_data(2),
      Q => pll_write_addres(2),
      R => '0'
    );
\pll_write_address_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_write_address_reg,
      D => in_uart_data(3),
      Q => pll_write_addres(3),
      R => '0'
    );
\pll_write_address_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => pll_write_address_reg,
      D => in_uart_data(4),
      Q => pll_write_addres(4),
      R => '0'
    );
\pll_write_data[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(0),
      O => \pll_write_data[0]_i_1_n_0\
    );
\pll_write_data[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(10),
      O => \pll_write_data[10]_i_1_n_0\
    );
\pll_write_data[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(3),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(11),
      O => \pll_write_data[11]_i_1_n_0\
    );
\pll_write_data[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(12),
      O => \pll_write_data[12]_i_1_n_0\
    );
\pll_write_data[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(5),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(13),
      O => \pll_write_data[13]_i_1_n_0\
    );
\pll_write_data[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(6),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(14),
      O => \pll_write_data[14]_i_1_n_0\
    );
\pll_write_data[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(7),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(15),
      O => \pll_write_data[15]_i_1_n_0\
    );
\pll_write_data[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(16),
      O => \pll_write_data[16]_i_1_n_0\
    );
\pll_write_data[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(17),
      O => \pll_write_data[17]_i_1_n_0\
    );
\pll_write_data[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(18),
      O => \pll_write_data[18]_i_1_n_0\
    );
\pll_write_data[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(3),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(19),
      O => \pll_write_data[19]_i_1_n_0\
    );
\pll_write_data[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(1),
      O => \pll_write_data[1]_i_1_n_0\
    );
\pll_write_data[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(20),
      O => \pll_write_data[20]_i_1_n_0\
    );
\pll_write_data[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(5),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(21),
      O => \pll_write_data[21]_i_1_n_0\
    );
\pll_write_data[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(6),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(22),
      O => \pll_write_data[22]_i_1_n_0\
    );
\pll_write_data[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => uart_byte_received,
      I1 => uart_byte_received_prev,
      I2 => \pll_write_data[23]_i_4_n_0\,
      I3 => is_uart_receiving_pll_read_address,
      I4 => is_uart_receiving_atten_value_reg_n_0,
      I5 => is_uart_receiving_pll_write_address,
      O => pll_write_address_reg
    );
\pll_write_data[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => uart_byte_received,
      I1 => uart_byte_received_prev,
      I2 => \pll_write_data[23]_i_4_n_0\,
      I3 => is_uart_receiving_pll_read_address,
      I4 => is_uart_receiving_atten_value_reg_n_0,
      I5 => is_uart_receiving_pll_data_reg_n_0,
      O => \pll_write_data[23]_i_2_n_0\
    );
\pll_write_data[23]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => in_uart_data(7),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(23),
      O => \pll_write_data[23]_i_3_n_0\
    );
\pll_write_data[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \received_byte_num_reg_n_0_[0]\,
      I1 => \received_byte_num_reg_n_0_[1]\,
      O => \pll_write_data[23]_i_4_n_0\
    );
\pll_write_data[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(2),
      O => \pll_write_data[2]_i_1_n_0\
    );
\pll_write_data[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(3),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(3),
      O => \pll_write_data[3]_i_1_n_0\
    );
\pll_write_data[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(4),
      O => \pll_write_data[4]_i_1_n_0\
    );
\pll_write_data[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(5),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(5),
      O => \pll_write_data[5]_i_1_n_0\
    );
\pll_write_data[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(6),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(6),
      O => \pll_write_data[6]_i_1_n_0\
    );
\pll_write_data[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(7),
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \^pll_write_data\(7),
      O => \pll_write_data[7]_i_1_n_0\
    );
\pll_write_data[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(8),
      O => \pll_write_data[8]_i_1_n_0\
    );
\pll_write_data[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF08"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num_reg_n_0_[0]\,
      I3 => \^pll_write_data\(9),
      O => \pll_write_data[9]_i_1_n_0\
    );
\pll_write_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[0]_i_1_n_0\,
      Q => \^pll_write_data\(0),
      R => pll_write_address_reg
    );
\pll_write_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[10]_i_1_n_0\,
      Q => \^pll_write_data\(10),
      R => pll_write_address_reg
    );
\pll_write_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[11]_i_1_n_0\,
      Q => \^pll_write_data\(11),
      R => pll_write_address_reg
    );
\pll_write_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[12]_i_1_n_0\,
      Q => \^pll_write_data\(12),
      R => pll_write_address_reg
    );
\pll_write_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[13]_i_1_n_0\,
      Q => \^pll_write_data\(13),
      R => pll_write_address_reg
    );
\pll_write_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[14]_i_1_n_0\,
      Q => \^pll_write_data\(14),
      R => pll_write_address_reg
    );
\pll_write_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[15]_i_1_n_0\,
      Q => \^pll_write_data\(15),
      R => pll_write_address_reg
    );
\pll_write_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[16]_i_1_n_0\,
      Q => \^pll_write_data\(16),
      R => pll_write_address_reg
    );
\pll_write_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[17]_i_1_n_0\,
      Q => \^pll_write_data\(17),
      R => pll_write_address_reg
    );
\pll_write_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[18]_i_1_n_0\,
      Q => \^pll_write_data\(18),
      R => pll_write_address_reg
    );
\pll_write_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[19]_i_1_n_0\,
      Q => \^pll_write_data\(19),
      R => pll_write_address_reg
    );
\pll_write_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[1]_i_1_n_0\,
      Q => \^pll_write_data\(1),
      R => pll_write_address_reg
    );
\pll_write_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[20]_i_1_n_0\,
      Q => \^pll_write_data\(20),
      R => pll_write_address_reg
    );
\pll_write_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[21]_i_1_n_0\,
      Q => \^pll_write_data\(21),
      R => pll_write_address_reg
    );
\pll_write_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[22]_i_1_n_0\,
      Q => \^pll_write_data\(22),
      R => pll_write_address_reg
    );
\pll_write_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[23]_i_3_n_0\,
      Q => \^pll_write_data\(23),
      R => pll_write_address_reg
    );
\pll_write_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[2]_i_1_n_0\,
      Q => \^pll_write_data\(2),
      R => pll_write_address_reg
    );
\pll_write_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[3]_i_1_n_0\,
      Q => \^pll_write_data\(3),
      R => pll_write_address_reg
    );
\pll_write_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[4]_i_1_n_0\,
      Q => \^pll_write_data\(4),
      R => pll_write_address_reg
    );
\pll_write_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[5]_i_1_n_0\,
      Q => \^pll_write_data\(5),
      R => pll_write_address_reg
    );
\pll_write_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[6]_i_1_n_0\,
      Q => \^pll_write_data\(6),
      R => pll_write_address_reg
    );
\pll_write_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[7]_i_1_n_0\,
      Q => \^pll_write_data\(7),
      R => pll_write_address_reg
    );
\pll_write_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[8]_i_1_n_0\,
      Q => \^pll_write_data\(8),
      R => pll_write_address_reg
    );
\pll_write_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \pll_write_data[23]_i_2_n_0\,
      D => \pll_write_data[9]_i_1_n_0\,
      Q => \^pll_write_data\(9),
      R => pll_write_address_reg
    );
pll_write_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400FFFF04000400"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address,
      I1 => is_uart_receiving_pll_data_reg_n_0,
      I2 => pll_write_i_2_n_0,
      I3 => pll_write_i_3_n_0,
      I4 => is_uart_receiving_pll_write_address67_out,
      I5 => \^pll_write\,
      O => pll_write_i_1_n_0
    );
pll_write_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => uart_byte_received_prev,
      I1 => uart_byte_received,
      O => pll_write_i_2_n_0
    );
pll_write_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \received_byte_num_reg_n_0_[1]\,
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => is_uart_receiving_atten_value_reg_n_0,
      I3 => is_uart_receiving_pll_read_address,
      O => pll_write_i_3_n_0
    );
pll_write_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000080000"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => in_uart_data(1),
      I2 => \pll_write_data[23]_i_4_n_0\,
      I3 => pll_write_i_2_n_0,
      I4 => in_uart_data(2),
      I5 => is_uart_receiving_pll_write_address_i_2_n_0,
      O => is_uart_receiving_pll_write_address67_out
    );
pll_write_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => pll_write_i_1_n_0,
      Q => \^pll_write\,
      R => '0'
    );
\received_byte_num[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBCCB0B0B0C0"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address,
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num[0]_i_2_n_0\,
      I3 => \received_byte_num[0]_i_3_n_0\,
      I4 => \received_byte_num[0]_i_4_n_0\,
      I5 => \received_byte_num[0]_i_5_n_0\,
      O => \received_byte_num[0]_i_1_n_0\
    );
\received_byte_num[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDDF"
    )
        port map (
      I0 => uart_byte_received,
      I1 => uart_byte_received_prev,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \received_byte_num_reg_n_0_[0]\,
      O => \received_byte_num[0]_i_2_n_0\
    );
\received_byte_num[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"10FF10FF10FF1010"
    )
        port map (
      I0 => sw_ctrl_reg_i_2_n_0,
      I1 => in_uart_data(4),
      I2 => \received_byte_num[0]_i_6_n_0\,
      I3 => \received_byte_num[0]_i_2_n_0\,
      I4 => is_uart_receiving_pll_data_reg_n_0,
      I5 => is_uart_receiving_pll_write_address,
      O => \received_byte_num[0]_i_3_n_0\
    );
\received_byte_num[0]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \uart_tx_data[4]_i_4_n_0\,
      I1 => in_uart_data(1),
      I2 => in_uart_data(0),
      O => \received_byte_num[0]_i_4_n_0\
    );
\received_byte_num[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => is_uart_receiving_pll_read_address,
      I1 => is_uart_receiving_atten_value_reg_n_0,
      O => \received_byte_num[0]_i_5_n_0\
    );
\received_byte_num[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => in_uart_data(0),
      I2 => in_uart_data(2),
      I3 => uart_byte_received,
      I4 => uart_byte_received_prev,
      I5 => \pll_write_data[23]_i_4_n_0\,
      O => \received_byte_num[0]_i_6_n_0\
    );
\received_byte_num[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => pll_write_i_2_n_0,
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => \received_byte_num[1]_i_2_n_0\,
      O => \received_byte_num[1]_i_1_n_0\
    );
\received_byte_num[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000004C00000C0C0"
    )
        port map (
      I0 => pll_write_i_2_n_0,
      I1 => \received_byte_num[0]_i_5_n_0\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \received_byte_num_reg_n_0_[0]\,
      I4 => is_uart_receiving_pll_write_address,
      I5 => is_uart_receiving_pll_data_reg_n_0,
      O => \received_byte_num[1]_i_2_n_0\
    );
\received_byte_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => \received_byte_num[0]_i_1_n_0\,
      Q => \received_byte_num_reg_n_0_[0]\,
      R => '0'
    );
\received_byte_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => \received_byte_num[1]_i_1_n_0\,
      Q => \received_byte_num_reg_n_0_[1]\,
      R => '0'
    );
start_init_clk_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEF00"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => start_init_clk_reg_i_2_n_0,
      I2 => start_init_clk_reg_i_3_n_0,
      I3 => \^start_init_clk\,
      I4 => start_init_clk_reg_i_4_n_0,
      O => start_init_clk_reg_i_1_n_0
    );
start_init_clk_reg_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(0),
      I1 => clk_sync_counter_reg_reg(1),
      I2 => clk_sync_counter_reg_reg(2),
      O => start_init_clk_reg_i_10_n_0
    );
start_init_clk_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(18),
      I1 => clk_sync_counter_reg_reg(19),
      I2 => clk_sync_counter_reg_reg(20),
      I3 => clk_sync_counter_reg_reg(21),
      I4 => clk_sync_counter_reg_reg(23),
      I5 => clk_sync_counter_reg_reg(22),
      O => start_init_clk_reg_i_11_n_0
    );
start_init_clk_reg_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(6),
      I1 => clk_sync_counter_reg_reg(4),
      I2 => clk_sync_counter_reg_reg(11),
      O => start_init_clk_reg_i_12_n_0
    );
start_init_clk_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(27),
      I1 => clk_sync_counter_reg_reg(28),
      I2 => clk_sync_counter_reg_reg(29),
      I3 => clk_sync_counter_reg_reg(26),
      I4 => clk_sync_counter_reg_reg(24),
      I5 => clk_sync_counter_reg_reg(25),
      O => start_init_clk_reg_i_13_n_0
    );
start_init_clk_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0155FFFFFFFFFFFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(8),
      I1 => clk_sync_counter_reg_reg(6),
      I2 => clk_sync_counter_reg_reg(5),
      I3 => clk_sync_counter_reg_reg(7),
      I4 => clk_sync_counter_reg_reg(10),
      I5 => clk_sync_counter_reg_reg(9),
      O => start_init_clk_reg_i_14_n_0
    );
start_init_clk_reg_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(9),
      I1 => clk_sync_counter_reg_reg(10),
      O => start_init_clk_reg_i_15_n_0
    );
start_init_clk_reg_i_16: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(7),
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(6),
      O => start_init_clk_reg_i_16_n_0
    );
start_init_clk_reg_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(30),
      I1 => clk_sync_counter_reg_reg(31),
      O => start_init_clk_reg_i_17_n_0
    );
start_init_clk_reg_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(12),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(7),
      O => start_init_clk_reg_i_18_n_0
    );
start_init_clk_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11110111"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(17),
      I1 => start_init_clk_reg_i_5_n_0,
      I2 => clk_sync_counter_reg_reg(15),
      I3 => clk_sync_counter_reg_reg(16),
      I4 => start_init_clk_reg_i_6_n_0,
      O => start_init_clk_reg_i_2_n_0
    );
start_init_clk_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEAA0000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(16),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => start_init_clk_reg_i_7_n_0,
      I3 => clk_sync_counter_reg_reg(15),
      I4 => clk_sync_counter_reg_reg(17),
      I5 => start_init_clk_reg_i_5_n_0,
      O => start_init_clk_reg_i_3_n_0
    );
start_init_clk_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => start_init_clk_reg_i_8_n_0,
      I1 => start_init_clk_reg_i_9_n_0,
      I2 => start_init_clk_reg_i_10_n_0,
      I3 => start_init_clk_reg_i_11_n_0,
      I4 => start_init_clk_reg_i_12_n_0,
      I5 => start_init_clk_reg_i_13_n_0,
      O => start_init_clk_reg_i_4_n_0
    );
start_init_clk_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(30),
      I1 => clk_sync_counter_reg_reg(31),
      I2 => start_init_clk_reg_i_11_n_0,
      I3 => start_init_clk_reg_i_13_n_0,
      O => start_init_clk_reg_i_5_n_0
    );
start_init_clk_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(13),
      I1 => clk_sync_counter_reg_reg(14),
      I2 => clk_sync_counter_reg_reg(11),
      I3 => clk_sync_counter_reg_reg(12),
      I4 => start_init_clk_reg_i_14_n_0,
      O => start_init_clk_reg_i_6_n_0
    );
start_init_clk_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => start_init_clk_reg_i_15_n_0,
      I1 => clk_sync_counter_reg_reg(11),
      I2 => clk_sync_counter_reg_reg(8),
      I3 => clk_sync_counter_reg_reg(13),
      I4 => clk_sync_counter_reg_reg(12),
      I5 => start_init_clk_reg_i_16_n_0,
      O => start_init_clk_reg_i_7_n_0
    );
start_init_clk_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => clk_sync_counter_reg_reg(15),
      I1 => clk_sync_counter_reg_reg(16),
      I2 => start_init_clk_reg_i_15_n_0,
      I3 => clk_sync_counter_reg_reg(14),
      I4 => clk_sync_counter_reg_reg(17),
      I5 => start_init_clk_reg_i_17_n_0,
      O => start_init_clk_reg_i_8_n_0
    );
start_init_clk_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => is_clk_initialized_reg,
      I1 => clk_sync_counter_reg_reg(5),
      I2 => clk_sync_counter_reg_reg(3),
      I3 => start_init_clk_reg_i_18_n_0,
      O => start_init_clk_reg_i_9_n_0
    );
start_init_clk_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => start_init_clk_reg_i_1_n_0,
      Q => \^start_init_clk\,
      R => '0'
    );
start_uart_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => uart_start_sending_reg_n_0,
      I1 => uart_adc_start_sending,
      O => start_uart_send
    );
sw_ctrl_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00010000"
    )
        port map (
      I0 => sw_ctrl_reg_i_2_n_0,
      I1 => in_uart_data(4),
      I2 => in_uart_data(1),
      I3 => sw_if3_reg_i_2_n_0,
      I4 => in_uart_data(0),
      I5 => \^sw_ctrl\,
      O => sw_ctrl_reg_i_1_n_0
    );
sw_ctrl_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => in_uart_data(5),
      I1 => in_uart_data(6),
      I2 => in_uart_data(7),
      I3 => in_uart_data(3),
      O => sw_ctrl_reg_i_2_n_0
    );
sw_ctrl_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => sw_ctrl_reg_i_1_n_0,
      Q => \^sw_ctrl\,
      R => '0'
    );
sw_if1_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF0400"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => in_uart_data(0),
      I2 => in_uart_data(1),
      I3 => pamp_en_reg_i_5_n_0,
      I4 => \^sw_if1\,
      O => sw_if1_reg_i_1_n_0
    );
sw_if1_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => sw_if1_reg_i_1_n_0,
      Q => \^sw_if1\,
      R => '0'
    );
sw_if2_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000200"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => in_uart_data(2),
      I2 => sw_if2_reg_i_2_n_0,
      I3 => in_uart_data(1),
      I4 => if_amp1_en_reg_i_2_n_0,
      I5 => \^sw_if2\,
      O => sw_if2_reg_i_1_n_0
    );
sw_if2_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => uart_byte_received,
      I1 => uart_byte_received_prev,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => \received_byte_num_reg_n_0_[0]\,
      O => sw_if2_reg_i_2_n_0
    );
sw_if2_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => sw_if2_reg_i_1_n_0,
      Q => \^sw_if2\,
      R => '0'
    );
sw_if3_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0004"
    )
        port map (
      I0 => if_amp1_en_reg_i_2_n_0,
      I1 => in_uart_data(1),
      I2 => in_uart_data(0),
      I3 => sw_if3_reg_i_2_n_0,
      I4 => \^sw_if3\,
      O => sw_if3_reg_i_1_n_0
    );
sw_if3_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFFFF"
    )
        port map (
      I0 => \received_byte_num_reg_n_0_[0]\,
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => uart_byte_received_prev,
      I3 => uart_byte_received,
      I4 => in_uart_data(2),
      O => sw_if3_reg_i_2_n_0
    );
sw_if3_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => sw_if3_reg_i_1_n_0,
      Q => \^sw_if3\,
      R => '0'
    );
t_sweep_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => t_sweep_INST_0_i_1_n_0,
      O => t_sweep
    );
t_sweep_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => trig_tsweep_counter_reg(2),
      I1 => trig_tsweep_counter_reg(1),
      I2 => trig_tsweep_counter_reg(0),
      I3 => t_sweep_INST_0_i_2_n_0,
      I4 => t_sweep_INST_0_i_3_n_0,
      I5 => t_sweep_INST_0_i_4_n_0,
      O => t_sweep_INST_0_i_1_n_0
    );
t_sweep_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(6),
      I1 => trig_tsweep_counter_reg(5),
      I2 => trig_tsweep_counter_reg(4),
      I3 => trig_tsweep_counter_reg(3),
      O => t_sweep_INST_0_i_2_n_0
    );
t_sweep_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(10),
      I1 => trig_tsweep_counter_reg(9),
      I2 => trig_tsweep_counter_reg(8),
      I3 => trig_tsweep_counter_reg(7),
      O => t_sweep_INST_0_i_3_n_0
    );
t_sweep_INST_0_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(11),
      I1 => trig_tsweep_counter_reg(12),
      I2 => trig_tsweep_counter_reg(13),
      I3 => trig_tsweep_counter_reg(14),
      I4 => trig_tsweep_counter_reg(16),
      I5 => trig_tsweep_counter_reg(15),
      O => t_sweep_INST_0_i_4_n_0
    );
\trig_tguard_counter[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"888F88F8"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_6_n_0\,
      I1 => \trig_tsweep_counter[0]_i_4_n_0\,
      I2 => \trig_tguard_counter_reg_n_0_[0]\,
      I3 => \trig_tguard_counter[0]_i_2_n_0\,
      I4 => trig_tguard_counter,
      O => \trig_tguard_counter[0]_i_1_n_0\
    );
\trig_tguard_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000040000000000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_8_n_0\,
      I1 => \trig_tguard_counter[0]_i_3_n_0\,
      I2 => \trig_tguard_counter_reg_n_0_[0]\,
      I3 => \trig_tguard_counter_reg_n_0_[2]\,
      I4 => \trig_tguard_counter_reg_n_0_[1]\,
      I5 => t_sweep_INST_0_i_1_n_0,
      O => \trig_tguard_counter[0]_i_2_n_0\
    );
\trig_tguard_counter[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[6]\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[4]\,
      I3 => \trig_tguard_counter_reg_n_0_[3]\,
      O => \trig_tguard_counter[0]_i_3_n_0\
    );
\trig_tguard_counter[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF404040"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_4_n_0\,
      I1 => \trig_tguard_counter[16]_i_5_n_0\,
      I2 => \trig_tguard_counter[16]_i_6_n_0\,
      I3 => t_sweep_INST_0_i_1_n_0,
      I4 => \trig_tguard_counter[16]_i_7_n_0\,
      I5 => \trig_tguard_counter[16]_i_8_n_0\,
      O => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter[16]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(1),
      I1 => trig_tsweep_counter_reg(0),
      I2 => trig_tsweep_counter_reg(3),
      I3 => trig_tsweep_counter_reg(2),
      O => \trig_tguard_counter[16]_i_10_n_0\
    );
\trig_tguard_counter[16]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[3]\,
      I1 => \trig_tguard_counter_reg_n_0_[4]\,
      O => \trig_tguard_counter[16]_i_11_n_0\
    );
\trig_tguard_counter[16]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[12]\,
      I1 => \trig_tguard_counter_reg_n_0_[15]\,
      I2 => \trig_tguard_counter_reg_n_0_[11]\,
      I3 => \trig_tguard_counter_reg_n_0_[9]\,
      O => \trig_tguard_counter[16]_i_12_n_0\
    );
\trig_tguard_counter[16]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[10]\,
      I1 => \trig_tguard_counter_reg_n_0_[14]\,
      I2 => \trig_tguard_counter_reg_n_0_[13]\,
      I3 => \trig_tguard_counter_reg_n_0_[16]\,
      O => \trig_tguard_counter[16]_i_13_n_0\
    );
\trig_tguard_counter[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000002A002AAA"
    )
        port map (
      I0 => t_sweep_INST_0_i_1_n_0,
      I1 => \trig_tguard_counter_reg_n_0_[6]\,
      I2 => \trig_tguard_counter_reg_n_0_[5]\,
      I3 => \trig_tguard_counter[16]_i_4_n_0\,
      I4 => \trig_tguard_counter[16]_i_9_n_0\,
      I5 => \trig_tguard_counter[16]_i_8_n_0\,
      O => trig_tguard_counter
    );
\trig_tguard_counter[16]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[4]\,
      I1 => \trig_tguard_counter_reg_n_0_[3]\,
      I2 => \trig_tguard_counter_reg_n_0_[2]\,
      O => \trig_tguard_counter[16]_i_4_n_0\
    );
\trig_tguard_counter[16]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_9_n_0\,
      I1 => trig_tsweep_counter_reg(15),
      I2 => trig_tsweep_counter_reg(16),
      I3 => trig_tsweep_counter_reg(13),
      I4 => trig_tsweep_counter_reg(14),
      O => \trig_tguard_counter[16]_i_5_n_0\
    );
\trig_tguard_counter[16]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_10_n_0\,
      I1 => trig_tsweep_counter_reg(7),
      I2 => trig_tsweep_counter_reg(6),
      I3 => trig_tsweep_counter_reg(5),
      I4 => trig_tsweep_counter_reg(4),
      I5 => \trig_tsweep_counter[0]_i_5_n_0\,
      O => \trig_tguard_counter[16]_i_6_n_0\
    );
\trig_tguard_counter[16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000400000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_11_n_0\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[6]\,
      I3 => \trig_tguard_counter_reg_n_0_[0]\,
      I4 => \trig_tguard_counter_reg_n_0_[2]\,
      I5 => \trig_tguard_counter_reg_n_0_[1]\,
      O => \trig_tguard_counter[16]_i_7_n_0\
    );
\trig_tguard_counter[16]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[8]\,
      I1 => \trig_tguard_counter_reg_n_0_[7]\,
      I2 => \trig_tguard_counter[16]_i_12_n_0\,
      I3 => \trig_tguard_counter[16]_i_13_n_0\,
      O => \trig_tguard_counter[16]_i_8_n_0\
    );
\trig_tguard_counter[16]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[6]\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      I2 => \trig_tguard_counter_reg_n_0_[1]\,
      I3 => \trig_tguard_counter_reg_n_0_[0]\,
      O => \trig_tguard_counter[16]_i_9_n_0\
    );
\trig_tguard_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => \trig_tguard_counter[0]_i_1_n_0\,
      Q => \trig_tguard_counter_reg_n_0_[0]\,
      R => '0'
    );
\trig_tguard_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(10),
      Q => \trig_tguard_counter_reg_n_0_[10]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(11),
      Q => \trig_tguard_counter_reg_n_0_[11]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(12),
      Q => \trig_tguard_counter_reg_n_0_[12]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(12 downto 9),
      S(3) => \trig_tguard_counter_reg_n_0_[12]\,
      S(2) => \trig_tguard_counter_reg_n_0_[11]\,
      S(1) => \trig_tguard_counter_reg_n_0_[10]\,
      S(0) => \trig_tguard_counter_reg_n_0_[9]\
    );
\trig_tguard_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(13),
      Q => \trig_tguard_counter_reg_n_0_[13]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(14),
      Q => \trig_tguard_counter_reg_n_0_[14]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(15),
      Q => \trig_tguard_counter_reg_n_0_[15]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(16),
      Q => \trig_tguard_counter_reg_n_0_[16]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[16]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(3) => \NLW_trig_tguard_counter_reg[16]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \trig_tguard_counter_reg[16]_i_3_n_1\,
      CO(1) => \trig_tguard_counter_reg[16]_i_3_n_2\,
      CO(0) => \trig_tguard_counter_reg[16]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(16 downto 13),
      S(3) => \trig_tguard_counter_reg_n_0_[16]\,
      S(2) => \trig_tguard_counter_reg_n_0_[15]\,
      S(1) => \trig_tguard_counter_reg_n_0_[14]\,
      S(0) => \trig_tguard_counter_reg_n_0_[13]\
    );
\trig_tguard_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(1),
      Q => \trig_tguard_counter_reg_n_0_[1]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(2),
      Q => \trig_tguard_counter_reg_n_0_[2]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(3),
      Q => \trig_tguard_counter_reg_n_0_[3]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(4),
      Q => \trig_tguard_counter_reg_n_0_[4]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[4]_i_1_n_3\,
      CYINIT => \trig_tguard_counter_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(4 downto 1),
      S(3) => \trig_tguard_counter_reg_n_0_[4]\,
      S(2) => \trig_tguard_counter_reg_n_0_[3]\,
      S(1) => \trig_tguard_counter_reg_n_0_[2]\,
      S(0) => \trig_tguard_counter_reg_n_0_[1]\
    );
\trig_tguard_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(5),
      Q => \trig_tguard_counter_reg_n_0_[5]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(6),
      Q => \trig_tguard_counter_reg_n_0_[6]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(7),
      Q => \trig_tguard_counter_reg_n_0_[7]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(8),
      Q => \trig_tguard_counter_reg_n_0_[8]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(8 downto 5),
      S(3) => \trig_tguard_counter_reg_n_0_[8]\,
      S(2) => \trig_tguard_counter_reg_n_0_[7]\,
      S(1) => \trig_tguard_counter_reg_n_0_[6]\,
      S(0) => \trig_tguard_counter_reg_n_0_[5]\
    );
\trig_tguard_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tguard_counter,
      D => p_2_in(9),
      Q => \trig_tguard_counter_reg_n_0_[9]\,
      R => \trig_tguard_counter[16]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => trig_tguard_counter,
      I1 => \trig_tguard_counter[16]_i_1_n_0\,
      O => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F010"
    )
        port map (
      I0 => trig_tsweep_counter_reg(6),
      I1 => trig_tsweep_counter_reg(7),
      I2 => \trig_tsweep_counter[0]_i_4_n_0\,
      I3 => \trig_tsweep_counter[0]_i_5_n_0\,
      O => trig_tsweep_counter074_out
    );
\trig_tsweep_counter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => \trig_tguard_counter[16]_i_4_n_0\,
      I1 => \trig_tsweep_counter[0]_i_7_n_0\,
      I2 => \trig_tguard_counter[16]_i_12_n_0\,
      I3 => \trig_tguard_counter[16]_i_13_n_0\,
      I4 => \trig_tsweep_counter[0]_i_8_n_0\,
      I5 => \trig_tguard_counter[16]_i_9_n_0\,
      O => \trig_tsweep_counter[0]_i_4_n_0\
    );
\trig_tsweep_counter[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => trig_tsweep_counter_reg(9),
      I1 => trig_tsweep_counter_reg(10),
      I2 => trig_tsweep_counter_reg(8),
      I3 => trig_tsweep_counter_reg(11),
      I4 => trig_tsweep_counter_reg(12),
      O => \trig_tsweep_counter[0]_i_5_n_0\
    );
\trig_tsweep_counter[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => trig_tsweep_counter_reg(0),
      O => \trig_tsweep_counter[0]_i_6_n_0\
    );
\trig_tsweep_counter[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[7]\,
      I1 => \trig_tguard_counter_reg_n_0_[8]\,
      O => \trig_tsweep_counter[0]_i_7_n_0\
    );
\trig_tsweep_counter[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => trig_tsweep_counter_reg(14),
      I1 => trig_tsweep_counter_reg(13),
      I2 => trig_tsweep_counter_reg(16),
      I3 => trig_tsweep_counter_reg(15),
      O => \trig_tsweep_counter[0]_i_8_n_0\
    );
\trig_tsweep_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      Q => trig_tsweep_counter_reg(0),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(2) => \trig_tsweep_counter_reg[0]_i_3_n_1\,
      CO(1) => \trig_tsweep_counter_reg[0]_i_3_n_2\,
      CO(0) => \trig_tsweep_counter_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      O(2) => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      O(1) => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      O(0) => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      S(3 downto 1) => trig_tsweep_counter_reg(3 downto 1),
      S(0) => \trig_tsweep_counter[0]_i_6_n_0\
    );
\trig_tsweep_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(10),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(11),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(12),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(3) => \trig_tsweep_counter_reg[12]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(15 downto 12)
    );
\trig_tsweep_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(13),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(14),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(15),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[16]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(16),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_trig_tsweep_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_trig_tsweep_counter_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \trig_tsweep_counter_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => trig_tsweep_counter_reg(16)
    );
\trig_tsweep_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      Q => trig_tsweep_counter_reg(1),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      Q => trig_tsweep_counter_reg(2),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      Q => trig_tsweep_counter_reg(3),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(4),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(3) => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(7 downto 4)
    );
\trig_tsweep_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(5),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(6),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(7),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(8),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(11 downto 8)
    );
\trig_tsweep_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => trig_tsweep_counter074_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(9),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
uart_byte_received_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => uart_byte_received,
      Q => uart_byte_received_prev,
      R => '0'
    );
uart_start_sending_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA8A888888"
    )
        port map (
      I0 => pll_read_i_2_n_0,
      I1 => \uart_tx_data[7]_i_4_n_0\,
      I2 => pll_data_ready_prev,
      I3 => pll_data_ready,
      I4 => uart_start_sending_i_2_n_0,
      I5 => uart_start_sending_reg_n_0,
      O => uart_start_sending_i_1_n_0
    );
uart_start_sending_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \^out_data_uart\(2),
      I1 => \^out_data_uart\(3),
      I2 => \^out_data_uart\(0),
      I3 => \^out_data_uart\(1),
      I4 => uart_start_sending_i_3_n_0,
      O => uart_start_sending_i_2_n_0
    );
uart_start_sending_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \^out_data_uart\(4),
      I1 => \^out_data_uart\(5),
      I2 => \^out_data_uart\(7),
      I3 => \^out_data_uart\(6),
      O => uart_start_sending_i_3_n_0
    );
uart_start_sending_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => uart_byte_received_prev0,
      CE => '1',
      D => uart_start_sending_i_1_n_0,
      Q => uart_start_sending_reg_n_0,
      R => '0'
    );
\uart_tx_data[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \uart_tx_data[0]_i_2_n_0\,
      I1 => \uart_tx_data[0]_i_3_n_0\,
      I2 => \uart_tx_data[0]_i_4_n_0\,
      I3 => \uart_tx_data[0]_i_5_n_0\,
      I4 => \uart_tx_data[0]_i_6_n_0\,
      O => p_1_in(0)
    );
\uart_tx_data[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000555500000300"
    )
        port map (
      I0 => is_uart_receiving_atten_value_reg_n_0,
      I1 => in_uart_data(1),
      I2 => in_uart_data(0),
      I3 => pll_read_data(0),
      I4 => \uart_tx_data[0]_i_7_n_0\,
      I5 => \pll_write_data[23]_i_4_n_0\,
      O => \uart_tx_data[0]_i_10_n_0\
    );
\uart_tx_data[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3F3F003F22220000"
    )
        port map (
      I0 => pll_read_data(8),
      I1 => \pll_write_data[23]_i_4_n_0\,
      I2 => is_uart_receiving_atten_value_reg_n_0,
      I3 => \uart_tx_data[2]_i_5_n_0\,
      I4 => in_uart_data(0),
      I5 => \uart_tx_data[0]_i_7_n_0\,
      O => \uart_tx_data[0]_i_2_n_0\
    );
\uart_tx_data[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0A0FF101010"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => in_uart_data(2),
      I2 => \uart_tx_data[2]_i_3_n_0\,
      I3 => \uart_tx_data[0]_i_8_n_0\,
      I4 => in_uart_data(1),
      I5 => in_uart_data(3),
      O => \uart_tx_data[0]_i_3_n_0\
    );
\uart_tx_data[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF111111111"
    )
        port map (
      I0 => \pll_write_data[23]_i_4_n_0\,
      I1 => \uart_tx_data[0]_i_9_n_0\,
      I2 => in_uart_data(7),
      I3 => in_uart_data(6),
      I4 => in_uart_data(5),
      I5 => \uart_tx_data[2]_i_3_n_0\,
      O => \uart_tx_data[0]_i_4_n_0\
    );
\uart_tx_data[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0000000E"
    )
        port map (
      I0 => is_uart_receiving_pll_data_reg_n_0,
      I1 => is_uart_receiving_pll_write_address,
      I2 => is_uart_receiving_atten_value_reg_n_0,
      I3 => is_uart_receiving_pll_read_address,
      I4 => \received_byte_num[0]_i_2_n_0\,
      I5 => \uart_tx_data[0]_i_10_n_0\,
      O => \uart_tx_data[0]_i_5_n_0\
    );
\uart_tx_data[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBBB0000FBBBFBBB"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => \uart_tx_data[2]_i_5_n_0\,
      I2 => in_uart_data(2),
      I3 => in_uart_data(4),
      I4 => uart_byte_received_prev,
      I5 => uart_byte_received,
      O => \uart_tx_data[0]_i_6_n_0\
    );
\uart_tx_data[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => in_uart_data(4),
      O => \uart_tx_data[0]_i_7_n_0\
    );
\uart_tx_data[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A8FFA0FFA8FF"
    )
        port map (
      I0 => in_uart_data(2),
      I1 => pll_read_data(16),
      I2 => pll_write_i_2_n_0,
      I3 => in_uart_data(4),
      I4 => \pll_write_data[23]_i_4_n_0\,
      I5 => is_uart_receiving_atten_value_reg_n_0,
      O => \uart_tx_data[0]_i_8_n_0\
    );
\uart_tx_data[0]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => in_uart_data(1),
      O => \uart_tx_data[0]_i_9_n_0\
    );
\uart_tx_data[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEAFFEAFFEA"
    )
        port map (
      I0 => \uart_tx_data[1]_i_2_n_0\,
      I1 => \uart_tx_data[7]_i_6_n_0\,
      I2 => pll_read_data(9),
      I3 => \uart_tx_data[1]_i_3_n_0\,
      I4 => \uart_tx_data[7]_i_5_n_0\,
      I5 => pll_read_data(1),
      O => p_1_in(1)
    );
\uart_tx_data[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFAAAAAAAEAAAAA"
    )
        port map (
      I0 => \uart_tx_data[1]_i_4_n_0\,
      I1 => in_uart_data(3),
      I2 => \uart_tx_data[2]_i_6_n_0\,
      I3 => \uart_tx_data[1]_i_5_n_0\,
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => \uart_tx_data[1]_i_2_n_0\
    );
\uart_tx_data[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0E04FFFF"
    )
        port map (
      I0 => \pll_write_data[23]_i_4_n_0\,
      I1 => pamp_en_reg_i_4_n_0,
      I2 => \uart_tx_data[2]_i_5_n_0\,
      I3 => \uart_tx_data[1]_i_6_n_0\,
      I4 => \uart_tx_data[2]_i_3_n_0\,
      I5 => \uart_tx_data[1]_i_7_n_0\,
      O => \uart_tx_data[1]_i_3_n_0\
    );
\uart_tx_data[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3030303230003032"
    )
        port map (
      I0 => \uart_tx_data[1]_i_8_n_0\,
      I1 => is_uart_receiving_pll_write_address_i_2_n_0,
      I2 => \uart_tx_data[1]_i_9_n_0\,
      I3 => pll_write_i_2_n_0,
      I4 => \pll_write_data[23]_i_4_n_0\,
      I5 => \uart_tx_data[1]_i_6_n_0\,
      O => \uart_tx_data[1]_i_4_n_0\
    );
\uart_tx_data[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => in_uart_data(5),
      I1 => in_uart_data(6),
      I2 => in_uart_data(7),
      I3 => in_uart_data(4),
      O => \uart_tx_data[1]_i_5_n_0\
    );
\uart_tx_data[1]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => is_uart_receiving_pll_read_address,
      I1 => is_uart_receiving_pll_data_reg_n_0,
      I2 => is_uart_receiving_pll_write_address,
      O => \uart_tx_data[1]_i_6_n_0\
    );
\uart_tx_data[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BBFB0000"
    )
        port map (
      I0 => uart_byte_received_prev,
      I1 => uart_byte_received,
      I2 => is_uart_receiving_pll_write_address_i_2_n_0,
      I3 => \pll_write_data[23]_i_4_n_0\,
      I4 => pll_data_ready,
      I5 => pll_data_ready_prev,
      O => \uart_tx_data[1]_i_7_n_0\
    );
\uart_tx_data[1]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => pll_read_data(17),
      I1 => in_uart_data(1),
      I2 => in_uart_data(0),
      O => \uart_tx_data[1]_i_8_n_0\
    );
\uart_tx_data[1]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => in_uart_data(1),
      I1 => in_uart_data(0),
      I2 => in_uart_data(2),
      O => \uart_tx_data[1]_i_9_n_0\
    );
\uart_tx_data[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAFFEAFFEAFF"
    )
        port map (
      I0 => \uart_tx_data[2]_i_2_n_0\,
      I1 => \uart_tx_data[7]_i_6_n_0\,
      I2 => pll_read_data(10),
      I3 => \uart_tx_data[2]_i_3_n_0\,
      I4 => \uart_tx_data[7]_i_7_n_0\,
      I5 => pll_read_data(18),
      O => p_1_in(2)
    );
\uart_tx_data[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F888888888888888"
    )
        port map (
      I0 => pll_read_data(2),
      I1 => \uart_tx_data[7]_i_5_n_0\,
      I2 => \uart_tx_data[2]_i_4_n_0\,
      I3 => in_uart_data(2),
      I4 => \uart_tx_data[2]_i_5_n_0\,
      I5 => \uart_tx_data[2]_i_6_n_0\,
      O => \uart_tx_data[2]_i_2_n_0\
    );
\uart_tx_data[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F1FFFFFF"
    )
        port map (
      I0 => \received_byte_num_reg_n_0_[0]\,
      I1 => \received_byte_num_reg_n_0_[1]\,
      I2 => uart_byte_received_prev,
      I3 => uart_byte_received,
      I4 => is_uart_receiving_atten_value_reg_n_0,
      O => \uart_tx_data[2]_i_3_n_0\
    );
\uart_tx_data[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000E"
    )
        port map (
      I0 => \uart_tx_data[2]_i_7_n_0\,
      I1 => in_uart_data(3),
      I2 => in_uart_data(5),
      I3 => in_uart_data(6),
      I4 => in_uart_data(7),
      I5 => in_uart_data(4),
      O => \uart_tx_data[2]_i_4_n_0\
    );
\uart_tx_data[2]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => pll_data_ready_prev,
      I1 => pll_data_ready,
      O => \uart_tx_data[2]_i_5_n_0\
    );
\uart_tx_data[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF1FFFFFFFFFF"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address,
      I1 => is_uart_receiving_pll_data_reg_n_0,
      I2 => is_uart_receiving_pll_read_address,
      I3 => \pll_write_data[23]_i_4_n_0\,
      I4 => uart_byte_received_prev,
      I5 => uart_byte_received,
      O => \uart_tx_data[2]_i_6_n_0\
    );
\uart_tx_data[2]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => in_uart_data(0),
      I1 => in_uart_data(1),
      O => \uart_tx_data[2]_i_7_n_0\
    );
\uart_tx_data[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \uart_tx_data[3]_i_2_n_0\,
      I1 => \uart_tx_data[7]_i_6_n_0\,
      I2 => pll_read_data(11),
      I3 => pll_read_data(19),
      I4 => \uart_tx_data[7]_i_7_n_0\,
      O => p_1_in(3)
    );
\uart_tx_data[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888F8F888F8"
    )
        port map (
      I0 => pll_read_data(3),
      I1 => \uart_tx_data[7]_i_5_n_0\,
      I2 => \uart_tx_data[4]_i_5_n_0\,
      I3 => pll_data_ready,
      I4 => pll_data_ready_prev,
      I5 => if_amp1_en_reg_i_2_n_0,
      O => \uart_tx_data[3]_i_2_n_0\
    );
\uart_tx_data[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAEAEAFAEAEAEA"
    )
        port map (
      I0 => \uart_tx_data[4]_i_2_n_0\,
      I1 => \uart_tx_data[4]_i_3_n_0\,
      I2 => \uart_tx_data[4]_i_4_n_0\,
      I3 => pll_read_data(20),
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => p_1_in(4)
    );
\uart_tx_data[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000222"
    )
        port map (
      I0 => \uart_tx_data[4]_i_5_n_0\,
      I1 => in_uart_data(2),
      I2 => in_uart_data(0),
      I3 => in_uart_data(1),
      I4 => is_uart_receiving_pll_write_address_i_2_n_0,
      I5 => \uart_tx_data[4]_i_6_n_0\,
      O => \uart_tx_data[4]_i_2_n_0\
    );
\uart_tx_data[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => pll_read_data(4),
      I1 => in_uart_data(0),
      I2 => pll_read_data(12),
      I3 => in_uart_data(1),
      O => \uart_tx_data[4]_i_3_n_0\
    );
\uart_tx_data[4]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address_i_2_n_0,
      I1 => \received_byte_num_reg_n_0_[0]\,
      I2 => \received_byte_num_reg_n_0_[1]\,
      I3 => uart_byte_received_prev,
      I4 => uart_byte_received,
      O => \uart_tx_data[4]_i_4_n_0\
    );
\uart_tx_data[4]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF4445"
    )
        port map (
      I0 => is_uart_receiving_atten_value_reg_n_0,
      I1 => is_uart_receiving_pll_read_address,
      I2 => is_uart_receiving_pll_data_reg_n_0,
      I3 => is_uart_receiving_pll_write_address,
      I4 => \received_byte_num[0]_i_2_n_0\,
      O => \uart_tx_data[4]_i_5_n_0\
    );
\uart_tx_data[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000CCFCEEEE"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address_i_2_n_0,
      I1 => pll_write_i_2_n_0,
      I2 => \uart_tx_data[1]_i_6_n_0\,
      I3 => is_uart_receiving_atten_value_reg_n_0,
      I4 => \pll_write_data[23]_i_4_n_0\,
      I5 => \uart_tx_data[2]_i_5_n_0\,
      O => \uart_tx_data[4]_i_6_n_0\
    );
\uart_tx_data[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \uart_tx_data[7]_i_5_n_0\,
      I1 => pll_read_data(5),
      I2 => \uart_tx_data[7]_i_6_n_0\,
      I3 => pll_read_data(13),
      I4 => pll_read_data(21),
      I5 => \uart_tx_data[7]_i_7_n_0\,
      O => p_1_in(5)
    );
\uart_tx_data[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \uart_tx_data[7]_i_5_n_0\,
      I1 => pll_read_data(6),
      I2 => \uart_tx_data[7]_i_6_n_0\,
      I3 => pll_read_data(14),
      I4 => pll_read_data(22),
      I5 => \uart_tx_data[7]_i_7_n_0\,
      O => p_1_in(6)
    );
\uart_tx_data[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \uart_tx_data[7]_i_4_n_0\,
      I1 => pll_data_ready,
      I2 => pll_data_ready_prev,
      O => \uart_tx_data[7]_i_1_n_0\
    );
\uart_tx_data[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \uart_tx_data[7]_i_5_n_0\,
      I1 => pll_read_data(7),
      I2 => \uart_tx_data[7]_i_6_n_0\,
      I3 => pll_read_data(15),
      I4 => pll_read_data(23),
      I5 => \uart_tx_data[7]_i_7_n_0\,
      O => p_1_in(7)
    );
\uart_tx_data[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_wizard_main_n_4,
      I1 => clk_wizard_main_n_1,
      O => uart_byte_received_prev0
    );
\uart_tx_data[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF03020F0F"
    )
        port map (
      I0 => in_uart_data(4),
      I1 => sw_ctrl_reg_i_2_n_0,
      I2 => sw_if2_reg_i_2_n_0,
      I3 => in_uart_data(2),
      I4 => if_amp1_en_reg_i_2_n_0,
      I5 => \uart_tx_data[7]_i_8_n_0\,
      O => \uart_tx_data[7]_i_4_n_0\
    );
\uart_tx_data[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address_i_2_n_0,
      I1 => in_uart_data(2),
      I2 => pll_write_i_2_n_0,
      I3 => \pll_write_data[23]_i_4_n_0\,
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => \uart_tx_data[7]_i_5_n_0\
    );
\uart_tx_data[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address_i_2_n_0,
      I1 => in_uart_data(2),
      I2 => pll_write_i_2_n_0,
      I3 => \pll_write_data[23]_i_4_n_0\,
      I4 => in_uart_data(1),
      I5 => in_uart_data(0),
      O => \uart_tx_data[7]_i_6_n_0\
    );
\uart_tx_data[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000100000"
    )
        port map (
      I0 => \pll_write_data[23]_i_4_n_0\,
      I1 => pll_write_i_2_n_0,
      I2 => in_uart_data(2),
      I3 => in_uart_data(0),
      I4 => in_uart_data(1),
      I5 => is_uart_receiving_pll_write_address_i_2_n_0,
      O => \uart_tx_data[7]_i_7_n_0\
    );
\uart_tx_data[7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFAA00000303"
    )
        port map (
      I0 => \uart_tx_data[7]_i_9_n_0\,
      I1 => \uart_tx_data[0]_i_9_n_0\,
      I2 => sw_ctrl_reg_i_2_n_0,
      I3 => is_uart_receiving_atten_value_reg_n_0,
      I4 => pll_write_i_2_n_0,
      I5 => \pll_write_data[23]_i_4_n_0\,
      O => \uart_tx_data[7]_i_8_n_0\
    );
\uart_tx_data[7]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => is_uart_receiving_pll_write_address,
      I1 => is_uart_receiving_pll_data_reg_n_0,
      I2 => is_uart_receiving_pll_read_address,
      O => \uart_tx_data[7]_i_9_n_0\
    );
\uart_tx_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(0),
      Q => \^out_data_uart\(0),
      R => '0'
    );
\uart_tx_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(1),
      Q => \^out_data_uart\(1),
      R => '0'
    );
\uart_tx_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(2),
      Q => \^out_data_uart\(2),
      R => '0'
    );
\uart_tx_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(3),
      Q => \^out_data_uart\(3),
      R => '0'
    );
\uart_tx_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(4),
      Q => \^out_data_uart\(4),
      R => '0'
    );
\uart_tx_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(5),
      Q => \^out_data_uart\(5),
      R => '0'
    );
\uart_tx_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(6),
      Q => \^out_data_uart\(6),
      R => '0'
    );
\uart_tx_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => uart_byte_received_prev0,
      CE => \uart_tx_data[7]_i_1_n_0\,
      D => p_1_in(7),
      Q => \^out_data_uart\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_top_logic_0_0 is
  port (
    in_uart_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk_100MHz_in : in STD_LOGIC;
    in_adc_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_adc_start_sending : in STD_LOGIC;
    pll_data_ready : in STD_LOGIC;
    uart_byte_received : in STD_LOGIC;
    pll_read_data : in STD_LOGIC_VECTOR ( 23 downto 0 );
    out_data_uart : out STD_LOGIC_VECTOR ( 7 downto 0 );
    t_sweep : out STD_LOGIC;
    clk_100MHz_out : out STD_LOGIC;
    user_led : out STD_LOGIC;
    pamp_en : out STD_LOGIC;
    sw_ls : out STD_LOGIC;
    if_amp1_stu_en : out STD_LOGIC;
    if_amp2_stu_en : out STD_LOGIC;
    sw_if1 : out STD_LOGIC;
    sw_if2 : out STD_LOGIC;
    sw_if3 : out STD_LOGIC;
    clk_sync : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    sw_ctrl : out STD_LOGIC;
    atten : out STD_LOGIC_VECTOR ( 5 downto 0 );
    start_uart_send : out STD_LOGIC;
    is_adc_data_sending : out STD_LOGIC;
    clk_10MHz_out : out STD_LOGIC;
    start_init_clk : out STD_LOGIC;
    clk_20MHz_out : out STD_LOGIC;
    pll_read : out STD_LOGIC;
    pll_write : out STD_LOGIC;
    pll_write_data : out STD_LOGIC_VECTOR ( 23 downto 0 );
    pll_write_addres : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_read_addres : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_top_logic_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_top_logic_0_0 : entity is "design_1_top_logic_0_0,top_logic,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_top_logic_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_top_logic_0_0 : entity is "top_logic,Vivado 2019.1";
end design_1_top_logic_0_0;

architecture STRUCTURE of design_1_top_logic_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^start_init_clk\ : STD_LOGIC;
begin
  clk_sync <= \^start_init_clk\;
  is_adc_data_sending <= \<const0>\;
  start_init_clk <= \^start_init_clk\;
  sw_ls <= \<const1>\;
  user_led <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.design_1_top_logic_0_0_top_logic
     port map (
      atten(5 downto 0) => atten(5 downto 0),
      clk_100MHz_in => clk_100MHz_in,
      clk_100MHz_out => clk_100MHz_out,
      clk_10MHz_out => clk_10MHz_out,
      clk_20MHz_out => clk_20MHz_out,
      if_amp1_stu_en => if_amp1_stu_en,
      if_amp2_stu_en => if_amp2_stu_en,
      in_uart_data(7 downto 0) => in_uart_data(7 downto 0),
      out_data_uart(7 downto 0) => out_data_uart(7 downto 0),
      pamp_en => pamp_en,
      pll_data_ready => pll_data_ready,
      pll_read => pll_read,
      pll_read_addres(7 downto 0) => pll_read_addres(7 downto 0),
      pll_read_data(23 downto 0) => pll_read_data(23 downto 0),
      pll_trig => pll_trig,
      pll_write => pll_write,
      pll_write_addres(4 downto 0) => pll_write_addres(4 downto 0),
      pll_write_data(23 downto 0) => pll_write_data(23 downto 0),
      start_init_clk => \^start_init_clk\,
      start_uart_send => start_uart_send,
      sw_ctrl => sw_ctrl,
      sw_if1 => sw_if1,
      sw_if2 => sw_if2,
      sw_if3 => sw_if3,
      t_sweep => t_sweep,
      uart_adc_start_sending => uart_adc_start_sending,
      uart_byte_received => uart_byte_received
    );
end STRUCTURE;

// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: bt.local:user:adc_control:1.0
// IP Revision: 35

(* X_CORE_INFO = "adc_control,Vivado 2019.1" *)
(* CHECK_LICENSE_TYPE = "design_1_adc_control_0_0,adc_control,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_adc_control_0_0 (
  test1,
  test2,
  test3,
  test4,
  tsweep,
  uart_clk,
  output_data,
  is_sending,
  send_uart_byte,
  adc_dax_p,
  adc_dax_n,
  adc_dbx_p,
  adc_dbx_n,
  adc_dcox_p,
  adc_dcox_n,
  fpga_clkx_p,
  fpga_clkx_n,
  adc_clkx_p,
  adc_clkx_n
);

input wire test1;
input wire test2;
input wire test3;
input wire test4;
input wire tsweep;
input wire uart_clk;
output wire [7 : 0] output_data;
input wire is_sending;
output wire send_uart_byte;
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *)
input wire adc_dax_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dax, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *)
input wire adc_dax_n;
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *)
input wire adc_dbx_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *)
input wire adc_dbx_n;
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *)
input wire adc_dcox_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *)
input wire adc_dcox_n;
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx p" *)
input wire fpga_clkx_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME fpga_clkx, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 fpga_clkx n" *)
input wire fpga_clkx_n;
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *)
output wire adc_clkx_p;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *)
output wire adc_clkx_n;

diff fpga_clkx();
assign fpga_clkx.p = fpga_clkx_p;
assign fpga_clkx.n = fpga_clkx_n;
diff adc_clkx();
assign adc_clkx_p = adc_clkx.p;
assign adc_clkx_n = adc_clkx.n;
diff adc_dax();
assign adc_dax.p = adc_dax_p;
assign adc_dax.n = adc_dax_n;
diff adc_dbx();
assign adc_dbx.p = adc_dbx_p;
assign adc_dbx.n = adc_dbx_n;
diff adc_dcox();
assign adc_dcox.p = adc_dcox_p;
assign adc_dcox.n = adc_dcox_n;

  adc_control inst (
    .test1(test1),
    .test2(test2),
    .test3(test3),
    .test4(test4),
    .tsweep(tsweep),
    .uart_clk(uart_clk),
    .output_data(output_data),
    .is_sending(is_sending),
    .send_uart_byte(send_uart_byte),
    .adc_dax(adc_dax),
    .adc_dbx(adc_dbx),
    .adc_dcox(adc_dcox),
    .fpga_clkx(fpga_clkx),
    .adc_clkx(adc_clkx)
  );
endmodule

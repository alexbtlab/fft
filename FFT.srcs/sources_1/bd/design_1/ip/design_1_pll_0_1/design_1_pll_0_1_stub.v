// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun  3 13:15:56 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_pll_0_1 -prefix
//               design_1_pll_0_1_ design_1_pll_0_1_stub.v
// Design      : design_1_pll_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "pll,Vivado 2019.1" *)
module design_1_pll_0_1(pll_ld_sdo, clk_pll_spi, pll_read, pll_write, 
  pll_write_data, pll_write_address_reg, pll_read_address_reg, pll_sen, pll_mosi, pll_sck, 
  pll_read_data, pll_data_ready)
/* synthesis syn_black_box black_box_pad_pin="pll_ld_sdo,clk_pll_spi,pll_read,pll_write,pll_write_data[23:0],pll_write_address_reg[4:0],pll_read_address_reg[7:0],pll_sen,pll_mosi,pll_sck,pll_read_data[23:0],pll_data_ready" */;
  input pll_ld_sdo;
  input clk_pll_spi;
  input pll_read;
  input pll_write;
  input [23:0]pll_write_data;
  input [4:0]pll_write_address_reg;
  input [7:0]pll_read_address_reg;
  output pll_sen;
  output pll_mosi;
  output pll_sck;
  output [23:0]pll_read_data;
  output pll_data_ready;
endmodule

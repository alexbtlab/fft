-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 12:27:52 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_init_clk_0_0 -prefix
--               design_1_init_clk_0_0_ design_1_init_clk_0_0_stub.vhdl
-- Design      : design_1_init_clk_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_init_clk_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    start : in STD_LOGIC;
    clk_sclk : out STD_LOGIC;
    clk_mosi : out STD_LOGIC;
    clk_cs : out STD_LOGIC
  );

end design_1_init_clk_0_0;

architecture stub of design_1_init_clk_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,start,clk_sclk,clk_mosi,clk_cs";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "init_clk,Vivado 2019.1";
begin
end;

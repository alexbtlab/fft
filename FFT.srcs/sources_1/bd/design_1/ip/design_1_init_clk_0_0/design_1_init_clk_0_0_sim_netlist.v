// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 12:27:52 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_init_clk_0_0 -prefix
//               design_1_init_clk_0_0_ design_1_init_clk_0_0_sim_netlist.v
// Design      : design_1_init_clk_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_init_clk_0_0,init_clk,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "init_clk,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_init_clk_0_0
   (clk,
    start,
    clk_sclk,
    clk_mosi,
    clk_cs);
  input clk;
  input start;
  output clk_sclk;
  output clk_mosi;
  output clk_cs;

  wire clk;
  wire clk_cs;
  wire clk_mosi;
  wire clk_sclk;
  wire start;

  design_1_init_clk_0_0_init_clk inst
       (.clk(clk),
        .clk_cs(clk_cs),
        .clk_mosi(clk_mosi),
        .clk_sclk(clk_sclk),
        .start(start));
endmodule

module design_1_init_clk_0_0_init_clk
   (clk_cs,
    clk_sclk,
    clk_mosi,
    start,
    clk);
  output clk_cs;
  output clk_sclk;
  output clk_mosi;
  input start;
  input clk;

  wire \bits_left[1]_i_2_n_0 ;
  wire \bits_left[3]_i_2_n_0 ;
  wire \bits_left[6]_i_1_n_0 ;
  wire \bits_left[6]_i_2_n_0 ;
  wire [6:0]bits_left_reg;
  wire clk;
  wire clk_cs;
  wire clk_cs_reg_i_1_n_0;
  wire clk_cs_reg_i_2_n_0;
  wire clk_mosi;
  wire clk_mosi_reg_i_1_n_0;
  wire clk_mosi_reg_i_2_n_0;
  wire clk_mosi_reg_i_3_n_0;
  wire clk_mosi_reg_i_4_n_0;
  wire clk_mosi_reg_i_5_n_0;
  wire clk_mosi_reg_i_6_n_0;
  wire clk_mosi_reg_i_7_n_0;
  wire clk_sclk;
  wire clk_sclk_INST_0_i_1_n_0;
  wire [6:0]p_0_in;
  wire start;
  wire start_prev;

  LUT6 #(
    .INIT(64'h3333333333333332)) 
    \bits_left[0]_i_1 
       (.I0(bits_left_reg[1]),
        .I1(bits_left_reg[0]),
        .I2(bits_left_reg[6]),
        .I3(bits_left_reg[4]),
        .I4(bits_left_reg[5]),
        .I5(\bits_left[1]_i_2_n_0 ),
        .O(p_0_in[0]));
  LUT6 #(
    .INIT(64'h9999999999999998)) 
    \bits_left[1]_i_1 
       (.I0(bits_left_reg[0]),
        .I1(bits_left_reg[1]),
        .I2(bits_left_reg[6]),
        .I3(bits_left_reg[4]),
        .I4(bits_left_reg[5]),
        .I5(\bits_left[1]_i_2_n_0 ),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \bits_left[1]_i_2 
       (.I0(bits_left_reg[2]),
        .I1(bits_left_reg[3]),
        .O(\bits_left[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF0F0F00E)) 
    \bits_left[2]_i_1 
       (.I0(\bits_left[3]_i_2_n_0 ),
        .I1(bits_left_reg[3]),
        .I2(bits_left_reg[2]),
        .I3(bits_left_reg[0]),
        .I4(bits_left_reg[1]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF0E1F0E0)) 
    \bits_left[3]_i_1 
       (.I0(bits_left_reg[1]),
        .I1(bits_left_reg[0]),
        .I2(bits_left_reg[3]),
        .I3(bits_left_reg[2]),
        .I4(\bits_left[3]_i_2_n_0 ),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \bits_left[3]_i_2 
       (.I0(bits_left_reg[5]),
        .I1(bits_left_reg[4]),
        .I2(bits_left_reg[6]),
        .O(\bits_left[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hF00E)) 
    \bits_left[4]_i_1 
       (.I0(bits_left_reg[6]),
        .I1(bits_left_reg[5]),
        .I2(bits_left_reg[4]),
        .I3(clk_sclk_INST_0_i_1_n_0),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hC9C8)) 
    \bits_left[5]_i_1 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[5]),
        .I2(bits_left_reg[4]),
        .I3(bits_left_reg[6]),
        .O(p_0_in[5]));
  LUT2 #(
    .INIT(4'h2)) 
    \bits_left[6]_i_1 
       (.I0(start),
        .I1(start_prev),
        .O(\bits_left[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \bits_left[6]_i_2 
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[5]),
        .I2(bits_left_reg[4]),
        .I3(bits_left_reg[6]),
        .I4(start),
        .O(\bits_left[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \bits_left[6]_i_3 
       (.I0(bits_left_reg[4]),
        .I1(bits_left_reg[5]),
        .I2(clk_sclk_INST_0_i_1_n_0),
        .I3(bits_left_reg[6]),
        .O(p_0_in[6]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[0] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[0]),
        .Q(bits_left_reg[0]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[1] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[1]),
        .Q(bits_left_reg[1]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[2] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[2]),
        .Q(bits_left_reg[2]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[3] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[3]),
        .Q(bits_left_reg[3]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[4] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[4]),
        .Q(bits_left_reg[4]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[5] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[5]),
        .Q(bits_left_reg[5]),
        .R(\bits_left[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_left_reg[6] 
       (.C(clk),
        .CE(\bits_left[6]_i_2_n_0 ),
        .D(p_0_in[6]),
        .Q(bits_left_reg[6]),
        .S(\bits_left[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8CAC)) 
    clk_cs_reg_i_1
       (.I0(start_prev),
        .I1(clk_cs),
        .I2(start),
        .I3(clk_cs_reg_i_2_n_0),
        .O(clk_cs_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_cs_reg_i_2
       (.I0(bits_left_reg[6]),
        .I1(bits_left_reg[4]),
        .I2(bits_left_reg[5]),
        .I3(clk_sclk_INST_0_i_1_n_0),
        .O(clk_cs_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    clk_cs_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(clk_cs_reg_i_1_n_0),
        .Q(clk_cs),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEFEFEEEFE0E0EEE0)) 
    clk_mosi_reg_i_1
       (.I0(clk_mosi_reg_i_2_n_0),
        .I1(clk_mosi_reg_i_3_n_0),
        .I2(\bits_left[6]_i_2_n_0 ),
        .I3(start),
        .I4(start_prev),
        .I5(clk_mosi),
        .O(clk_mosi_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h000F00F800080008)) 
    clk_mosi_reg_i_2
       (.I0(clk_mosi_reg_i_4_n_0),
        .I1(clk_mosi_reg_i_5_n_0),
        .I2(bits_left_reg[2]),
        .I3(bits_left_reg[3]),
        .I4(bits_left_reg[1]),
        .I5(clk_mosi_reg_i_6_n_0),
        .O(clk_mosi_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'hAAAEAAAAAAAAAAAA)) 
    clk_mosi_reg_i_3
       (.I0(clk_mosi_reg_i_7_n_0),
        .I1(bits_left_reg[5]),
        .I2(start),
        .I3(bits_left_reg[2]),
        .I4(bits_left_reg[4]),
        .I5(bits_left_reg[1]),
        .O(clk_mosi_reg_i_3_n_0));
  LUT4 #(
    .INIT(16'h5F44)) 
    clk_mosi_reg_i_4
       (.I0(bits_left_reg[1]),
        .I1(bits_left_reg[5]),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[4]),
        .O(clk_mosi_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    clk_mosi_reg_i_5
       (.I0(bits_left_reg[6]),
        .I1(start_prev),
        .I2(start),
        .O(clk_mosi_reg_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h04044404)) 
    clk_mosi_reg_i_6
       (.I0(bits_left_reg[4]),
        .I1(bits_left_reg[0]),
        .I2(start),
        .I3(start_prev),
        .I4(bits_left_reg[6]),
        .O(clk_mosi_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    clk_mosi_reg_i_7
       (.I0(start_prev),
        .I1(bits_left_reg[6]),
        .I2(bits_left_reg[0]),
        .I3(bits_left_reg[5]),
        .I4(bits_left_reg[3]),
        .I5(bits_left_reg[2]),
        .O(clk_mosi_reg_i_7_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    clk_mosi_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(clk_mosi_reg_i_1_n_0),
        .Q(clk_mosi),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    clk_sclk_INST_0
       (.I0(clk_sclk_INST_0_i_1_n_0),
        .I1(bits_left_reg[5]),
        .I2(bits_left_reg[4]),
        .I3(bits_left_reg[6]),
        .I4(clk),
        .O(clk_sclk));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    clk_sclk_INST_0_i_1
       (.I0(bits_left_reg[1]),
        .I1(bits_left_reg[0]),
        .I2(bits_left_reg[3]),
        .I3(bits_left_reg[2]),
        .O(clk_sclk_INST_0_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(start),
        .Q(start_prev),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

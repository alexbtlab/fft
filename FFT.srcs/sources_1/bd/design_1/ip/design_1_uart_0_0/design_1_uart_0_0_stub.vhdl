-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  2 22:08:59 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_uart_0_0 -prefix
--               design_1_uart_0_0_ design_1_uart_0_0_stub.vhdl
-- Design      : design_1_uart_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_uart_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    uart_rx : in STD_LOGIC;
    uart_data_tx : in STD_LOGIC_VECTOR ( 7 downto 0 );
    byte_received : out STD_LOGIC;
    uart_data_rx : out STD_LOGIC_VECTOR ( 7 downto 0 );
    uart_tx : out STD_LOGIC;
    is_transmission_going : out STD_LOGIC;
    send_byte : in STD_LOGIC
  );

end design_1_uart_0_0;

architecture stub of design_1_uart_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,uart_rx,uart_data_tx[7:0],byte_received,uart_data_rx[7:0],uart_tx,is_transmission_going,send_byte";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "uart,Vivado 2019.1";
begin
end;

// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  2 22:08:59 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_uart_0_0 -prefix
//               design_1_uart_0_0_ design_1_uart_0_0_sim_netlist.v
// Design      : design_1_uart_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_uart_0_0,uart,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "uart,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_uart_0_0
   (clk,
    uart_rx,
    uart_data_tx,
    byte_received,
    uart_data_rx,
    uart_tx,
    is_transmission_going,
    send_byte);
  input clk;
  input uart_rx;
  input [7:0]uart_data_tx;
  output byte_received;
  output [7:0]uart_data_rx;
  output uart_tx;
  output is_transmission_going;
  input send_byte;

  wire byte_received;
  wire clk;
  wire is_transmission_going;
  wire send_byte;
  wire [7:0]uart_data_rx;
  wire [7:0]uart_data_tx;
  wire uart_rx;
  wire uart_tx;

  design_1_uart_0_0_uart inst
       (.byte_received(byte_received),
        .clk(clk),
        .is_transmission_going_reg_reg(is_transmission_going),
        .send_byte(send_byte),
        .uart_data_rx(uart_data_rx),
        .uart_data_tx(uart_data_tx),
        .uart_rx(uart_rx),
        .uart_tx(uart_tx));
endmodule

module design_1_uart_0_0_uart
   (is_transmission_going_reg_reg,
    uart_data_rx,
    uart_tx,
    byte_received,
    send_byte,
    clk,
    uart_rx,
    uart_data_tx);
  output is_transmission_going_reg_reg;
  output [7:0]uart_data_rx;
  output uart_tx;
  output byte_received;
  input send_byte;
  input clk;
  input uart_rx;
  input [7:0]uart_data_tx;

  wire byte_received;
  wire clk;
  wire is_transmission_going_reg_reg;
  wire send_byte;
  wire [7:0]uart_data_rx;
  wire [7:0]uart_data_tx;
  wire uart_rx;
  wire uart_tx;

  design_1_uart_0_0_uart_receive uart_receive_inst
       (.byte_received(byte_received),
        .clk(clk),
        .uart_data_rx(uart_data_rx),
        .uart_rx(uart_rx));
  design_1_uart_0_0_uart_send uart_send_inst
       (.clk(clk),
        .is_transmission_going_reg_reg_0(is_transmission_going_reg_reg),
        .send_byte(send_byte),
        .uart_data_tx(uart_data_tx),
        .uart_tx(uart_tx));
endmodule

module design_1_uart_0_0_uart_receive
   (byte_received,
    uart_data_rx,
    clk,
    uart_rx);
  output byte_received;
  output [7:0]uart_data_rx;
  input clk;
  input uart_rx;

  wire byte_received;
  wire byte_received_reg_i_1_n_0;
  wire byte_received_reg_i_2_n_0;
  wire byte_received_reg_i_3_n_0;
  wire byte_received_reg_i_4_n_0;
  wire clk;
  wire is_receiving_going_i_1_n_0;
  wire is_receiving_going_reg_n_0;
  wire [2:0]p_0_in;
  wire start_bit_recieved_i_1_n_0;
  wire start_bit_recieved_reg_n_0;
  wire [6:0]uart_cnt;
  wire \uart_cnt[0]_i_1_n_0 ;
  wire \uart_cnt[1]_i_1_n_0 ;
  wire \uart_cnt[1]_i_2_n_0 ;
  wire \uart_cnt[2]_i_1_n_0 ;
  wire \uart_cnt[2]_i_2_n_0 ;
  wire \uart_cnt[3]_i_1_n_0 ;
  wire \uart_cnt[4]_i_1_n_0 ;
  wire \uart_cnt[5]_i_1_n_0 ;
  wire \uart_cnt[6]_i_1_n_0 ;
  wire \uart_cnt[6]_i_2_n_0 ;
  wire \uart_cnt[6]_i_3_n_0 ;
  wire \uart_cnt[6]_i_4_n_0 ;
  wire \uart_data_reg[0]_i_1_n_0 ;
  wire \uart_data_reg[1]_i_1_n_0 ;
  wire \uart_data_reg[2]_i_1_n_0 ;
  wire \uart_data_reg[3]_i_1_n_0 ;
  wire \uart_data_reg[3]_i_2_n_0 ;
  wire \uart_data_reg[4]_i_1_n_0 ;
  wire \uart_data_reg[5]_i_1_n_0 ;
  wire \uart_data_reg[6]_i_1_n_0 ;
  wire \uart_data_reg[6]_i_2_n_0 ;
  wire \uart_data_reg[6]_i_3_n_0 ;
  wire \uart_data_reg[7]_i_1_n_0 ;
  wire \uart_data_reg[7]_i_2_n_0 ;
  wire [7:0]uart_data_rx;
  wire \uart_num_of_rbit[3]_i_1_n_0 ;
  wire \uart_num_of_rbit[3]_i_2_n_0 ;
  wire \uart_num_of_rbit[3]_i_3_n_0 ;
  wire [3:0]uart_num_of_rbit_reg;
  wire uart_rx;

  LUT3 #(
    .INIT(8'hDC)) 
    byte_received_reg_i_1
       (.I0(byte_received_reg_i_2_n_0),
        .I1(byte_received_reg_i_3_n_0),
        .I2(byte_received),
        .O(byte_received_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    byte_received_reg_i_2
       (.I0(byte_received_reg_i_4_n_0),
        .I1(uart_cnt[6]),
        .I2(start_bit_recieved_reg_n_0),
        .I3(uart_cnt[3]),
        .I4(uart_cnt[0]),
        .O(byte_received_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    byte_received_reg_i_3
       (.I0(\uart_num_of_rbit[3]_i_1_n_0 ),
        .I1(uart_num_of_rbit_reg[2]),
        .I2(uart_num_of_rbit_reg[3]),
        .I3(uart_num_of_rbit_reg[0]),
        .I4(uart_num_of_rbit_reg[1]),
        .O(byte_received_reg_i_3_n_0));
  LUT4 #(
    .INIT(16'h0040)) 
    byte_received_reg_i_4
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[4]),
        .I2(uart_cnt[5]),
        .I3(uart_cnt[1]),
        .O(byte_received_reg_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    byte_received_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(byte_received_reg_i_1_n_0),
        .Q(byte_received),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0F0B0008)) 
    is_receiving_going_i_1
       (.I0(start_bit_recieved_reg_n_0),
        .I1(byte_received_reg_i_2_n_0),
        .I2(byte_received_reg_i_3_n_0),
        .I3(uart_rx),
        .I4(is_receiving_going_reg_n_0),
        .O(is_receiving_going_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_receiving_going_reg
       (.C(clk),
        .CE(1'b1),
        .D(is_receiving_going_i_1_n_0),
        .Q(is_receiving_going_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0F01)) 
    start_bit_recieved_i_1
       (.I0(uart_rx),
        .I1(is_receiving_going_reg_n_0),
        .I2(byte_received_reg_i_2_n_0),
        .I3(start_bit_recieved_reg_n_0),
        .O(start_bit_recieved_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_bit_recieved_reg
       (.C(clk),
        .CE(1'b1),
        .D(start_bit_recieved_i_1_n_0),
        .Q(start_bit_recieved_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \uart_cnt[0]_i_1 
       (.I0(uart_cnt[0]),
        .O(\uart_cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \uart_cnt[1]_i_1 
       (.I0(\uart_cnt[1]_i_2_n_0 ),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[1]),
        .O(\uart_cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFD5FF)) 
    \uart_cnt[1]_i_2 
       (.I0(byte_received_reg_i_4_n_0),
        .I1(uart_cnt[4]),
        .I2(uart_cnt[3]),
        .I3(uart_cnt[5]),
        .I4(uart_cnt[6]),
        .I5(is_receiving_going_reg_n_0),
        .O(\uart_cnt[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h2A6A)) 
    \uart_cnt[2]_i_1 
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[1]),
        .I3(\uart_cnt[2]_i_2_n_0 ),
        .O(\uart_cnt[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \uart_cnt[2]_i_2 
       (.I0(uart_cnt[3]),
        .I1(is_receiving_going_reg_n_0),
        .I2(uart_cnt[6]),
        .I3(uart_cnt[4]),
        .I4(uart_cnt[5]),
        .O(\uart_cnt[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \uart_cnt[3]_i_1 
       (.I0(uart_cnt[3]),
        .I1(uart_cnt[2]),
        .I2(uart_cnt[1]),
        .I3(uart_cnt[0]),
        .O(\uart_cnt[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2AAAAAAA80000000)) 
    \uart_cnt[4]_i_1 
       (.I0(\uart_cnt[6]_i_4_n_0 ),
        .I1(uart_cnt[3]),
        .I2(uart_cnt[2]),
        .I3(uart_cnt[1]),
        .I4(uart_cnt[0]),
        .I5(uart_cnt[4]),
        .O(\uart_cnt[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h2AAA8000)) 
    \uart_cnt[5]_i_1 
       (.I0(\uart_cnt[6]_i_4_n_0 ),
        .I1(\uart_cnt[6]_i_3_n_0 ),
        .I2(uart_cnt[3]),
        .I3(uart_cnt[4]),
        .I4(uart_cnt[5]),
        .O(\uart_cnt[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \uart_cnt[6]_i_1 
       (.I0(is_receiving_going_reg_n_0),
        .I1(start_bit_recieved_reg_n_0),
        .O(\uart_cnt[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFF800000000000)) 
    \uart_cnt[6]_i_2 
       (.I0(uart_cnt[4]),
        .I1(uart_cnt[3]),
        .I2(\uart_cnt[6]_i_3_n_0 ),
        .I3(uart_cnt[5]),
        .I4(uart_cnt[6]),
        .I5(\uart_cnt[6]_i_4_n_0 ),
        .O(\uart_cnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \uart_cnt[6]_i_3 
       (.I0(uart_cnt[0]),
        .I1(uart_cnt[1]),
        .I2(uart_cnt[2]),
        .O(\uart_cnt[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h45444545)) 
    \uart_cnt[6]_i_4 
       (.I0(\uart_num_of_rbit[3]_i_1_n_0 ),
        .I1(\uart_cnt[1]_i_2_n_0 ),
        .I2(uart_cnt[2]),
        .I3(uart_cnt[1]),
        .I4(uart_cnt[0]),
        .O(\uart_cnt[6]_i_4_n_0 ));
  FDRE \uart_cnt_reg[0] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[0]_i_1_n_0 ),
        .Q(uart_cnt[0]),
        .R(1'b0));
  FDRE \uart_cnt_reg[1] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[1]_i_1_n_0 ),
        .Q(uart_cnt[1]),
        .R(1'b0));
  FDRE \uart_cnt_reg[2] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[2]_i_1_n_0 ),
        .Q(uart_cnt[2]),
        .R(1'b0));
  FDRE \uart_cnt_reg[3] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[3]_i_1_n_0 ),
        .Q(uart_cnt[3]),
        .R(1'b0));
  FDRE \uart_cnt_reg[4] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[4]_i_1_n_0 ),
        .Q(uart_cnt[4]),
        .R(1'b0));
  FDRE \uart_cnt_reg[5] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[5]_i_1_n_0 ),
        .Q(uart_cnt[5]),
        .R(1'b0));
  FDRE \uart_cnt_reg[6] 
       (.C(clk),
        .CE(\uart_cnt[6]_i_1_n_0 ),
        .D(\uart_cnt[6]_i_2_n_0 ),
        .Q(uart_cnt[6]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    \uart_data_reg[0]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(\uart_data_reg[3]_i_2_n_0 ),
        .I4(uart_data_rx[0]),
        .O(\uart_data_reg[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \uart_data_reg[1]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(\uart_data_reg[3]_i_2_n_0 ),
        .I4(uart_data_rx[1]),
        .O(\uart_data_reg[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \uart_data_reg[2]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[0]),
        .I2(uart_num_of_rbit_reg[1]),
        .I3(\uart_data_reg[3]_i_2_n_0 ),
        .I4(uart_data_rx[2]),
        .O(\uart_data_reg[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \uart_data_reg[3]_i_1 
       (.I0(uart_rx),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(\uart_data_reg[3]_i_2_n_0 ),
        .I4(uart_data_rx[3]),
        .O(\uart_data_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFBFF)) 
    \uart_data_reg[3]_i_2 
       (.I0(uart_num_of_rbit_reg[3]),
        .I1(is_receiving_going_reg_n_0),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(\uart_data_reg[6]_i_2_n_0 ),
        .O(\uart_data_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000008)) 
    \uart_data_reg[4]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[6]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(uart_num_of_rbit_reg[1]),
        .I4(\uart_data_reg[6]_i_3_n_0 ),
        .I5(uart_data_rx[4]),
        .O(\uart_data_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \uart_data_reg[5]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[6]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(uart_num_of_rbit_reg[1]),
        .I4(\uart_data_reg[6]_i_3_n_0 ),
        .I5(uart_data_rx[5]),
        .O(\uart_data_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \uart_data_reg[6]_i_1 
       (.I0(uart_rx),
        .I1(\uart_data_reg[6]_i_2_n_0 ),
        .I2(uart_num_of_rbit_reg[1]),
        .I3(uart_num_of_rbit_reg[0]),
        .I4(\uart_data_reg[6]_i_3_n_0 ),
        .I5(uart_data_rx[6]),
        .O(\uart_data_reg[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00100000)) 
    \uart_data_reg[6]_i_2 
       (.I0(\uart_num_of_rbit[3]_i_3_n_0 ),
        .I1(uart_cnt[3]),
        .I2(uart_cnt[6]),
        .I3(uart_cnt[4]),
        .I4(uart_cnt[5]),
        .O(\uart_data_reg[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \uart_data_reg[6]_i_3 
       (.I0(is_receiving_going_reg_n_0),
        .I1(uart_num_of_rbit_reg[3]),
        .I2(uart_num_of_rbit_reg[2]),
        .O(\uart_data_reg[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \uart_data_reg[7]_i_1 
       (.I0(uart_rx),
        .I1(\uart_num_of_rbit[3]_i_1_n_0 ),
        .I2(\uart_data_reg[7]_i_2_n_0 ),
        .I3(uart_num_of_rbit_reg[2]),
        .I4(uart_num_of_rbit_reg[3]),
        .I5(uart_data_rx[7]),
        .O(\uart_data_reg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \uart_data_reg[7]_i_2 
       (.I0(uart_num_of_rbit_reg[0]),
        .I1(uart_num_of_rbit_reg[1]),
        .O(\uart_data_reg[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[0]_i_1_n_0 ),
        .Q(uart_data_rx[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[1]_i_1_n_0 ),
        .Q(uart_data_rx[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[2]_i_1_n_0 ),
        .Q(uart_data_rx[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[3]_i_1_n_0 ),
        .Q(uart_data_rx[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[4]_i_1_n_0 ),
        .Q(uart_data_rx[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[5]_i_1_n_0 ),
        .Q(uart_data_rx[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[6]_i_1_n_0 ),
        .Q(uart_data_rx[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_data_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\uart_data_reg[7]_i_1_n_0 ),
        .Q(uart_data_rx[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0F0B)) 
    \uart_num_of_rbit[0]_i_1 
       (.I0(uart_num_of_rbit_reg[2]),
        .I1(uart_num_of_rbit_reg[3]),
        .I2(uart_num_of_rbit_reg[0]),
        .I3(uart_num_of_rbit_reg[1]),
        .O(p_0_in[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \uart_num_of_rbit[1]_i_1 
       (.I0(uart_num_of_rbit_reg[0]),
        .I1(uart_num_of_rbit_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \uart_num_of_rbit[2]_i_1 
       (.I0(uart_num_of_rbit_reg[2]),
        .I1(uart_num_of_rbit_reg[1]),
        .I2(uart_num_of_rbit_reg[0]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \uart_num_of_rbit[3]_i_1 
       (.I0(uart_cnt[5]),
        .I1(uart_cnt[4]),
        .I2(uart_cnt[6]),
        .I3(is_receiving_going_reg_n_0),
        .I4(uart_cnt[3]),
        .I5(\uart_num_of_rbit[3]_i_3_n_0 ),
        .O(\uart_num_of_rbit[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7E80)) 
    \uart_num_of_rbit[3]_i_2 
       (.I0(uart_num_of_rbit_reg[1]),
        .I1(uart_num_of_rbit_reg[0]),
        .I2(uart_num_of_rbit_reg[2]),
        .I3(uart_num_of_rbit_reg[3]),
        .O(\uart_num_of_rbit[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \uart_num_of_rbit[3]_i_3 
       (.I0(uart_cnt[2]),
        .I1(uart_cnt[0]),
        .I2(uart_cnt[1]),
        .O(\uart_num_of_rbit[3]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[0] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[0]),
        .Q(uart_num_of_rbit_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[1] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[1]),
        .Q(uart_num_of_rbit_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[2] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(p_0_in[2]),
        .Q(uart_num_of_rbit_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \uart_num_of_rbit_reg[3] 
       (.C(clk),
        .CE(\uart_num_of_rbit[3]_i_1_n_0 ),
        .D(\uart_num_of_rbit[3]_i_2_n_0 ),
        .Q(uart_num_of_rbit_reg[3]),
        .R(1'b0));
endmodule

module design_1_uart_0_0_uart_send
   (is_transmission_going_reg_reg_0,
    uart_tx,
    send_byte,
    clk,
    uart_data_tx);
  output is_transmission_going_reg_reg_0;
  output uart_tx;
  input send_byte;
  input clk;
  input [7:0]uart_data_tx;

  wire [3:0]bit_num;
  wire \bit_num[3]_i_2_n_0 ;
  wire [3:0]bit_num_reg;
  wire clk;
  wire [6:0]cnt;
  wire \cnt[2]_i_2_n_0 ;
  wire \cnt[2]_i_3_n_0 ;
  wire \cnt[2]_i_4_n_0 ;
  wire \cnt[4]_i_2_n_0 ;
  wire \cnt[5]_i_2_n_0 ;
  wire \cnt[5]_i_3_n_0 ;
  wire \cnt[6]_i_2_n_0 ;
  wire \cnt[6]_i_3_n_0 ;
  wire \cnt[6]_i_4_n_0 ;
  wire [6:0]cnt_0;
  wire is_transmission_going_reg_i_1_n_0;
  wire is_transmission_going_reg_i_2_n_0;
  wire is_transmission_going_reg_reg_0;
  wire send_byte;
  wire send_byte_prev;
  wire [7:0]uart_data_tx;
  wire uart_tx;
  wire uart_tx_reg_i_10_n_0;
  wire uart_tx_reg_i_11_n_0;
  wire uart_tx_reg_i_12_n_0;
  wire uart_tx_reg_i_13_n_0;
  wire uart_tx_reg_i_14_n_0;
  wire uart_tx_reg_i_1_n_0;
  wire uart_tx_reg_i_2_n_0;
  wire uart_tx_reg_i_3_n_0;
  wire uart_tx_reg_i_4_n_0;
  wire uart_tx_reg_i_5_n_0;
  wire uart_tx_reg_i_6_n_0;
  wire uart_tx_reg_i_7_n_0;
  wire uart_tx_reg_i_8_n_0;
  wire uart_tx_reg_i_9_n_0;

  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h99999959)) 
    \bit_num[0]_i_1 
       (.I0(\cnt[5]_i_3_n_0 ),
        .I1(bit_num_reg[0]),
        .I2(send_byte),
        .I3(send_byte_prev),
        .I4(is_transmission_going_reg_reg_0),
        .O(bit_num[0]));
  LUT6 #(
    .INIT(64'hA8AA5455A8AAA8AA)) 
    \bit_num[1]_i_1 
       (.I0(bit_num_reg[1]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(\cnt[5]_i_3_n_0 ),
        .I5(bit_num_reg[0]),
        .O(bit_num[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h5559AAAA)) 
    \bit_num[2]_i_1 
       (.I0(\bit_num[3]_i_2_n_0 ),
        .I1(send_byte),
        .I2(send_byte_prev),
        .I3(is_transmission_going_reg_reg_0),
        .I4(bit_num_reg[2]),
        .O(bit_num[2]));
  LUT6 #(
    .INIT(64'h7777770788888808)) 
    \bit_num[3]_i_1 
       (.I0(bit_num_reg[2]),
        .I1(\bit_num[3]_i_2_n_0 ),
        .I2(send_byte),
        .I3(send_byte_prev),
        .I4(is_transmission_going_reg_reg_0),
        .I5(bit_num_reg[3]),
        .O(bit_num[3]));
  LUT6 #(
    .INIT(64'h00000000A8AA0000)) 
    \bit_num[3]_i_2 
       (.I0(bit_num_reg[1]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .I4(bit_num_reg[0]),
        .I5(\cnt[5]_i_3_n_0 ),
        .O(\bit_num[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[0]),
        .Q(bit_num_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[1]),
        .Q(bit_num_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[2]),
        .Q(bit_num_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \bit_num_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(bit_num[3]),
        .Q(bit_num_reg[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h6466)) 
    \cnt[0]_i_1 
       (.I0(cnt[0]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(cnt_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h6C606C6C)) 
    \cnt[1]_i_1 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .I2(is_transmission_going_reg_reg_0),
        .I3(send_byte_prev),
        .I4(send_byte),
        .O(cnt_0[1]));
  LUT6 #(
    .INIT(64'h0220202020202020)) 
    \cnt[2]_i_1 
       (.I0(\cnt[2]_i_2_n_0 ),
        .I1(\cnt[6]_i_3_n_0 ),
        .I2(cnt[2]),
        .I3(is_transmission_going_reg_reg_0),
        .I4(cnt[0]),
        .I5(cnt[1]),
        .O(cnt_0[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFDFF7FF)) 
    \cnt[2]_i_2 
       (.I0(cnt[6]),
        .I1(cnt[5]),
        .I2(cnt[4]),
        .I3(\cnt[2]_i_3_n_0 ),
        .I4(cnt[3]),
        .I5(\cnt[2]_i_4_n_0 ),
        .O(\cnt[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \cnt[2]_i_3 
       (.I0(is_transmission_going_reg_reg_0),
        .I1(cnt[0]),
        .I2(cnt[1]),
        .I3(cnt[2]),
        .O(\cnt[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h7E)) 
    \cnt[2]_i_4 
       (.I0(is_transmission_going_reg_reg_0),
        .I1(cnt[0]),
        .I2(cnt[1]),
        .O(\cnt[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h7FFF800000000000)) 
    \cnt[3]_i_1 
       (.I0(is_transmission_going_reg_reg_0),
        .I1(cnt[0]),
        .I2(cnt[1]),
        .I3(cnt[2]),
        .I4(cnt[3]),
        .I5(uart_tx_reg_i_2_n_0),
        .O(cnt_0[3]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    \cnt[4]_i_1 
       (.I0(\cnt[4]_i_2_n_0 ),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(cnt_0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \cnt[4]_i_2 
       (.I0(cnt[4]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(cnt[0]),
        .I3(cnt[1]),
        .I4(cnt[2]),
        .I5(cnt[3]),
        .O(\cnt[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h9999990900000000)) 
    \cnt[5]_i_1 
       (.I0(cnt[5]),
        .I1(\cnt[5]_i_2_n_0 ),
        .I2(send_byte),
        .I3(send_byte_prev),
        .I4(is_transmission_going_reg_reg_0),
        .I5(\cnt[5]_i_3_n_0 ),
        .O(cnt_0[5]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt[5]_i_2 
       (.I0(cnt[4]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(cnt[0]),
        .I3(cnt[1]),
        .I4(cnt[2]),
        .I5(cnt[3]),
        .O(\cnt[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hFFFFBDFF)) 
    \cnt[5]_i_3 
       (.I0(cnt[5]),
        .I1(\cnt[6]_i_4_n_0 ),
        .I2(cnt[4]),
        .I3(cnt[6]),
        .I4(uart_tx_reg_i_12_n_0),
        .O(\cnt[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0320303030302030)) 
    \cnt[6]_i_1 
       (.I0(\cnt[6]_i_2_n_0 ),
        .I1(\cnt[6]_i_3_n_0 ),
        .I2(cnt[6]),
        .I3(cnt[5]),
        .I4(cnt[4]),
        .I5(\cnt[6]_i_4_n_0 ),
        .O(cnt_0[6]));
  LUT6 #(
    .INIT(64'hFFFD7FFFFFFC3FFF)) 
    \cnt[6]_i_2 
       (.I0(cnt[4]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(cnt[0]),
        .I3(cnt[1]),
        .I4(cnt[2]),
        .I5(cnt[3]),
        .O(\cnt[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \cnt[6]_i_3 
       (.I0(send_byte),
        .I1(send_byte_prev),
        .I2(is_transmission_going_reg_reg_0),
        .O(\cnt[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \cnt[6]_i_4 
       (.I0(cnt[3]),
        .I1(cnt[2]),
        .I2(cnt[1]),
        .I3(cnt[0]),
        .I4(is_transmission_going_reg_reg_0),
        .O(\cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[0]),
        .Q(cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[1]),
        .Q(cnt[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[2]),
        .Q(cnt[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[3]),
        .Q(cnt[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[4]),
        .Q(cnt[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[5]),
        .Q(cnt[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(cnt_0[6]),
        .Q(cnt[6]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00BA)) 
    is_transmission_going_reg_i_1
       (.I0(is_transmission_going_reg_reg_0),
        .I1(send_byte_prev),
        .I2(send_byte),
        .I3(is_transmission_going_reg_i_2_n_0),
        .O(is_transmission_going_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000040000)) 
    is_transmission_going_reg_i_2
       (.I0(\cnt[6]_i_3_n_0 ),
        .I1(bit_num_reg[3]),
        .I2(bit_num_reg[2]),
        .I3(bit_num_reg[1]),
        .I4(bit_num_reg[0]),
        .I5(\cnt[5]_i_3_n_0 ),
        .O(is_transmission_going_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    is_transmission_going_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(is_transmission_going_reg_i_1_n_0),
        .Q(is_transmission_going_reg_reg_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    send_byte_prev_reg
       (.C(clk),
        .CE(1'b1),
        .D(send_byte),
        .Q(send_byte_prev),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFA8ABA8A8)) 
    uart_tx_reg_i_1
       (.I0(uart_tx),
        .I1(uart_tx_reg_i_2_n_0),
        .I2(uart_tx_reg_i_3_n_0),
        .I3(uart_tx_reg_i_4_n_0),
        .I4(uart_tx_reg_i_5_n_0),
        .I5(uart_tx_reg_i_6_n_0),
        .O(uart_tx_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    uart_tx_reg_i_10
       (.I0(bit_num_reg[2]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(uart_tx_reg_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hBD)) 
    uart_tx_reg_i_11
       (.I0(cnt[5]),
        .I1(\cnt[6]_i_4_n_0 ),
        .I2(cnt[4]),
        .O(uart_tx_reg_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFFEBFFF)) 
    uart_tx_reg_i_12
       (.I0(cnt[3]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(cnt[0]),
        .I3(cnt[1]),
        .I4(cnt[2]),
        .O(uart_tx_reg_i_12_n_0));
  LUT5 #(
    .INIT(32'h0303FF47)) 
    uart_tx_reg_i_13
       (.I0(uart_data_tx[3]),
        .I1(bit_num_reg[0]),
        .I2(uart_data_tx[2]),
        .I3(bit_num_reg[2]),
        .I4(\cnt[6]_i_3_n_0 ),
        .O(uart_tx_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'hFDFDDDFDFDDDDDDD)) 
    uart_tx_reg_i_14
       (.I0(bit_num_reg[1]),
        .I1(\cnt[6]_i_3_n_0 ),
        .I2(bit_num_reg[2]),
        .I3(bit_num_reg[0]),
        .I4(uart_data_tx[7]),
        .I5(uart_data_tx[6]),
        .O(uart_tx_reg_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    uart_tx_reg_i_2
       (.I0(\cnt[5]_i_3_n_0 ),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(uart_tx_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    uart_tx_reg_i_3
       (.I0(bit_num_reg[3]),
        .I1(is_transmission_going_reg_reg_0),
        .I2(send_byte_prev),
        .I3(send_byte),
        .O(uart_tx_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hAABBABABBBBBBBBB)) 
    uart_tx_reg_i_4
       (.I0(uart_tx_reg_i_7_n_0),
        .I1(uart_tx_reg_i_8_n_0),
        .I2(uart_data_tx[5]),
        .I3(uart_data_tx[4]),
        .I4(uart_tx_reg_i_9_n_0),
        .I5(uart_tx_reg_i_10_n_0),
        .O(uart_tx_reg_i_4_n_0));
  LUT6 #(
    .INIT(64'h1011101100001011)) 
    uart_tx_reg_i_5
       (.I0(uart_tx_reg_i_11_n_0),
        .I1(uart_tx_reg_i_12_n_0),
        .I2(\cnt[6]_i_3_n_0 ),
        .I3(bit_num_reg[3]),
        .I4(uart_tx_reg_i_13_n_0),
        .I5(uart_tx_reg_i_14_n_0),
        .O(uart_tx_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    uart_tx_reg_i_6
       (.I0(\cnt[6]_i_3_n_0 ),
        .I1(bit_num_reg[3]),
        .I2(bit_num_reg[2]),
        .I3(bit_num_reg[1]),
        .I4(bit_num_reg[0]),
        .I5(\cnt[5]_i_3_n_0 ),
        .O(uart_tx_reg_i_6_n_0));
  LUT3 #(
    .INIT(8'h59)) 
    uart_tx_reg_i_7
       (.I0(cnt[6]),
        .I1(cnt[5]),
        .I2(\cnt[5]_i_2_n_0 ),
        .O(uart_tx_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'hF0BBF0BAF0AAF0BA)) 
    uart_tx_reg_i_8
       (.I0(bit_num_reg[1]),
        .I1(bit_num_reg[2]),
        .I2(uart_data_tx[0]),
        .I3(\cnt[6]_i_3_n_0 ),
        .I4(bit_num_reg[0]),
        .I5(uart_data_tx[1]),
        .O(uart_tx_reg_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h10FF)) 
    uart_tx_reg_i_9
       (.I0(is_transmission_going_reg_reg_0),
        .I1(send_byte_prev),
        .I2(send_byte),
        .I3(bit_num_reg[0]),
        .O(uart_tx_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b1)) 
    uart_tx_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(uart_tx_reg_i_1_n_0),
        .Q(uart_tx),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

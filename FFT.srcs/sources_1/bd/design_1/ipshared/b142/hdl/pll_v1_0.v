
`timescale 1 ns / 1 ps

module pll(
    input wire pll_ld_sdo,
    input wire clk_pll_spi,
    input wire pll_read,
    input wire pll_write,
    input wire [23:0] pll_write_data,
    input wire [4:0] pll_write_address_reg,
    input wire [7:0] pll_read_address_reg,
    
    output wire pll_sen,
    output wire pll_mosi,
    output wire pll_sck,
    output wire [23:0] pll_read_data,
    output wire pll_data_ready
);
	
    wire [7:0] pll_address_reg;
    wire pll_sck_write;
    wire pll_sck_read;
    assign pll_sck = pll_sck_write | pll_sck_read;
    
    wire pll_mosi_read;    
    wire pll_mosi_write;
    assign pll_mosi = pll_mosi_write | pll_mosi_read;
    
    wire pll_sen_read;
    wire pll_sen_write;
    assign pll_sen = pll_sen_write | pll_sen_read;
    
	pll_receive pll_receive(
        .clk(clk_pll_spi),
        .reg_address(pll_read_address_reg),
        .start(pll_read),
        .pll_clk(pll_sck_read),
        .pll_mosi(pll_mosi_read),
        .pll_miso(pll_ld_sdo),
        .pll_cs(pll_sen_read),
        .read_data(pll_read_data),
        .data_ready(pll_data_ready)        
    );
    
    pll_send pll_send(
        .clk(clk_pll_spi),
        .pll_data(pll_write_data),
        .start(pll_write),
        .pll_clk(pll_sck_write),
        .pll_mosi(pll_mosi_write),
        .pll_cs(pll_sen_write),
        .reg_address(pll_write_address_reg)          
    );    
	
	endmodule

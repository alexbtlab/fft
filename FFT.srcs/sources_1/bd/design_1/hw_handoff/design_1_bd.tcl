
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a100tfgg484-2
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set adc_clk [ create_bd_intf_port -mode Master -vlnv bt.local:interface:diff_rtl:1.0 adc_clk ]

  set adc_da [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_da ]

  set adc_db [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_db ]

  set adc_dco [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 adc_dco ]

  set fpga_clk [ create_bd_intf_port -mode Slave -vlnv bt.local:interface:diff_rtl:1.0 fpga_clk ]


  # Create ports
  set atten [ create_bd_port -dir O -from 5 -to 0 atten ]
  set clk_100MHz [ create_bd_port -dir I clk_100MHz ]
  set clk_cs [ create_bd_port -dir O clk_cs ]
  set clk_mosi [ create_bd_port -dir O clk_mosi ]
  set clk_sclk [ create_bd_port -dir O clk_sclk ]
  set clk_sync [ create_bd_port -dir O clk_sync ]
  set if_amp1_stu_en [ create_bd_port -dir O if_amp1_stu_en ]
  set if_amp2_stu_en [ create_bd_port -dir O if_amp2_stu_en ]
  set pamp_en [ create_bd_port -dir O pamp_en ]
  set pll_ld_sdo [ create_bd_port -dir I pll_ld_sdo ]
  set pll_mosi [ create_bd_port -dir O pll_mosi ]
  set pll_sck [ create_bd_port -dir O pll_sck ]
  set pll_sen [ create_bd_port -dir O pll_sen ]
  set pll_trig [ create_bd_port -dir O pll_trig ]
  set sw_ctrl [ create_bd_port -dir O sw_ctrl ]
  set sw_if1 [ create_bd_port -dir O sw_if1 ]
  set sw_if2 [ create_bd_port -dir O sw_if2 ]
  set sw_if3 [ create_bd_port -dir O sw_if3 ]
  set sw_ls [ create_bd_port -dir O sw_ls ]
  set user_io_9 [ create_bd_port -dir I user_io_9 ]
  set user_io_10 [ create_bd_port -dir O user_io_10 ]
  set user_led [ create_bd_port -dir O user_led ]

  # Create instance: adc_control, and set properties
  set adc_control [ create_bd_cell -type ip -vlnv bt.local:user:adc_control:1.0 adc_control ]

  # Create instance: init_clk, and set properties
  set init_clk [ create_bd_cell -type ip -vlnv bt.local:user:init_clk:1.0 init_clk ]

  # Create instance: pll, and set properties
  set pll [ create_bd_cell -type ip -vlnv bt.local:user:pll:1.0 pll ]

  # Create instance: top_logic, and set properties
  set top_logic [ create_bd_cell -type ip -vlnv bt.local:user:top_logic:1.0 top_logic ]

  # Create instance: uart, and set properties
  set uart [ create_bd_cell -type ip -vlnv xilinx.com:user:uart:1.0 uart ]

  # Create interface connections
  connect_bd_intf_net -intf_net adc_control_0_adc_clkx [get_bd_intf_ports adc_clk] [get_bd_intf_pins adc_control/adc_clkx]
  connect_bd_intf_net -intf_net adc_dax_0_1 [get_bd_intf_ports adc_da] [get_bd_intf_pins adc_control/adc_dax]
  connect_bd_intf_net -intf_net adc_dbx_0_1 [get_bd_intf_ports adc_db] [get_bd_intf_pins adc_control/adc_dbx]
  connect_bd_intf_net -intf_net adc_dcox_0_1 [get_bd_intf_ports adc_dco] [get_bd_intf_pins adc_control/adc_dcox]
  connect_bd_intf_net -intf_net fpga_clkx_0_1 [get_bd_intf_ports fpga_clk] [get_bd_intf_pins adc_control/fpga_clkx]

  # Create port connections
  connect_bd_net -net adc_control_0_output_data [get_bd_pins adc_control/output_data] [get_bd_pins top_logic/in_adc_data]
  connect_bd_net -net adc_control_0_send_uart_byte [get_bd_pins adc_control/send_uart_byte] [get_bd_pins top_logic/uart_adc_start_sending]
  connect_bd_net -net init_clk_0_clk_cs [get_bd_ports clk_cs] [get_bd_pins init_clk/clk_cs]
  connect_bd_net -net init_clk_0_clk_mosi [get_bd_ports clk_mosi] [get_bd_pins init_clk/clk_mosi]
  connect_bd_net -net init_clk_0_clk_sclk [get_bd_ports clk_sclk] [get_bd_pins init_clk/clk_sclk]
  connect_bd_net -net pll_0_pll_mosi [get_bd_ports pll_mosi] [get_bd_pins pll/pll_mosi]
  connect_bd_net -net pll_0_pll_read_data [get_bd_pins pll/pll_read_data] [get_bd_pins top_logic/pll_read_data]
  connect_bd_net -net pll_0_pll_sck [get_bd_ports pll_sck] [get_bd_pins pll/pll_sck]
  connect_bd_net -net pll_0_pll_sen [get_bd_ports pll_sen] [get_bd_pins pll/pll_sen]
  connect_bd_net -net pll_ld_sdo_0_1 [get_bd_ports pll_ld_sdo] [get_bd_pins pll/pll_ld_sdo]
  connect_bd_net -net pll_pll_data_ready [get_bd_pins pll/pll_data_ready] [get_bd_pins top_logic/pll_data_ready]
  connect_bd_net -net top_logic_0_clk_100MHz_out [get_bd_pins top_logic/clk_100MHz_out] [get_bd_pins uart/clk]
  connect_bd_net -net top_logic_0_clk_10MHz_out [get_bd_pins init_clk/clk] [get_bd_pins top_logic/clk_10MHz_out]
  connect_bd_net -net top_logic_0_clk_20MHz_out [get_bd_pins pll/clk_pll_spi] [get_bd_pins top_logic/clk_20MHz_out]
  connect_bd_net -net top_logic_0_out_data_uart [get_bd_pins top_logic/out_data_uart] [get_bd_pins uart/uart_data_tx]
  connect_bd_net -net top_logic_0_pll_read [get_bd_pins pll/pll_read] [get_bd_pins top_logic/pll_read]
  connect_bd_net -net top_logic_0_pll_read_addres [get_bd_pins pll/pll_read_address_reg] [get_bd_pins top_logic/pll_read_addres]
  connect_bd_net -net top_logic_0_pll_write [get_bd_pins pll/pll_write] [get_bd_pins top_logic/pll_write]
  connect_bd_net -net top_logic_0_pll_write_addres [get_bd_pins pll/pll_write_address_reg] [get_bd_pins top_logic/pll_write_addres]
  connect_bd_net -net top_logic_0_pll_write_data [get_bd_pins pll/pll_write_data] [get_bd_pins top_logic/pll_write_data]
  connect_bd_net -net top_logic_0_start_init_clk [get_bd_pins init_clk/start] [get_bd_pins top_logic/start_init_clk]
  connect_bd_net -net top_logic_0_start_uart_send [get_bd_pins top_logic/start_uart_send] [get_bd_pins uart/send_byte]
  connect_bd_net -net top_logic_0_t_sweep [get_bd_pins adc_control/tsweep] [get_bd_pins top_logic/t_sweep]
  connect_bd_net -net top_logic_atten [get_bd_ports atten] [get_bd_pins top_logic/atten]
  connect_bd_net -net top_logic_clk_sync [get_bd_ports clk_sync] [get_bd_pins top_logic/clk_sync]
  connect_bd_net -net top_logic_if_amp1_stu_en [get_bd_ports if_amp1_stu_en] [get_bd_pins top_logic/if_amp1_stu_en]
  connect_bd_net -net top_logic_if_amp2_stu_en [get_bd_ports if_amp2_stu_en] [get_bd_pins top_logic/if_amp2_stu_en]
  connect_bd_net -net top_logic_is_adc_data_sending [get_bd_pins adc_control/is_sending] [get_bd_pins top_logic/is_adc_data_sending]
  connect_bd_net -net top_logic_pamp_en [get_bd_ports pamp_en] [get_bd_pins top_logic/pamp_en]
  connect_bd_net -net top_logic_pll_trig [get_bd_ports pll_trig] [get_bd_pins top_logic/pll_trig]
  connect_bd_net -net top_logic_sw_ctrl [get_bd_ports sw_ctrl] [get_bd_pins top_logic/sw_ctrl]
  connect_bd_net -net top_logic_sw_if1 [get_bd_ports sw_if1] [get_bd_pins top_logic/sw_if1]
  connect_bd_net -net top_logic_sw_if2 [get_bd_ports sw_if2] [get_bd_pins top_logic/sw_if2]
  connect_bd_net -net top_logic_sw_if3 [get_bd_ports sw_if3] [get_bd_pins top_logic/sw_if3]
  connect_bd_net -net top_logic_sw_ls [get_bd_ports sw_ls] [get_bd_pins top_logic/sw_ls]
  connect_bd_net -net top_logic_user_led [get_bd_ports user_led] [get_bd_pins top_logic/user_led]
  connect_bd_net -net uart_0_byte_received [get_bd_pins top_logic/uart_byte_received] [get_bd_pins uart/byte_received]
  connect_bd_net -net uart_0_uart_data_rx [get_bd_pins top_logic/in_uart_data] [get_bd_pins uart/uart_data_rx]
  connect_bd_net -net uart_0_uart_tx [get_bd_ports user_io_10] [get_bd_pins uart/uart_tx]
  connect_bd_net -net uart_clk_0_1 [get_bd_ports clk_100MHz] [get_bd_pins adc_control/uart_clk] [get_bd_pins top_logic/clk_100MHz_in]
  connect_bd_net -net uart_rx_0_1 [get_bd_ports user_io_9] [get_bd_pins uart/uart_rx]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


